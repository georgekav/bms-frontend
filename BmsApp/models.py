# -*- coding: utf-8 -*-
__author__ = 'Georgios Lilis, Thomas Guibentif'

from itertools import combinations
from conf_general import LOADS_API_TYPES, SENSORS_API_TYPES, ACTUATOR_API_TYPES
from conf_plc import PLC_ACTUATOR_TYPES
from django.db import models
from django.core.exceptions import ValidationError
from django.utils.safestring import mark_safe
from django.utils import timezone
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.validators import MaxValueValidator,MinValueValidator
from modelFields import UUIDField
from jsonfield import JSONField
from django.core import serializers
import json
from uuid import uuid4
import datetime
import os

class Choices():
    #In models measures are the index 0, quantity is index 1
    #These quantities are for display purposes only. They are NOT related to the equivalents  in backend structures.
    room_sensor_choices = (
        (SENSORS_API_TYPES[0], 'Temperature'),
        (SENSORS_API_TYPES[1], 'Luminance'),
        (SENSORS_API_TYPES[2], 'Humidity'),
        (SENSORS_API_TYPES[3], 'Presence'),
        ('IRR', 'Irradiance'),
    )
    load_sensor_choices = (
        (LOADS_API_TYPES[0], 'Active Power'),
        (LOADS_API_TYPES[1], 'Reactive Power'),
        (LOADS_API_TYPES[2], 'Apparent Power'),
        (LOADS_API_TYPES[3], 'Power Factor'),
    )
    load_type_choices = (
        ('USER-DRIVEN', 'Non-controllable'),
        ('DEF', 'Deferrable'),
        ('THERM', 'Thermal load')
    )
    room_constraint_choices = (
        (SENSORS_API_TYPES[0], 'Temperature'),
        (SENSORS_API_TYPES[1], 'Luminance'),
        (SENSORS_API_TYPES[2], 'Humidity')
    )
    room_nature_choices = (
        ('STANDARD', 'Human space'),
        ('ENERGY', 'Energy water storage'),
        ('ENV', 'Environment')
    )
    aux_sensor_choices = (
        (SENSORS_API_TYPES[6], 'Battery Level'),
        ('NULL', 'Null Sensor')
    )
    action_choices = (
        (ACTUATOR_API_TYPES[0], 'On/Off'),  # such an actuator can receive the commands 'ON','OFF' and 'TOGGLE'
        (ACTUATOR_API_TYPES[1], 'Dimmer'),
        (ACTUATOR_API_TYPES[2], 'Switch mode'),
    )
    middleware_choices = (
        ('PLC', 'PLC Network'),
        ('ZWAVE', 'ZWAVE Network'),
        ('IP', 'IP Network'),
        ('vMID', 'Virtual Middleware'),
        ('DB', 'Database')
    )

    generator_choices = (
        ('Solar','Photovoltaic'),
        ('Wind','Wind Turbine'),
        ('Hydro','Micro-Hydro Turbine')
    )

    storage_choices = (
        ('Mechanical', [('Flywheel','Flywheel'),('Air','Compressed Air Storage'),('Hydro','Pumped Hydro-Power')]),
        ('Electrochemical', [('Secondary','Secondary Battery'),('Flow','Flow Battery')]),
        ('Electrical',[('DLC','Double-layer Capacitor'),('SMES','Superconducting Magnetic Coil')]),
        ('Chemical', [('Hydrogen','Fuel Cell')]),
        ('Thermal',[('Thermal','Heat storage')])

    )

    interface_type_choices = (
        ('wall', 'Wall'),
        ('window', 'Window'),
        ('door', 'Door')
    )

# ---------------------#---------------------#---------------------#---------------------#---------------------#---------
# Structual information
class Unit(models.Model):
    name = models.CharField(max_length=45, unique=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    comment = models.TextField(blank=True)

    def __unicode__(self):
        return self.name

    def clean(self):
        if bool(self.latitude) != bool(self.longitude):
            raise ValidationError('Both coordinates should be empty or filled')

    def room_ord_set(self):
        retQuerySet = self.room_set.order_by('name')
        return retQuerySet

    def load_set(self):
        #combine the querysets to get the load either with actuator or sensor
        retQuerySet = Load.objects.filter(actuator__room__unit=self) | Load.objects.filter(sensor__room__unit=self)
        return retQuerySet.distinct()

    def load_set__type(self, type):
        retQuerySet = self.load_set().filter(device__type=type)
        return retQuerySet

    def deviceTypes_set(self):
        retQuerySet = DeviceType.objects.filter(device__load__actuator__room__unit=self) | DeviceType.objects.filter(device__load__sensor__room__unit=self)
        return retQuerySet.distinct()

    # def generator_set(self):
    #     #we may consider per room generation (unlikely), and thus only this function will need change since wrap the default _set
    #     retQuerySet = self.generator_set.all()
    #     return retQuerySet
    #
    # def storage_set(self):
    #     # we may consider per room storage, and thus only this function will need change since wrap the default _set
    #     retQuerySet = self.storage_set.all()
    #     return retQuerySet


class RoomType(models.Model):
    name = models.CharField(max_length=45)

    def __unicode__(self):
        return self.name


class Room(models.Model):
    unit = models.ForeignKey('Unit')
    name = models.CharField(max_length=45)
    nature = models.CharField(blank=False,max_length=45, choices=Choices.room_nature_choices,
                              default=Choices.room_nature_choices[0][0])
    type = models.ForeignKey('RoomType')
    surface = models.FloatField(help_text='Floor surface', null=True, blank=True)
    volume = models.FloatField(help_text='Room volume', null=True, blank=True)

    comment = models.TextField(blank=True)
    uuid = UUIDField(blank=True, null=True, unique=True, verbose_name="Room UUID", help_text="The UUID in hex value "
                                                                                    "with ':' or '-'")

    class Meta:
        unique_together = ("name", "unit")

    def load_set(self):
        #Attention by returning a QuerySet we have an extra penalty of 1+ Query to SQL
        #combine the querysets to get the load either with actuator or sensor
        retQueryset = Load.objects.filter(actuator__room=self) | Load.objects.filter(sensor__room=self)
        return retQueryset.distinct()

    def load_set__type(self, type):
        retQueryset = self.load_set().filter(device__type=type)
        return retQueryset

    def user_set(self):
        #Return the users in the room at the moment
        retQueryset = UserProfile.objects.filter(current_location=self)
        return retQueryset

    def __unicode__(self):
        return self.name


class RoomInterface(models.Model):
    rc_model_pattern = {'r': [0], 'c': []}
    name = models.CharField(max_length=30, blank=False, default="Interface")  # Typical name: north wall
    interface_type = models.CharField(blank=False, max_length=30, choices=Choices.interface_type_choices, default=Choices.interface_type_choices[0][0])

    # An interface separates two rooms
    room_x = models.ForeignKey('Room', blank=False, related_name='+')
    room_y = models.ForeignKey('Room', blank=False, related_name='+')

    rc_model = JSONField(default=json.dumps(rc_model_pattern), help_text="(n+1)RnC equivalent model")
    surface = models.FloatField(help_text='Interface surface')
    thickness = models.FloatField(help_text='Interface thickness')

    comment = models.TextField(blank=True)

    def clean(self):
        if self.room_x == self.room_y:
            raise ValidationError('Please choose 2 different rooms')
        if not(self.room_x.unit == self.room_y.unit):
            raise ValidationError('Please 2 rooms that are in the same Unit')

    def unit(self):
        return self.room_x.unit

    def __unicode__(self):
        return self.name

    def natural_key(self):
        return json.loads(serializers.serialize('json', [self]))[0]  # Serializer returns a list, we pick the first

class RoomConstraints(models.Model):
    room_default_constraint = {'comfort_timevector': [0, 24*3600],
                               'comfort_minvector': [0, 0],
                               'comfort_maxvector': [0, 0]}

    room = models.ForeignKey('Room')
    quantity_type = models.CharField(max_length=15, blank=False, choices=Choices.room_constraint_choices)
    constraints_timeseries = JSONField(default=json.dumps(room_default_constraint),
                                       help_text="Enter the pairs (t,v) such that the constraints takes the value 'v' from time 't'. Repeat the last pair to end the constraint",)

    ideal_quantity = models.FloatField(verbose_name="Ideal constrained quantity", blank=True, null=True, default=None)
    weekdays = models.BooleanField(verbose_name="Week-days", blank=True, default=True)
    weekend = models.BooleanField(verbose_name="Week-ends", blank=True, default=True)

    comment = models.TextField(blank=True)

    def unit(self):
        return self.room.unit

    def __unicode__(self):
        return '{0} for room {1}'.format(self.quantity_type, self.room.name)

    def natural_key(self):
        return json.loads(serializers.serialize('json', [self]))[0]  # Serializer returns a list, we pick the first

# ---------------------#---------------------#---------------------#---------------------#---------------------#---------
# Backend Middlewares information
class Middleware(models.Model):
    name = models.CharField(max_length=45, unique=True)
    middleware_type = models.CharField(max_length=15, blank=False, choices=Choices.middleware_choices)
    IP = models.GenericIPAddressField(verbose_name='Middleware IP', help_text='Enter: 255.255.255.255 for auto resolving, 127.0.0.1 if the given service should run with its configured OPENBMS IP (local but with visible socket)')
    REP_port = models.DecimalField(max_digits=5, blank=True, null=True, decimal_places=0, verbose_name='Middleware\'s REP port', help_text='Defaults: db_50100 plc_50200 zw_50300 vmid_51400')
    PUB_port = models.DecimalField(max_digits=5, blank=True, null=True, decimal_places=0, verbose_name='Middleware\'s PUB port', help_text='Defaults: plc_50250 zw_50350 tcp_51050 loc_51150 vmid_51450')

    def clean(self):
        if not self.REP_port and not self.PUB_port:
            raise ValidationError('Please indicate at least one port number')
        same_ip_rep = Middleware.objects.filter(REP_port=self.REP_port, IP=self.IP).exclude(id=self.id).exclude(REP_port=None).first()
        same_ip_pub = Middleware.objects.filter(PUB_port=self.PUB_port, IP=self.IP).exclude(id=self.id).exclude(PUB_port=None).first()
        if self.REP_port and same_ip_rep and same_ip_rep.REP_port:
            raise ValidationError('Same IP and REP port combination is already used by middleware: {0}'.format(str(same_ip_rep.name)))
        if self.PUB_port and same_ip_pub and same_ip_pub.PUB_port:
            raise ValidationError('Same IP and PUB port combination is already used by middleware: {0}'.format(str(same_ip_pub.name)))

    def __unicode__(self):
        return self.name


# ----------------------------------------------------------------------------------------------------------------------
# Load Characterisation
class DeviceType(models.Model):
    name = models.CharField(max_length=45, unique=True)

    def load_set(self):
        retQuerySet = Load.objects.filter(device__type=self)
        return retQuerySet

    def __unicode__(self):
        return self.name

def get_device_pic_filename(instance, filename):
    filename, file_extension = os.path.splitext(filename)
    if instance.id:
        primary_key = instance.id
    else:
        primary_key = uuid4().hex
    newfilename = os.path.join('device','device_'+str(primary_key)+file_extension)
    return newfilename

class Device(models.Model):
    name = models.CharField(max_length=45, unique=True)
    type = models.ForeignKey('DeviceType')
    comment = models.TextField(blank=True)
    device_image = models.ImageField(upload_to=get_device_pic_filename, blank=True, null=True)

    def image_tag(self):
        return u'<img src="{0}" width="120" />'.format(self.device_image.url)
    image_tag.short_description = 'Device image'
    image_tag.allow_tags = True


    def affords(self, action):
        for mode in Mode.objects.filter(device=self):
            if action in mode.afforded_actions:
                return True
            else:
                return False

    def save(self, *args, **kwargs):
        # delete old file when replacing by updating the file
        if not self.device_image:
            self.device_image = os.path.join('device', 'device_0.png')
        try:
            this = Device.objects.get(id=self.id)
            if this.device_image != self.device_image and this.device_image != os.path.join('device','device_0.png'): #dont delete default
                this.device_image.delete(save=False)
        except:
            #New Device
            pass
        super(Device, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name

# ---------------------#---------------------#---------------------#---------------------#---------------------#---------
# Emergy management infrastructure
class Generator(models.Model):
    name = models.CharField(max_length=45)
    comment = models.TextField(blank=True)
    type = models.CharField(max_length=10, choices=Choices.generator_choices, verbose_name="Generator type", help_text="On-Site electrical generator type")
    nominal_power = models.FloatField(verbose_name="Nominal generation power", help_text="The installed capacity in W")
    current_production = models.FloatField(blank=True, null=True, verbose_name="Current generation power", help_text="The real-time generating power")
    integrated_storage_1 = models.ForeignKey('Storage', blank=True, null=True, related_name='+', help_text="Storage capacity associated/integrated with the generation") #related_name='+' because we dont want backwards relation
    integrated_storage_2 = models.ForeignKey('Storage', blank=True, null=True, related_name='+', help_text="Storage capacity associated/integrated with the generation")
    integrated_storage_3 = models.ForeignKey('Storage', blank=True, null=True, related_name='+', help_text="Storage capacity associated/integrated with the generation")
    integrated_efficiency = models.FloatField(validators=[MinValueValidator(0), MaxValueValidator(100)], verbose_name="Integrated storage efficiency", help_text="The efficiency of energy trasfer from generation directly to integrated storage in %")
    additional_parameters = JSONField(blank=True, help_text="Additional specification and operation parameters, in JSON format used by mainly EMS")

    def room(self):
        #Try to extract from actuator or sensor
        generatoract = self.actuator_set.first()
        if generatoract:
            roumobj = generatoract.room
            return roumobj
        generatorsen = self.sensor_set.first()
        if generatorsen:
            roumobj = generatorsen.room
            return roumobj

    def natural_key(self):
        return json.loads(serializers.serialize('json', [self]))[0]  # Serializer returns a list, we pick the first

    def __unicode__(self):
       return self.name


class Storage(models.Model):
    name = models.CharField(max_length=45)
    comment = models.TextField(blank=True)
    type = models.CharField(max_length=10, choices=Choices.storage_choices, verbose_name="Storage type", help_text="On-Site storage type")
    nominal_capacity = models.FloatField(verbose_name="Nominal storage capacity", help_text="The installed capacity in Wh")
    state_of_charge = models.FloatField(blank=True, null=True, validators=[MinValueValidator(0), MaxValueValidator(100)], help_text="State of charge n% [0-100]")
    average_efficiency = models.FloatField(validators=[MinValueValidator(0), MaxValueValidator(100)], help_text="Average efficiency n% [0-100]")
    life_cycles = models.IntegerField(validators=[MinValueValidator(0)], help_text="The nominal ability of charge/discharge cycles")
    currect_cycles = models.IntegerField(blank=True, null=True, validators=[MinValueValidator(0)], help_text="The currectly recorded charge/discharge cycles")
    additional_parameters = JSONField(blank=True, help_text="Additional specification and operation parameters, in JSON format used mainly by EMS")

    min_soc = models.FloatField(verbose_name="Minimum SoC", help_text="The minimum allowable SoC, between 0 and 1", default=0)
    max_soc = models.FloatField(verbose_name="Maximum SoC", help_text="The maximum allowable SoC, between 0 and 1", default=1)

    def room(self):
        #Try to extract from actuator or sensor
        storageact = self.actuator_set.first()
        if storageact:
            roumobj = storageact.room
            return roumobj
        storagesen = self.sensor_set.first()
        if storagesen:
            roumobj = storagesen.room
            return roumobj

    def natural_key(self):
        return json.loads(serializers.serialize('json', [self]))[0]  # Serializer returns a list, we pick the first

    def __unicode__(self):
       return self.name


class Load(models.Model):
    name = models.CharField(max_length=45)
    comment = models.TextField(blank=True)
    device = models.ForeignKey('Device')
    ems_type = models.CharField(max_length=20, choices=Choices.load_type_choices, default=Choices.load_type_choices[0][0])
    user_driven = models.BooleanField(default=True, help_text="False if the load is automated (e.g. server, automatic window)")
    additional_parameters = JSONField(blank=True, help_text="Additional specification and operation parameters, in JSON format used mainly by EMS")

    uuid = UUIDField(blank=True, null=True, unique=True, verbose_name="Load UUID", help_text="The UUID in hex value "
                                                                                    "with ':' or '-'")

    ##### Load Profile model
    lp_states_list = JSONField(blank=True, default='["OFF"]', help_text="Enter the list of load states (on, off, standby, etc)")
    lp_states_default = models.CharField(max_length=20, default='OFF')

    mode_default = {'name': {'min_p': 0, 'max_p': 0, 'type_power_distribution': 'NORM', 'param_power_distribution': [0, 0]}}
    lp_modes = JSONField(blank=True, default=json.dumps(mode_default), help_text="Enter the list of load modes")

    seq_default = {'sequence_list': [{'seq_mode': 'seq_label', 'seq_dur_type': 'NORM', 'seq_dur_param': [0, 0]}],
                   'sequence_transitions': {}}
    lp_seq = JSONField(blank=True, default=json.dumps({'state_name': seq_default}),
                       help_text="Enter the list of sequences per state")

    state_trans_default = {}
    lp_state_trans = JSONField(blank=True, default=json.dumps(state_trans_default),
                               help_text="Enter the transition sequence between states")

    act_list_default = [{'start_state': 'state', 'stop_state': 'state',
                         'time_instant_is_starting_time': True,
                         't_instant_stat_model_type': 'NORM', 't_instant_stat_model_param': [0, 0],
                         'dur_stat_model_type': 'NORM', 'dur_stat_model_param': [0, 0],
                         'param': None, 'proba': 1}]
    lp_activity_period = models.FloatField(verbose_name="Activities period", default=0 , help_text="The period of the list, in seconds")
    lp_activity_list = JSONField(blank=True, default=json.dumps(act_list_default), verbose_name="Activities list",
                                 help_text="Enter the load activities as a list")

    scheduling_constraints_min_start = models.FloatField(verbose_name="Minimum starting time", default=0,
                                                         help_text="Enter the minimum starting time of the deferrable load")
    scheduling_constraints_max_stop = models.FloatField(verbose_name="Maximum stopping time", default=0,
                                                         help_text="Enter the maximum ending time of the deferrable load")

    ordering = ['room']
    def room(self):
        #Try to extract from actuator or sensor
        loadact = self.actuator_set.first()
        if loadact:
            roumobj = loadact.room
            return roumobj
        loadsen = self.sensor_set.first()
        if loadsen:
            roumobj = loadsen.room
            return roumobj

    def affords(self, action):
        #Check if there is an actuator to do the requested action
        loadactors = self.actuator_set.all()
        for act in loadactors:
            if act.action == action:
                return True

    def actions(self):
        return self.actuator_set.all().values_list('action', flat=True)

    def measures(self):
        return self.sensor_set.all().values_list('measure', flat=True)

    def mode_set(self):
        return self.device.mode_set.order_by('-power')

    def type(self):
        return self.device.type

    type.allow_tags = True

    def natural_key(self):
        return json.loads(serializers.serialize('json', [self]))[0]  # Serializer returns a list, we pick the first

    def __unicode__(self):
        return self.name


class SchedulingConstraints(models.Model):
    name = models.CharField(max_length=45)

    # Minimum starting time parameters: normal law
    scheduling_constraints_min_start_mean = models.FloatField(verbose_name="Mean minimum starting time",
                                                              default=17*60*60,
                                                              help_text="Enter the mean value of minimum starting time(in s)")
    scheduling_constraints_min_start_std = models.FloatField(verbose_name="Standard Deviation of the min. start.",
                                                             default=0,
                                                             help_text="Enter the Standard Deviation of minimum starting time (in s)")

    # Minimum starting time parameters: normal law
    scheduling_constraints_max_stop_mean = models.FloatField(verbose_name="Mean minimum stopping time",
                                                             default=6*3600,
                                                             help_text="Enter the mean value of maximum ending time (in s)")
    scheduling_constraints_max_stop_std = models.FloatField(verbose_name="Standard Deviation of the max. start.",
                                                            default=0,
                                                            help_text="Enter the Standard Deviation of maximum ending time (in s)")

    def natural_key(self):
        return json.loads(serializers.serialize('json', [self]))[0]  # Serializer returns a list, we pick the first

    def __unicode__(self):
        return self.name


class ElectricalVehicle(models.Model):
    name = models.CharField(max_length=45)
    comment = models.TextField(blank=True)

    # This EV can be used as a battery (charge/discharge) ?
    allow_discharge = models.BooleanField(verbose_name="Allow Discharge", default=False)

    # Battery
    battery = models.ForeignKey('Storage')

    # Scheduling constraints
    schedule = models.ForeignKey('SchedulingConstraints')

    def __unicode__(self):
        return self.name

    def natural_key(self):
        return json.loads(serializers.serialize('json', [self]))[0]  # Serializer returns a list, we pick the first

# ---------------------#---------------------#---------------------#---------------------#---------------------#---------
# Smart Infrastructure
# A change in implementation may require adding Sensor MEASURE_CHOICES or ACTION_CHOICES
class SensAct(models.Model):
    ref = models.IntegerField(verbose_name="Endpoint ID")
    name = models.CharField(max_length=45, blank=True)
    room = models.ForeignKey('Room')
    comment = models.TextField(blank=True)
    #related_name will reverse give all the sensor or actuator of the given middleware
    network_middleware = models.ForeignKey('Middleware', related_name='%(class)s_network_middleware_set',
                                           help_text='The participating network or virtual middleware')
    additional_parameters = JSONField(blank=True, help_text="Additional specification and operation parameters, in JSON format used mainly by EMS")

    class Meta:
        abstract = True

    def __unicode__(self):
        return self.name

    def clean(self):
        try:
            if self.network_middleware.middleware_type not in [ mid_choice[0] for mid_choice in Choices.middleware_choices] \
                    or self.network_middleware.middleware_type == 'DB':
                types_str = ''
                for mid_choice in Choices.middleware_choices:
                    if mid_choice[0] != 'DB':
                        types_str +=  mid_choice[0]+' | '
                raise ValidationError('A Network middleware middleware must be of type: '+types_str)

            if self.load and self.load.room():  # Get the room from the load
                self.room = self.load.room()

            elif self.generator and self.generator.room():  # Get the room from the generator
                self.room = self.generator.room()

            elif self.storage and self.storage.room():  # Get the room from the storage
                self.room = self.storage.room()

            if self.__class__.__name__ == 'Actuator':
                type = self.action
            else:
                type = self.measure

            if not self.name:
                if self.load:
                    self.name = '{0} {1} type {2} load {3}'.format(self.__class__.__name__, self.ref, type, self.load.name)
                elif self.generator:
                    self.name = '{0} {1} type {2} generator {3}'.format(self.__class__.__name__, self.ref, type, self.generator.name)
                elif self.storage:
                    self.name = '{0} {1} type {2} storage {3}'.format(self.__class__.__name__, self.ref, type, self.storage.name)
                else:
                    self.name = '{0} {1} type {2} room {3}'.format(self.__class__.__name__, self.ref, type, self.room.name)

            if sum((bool(self.load), bool(self.generator), bool(self.storage))) not in [0, 1]:  # none or at least one set
                raise ValidationError('Only ONE of Load, Generator, Storage, Load can be set')

        except ObjectDoesNotExist:
            pass
            #The other validators will protect us if we reference a foreign key that is not set but is required.

    def natural_key(self):
        return json.loads(serializers.serialize('json', [self]))[0] #Serializer returns a list, we pick the first


class Sensor(SensAct):
    MEASURE_CHOICES = (
        ('ENVIRONMENT', Choices.room_sensor_choices),
        ('ENERGY', Choices.load_sensor_choices),
    )
    MEASURES_CHOICES_WITH_AUX = list(MEASURE_CHOICES)+ [('AUX', Choices.aux_sensor_choices)]

    CHOICES = (MEASURE_CHOICES[0][1]) + (MEASURE_CHOICES[1][1])  #We need this is url.py
    CHOICES_WITH_AUX = (MEASURES_CHOICES_WITH_AUX[0][1]) + (MEASURES_CHOICES_WITH_AUX[1][1]) + (MEASURES_CHOICES_WITH_AUX[2][1])
    # ROOM_CHOICES & etc is the compination of internal with human readable form list
    ROOM_CHOICES = [item[1] for item in MEASURE_CHOICES if item[0] == 'ENVIRONMENT'][0]
    LOAD_CHOICES = [item[1] for item in MEASURE_CHOICES if item[0] == 'ENERGY'][0]
    # room_quantities & etc is the human readable form list
    room_quantities = [item[1] for item in ROOM_CHOICES]
    load_quantities = [item[1] for item in LOAD_CHOICES]
    batterylvl_quantity = ['Battery Level'] #in list for easy appending with other quantities
    # room_measures & etc is the internal and json representation
    room_measures = [item[0] for item in ROOM_CHOICES]
    load_measures = [item[0] for item in LOAD_CHOICES]
    aux_measures = [item[0] for item in Choices.aux_sensor_choices]
    batterylvl_measure = [SENSORS_API_TYPES[6]]
    quantity_to_measure_room = dict(zip(room_quantities,room_measures))
    quantity_to_measure_load = dict(zip(load_quantities,load_measures))
    measure_to_quantity_all = dict(zip(room_measures + load_measures, room_quantities + load_quantities))

    room_units = ['\u2103', 'Lux', '%', '', 'W/m2'] # '\u2103' = oC
    load_units = ['Watt', 'Var', 'VA', '']
    units = room_units + load_units
    # units and choices must be in sync
    room_choices_units = dict()
    load_choices_units = dict()
    for index, choice in enumerate(room_measures):
        room_choices_units[choice] = room_units[index]
    for index, choice in enumerate(load_measures):
        load_choices_units[choice] = load_units[index]

    load = models.ForeignKey('Load', blank=True, null=True)
    measure = models.CharField(max_length=10, choices=MEASURES_CHOICES_WITH_AUX)
    store_middleware = models.ForeignKey('Middleware', related_name='%(class)s_store_middleware_set')

    generator = models.ForeignKey('Generator', blank=True, null=True)
    storage = models.ForeignKey('Storage', blank=True, null=True)

    class Meta:
        unique_together = ("ref", "measure", "network_middleware")

    def quantity(self):
        return [item[1] for item in self.CHOICES_WITH_AUX if item[0] == self.measure][0]

    def clean(self):
        super(Sensor, self).clean() #Call parent clean first
        try:
            if not hasattr(self, 'room'):  # We need to check with attr incase it does not exist at all
                raise ValidationError('Room is required')

            if self.measure in [item[0] for item in zip(self.MEASURE_CHOICES[0])[1][0]] and not self.load is None:
                raise ValidationError('A sensor in a room should not be associated with a load!')
            elif self.measure in [item[0] for item in zip(self.MEASURE_CHOICES[0])[1][0]] and not self.generator is None:
                raise ValidationError('A sensor in a room should not be associated with a generator!')
            elif self.measure in [item[0] for item in zip(self.MEASURE_CHOICES[0])[1][0]] and not self.storage is None:
                raise ValidationError('A sensor in a room should not be associated with a storage!')

            if self.load and self.room and self.load.room() and self.room != self.load.room(): #if load doesnt have room we dont proceed, only actuator can force room
                #if self.load.room is None, thus no room assigment to load then we wont permit sensor assigment
                loadlist = ""
                for load in self.room.load_set():
                    loadlist += load.name + "<br>"
                #Mark_safe allows to use html that would escape from any further django templating
                raise ValidationError(mark_safe('Load ' + self.load.name + ' is not in room ' + self.room.name + "<br> Options are: " + loadlist))

            if self.generator and self.room and self.generator.room() and self.room != self.generator.room():  # if generator doesnt have room we dont proceed, only actuator can force room
                # if self.generator.room is None, thus no room assigment to generator then we wont permit sensor assigment
                generatorlist = ""
                for generator in self.room.generator_set():
                    generatorlist += generator.name + "<br>"
                # Mark_safe allows to use html that would escape from any further django templating
                raise ValidationError(mark_safe(
                    'Generator ' + self.generator.name + ' is not in room ' + self.room.name + "<br> Options are: " + generatorlist))

            if self.storage and self.room and self.storage.room() and self.room != self.storage.room():  # if storage doesnt have room we dont proceed, only actuator can force room
                # if self.storage.room is None, thus no room assigment to storage then we wont permit sensor assigment
                storagelist = ""
                for storage in self.room.storage_set():
                    storagelist += storage.name + "<br>"
                # Mark_safe allows to use html that would escape from any further django templating
                raise ValidationError(mark_safe(
                    'Storage ' + self.storage.name + ' is not in room ' + self.room.name + "<br> Options are: " + storagelist))

            if self.load and self.load.sensor_set.filter(measure=self.measure).exclude(id=self.id):
                raise ValidationError('A load is not allowed to have two sensors of same measure')
            if self.generator and self.generator.sensor_set.filter(measure=self.measure).exclude(id=self.id):
                raise ValidationError('A generator is not allowed to have two sensors of same measure')
            if self.storage and self.storage.sensor_set.filter(measure=self.measure).exclude(id=self.id):
                raise ValidationError('A storage is not allowed to have two sensors of same measure')

            if self.store_middleware.middleware_type != 'DB':
                raise ValidationError('A Store middleware must be of type DB')

            #This is only for esmart actuator since removal/adding from sensor means removal/adding from actuator also.
            if self.measure in self.load_measures and not self.load:
                for act in Actuator.objects.filter(ref=self.ref, action__in=Actuator.esmartactions, network_middleware=self.network_middleware).exclude(load=None):#we dont need the none since this is the end target
                    act.load=None
                    act.save()
            elif self.measure in self.load_measures and self.load:
                for act in Actuator.objects.filter(ref=self.ref, action__in=Actuator.esmartactions, network_middleware=self.network_middleware):
                    act.load=self.load
                    act.save()

            if VirtualEntity.objects.all().filter(sensor=self).first() and self.ref != VirtualEntity.objects.all().get(sensor=self).id:
                #first because only one should be in fact due to onetoone relation
                raise ValidationError('This Sensor is referenced by a vEntity. The Ref should be the same with the vEntity id')

            if VirtualEntity.objects.all().filter(sensor=self).first() and self.network_middleware.middleware_type != 'vMID':
                # first because only one should be in fact due to onetoone relation
                raise ValidationError('This Sensor is referenced by a vEntity. The network middleware MUST be of virtual type')

        except ObjectDoesNotExist:
            pass
            # The other validators will protect us if we reference a foreign key that is not set but is required.


class Actuator(SensAct):
    ACTION_CHOICES = Choices.action_choices

    choices = [item[0] for item in ACTION_CHOICES]
    combinations_set = set(
        combinations(choices + len(choices) * [''], len(choices)))  # Combination of alla possible actions, for the modes

    load = models.ForeignKey('Load', blank=True, null=True)
    action = models.CharField(max_length=10, default=PLC_ACTUATOR_TYPES[0], choices=ACTION_CHOICES)
    
    generator = models.ForeignKey('Generator', blank=True, null=True)
    storage = models.ForeignKey('Storage', blank=True, null=True)
    
    esmartactions=(ACTION_CHOICES[0][0],ACTION_CHOICES[1][0])
    class Meta:
        unique_together = ("ref", "action", "network_middleware")

    def clean(self):
        super(Actuator, self).clean() #Call parent clean first
        try:
            if not hasattr(self, 'room'):  # We need to check with attr incase it does not exist at all
                raise ValidationError('Room is required')

            if self.load and self.load.actuator_set.filter(action=self.action).exclude(id=self.id):
                raise ValidationError('A load is not allowed to have two actuator of same action')
            if self.generator and self.generator.actuator_set.filter(action=self.action).exclude(id=self.id):
                raise ValidationError('A generator is not allowed to have two actuator of same action')
            if self.storage and self.storage.actuator_set.filter(action=self.action).exclude(id=self.id):
                raise ValidationError('A storage is not allowed to have two actuator of same action')

            if self.load and self.room and self.load.room() and self.room != self.load.room():
                #We check (and self.load.room) in case the load is None, if so, we dont have conflict and load will be in the actuator room
                loadlist = ""
                for load in self.room.load_set():
                    loadlist += load.name + "<br>"
                #Mark_safe allows to use html that would escape from any further django templating
                raise ValidationError(mark_safe('Load ' + self.load.name + ' is not in room ' + self.room.name + "<br> Options are: " + loadlist))

            if self.generator and self.room and self.generator.room() and self.room != self.generator.room():
                # We check (and self.generator.room) in case the generator is None, if so, we dont have conflict and generator will be in the actuator room
                generatorlist = ""
                for generator in self.room.generator_set():
                    generatorlist += generator.name + "<br>"
                # Mark_safe allows to use html that would escape from any further django templating
                raise ValidationError(mark_safe(
                    'Generator ' + self.generator.name + ' is not in room ' + self.room.name + "<br> Options are: " + generatorlist))

            if self.storage and self.room and self.storage.room() and self.room != self.storage.room():
                # We check (and self.storage.room) in case the storage is None, if so, we dont have conflict and storage will be in the actuator room
                storagelist = ""
                for storage in self.room.storage_set():
                    storagelist += storage.name + "<br>"
                # Mark_safe allows to use html that would escape from any further django templating
                raise ValidationError(mark_safe(
                    'Storage ' + self.storage.name + ' is not in room ' + self.room.name + "<br> Options are: " + storagelist))

            #This is only for esmart since removal/adding from actuator means removal/addint from sensor also.
            if not self.load:
                for sen in Sensor.objects.filter(ref=self.ref, measure__in=Sensor.load_measures, network_middleware=self.network_middleware).exclude(load=None):
                    sen.load=None
                    sen.save()
            elif self.load:
                for sen in Sensor.objects.filter(ref=self.ref, measure__in=Sensor.load_measures, network_middleware=self.network_middleware):
                    sen.load=self.load
                    sen.save()

            if VirtualEntity.objects.all().filter(actuator=self).first() and self.ref != VirtualEntity.objects.all().get(
                    actuator=self).id:
                # first because only one should be in fact due to onetoone relation
                raise ValidationError(
                    'This Actuator is referenced by a vEntity. The Ref should be the same with the vEntity id')

            if VirtualEntity.objects.all().filter(
                    actuator=self).first() and self.network_middleware.middleware_type != 'vMID':
                # first because only one should be in fact due to onetoone relation
                raise ValidationError(
                    'This Actuator is referenced by a vEntity. The network middleware MUST be of virtual type')

        except ObjectDoesNotExist:
            pass
            # The other validators will protect us if we reference a foreign key that is not set but is required.


# Consumption Characterisation and effect of actions
class Mode(models.Model):
    name = models.CharField(max_length=45)
    device = models.ForeignKey('Device')
    # Nominal power of the mode to anticipate power decrease if change mode and identify the modes.
    # May be replaced by mean power after the first measurements, to avoid accessing the measurement each time an action must be taken.
    power = models.FloatField(verbose_name="Nominal power",
                              help_text="To anticipate power decrease if change or turnOff, without accessing measurements")
    powerTo = models.CommaSeparatedIntegerField(max_length=len(Sensor.room_quantities) * 6,
                                                help_text="Capacity of heating, humidifying etc in order: " + ', '.join(
                                                    [choice[1] for choice in
                                                     Choices.room_sensor_choices]) + ". Comma separated integers, efficiency in %")
    afforded_actions = models.CharField(verbose_name="Afforded actions", max_length=len(Sensor.room_quantities) * 6,
                                        choices=[('|'.join(
                                            ['(' + action + ')' for action in list(action_set) if not len(action) == 0]),
                                                  '|'.join(['(' + action + ')' for action in list(action_set) if
                                                            not len(action) == 0]))
                                                 for action_set in Actuator.combinations_set],
                                        default=None,
                                        help_text="Technical constraints allow, or not, given actions - a computer should not be unplugged neither dimmed, a screen can be unplugged but not dimmed...")

    ordering = ['name']

    class Meta:
        unique_together = ("name", "device")

    def afforded_actions_list(self):
        retVal = []
        for action in Actuator.choices:
            if action in self.afforded_actions:
                retVal.append(action)
        return retVal

    def __unicode__(self):
        return self.name

# ---------------------#---------------------#---------------------#---------------------#---------------------#---------
# User Related
def get_profile_pic_filename(instance, filename):
    filename, file_extension = os.path.splitext(filename)
    if instance.id:
        primary_key = instance.id
    else:
        primary_key = uuid4().hex
    newfilename = os.path.join('user_profile','user_'+str(primary_key)+file_extension)
    return newfilename


class UserProfile(models.Model):
    surname = models.CharField(verbose_name="Surname", max_length=45)
    name = models.CharField(verbose_name="Name", max_length=45)
    profile_image = models.ImageField(upload_to=get_profile_pic_filename, blank=True, null=True)
    uuid = UUIDField(blank=True, null=True, unique=True,  verbose_name="User UUID", help_text="The UUID in hex value with ':' or '-'")
    int_user = models.OneToOneField(User, null=True, blank=True, verbose_name="Site Username")
    current_location = models.ForeignKey('Room', blank=True, null=True, verbose_name="Current fine location")
    unit = models.ForeignKey('Unit', blank=True, null=True, verbose_name="Unit reference", help_text="The building interest to track the corse distance from")
    distance = models.DecimalField(blank=True, null=True, max_digits=8, decimal_places=2, verbose_name="Corse distance", help_text="Distance from unit of reference in km")
    location_type = models.CharField(max_length=45, default='Unknown', verbose_name="Corse location type", help_text="The activity of the user at that location")
    direction = models.CharField(max_length=20, default='Unknown', help_text="Direction of movement corresponding to corse location", verbose_name="Direction of movement")
    duration = models.IntegerField(blank=True, null=True, verbose_name="Time to travel", help_text="The distance to travel if direction is towards unit in min")
    accuracy = models.DecimalField(blank=True, null=True, max_digits=8, decimal_places=2, verbose_name="Corse location accuracy", help_text="The accuracy returned by localization services in meters")
    time_of_location = models.DateTimeField(blank=True, null=True, verbose_name='Location time', help_text="Location update event time")

    def image_tag(self):
        return u'<img src="{0}" width="120" />'.format(self.profile_image.url)
    image_tag.short_description = 'Profile image'
    image_tag.allow_tags = True


    def clean(self): #update internal user info
        if self.int_user:
            self.int_user.first_name = self.name
            self.int_user.last_name = self.surname
            self.int_user.save()

    def save(self, *args, **kwargs):
        # delete old file when replacing by updating the file
        if not self.profile_image:
            self.profile_image = os.path.join('user_profile', 'user_0.jpg')
        try:
            this = UserProfile.objects.get(id=self.id)
            if this.profile_image != self.profile_image and this.profile_image != os.path.join('user_profile','user_0.jpg'): #dont delete default
                this.profile_image.delete(save=False)
        except:
            #New profile
            pass
        super(UserProfile, self).save(*args, **kwargs)

    def __unicode__(self):
        return '{0} {1}'.format(self.name, self.surname)

    def natural_key(self):
        return json.loads(serializers.serialize('json', [self]))[0]  # Serializer returns a list, we pick the first


class UserPreferences(models.Model):
    user = models.ForeignKey('UserProfile')
    load_setting = models.ForeignKey('LoadSetting', blank=True, null=True, help_text="Choose the desired load setting")
    room_setting = models.ForeignKey('RoomSetting', blank=True, null=True, help_text="Choose the desired room setting")

    def clean(self):
        if not self.room_setting and not self.load_setting:
            raise ValidationError('Both settings cannot be empty')
        elif self.room_setting and self.load_setting:
            raise ValidationError('Please define one setting per UserPreferences enty')

    class Meta:
        verbose_name = "User Preferences"
        verbose_name_plural = "User Preferences"


class TimeSlot(models.Model):
    fromTime = models.TimeField(verbose_name='From', null=True, blank=True, help_text="Not required", default=datetime.time(hour=0,minute=0))
    toTime = models.TimeField(verbose_name='To', null=True, blank=True, help_text="Not required", default=datetime.time(hour=0,minute=0))

    mon = models.BooleanField(verbose_name="MON", blank=True, default=True)
    tue = models.BooleanField(verbose_name="TUE", blank=True, default=True)
    wed = models.BooleanField(verbose_name="WED", blank=True, default=True)
    thu = models.BooleanField(verbose_name="THU", blank=True, default=True)
    fri = models.BooleanField(verbose_name="FRI", blank=True, default=True)
    sat = models.BooleanField(verbose_name="SAT", blank=True, default=True)
    sun = models.BooleanField(verbose_name="SUN", blank=True, default=True)

    fromDay = models.DateField(verbose_name='Start', null=True, blank=True, help_text="Not required", default=timezone.now)
    toDay = models.DateField(verbose_name='End', null=True, blank=True, help_text="Not required", default=datetime.date(year=2050, month=1, day=1))

    class Meta:
        abstract = True

    def clean(self):
        if bool(self.fromDay) != bool(self.toDay):
            raise ValidationError('Both day fields should be empty or filled')
        if bool(self.fromTime) != bool(self.toTime):
            raise ValidationError('Both time fields should be empty or filled')
        if self.fromDay:
            if self.fromDay > self.toDay:
                raise ValidationError('First day should be before last day')
            if self.fromDay == self.toDay and self.fromTime > self.toTime:
                raise ValidationError('Starting Time should be before end time')

    def overlaps(self, ts):
        # only the "from" are checked if exist since the "to" are forced to have the same boolean value(exist/not)
        # if same days in the week
        if (self.mon and ts.mon) or (self.tue and ts.tue) or (self.wed and ts.wed) or (self.thu and ts.thu) \
                or (self.fri and ts.fri) or (self.sat and ts.sat) or (self.sun and ts.sun):

            if ts.fromDay and self.fromDay:
                if self.fromDay <= ts.toDay and self.toDay >= ts.fromDay:
                    #if times overlap
                    if ts.fromTime and self.fromTime:
                        if self.fromTime < ts.toTime and self.toTime > ts.fromTime:
                            return True
            else:  #incase of no dates
                #if times overlap
                if ts.fromTime and self.fromTime:
                    if self.fromTime < ts.toTime and self.toTime > ts.fromTime:
                        return True
                else: #Both records miss the date and time and they have the same days and quantity
                    return True
        return False

    def days(self):
        retVal = ''
        if self.mon:
            retVal += 'mon '
        if self.tue:
            retVal += 'tue '
        if self.wed:
            retVal += 'wed '
        if self.thu:
            retVal += 'thu '
        if self.fri:
            retVal += 'fri '
        if self.sat:
            retVal += 'sat '
        if self.sun:
            retVal += 'sun '
        return retVal


class LoadSetting(TimeSlot):
    load = models.ForeignKey('Load')

    load_action = models.CharField(verbose_name="Action type", max_length=10, choices=Actuator.ACTION_CHOICES)
    action_setting = models.IntegerField(validators=[MaxValueValidator(100)], verbose_name="Above action setting", help_text="0 is OFF | 100 is ON")

    comment = models.TextField(blank=True)

    def clean(self):
        if not self.load: raise ValidationError('Load not supplied')
        TimeSlot.clean(self)
        for s in LoadSetting.objects.filter(load=self.load):
            if not s == self:
                if self.load_action == s.load_action and self.overlaps(s):
                    raise ValidationError('This setting may conflict with setting ' + str(s.id))
        if not self.load.affords(self.load_action):
            raise ValidationError('This load can technically not afford that action')


    def __unicode__(self):
        return '{0} for {1}'.format(self.id,self.load.name)



class RoomSetting(TimeSlot):
    room = models.ForeignKey('Room')
    set_quantity = models.CharField(verbose_name="Set Quantity", blank=False, max_length=10, choices=Sensor.ROOM_CHOICES,
                                   help_text="Quantity that the control loop should keep constant")
    set_value = models.FloatField(verbose_name="Set Value", help_text="Value that the control loop should keep")
    comment = models.TextField(blank=True)

    def clean(self):
        if not self.room: raise ValidationError('Room not supplied')
        TimeSlot.clean(self)
        for s in RoomSetting.objects.filter(room=self.room):
            if not s == self:
                if self.set_quantity == s.set_quantity and self.overlaps(s):
                    raise ValidationError('This setting may conflict with setting ' + str(s.id))

    def __unicode__(self):
        return '{0} for {1}'.format(self.id,self.room.name)


class RoomRemark(models.Model):
    room = models.ForeignKey('Room')
    info = models.CharField(verbose_name="The room remarks", blank=False, default=' ', max_length=40,
                               help_text="Enter the messages for users of the room")

    class Meta:
        verbose_name_plural = "Room remarks"


class Parameter(models.Model):
    name = models.CharField(verbose_name="Name", blank=False, default=None, unique=True, max_length=20, help_text="Enter the unique parameter name")
    value = models.CharField(verbose_name="Value", blank=False, default=None, max_length=15, help_text="Enter the parameter value")


class ConfigMsg(models.Model):
    configid = models.IntegerField(verbose_name="Configuration ID", help_text="The random configuration ID", primary_key=True)
    configstring = models.TextField(verbose_name="Configuration string", help_text="The configuration in YAML format")
    configtype =  models.CharField(max_length=3, blank=False, choices=[('REQ','Request'), ('REP','Reply')])

    class Meta:
        verbose_name = "Config message"
        verbose_name_plural = "Config messages"


#--------------------------------------------------------------------------------
#Spice Extension model
class Zone(models.Model):
    unit = models.ForeignKey('Unit', help_text='Required, the unit this zone belongs')
    room = models.OneToOneField('Room', null=True, blank=True, verbose_name="Building Room", help_text='Optional \'real-life\' room')
    volume = models.FloatField(help_text='Zone volume in m3')
    heatpower = models.FloatField(help_text='The installed heat power in watt')
    def __unicode__(self):
        if self.room:
            return self.room.name
        else:
            return 'Zone with ID {0}'.format(self.pk)

class MaterialAttribute(models.Model):
    name = models.CharField(verbose_name="Name", unique=True, max_length=45, help_text="Enter the unique material name")
    cp = models.FloatField(verbose_name="Cp")
    d = models.FloatField(verbose_name="D")
    p = models.FloatField(verbose_name="ρ")

    def __unicode__(self):
        return self.name

class WindowAttribute(models.Model):
    name = models.CharField(verbose_name="Name", unique=True, max_length=45, help_text="Enter the unique window type name")
    uvalue = models.FloatField(verbose_name="U-value")
    shgc = models.FloatField(verbose_name="shgc")

    def __unicode__(self):
        return self.name

class ZoneBorderAttribute(models.Model):
    name = models.CharField(verbose_name="Name", unique=True, max_length=45, help_text="Enter the unique border type name")
    mat1 = models.ForeignKey('MaterialAttribute', blank=True, null=True, verbose_name='Material', related_name='+') #the + ignored the reverse relation creation
    mat2 = models.ForeignKey('MaterialAttribute', blank=True, null=True, verbose_name='Material', related_name='+')
    mat3 = models.ForeignKey('MaterialAttribute', blank=True, null=True, verbose_name='Material', related_name='+')
    mat4 = models.ForeignKey('MaterialAttribute', blank=True, null=True, verbose_name='Material', related_name='+')
    mat5 = models.ForeignKey('MaterialAttribute', blank=True, null=True, verbose_name='Material', related_name='+')

    thickness1 = models.FloatField(blank=True, null=True, verbose_name="Thickness", help_text='Thickness in m')
    thickness2 = models.FloatField(blank=True, null=True, verbose_name="Thickness", help_text='Thickness in m')
    thickness3 = models.FloatField(blank=True, null=True, verbose_name="Thickness", help_text='Thickness in m')
    thickness4 = models.FloatField(blank=True, null=True, verbose_name="Thickness", help_text='Thickness in m')
    thickness5 = models.FloatField(blank=True, null=True, verbose_name="Thickness", help_text='Thickness in m')

    conductive_coef_A = models.FloatField(verbose_name="Conductive Border Coefficient A")
    conductive_coef_B = models.FloatField(verbose_name="Conductive Border Coefficient B")

    def clean(self):
        if bool(self.mat1) != bool(self.thickness1):
            raise ValidationError('Both layer attributes should be empty or filled')
        if bool(self.mat2) != bool(self.thickness2):
            raise ValidationError('Both layer attributes should be empty or filled')
        if bool(self.mat3) != bool(self.thickness3):
            raise ValidationError('Both layer attributes should be empty or filled')
        if bool(self.mat4) != bool(self.thickness4):
            raise ValidationError('Both layer attributes should be empty or filled')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "Zone border attributes"
        verbose_name_plural = "Zone border attributes"


class ZoneBorder(models.Model):
    surface = models.FloatField(help_text='Border surface')
    border_attr = models.ForeignKey('ZoneBorderAttribute', verbose_name='Border Attributes')
    l_zone = models.ForeignKey('Zone', verbose_name='Left adjacent zone', related_name='zoneborder_left_set')
    r_zone = models.ForeignKey('Zone', verbose_name='Right adjacent zone', related_name='zoneborder_right_set')
    window = models.ForeignKey('WindowAttribute', verbose_name='Window Attributes')
    window_surface = models.FloatField(help_text='The window Attributes')

    def __unicode__(self):
        return 'Border between zones left: {0} right:{1}'.format(self.l_zone.__unicode__(), self.r_zone.__unicode__())

    def clean(self):
        if self.l_zone == self.r_zone:
            raise ValidationError('Both left and right zone cannot be the same...')


class VirtualEntity(models.Model):
    name = models.CharField(verbose_name="Name", max_length=45)
    comment = models.TextField(blank=True)

    STATE_CHOICES = (
        (True, u'Yes'),
        (False, u'No'),
    )

    activated = models.BooleanField(verbose_name="Activated in simulation?", default=True, choices=STATE_CHOICES)

    virtual_middleware = models.ForeignKey('Middleware', help_text='The virtual middleware that would simulate that vEntity')
    user = models.OneToOneField('UserProfile', null=True, blank=True, help_text='The linked UserProfile object, if any')
    generator = models.OneToOneField('Generator', null=True, blank=True, help_text='The linked Generator object, if any')
    storage = models.OneToOneField('Storage', null=True, blank=True, help_text='The linked Storage object, if any')
    phev = models.OneToOneField('ElectricalVehicle', null=True, blank=True, help_text='The linked PHEV object, if any')
    load = models.OneToOneField('Load', null=True, blank=True, help_text='The linked Load object, if any')
    sensor = models.OneToOneField('Sensor', null=True, blank=True, help_text='The linked Sensor object, if any')
    actuator = models.OneToOneField('Actuator', null=True, blank=True, help_text='The linked Actuator object, if any')
    sensors_interest = models.ManyToManyField('Sensor', blank=True, related_name='+', verbose_name='Sensors of interest')
    internal_interest = models.ManyToManyField('VirtualEntity', blank=True, related_name='+', verbose_name='vEntities of interest - blocking')
    internal_interest_nonblocking = models.ManyToManyField('VirtualEntity', blank=True, related_name='+', verbose_name='vEntities of interest - Non-blocking')
    simulation_parameters_default = {
                                      "ventity_class": "classname",
                                      "receive_timeout_ms": 1000,
                                      "quantity_change_threshold": {}
                                    }
    simulation_parameters = JSONField(default=json.dumps(simulation_parameters_default),
                                      help_text="The simulation parameters of this vEntity, in JSON format used by vMiddleware in the vEntities models",)

    def __unicode__(self):
        return '{0} at the {1}'.format(self.name, self.virtual_middleware)

    def clean(self):
        try:
            if sum((bool(self.user), bool(self.generator), bool(self.storage),
                    bool(self.load), bool(self.sensor), bool(self.actuator))) not in [0,1] : #none or at least one set
                raise ValidationError('Only ONE of User, Generator, Storage, Load, Sensor or Actuator should be set')


            if self.virtual_middleware and self.virtual_middleware.middleware_type != 'vMID':
                raise ValidationError('The virtual middleware must be of type vMID')

            #Validate the FK in the database associated models.
            if self.load:
                for sensor in self.load.sensor_set.all():
                    if sensor.network_middleware != self.virtual_middleware:
                        raise ValidationError('The sensor {0} of Load {1} is not in the same virtual middleware as this vEntity'
                                          .format(sensor.name, self.load.name))

            if self.generator:
                for sensor in self.generator.sensor_set.all():
                    if sensor.network_middleware != self.virtual_middleware:
                        raise ValidationError('The sensor {0} of Generator {1} is not in the same virtual middleware as this vEntity'
                                          .format(sensor.name, self.generator.name))
            if self.storage:
                for sensor in self.storage.sensor_set.all():
                    if sensor.network_middleware != self.virtual_middleware:
                        raise ValidationError('The sensor {0} of Storage {1} is not in the same virtual middleware as this vEntity'
                                          .format(sensor.name, self.storage.name))

            if self.phev:
                for sensor in self.phev.battery.sensor_set.all():
                    if sensor.network_middleware != self.virtual_middleware:
                        raise ValidationError('The sensor {0} of PHEV {1} is not in the same virtual middleware as this vEntity'
                                          .format(sensor.name, self.phev.name))

            if self.sensor:
                if self.virtual_middleware and self.sensor.network_middleware != self.virtual_middleware:
                    raise ValidationError('The sensor {0} is not in the same virtual middleware as this vEntity'
                                              .format(self.sensor))
                if self.id and self.sensor.ref != self.id:
                    raise ValidationError('The sensor {0} Endpoint reference does not match this vEntity primary key'
                                          .format(self.sensor))
            if self.actuator:
                if self.virtual_middleware and self.actuator.network_middleware != self.virtual_middleware:
                    raise ValidationError('The actuator {0} is not in the same virtual middleware as this vEntity'
                                          .format(self.actuator))
                if self.id and self.actuator.ref != self.id:
                    raise ValidationError('The actuator {0} Endpoint reference does not match this vEntity primary key'
                                          .format(self.actuator))

        except ObjectDoesNotExist:
            pass
            # The other validators will protect us if we reference a foreign key that is not set but is required.

    def sensor_set(self):
        retQuerySet = Sensor.objects.filter(network_middleware=self.virtual_middleware)
        return retQuerySet

    def actuator_set(self):
        retQuerySet = Actuator.objects.filter(network_middleware=self.virtual_middleware)
        return retQuerySet

    class Meta:
        verbose_name_plural = "Virtual entities"
