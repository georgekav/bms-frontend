__author__ = 'Georgios Lilis'
from adminInline import *


class MiddlewareAdmin(admin.ModelAdmin):
    readonly_fields = ['id']
    fieldsets = [
        ('Characteristics', {'fields': ['name', 'middleware_type', 'IP', 'REP_port', 'PUB_port']}),
    ]
    list_display = ['id', 'name', 'middleware_type', 'IP', 'REP_port', 'PUB_port']
    formfield_overrides = {
        models.DecimalField: {'widget': TextInput(attrs={'style': 'width: 4em;'})},
        models.GenericIPAddressField: {'widget': TextInput(attrs={'style': 'width: 8em;'})},
        }

    def __init__(self, *args, **kwargs):
        super(MiddlewareAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = ('name', )


class UnitAdmin(admin.ModelAdmin):
    form = UnitForm
    readonly_fields = ['id']

    fieldsets = [
        ('ID', {'fields': ['id']}),
        (None, {'fields': ['name', 'latitude', 'longitude']}),
        ('Comment', {'fields': ['comment']}),
    ]
    list_display = ['id', 'name', 'latitude', 'longitude' ]
    inlines = [RoomInline]

    def __init__(self, *args, **kwargs):
        super(UnitAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = ('name', )


class RoomAdmin(admin.ModelAdmin):
    form = RoomForm
    readonly_fields = ['id']

    fieldsets = [
        ('ID', {'fields': ['id']}),
        (None, {'fields': ['name', 'comment', 'nature']}),
        ("Characteristics", {"fields": [ 'uuid', 'unit', 'type', 'surface', 'volume']}),
    ]
    list_display = ['id', 'name', 'nature', 'unit', 'uuid']
    inlines = [RoomSettingInline, SensorInline, ActuatorInline]

    def __init__(self, *args, **kwargs):
        super(RoomAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = ('name', )


class RoomInterfaceAdmin(admin.ModelAdmin):
    form = InterfaceForm

    readonly_fields = ['id', 'unit']

    fieldsets = [('ID and Unit', {'fields': ['id', 'unit']}),
                 ('Interfaced rooms', {'fields': ['name', 'room_x', 'room_y', 'comment']}),
                 ('Type and dimension', {'fields': ['interface_type', 'surface', 'thickness']}),
                 ('Thermal characteristics', {'fields': ['rc_model']}),
                 ]
    list_display = ['id', 'unit', 'interface_type', 'room_x', 'room_y']


class RoomConstraintsAdmin(admin.ModelAdmin):
    form = ConstraintForm

    readonly_fields = ['id', 'unit']

    fieldsets = [('ID and Unit', {'fields': ['id', 'unit']}),
                 (None, {'fields': ['room', 'comment', 'quantity_type']}),
                 ('Constraints timeseries and ideal value', {'fields': ['constraints_timeseries', 'ideal_quantity', 'weekdays', 'weekend']}),
                 ]
    list_display = ['id', 'unit', 'room', 'quantity_type']


class ElectricalVehicleAdmin(admin.ModelAdmin):
    form = ElectricalVehicleForm

    readonly_fields = ['id']

    fieldsets = [('ID and Room', {'fields': ['id', 'name', 'comment']}),
                 ('EV Battery', {'fields': ['battery', 'allow_discharge']}),
                 ('EV Constraints timeseries', {'fields': ['schedule']}),
                 ]
    list_display = ['id', 'name', 'battery', 'allow_discharge']

class SchedulingConstraintsAdmin(admin.ModelAdmin):
    form = SchedulingConstraintsForm

    readonly_fields = ['id']

    fieldsets = [('ID and Unit', {'fields': ['id', 'name']}),
                 ('Starting time normal distribution parameters', {'fields': ['scheduling_constraints_min_start_mean',
                                                                              'scheduling_constraints_min_start_std']}),
                 ('Ending time normal distribution parameters', {'fields': ['scheduling_constraints_max_stop_mean',
                                                                            'scheduling_constraints_max_stop_std']}),
                 ]

    list_display = ['id', 'name', 'scheduling_constraints_min_start_mean', 'scheduling_constraints_max_stop_mean']


class LoadAdmin(admin.ModelAdmin):
    form = LoadForm
    readonly_fields = ['id','room']

    lp_model_mode_states_set = ['lp_states_list', 'lp_states_default', 'lp_modes']
    lp_model_seq_trans = ['lp_seq', 'lp_state_trans']
    lp_model_act = ['lp_activity_list', 'lp_activity_period']
    lp_model_scheduling = ['scheduling_constraints_min_start', 'scheduling_constraints_max_stop']
    lp_model = lp_model_mode_states_set+lp_model_seq_trans+lp_model_act+lp_model_scheduling

    fieldsets = [('ID', {'fields': ['id']}),
                 (None, {'fields': ['name', 'room', 'comment']}),
                 ('Load Profile model: states and modes', {'fields': lp_model_mode_states_set}),
                 ('Load Profile model: steady-state/transition sequences', {'fields': lp_model_seq_trans}),
                 ('Load Profile model: user activities', {'fields': lp_model_act}),
                 ('Load Profile model: scheduling constraints', {'fields': lp_model_scheduling}),
                 ('Characteristics', {'fields': [field.name for field in Load._meta.fields if field.name not in readonly_fields+['name', 'comment']+lp_model] }),
    ]
    list_display = ['id', 'name', 'room', 'type', 'uuid']
    inlines = [LoadSettingInline, SensorInline, ActuatorInline]


    def __init__(self, *args, **kwargs):
        super(LoadAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = ('name', )


class GeneratorAdmin(admin.ModelAdmin):
    form = GeneratorForm
    readonly_fields = ['id', 'room', 'current_production']
    fieldsets = [
        ('ID', {'fields': ['id']}),
        (None, {'fields': ['name', 'room', 'comment']}),
        ('Generation Status', {'fields': ['current_production']}),
        ('Characteristics', {'fields': [field.name for field in Generator._meta.fields if field.name not in readonly_fields+['name', 'comment']] }),
    ]

    list_display = ['id', 'name', 'room', 'type', 'nominal_power', 'current_production']

    def __init__(self, *args, **kwargs):
        super(GeneratorAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = ('name', )


class StorageAdmin(admin.ModelAdmin):
    form = StorageForm
    readonly_fields = ['id', 'room', 'state_of_charge','currect_cycles']
    fieldsets = [
        ('ID', {'fields': ['id']}),
        (None, {'fields': ['name', 'room', 'comment']}),
        ('Storage Status', {'fields': ['state_of_charge','currect_cycles']}),
        ('Characteristics', {'fields': [field.name for field in Storage._meta.fields if field.name not in readonly_fields+['name', 'comment']] }),
    ]
    list_display = ['id', 'name', 'room', 'type', 'nominal_capacity', 'state_of_charge']

    def __init__(self, *args, **kwargs):
        super(StorageAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = ('name', )


class DeviceAdmin(admin.ModelAdmin):
    form = GeneralForm
    readonly_fields = ['image_tag']
    list_display = [field.name for field in Device._meta.fields] #include all fields

    inlines = [ModeInline]

    def __init__(self, *args, **kwargs):
        super(DeviceAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = ('name', )


class ModeAdmin(admin.ModelAdmin):
    form = GeneralForm
    readonly_fields = ['id']
    fieldsets = [
        ('ID', {'fields': ['id']}),
        ('Characteristics', {'fields': ['name', 'device', 'power']}),
        ("Control Information", {'fields': ['afforded_actions', 'powerTo', ]})
    ]
    list_display = ['id', 'name', 'device', 'power', 'afforded_actions']

    def __init__(self, *args, **kwargs):
        super(ModeAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = ('name', )


class SensorAdmin(admin.ModelAdmin):
    form = SensActForm
    readonly_fields = ['id']

    fieldsets = [
        ('ID', {'fields': ['id']}),
        (None, {'fields': ['name', 'comment']}),
        ('Sensing Object', {'fields': ['room', 'load', 'generator', 'storage']}),
        (None, {'fields': ['measure', 'additional_parameters']}),
        ('Backend', {'fields': ['ref', 'network_middleware', 'store_middleware']}),
    ]

    list_display = ['id', 'name', 'room', 'measure', 'load', 'generator', 'storage', 'ref', 'network_middleware', 'store_middleware']
    def __init__(self, *args, **kwargs):
        super(SensorAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = ('name', )


class ActuatorAdmin(admin.ModelAdmin):
    form = SensActForm
    readonly_fields = ['id']
    fieldsets = [
        ('ID', {'fields': ['id']}),
        (None, {'fields': ['name', 'comment']}),
        ('Actionable Object', {'fields': ['room', 'load', 'generator', 'storage']}),
        (None, {'fields': ['action', 'additional_parameters']}),
        ('Backend', {'fields': ['ref', 'network_middleware']}),
    ]
    list_display = ['id', 'name', 'room', 'action', 'load', 'generator', 'storage', 'ref', 'network_middleware']

    def __init__(self, *args, **kwargs):
        super(ActuatorAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = ('name', )


class DeviceTypeAdmin(admin.ModelAdmin):
    form = GeneralForm
    readonly_fields = ['id']
    fields = ['id', 'name', ]
    list_display = fields

    def __init__(self, *args, **kwargs):
        super(DeviceTypeAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = ('name', )


class RoomTypeAdmin(admin.ModelAdmin):
    form = GeneralForm
    readonly_fields = ['id']
    fields = ['id', 'name']
    list_display = fields

    def __init__(self, *args, **kwargs):
        super(RoomTypeAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = ('name', )


class LoadSettingAdmin(admin.ModelAdmin):
    form = GeneralForm
    readonly_fields = ['id']
    fieldsets = [
        ('ID', {'fields': ['id']}),
        ('Load Setting', {'fields': ['load', 'load_action', 'action_setting']}),
        ('TimeSchedule', {'fields': ['fromTime', 'toTime', 'fromDay', 'toDay',
              'mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun',]}),
        ('Comment', {'fields': ['comment']}),
    ]
    list_display = ['id', 'load', 'load_action', 'action_setting', 'fromTime', 'toTime', 'fromDay', 'toDay',
                    'mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']
    list_editable = ['load_action', 'action_setting', 'fromTime', 'toTime', 'fromDay', 'toDay',
                     'mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']


class RoomSettingAdmin(admin.ModelAdmin):
    form = RoomSettingForm
    readonly_fields = ['id']
    fieldsets = [
        ('ID', {'fields': ['id']}),
        ('Room Setting', {'fields': ['room', 'set_quantity', 'set_value']}),
        ('TimeSchedule', {'fields': ['fromTime', 'toTime', 'fromDay', 'toDay',
              'mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun',]}),
        ('Comment', {'fields': ['comment']}),
    ]
    list_display = ['id', 'room', 'set_quantity', 'set_value', 'fromTime', 'toTime', 'fromDay', 'toDay',
                    'mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']
    list_editable = ['set_quantity', 'set_value', 'fromTime', 'toTime', 'fromDay', 'toDay',
                    'mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']

    formfield_overrides = {
        models.FloatField: {'widget': TextInput(attrs={'style': 'width: 4em;'})},
    }
    def __init__(self, *args, **kwargs):
        super(RoomSettingAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = ('room', )


class RoomRemarkAdmin(admin.ModelAdmin):
    readonly_fields = ['id']
    fields = ['id', 'room', 'info']
    list_display = fields
    list_editable = ['info']

    def __init__(self, *args, **kwargs):
        super(RoomRemarkAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = ('room', )


class ParameterAdmin(admin.ModelAdmin):
    readonly_fields = ['id']
    fields = ['id', 'name', 'value']
    list_display = fields
    list_editable = ['value']

    def __init__(self, *args, **kwargs):
        super(ParameterAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = ('name', )


class ConfigMsgAdmin(admin.ModelAdmin):
    fields = ['configid', 'configtype', 'configstring']
    formfield_overrides = {
        models.TextField: {'widget': TextInput(attrs={'style': 'width: 70em; '
                                                               'height: 50em'})},
    }

class UserProfileAdmin(admin.ModelAdmin):
    list_display = [field.name for field in UserProfile._meta.fields] #include all fields

    readonly_fields = ['image_tag', 'current_location', 'location_type', 'distance', 'accuracy', 'time_of_location', 'duration', 'direction']
    inlines = [UserPreferanceInline]

class UserPreferanceAdmin(admin.ModelAdmin):
    list_display = [field.name for field in UserPreferences._meta.fields] #include all fields


#--------------------------------------------------------------------------------
#Spice Extension model
class ZoneAdmin(admin.ModelAdmin):
    readonly_fields = ['id']
    list_display = [field.name for field in Zone._meta.fields] #include all fields
    fieldsets = [
        ('ID', {'fields': ['id']}),
        ('Characteristics', {'fields': [field.name for field in Zone._meta.fields if field.name not in readonly_fields]}),
    ]

class MaterialAttributeAdmin(admin.ModelAdmin):
    readonly_fields = ['id']
    list_display = [field.name for field in MaterialAttribute._meta.fields] #include all fields
    fieldsets = [
        ('ID', {'fields': ['id']}),
        ('Characteristics', {'fields': [field.name for field in MaterialAttribute._meta.fields if field.name not in readonly_fields]}),
    ]

class WindowAttributeAdmin(admin.ModelAdmin):
    readonly_fields = ['id']
    list_display = [field.name for field in WindowAttribute._meta.fields] #include all fields
    fieldsets = [
        ('ID', {'fields': ['id']}),
        ('Characteristics', {'fields': [field.name for field in WindowAttribute._meta.fields if field.name not in readonly_fields]}),
    ]

class ZoneBorderAttributeAdmin(admin.ModelAdmin):
    readonly_fields = ['id']
    list_display = [field.name for field in ZoneBorderAttribute._meta.fields] #include all fields
    fieldsets = [
        ('ID', {'fields': ['id', 'name']}),
        ('Layer 1', {'fields': ['mat1', 'thickness1']}),
        ('Layer 2', {'fields': ['mat2', 'thickness2']}),
        ('Layer 3', {'fields': ['mat3', 'thickness3']}),
        ('Layer 4', {'fields': ['mat4', 'thickness4']}),
        ('Layer 5', {'fields': ['mat5', 'thickness5']}),
        ('Conductive Coefficients', {'fields': ['conductive_coef_A', 'conductive_coef_B']}),
    ]


class ZoneBorderAdmin(admin.ModelAdmin):
    readonly_fields = ['id']
    list_display = [field.name for field in ZoneBorder._meta.fields] #include all fields
    fieldsets = [
        ('ID', {'fields': ['id']}),
        ('Characteristics', {'fields': [field.name for field in ZoneBorder._meta.fields if field.name not in readonly_fields]}),
    ]

class VirtualEntityAdmin(admin.ModelAdmin):
    form = VirtualEntityForm
    readonly_fields = ['id']
    list_display = ['id', 'activated', 'virtual_middleware', 'name', 'sensor', 'actuator', 'load', 'generator', 'storage', 'phev', 'user']
    fieldsets = [
        ('ID', {'fields': ['id']}),
        (None, {'fields': ['name', 'comment', 'activated']}),
        ('Interest', {'fields': ['sensors_interest', 'internal_interest', 'internal_interest_nonblocking']}),
        ('Characteristics', {'fields': [field.name for field in VirtualEntity._meta.fields if field.name not in readonly_fields+['name', 'comment', 'activated']]}),
    ]

admin.site.register(Middleware, MiddlewareAdmin)
admin.site.register(Unit, UnitAdmin)
admin.site.register(Room, RoomAdmin)
admin.site.register(RoomType, RoomTypeAdmin)

admin.site.register(Device, DeviceAdmin)
admin.site.register(Mode, ModeAdmin)
admin.site.register(DeviceType, DeviceTypeAdmin)
admin.site.register(Load, LoadAdmin)
admin.site.register(SchedulingConstraints, SchedulingConstraintsAdmin)

admin.site.register(Generator, GeneratorAdmin)
admin.site.register(Storage, StorageAdmin)
admin.site.register(ElectricalVehicle, ElectricalVehicleAdmin)

admin.site.register(Actuator, ActuatorAdmin)
admin.site.register(Sensor, SensorAdmin)

admin.site.register(LoadSetting, LoadSettingAdmin)
admin.site.register(RoomSetting, RoomSettingAdmin)

admin.site.register(RoomRemark, RoomRemarkAdmin)
admin.site.register(Parameter, ParameterAdmin)
admin.site.register(ConfigMsg, ConfigMsgAdmin)

admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(UserPreferences, UserPreferanceAdmin)

#-----------------

admin.site.register(Zone, ZoneAdmin)
admin.site.register(MaterialAttribute, MaterialAttributeAdmin)
admin.site.register(WindowAttribute, WindowAttributeAdmin)
admin.site.register(ZoneBorderAttribute, ZoneBorderAttributeAdmin)
admin.site.register(ZoneBorder, ZoneBorderAdmin)

admin.site.register(RoomInterface, RoomInterfaceAdmin)
admin.site.register(RoomConstraints, RoomConstraintsAdmin)

#-----------------

admin.site.register(VirtualEntity, VirtualEntityAdmin)