__author__ = 'Georgios Lilis'

from django.contrib import admin
from django.utils.html import format_html
from django.core.urlresolvers import reverse

from BmsApp.models import *
from modelForms import *


class MiddlewareInline(admin.TabularInline):
    model = Middleware
    extra = 0
    formfield_overrides = {
        models.DecimalField: {'widget': TextInput(attrs={'style': 'width: 3.5em;'})},
        models.GenericIPAddressField: {'widget': TextInput(attrs={'style': 'width: 7.5em;'})},
        }



class RoomInline(admin.TabularInline):
    model = Room
    form = RoomInlineForm
    extra = 0

    exclude = ['comment']

    def admin_link(self, instance):
        url = reverse('admin:%s_%s_change' % (instance._meta.app_label, instance._meta.model_name), args=(instance.id,))
        return format_html(u'<a href="{}">Edit</a>', url)

    readonly_fields = ('admin_link',)


class LoadInline(admin.TabularInline):
    model = Load
    form = LoadInlineForm
    extra = 0
    formfield_overrides = {
        models.TextField: {'widget': Textarea(
            attrs={'rows': 1,
                   'cols': 40,
                   'style': 'height: 2em;'})},
    }
    exclude = ['comment', 'uuid']

    def admin_link(self, instance):
        url = reverse('admin:%s_%s_change' % (instance._meta.app_label, instance._meta.model_name), args=(instance.id,))
        return format_html(u'<a href="{}">Edit</a>', url)
        # or if you want to include other fields:
        # return format_html(u'<a href="{}">Edit: {}</a>', url, instance.title)

    readonly_fields = ('admin_link',)


class SensorInline(admin.TabularInline):
    model = Sensor
    form = SensActInlineForm
    extra = 0
    formfield_overrides = {
        models.TextField: {'widget': Textarea(
            attrs={'rows': 1,
                   'cols': 40,
                   'style': 'height: 2em;'})},
    }
    exclude = ['comment']

    def admin_link(self, instance):
        url = reverse('admin:%s_%s_change' % (instance._meta.app_label, instance._meta.model_name), args=(instance.id,))
        return format_html(u'<a href="{}">Edit</a>', url)

    readonly_fields = ('admin_link',)


class ActuatorInline(admin.TabularInline):
    model = Actuator
    form = SensActInlineForm
    extra = 0
    formfield_overrides = {
        models.TextField: {'widget': Textarea(
            attrs={'rows': 1,
                   'cols': 40,
                   'style': 'height: 2em;'})},
    }
    exclude = ['comment']

    def admin_link(self, instance):
        url = reverse('admin:%s_%s_change' % (instance._meta.app_label, instance._meta.model_name), args=(instance.id,))
        return format_html(u'<a href="{}">Edit</a>', url)

    readonly_fields = ('admin_link',)


class LoadSettingInline(admin.TabularInline):
    model = LoadSetting
    form = LoadSettingInlineForm
    extra = 0
    formfield_overrides = {
        models.TextField: {'widget': Textarea(
            attrs={'rows': 1,
                   'cols': 40,
                   'style': 'height: 2em;'})},
        models.BooleanField: {'widget': CheckboxInput(
            attrs={'style': 'width: 1em;'})},
    }
    exclude = ['comment', 'fromDay', 'toDay']

    def admin_link(self, instance):
        url = reverse('admin:%s_%s_change' % (instance._meta.app_label, instance._meta.model_name), args=(instance.id,))
        return format_html(u'<a href="{}">Edit</a>', url)

    readonly_fields = ('admin_link',)


class RoomSettingInline(admin.TabularInline):
    model = RoomSetting
    form = RoomSettingInlineForm
    extra = 0
    formfield_overrides = {
        models.TextField: {'widget': Textarea(
            attrs={'rows': 1,
                   'cols': 40,
                   'style': 'height: 2em;'})},
        models.BooleanField: {'widget': CheckboxInput(
            attrs={'style': 'width: 1em;'})},
    }
    exclude = ['comment', 'fromDay', 'toDay']

    def admin_link(self, instance):
        url = reverse('admin:%s_%s_change' % (instance._meta.app_label, instance._meta.model_name), args=(instance.id,))
        return format_html(u'<a href="{}">Edit</a>', url)

    readonly_fields = ('admin_link',)


class ModeInline(admin.TabularInline):
    model = Mode
    form = ModeInlineForm
    extra = 0

    exclude = ['comment']

    def admin_link(self, instance):
        url = reverse('admin:%s_%s_change' % (instance._meta.app_label, instance._meta.model_name), args=(instance.id,))
        return format_html(u'<a href="{}">Edit</a>', url)

    readonly_fields = ('admin_link',)

class UserPreferanceInline(admin.TabularInline):
    model = UserPreferences
    extra = 0
