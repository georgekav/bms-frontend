from django.shortcuts import render, get_object_or_404
from django.views.decorators.http import require_GET
from BmsApp.models import *
from collections import OrderedDict
from conf_general import PUB_MSG_TYPE
from conf_rtserver import RT_SERVER_IP, RT_SERVER_PORT, PUB_ID_FRONTEND, PUB_ID_BMS_USER_REC, PUB_ID_BMS_USER_LOC
from conf_openbms import DJANGO_TIMEOUT_TIMESERIES, DJANGO_TIMEOUT_LASTVALUE, DJANGO_TIMEOUT_ACTION, DJANGO_TIMEOUT_STATUS, DJANGO_ENERGYRES, \
    DJANGO_UPDATE_INTERVAL, DJANGO_UPDATE_GRAPH_INTERVAL, DJANGO_MAX_POWERBAR, DJANGO_MAX_CO2GAUGE, DJANGO_MAX_COSTGAUGE, DJANGO_MAX_ENERGYGAUGE, DJANGO_MAX_SAMPLES

def create_contexts():
    """reads from DB and creates the essential context variables in memory. The views pick the context they need to include from
    the globals and update the context dictionaries."""
    global basic_context, timeout_context, extra_conf_context, websocket_context
    global type_list

    unit_list = Unit.objects.all()
    unit_ord_list = Unit.objects.order_by('name')
    device_list = Device.objects.all()
    type_list = DeviceType.objects.all()
    weatherlocation = Parameter.objects.get(name='weatherloc').value
    kwhprice = Parameter.objects.get(name='kwhprice').value
    co2perkhw = Parameter.objects.get(name='co2perkhw').value

    #The fundamental context for all views
    basic_context = {'unit_list': unit_list, 'unit_ord_list': unit_ord_list, 'type_list': type_list, 'device_list': device_list,
                 'weatherloc':weatherlocation, 'kwhprice': kwhprice, 'co2perkhw': co2perkhw, "static_serve_all_js_css": True}

    #The timeout context in the future we may take them from a DB
    timeout_timeseries = DJANGO_TIMEOUT_TIMESERIES
    timeout_lastvalue = DJANGO_TIMEOUT_LASTVALUE
    timeout_action = DJANGO_TIMEOUT_ACTION
    timeout_status = DJANGO_TIMEOUT_STATUS
    timeout_context = {'timeout_timeseries':timeout_timeseries, 'timeout_lastvalue': timeout_lastvalue,
                       'timeout_action': timeout_action, 'timeout_status': timeout_status}

    #Misc conf context
    energyres  = DJANGO_ENERGYRES # The resolution of the intergrator of energy
    updateinterval = DJANGO_UPDATE_INTERVAL #the update interval of the last points
    updategraphinterval = DJANGO_UPDATE_GRAPH_INTERVAL #the update interval of the graph, the overhead is to the client
    maxsamples = DJANGO_MAX_SAMPLES

    maxpowerbar = DJANGO_MAX_POWERBAR
    maxenergygauge = DJANGO_MAX_ENERGYGAUGE
    maxcostgauge = DJANGO_MAX_COSTGAUGE
    maxco2gauge = DJANGO_MAX_CO2GAUGE

    extra_conf_context = {'energyres': energyres, 'updateinterval': updateinterval, 'updategraphinterval': updategraphinterval,
                   'maxsamples':maxsamples, 'maxpowerbar': maxpowerbar, 'maxenergygauge': maxenergygauge,
                   'maxcostgauge': maxcostgauge, 'maxco2gauge': maxco2gauge,}

    websocket_context = {'rt_server_ip': RT_SERVER_IP, 'rt_server_port': RT_SERVER_PORT, 'rt_room_list': [],
                         'frontend_websock_id': PUB_ID_FRONTEND, 'extra_pub_id_list' : [PUB_ID_BMS_USER_REC]+[PUB_ID_BMS_USER_LOC],
                         'websocket_msg_class': PUB_MSG_TYPE}


create_contexts() #initial config

#The mobile view related context
#general_mobile_view takes priority and skips all unnecessary elements from webapp to show a mobile view
mobile_context = {"skip_navbar" : True, "general_mobile_view": True}


@require_GET
def index(request, mobile_view = False):
    if request.META['HTTP_USER_AGENT'] == 'android/app': mobile_view=True #Failsafe in case the app requests a regular url (non mobile)
    context = { "view_title": "openBMS WebApp | Index"}
    create_contexts()
    render_context = {}
    render_context.update(basic_context)
    render_context.update(websocket_context)
    render_context.update(context)
    if mobile_view:
        render_context.update(mobile_context)
    return render(request, 'index.html', render_context)


@require_GET
def detail_unit(request, unit_id, mobile_view = False):
    if request.META['HTTP_USER_AGENT'] == 'android/app': mobile_view=True #Failsafe in case the app requests a regular url (non mobile)
    unit = get_object_or_404(Unit, pk=unit_id)
    context = {'unit': unit, "view_title": "openBMS WebApp | Unit "+unit.name}
    create_contexts()
    render_context = {}
    render_context.update(basic_context)
    render_context.update(websocket_context)
    render_context.update(context)
    if mobile_view:
        render_context.update(mobile_context)
    return render(request, 'detail_unit.html', render_context)


@require_GET
def detail_room(request, room_id, mobile_view = False):
    global type_list
    if request.META['HTTP_USER_AGENT'] == 'android/app': mobile_view=True #Failsafe in case the app requests a regular url (non mobile)
    room = get_object_or_404(Room, pk=room_id)

    all_actuators = room.actuator_set.all()
    all_sensors = room.sensor_set.exclude(measure__in=Sensor.aux_measures)
    sensors = room.sensor_set.filter(measure__in=Sensor.room_measures) # Room sensors only
    bat_sensors = room.sensor_set.filter(measure__in=Sensor.batterylvl_measure) # battery sensors only

    load_sensors_list = room.sensor_set.filter(measure__in=Sensor.load_measures).exclude(load=None).values_list('measure', flat=True) # Load sensors only

    #All load objects of room by scanning the sensors
    load_list = room.load_set()

    load_per_devtype_list = {}
    for type in type_list:
        load_per_devtype_list[type.id] = room.load_set__type(type)

    ref_list_room = []
    for sensor in sensors:  # Room sensors only
        if sensor.ref not in ref_list_room:
            ref_list_room.append(sensor.ref)
    sensors_per_ref = {}
    sensors_with_bat = {}
    for reference in ref_list_room: # Room sensors only
        sensors_per_ref[reference] = sensors.filter(ref=reference)
        sensors_with_bat[reference] = bat_sensors.filter(ref=reference) #It would be only one per ref
    # List of settin
    # Take the from setting table now
    settings = room.roomsetting_set.all()
    setting_list = []
    for setting in settings:
        settingtype = [item[1] for item in Sensor.ROOM_CHOICES if item[0] == setting.set_quantity][0]
        setting_list.append((settingtype, setting.set_value, Sensor.room_choices_units[setting.set_quantity], setting))

    # Sorting according to human readable name
    setting_list = sorted(setting_list, key=lambda sett: sett[0])

    roomremark_list = room.roomremark_set.all()

    measure_units_all = dict(Sensor.room_choices_units.items() + Sensor.load_choices_units.items() + {'E':'Wh'}.items())
    powers_type = OrderedDict(Sensor.LOAD_CHOICES) # It is a dict that maps the internal with human readable choices
    for key in powers_type.keys():  #Remove the power type if no load sensor in room supports it.
        if not key in load_sensors_list:
            del powers_type[key]
    #This holds the keys of the interested room id to be transfered to rtserver
    rt_room_list = [int(room_id)]

    sensors_type = OrderedDict(Sensor.ROOM_CHOICES)
    context = {'room': room, 'isroomview' : True,
               'load_per_devtype_list': load_per_devtype_list, 'load_list':load_list ,
               'room_sensors_list': sensors, 'measure_units_all': measure_units_all,
               'load_measures_to_quantities': powers_type, 'room_measures_to_quantities': sensors_type,
               'actuators_list': all_actuators, 'ref_list_room': ref_list_room,
               'sensors_per_ref': sensors_per_ref, "sensors_with_bat":sensors_with_bat,
               'all_sensors': all_sensors, 'setting_list': setting_list, 'roomremark_list': roomremark_list, 'rt_room_list': rt_room_list,
               "view_title": "openBMS WebApp | Room " + room.name
    }
    create_contexts()
    render_context = {}
    render_context.update(basic_context)
    render_context.update(timeout_context)
    render_context.update(extra_conf_context)
    render_context.update(websocket_context)
    render_context.update(context)
    if mobile_view:
        render_context.update(mobile_context)
    return render(request, 'detail_room.html', render_context)


@require_GET
def detail_load(request, load_id, mobile_view = False):
    if request.META['HTTP_USER_AGENT'] == 'android/app': mobile_view=True #Failsafe in case the app requests a regular url (non mobile)
    load = get_object_or_404(Load, pk=load_id)

    sensors = load.sensor_set.exclude(measure__in=Sensor.aux_measures)
    actuators = load.actuator_set.all()
    ref_list = []
    load_sensors_list = []
    for sensor in sensors:
        if sensor.ref not in ref_list:
            ref_list.append(sensor.ref)  # All are load sensor of course
        if sensor.measure not in load_sensors_list:
            load_sensors_list.append(sensor.measure)
    sensors_per_ref = {}
    for reference in ref_list:
        sensors_per_ref[reference] = load.sensor_set.exclude(measure__in=Sensor.aux_measures).filter(ref=reference)

    powers_type = OrderedDict(Sensor.LOAD_CHOICES)
    for key in powers_type.keys():  #Remove the power type if no load sensor.
        if not key in load_sensors_list:
            del powers_type[key]
    measure_units_all = dict(Sensor.room_choices_units.items() + Sensor.load_choices_units.items())
    #This holds the keys of the interested room id to be transfered to rtserver
    if load.room():#in case load not in room
        load_room = load.room()
        rt_room_list = [int(load_room.id)]
    else:
        load_room = None
        rt_room_list = []
    context = {'load': load, 'room': load_room, 'isroomview' : False,
               'ref_list': ref_list, 'sensors_per_ref': sensors_per_ref, 'actuators_list': actuators,
               'load_sensors': sensors, 'all_sensors': sensors,
               'measure_units_all': measure_units_all, 'load_measures_to_quantities': powers_type,
               'rt_room_list': rt_room_list,
               "view_title": "openBMS WebApp | Load " + load.name
    }
    create_contexts()
    render_context = {}
    render_context.update(basic_context)
    render_context.update(timeout_context)
    render_context.update(extra_conf_context)
    render_context.update(websocket_context)
    render_context.update(context)
    if mobile_view:
        render_context.update(mobile_context)
    return render(request, 'detail_load.html', render_context)





