__author__ = 'Georgios Lilis'
from django.forms import Textarea, TextInput, CharField, ModelForm, CheckboxInput, Select, ValidationError, SelectMultiple, RadioSelect
from modelFields import RoomChoiceField
from BmsApp.models import Room, Load, Generator, Storage, VirtualEntity, RoomInterface, RoomConstraints, SchedulingConstraints, ElectricalVehicle
import copy


class RoomInlineForm(ModelForm):
    def clean(self, *args, **kwargs):
        cleaned_data = super(RoomInlineForm, self).clean()
        uuid = cleaned_data['uuid']
        if uuid == '':
            uuid = None
        elif uuid is not None:
            #Replace the - with universal system compatible :
            uuid = uuid.replace('-',':')
        cleaned_data['uuid'] = uuid


    class Meta:
        widgets = {"name": TextInput(attrs={'style': 'width: 10em;'}),
                   "type": Select(attrs={'style': 'width: 10em;'}),
                   "surface": TextInput(attrs={'style': 'width: 7em;'}),
                   }


class LoadInlineForm(ModelForm):
    def clean(self, *args, **kwargs):
        cleaned_data = super(LoadInlineForm, self).clean()
        uuid = cleaned_data['uuid']
        if uuid == '':
            uuid = None
        elif uuid is not None:
            #Replace the - with universal system compatible :
            uuid = uuid.replace('-',':')
        cleaned_data['uuid'] = uuid

    class Meta:
        widgets = {"name": TextInput(attrs={'style': 'width: 20em;'})
        }


class ModeInlineForm(ModelForm):
    class Meta:
        widgets = {"name": TextInput(attrs={'style': 'width: 5em;'}),
                   "power": TextInput(attrs={'style': 'width: 5em;'}),
                   "powerTo": TextInput(attrs={'style': 'width: 5em;'})
        }


class LoadSettingInlineForm(ModelForm):
    class Meta:
        widgets = {"conditional": CheckboxInput(attrs={"help_text": "True if the load may be actioned"
                                                                    " but only if there is no user present"}),
                   "load_action": Select(attrs={'style': 'width: 10em;'}),
                   }


class RoomSettingInlineForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RoomSettingInlineForm, self).__init__(*args, **kwargs)
        self.fields['room'] = RoomChoiceField(queryset=Room.objects.select_related('unit').all())

    class Meta:
        widgets = {"set_value": TextInput(attrs={'style': 'width: 4em;'}),
                   "set_quantity": Select(attrs={'style': 'width: 10em;'})}


class RoomSettingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RoomSettingForm, self).__init__(*args, **kwargs)
        self.fields['room'] = RoomChoiceField(queryset=Room.objects.select_related('unit').all())

    class Meta:
        widgets = {"comment": Textarea(attrs={'style': 'width: 30em;', 'rows': 2}),
                   "set_value": TextInput(attrs={'style': 'width: 4em;'}),
                   "set_quantity": Select(attrs={'style': 'width: 10em;'})
        }


class SensActForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SensActForm, self).__init__(*args, **kwargs)
        self.fields['room'] = RoomChoiceField(queryset=Room.objects.select_related('unit').all())
        self.fields['room'].required = False  #Remove the requirement, the model.clean() can try to derive it from load

    def clean(self, *args, **kwargs):
        cleaned_data = super(SensActForm, self).clean()
        room = cleaned_data['room']
        load = cleaned_data['load']
        generator = cleaned_data['generator']
        storage = cleaned_data['storage']
        if not room and not (load or generator or storage):
            del cleaned_data['room'], cleaned_data['load'], cleaned_data['generator'], cleaned_data['storage']
            raise ValidationError('Neither room nor one of (load/generator/storage) is defined as sensing object')
        elif not room:
            if load and load.room():
                self.cleaned_data['room'] = load.room()
            elif generator and generator.room():
                self.cleaned_data['room'] = generator.room()
            elif storage and storage.room():
                self.cleaned_data['room'] = storage.room()
            else:
                del cleaned_data['room'], cleaned_data['load'], cleaned_data['generator'], cleaned_data['storage']
                raise ValidationError('No room provided nor room can be extracted from the (load/generator/storage)')

    class Meta:
        widgets = {
            "name": TextInput(attrs={'style': 'width: 30em;'}),
            "ref": TextInput(attrs={'style': 'width: 4em;'}),
            "action": Select(attrs={'style': 'width: 20em;'}),
            "measure": Select(attrs={'style': 'width: 20em;'}),
            "network_middleware": Select(attrs={'style': 'width: 20em;'}),
            "store_middleware": Select(attrs={'style': 'width: 20em;'}),
            }


class SensActInlineForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SensActInlineForm, self).__init__(*args, **kwargs)
        self.fields['room'] = RoomChoiceField(queryset=Room.objects.select_related('unit').all())
        self.fields['room'].required = False  #Remove the requirement, the model.clean() can try to derive it from load

    class Meta(SensActForm.Meta):
        #This is how we modify the widget of parent class
        tempwid = copy.deepcopy(SensActForm.Meta.widgets)
        tempwid["measure"] = Select(attrs={'style': 'width: 7em;'})
        tempwid["action"] = Select(attrs={'style': 'width: 7em;'})
        tempwid["network_middleware"] = Select(attrs={'style': 'width: 10em;'})
        tempwid["store_middleware"] = Select(attrs={'style': 'width: 10em;'})
        tempwid["load"] = Select(attrs={'style': 'width: 10em;'})
        tempwid["room"] = Select(attrs={'style': 'width: 7em;'})
        tempwid["name"] = TextInput(attrs={'style': 'width: 10em;'})
        widgets = tempwid


class GeneralForm(ModelForm):
    class Meta:
        widgets = {"comment": Textarea(attrs={'style': 'width: 30em;', 'rows': 2}),
                   "ref": TextInput(attrs={'style': 'width: 4em;'})}


class UnitForm(ModelForm):
    class Meta:
        widgets = {"comment": Textarea(attrs={'style': 'width: 30em;', 'rows': 2}),
                   "latitude": TextInput(attrs={'style': 'width: 10em;'}),
                   "longitude": TextInput(attrs={'style': 'width: 10em;'})
        }


class RoomForm(ModelForm):
    class Meta:
        widgets = {"comment": Textarea(attrs={'style': 'width: 30em;', 'rows': 2}),
                   "surface": TextInput(attrs={'style': 'width: 4em;'})}

class InterfaceForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(InterfaceForm, self).__init__(*args, **kwargs)
        self.fields['room_x'] = RoomChoiceField(queryset=Room.objects.select_related('unit').all())
        self.fields['room_y'] = RoomChoiceField(queryset=Room.objects.select_related('unit').all())

    class Meta:
        model = RoomInterface # define it here as well for use by viewAPI POST, the other association is in admin.py
        fields = '__all__' # indicate that all fields in the model should be used.
        widgets = {"comment": Textarea(attrs={'style': 'width: 30em;', 'rows': 2}),
                   "rc_model": Textarea(attrs={'style': 'width: 30em;', 'rows': 10})}


class ConstraintForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ConstraintForm, self).__init__(*args, **kwargs)
        self.fields['room'] = RoomChoiceField(queryset=Room.objects.select_related('unit').all())

    class Meta:
        model = SchedulingConstraints # define it here as well for use by viewAPI POST, the other association is in admin.py
        fields = '__all__' # indicate that all fields in the model should be used.
        widgets = {"comment": Textarea(attrs={'style': 'width: 30em;', 'rows': 2}),
                   "constraints_timeseries": Textarea(attrs={'style': 'width: 30em;', 'rows': 10})}


class ElectricalVehicleForm(ModelForm):

    class Meta:
        model = ElectricalVehicle  # define it here as well for use by viewAPI POST, the other association is in admin.py
        fields = '__all__'  # indicate that all fields in the model should be used.
        widgets = {"comment": Textarea(attrs={'style': 'width: 30em;', 'rows': 2})}


class SchedulingConstraintsForm(ModelForm):

    class Meta:
        model = SchedulingConstraints  # define it here as well for use by viewAPI POST, the other association is in admin.py
        fields = '__all__'  # indicate that all fields in the model should be used.


class LoadForm(ModelForm):
    class Meta:
        model = Load # define it here as well for use by viewAPI POST, the other association is in admin.py
        fields = '__all__' # indicate that all fields in the model should be used.
        widgets = {"comment": Textarea(attrs={'style': 'width: 30em;', 'rows': 2}),
                   "additional_parameters": Textarea(attrs={'style': 'width: 30em;', 'rows': 10})}


class GeneratorForm(ModelForm):
    class Meta:
        model = Generator # define it here as well for use by viewAPI POST, the other association is in admin.py
        fields = '__all__' # indicate that all fields in the model should be used.
        widgets = {"comment": Textarea(attrs={'style': 'width: 30em;', 'rows': 2}),
                   "additional_parameters": Textarea(attrs={'style': 'width: 30em;', 'rows': 10})}


class StorageForm(ModelForm):
    class Meta:
        model = Storage # define it here as well for use by viewAPI POST, the other association is in admin.py
        fields = '__all__' # indicate that all fields in the model should be used.
        widgets = {"comment": Textarea(attrs={'style': 'width: 30em;', 'rows': 2}),
                   "additional_parameters": Textarea(attrs={'style': 'width: 30em;', 'rows': 10})}

class VirtualEntityForm(ModelForm):
    class Meta:
        model = VirtualEntity # define it here as well for use by viewAPI POST, the other association is in admin.py
        fields = '__all__' # indicate that all fields in the model should be used.

        widgets = {"comment": Textarea(attrs={'style': 'width: 30em;', 'rows': 2}),
                   "activated": RadioSelect,
                   "sensors_interest": SelectMultiple(attrs={'style': 'width: 30em;', 'size': 10}),
                   "internal_interest": SelectMultiple(attrs={'style': 'width: 30em;', 'size': 10}),
                   "internal_interest_nonblocking": SelectMultiple(attrs={'style': 'width: 30em;', 'size': 10}),
                   "simulation_parameters": Textarea(attrs={'style': 'width: 30em;', 'rows': 10})}