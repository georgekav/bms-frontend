__author__ = 'Georgios Lilis, Gilles Vetsch'
from django import template

register = template.Library()

#-------QuerySet Custom Filter---------
@register.filter
def filter_measure(value, measure):
    return value.filter(measure=measure)

@register.filter
def filter_action(value, actiontype):
    return value.filter(action=actiontype)

@register.filter
def get_id(value, objid):
    return value.get(id=objid)
#------------------------------------------

#-------General Custom Filter---------
@register.filter
def get_attr(value, attr):
    return getattr(value, attr)


@register.filter
def take_first_name(value, arg):
    return get_attr(value[arg][0],'name')


@register.filter
def take_arg_id(value, arg):
    return get_attr(value[arg], 'id')


@register.filter
def take_arg(value, arg):
    return value[arg]


@register.filter
def test_equal(value, arg):
    return arg in value

@register.filter
def by_type(value, type_id):
    list_device = value.filter(type=type_id)
    return list_device