__author__ = 'Georgios Lilis'
import re

from django.utils.functional import allow_lazy
from django.utils.encoding import force_unicode
from django.template import Node, Library

register = Library()

def strip_empty_lines(value):
    """Return the given HTML with empty and all-whitespace lines removed."""
    return re.sub(r'^[\r\n?|\n\s*\n]+', ' ', force_unicode(value),flags=re.MULTILINE)
strip_empty_lines = allow_lazy(strip_empty_lines, unicode)

class GaplessNode(Node):
    def __init__(self, nodelist):
        self.nodelist = nodelist

    def render(self, context):
        return strip_empty_lines(self.nodelist.render(context).strip())

def gapless(parser, token):
    """
    Remove empty and whitespace-only lines.  Useful for getting rid of those
    empty lines caused by template lines with only template tags and possibly
    whitespace.
    """
    nodelist = parser.parse(('endgapless',))
    parser.delete_first_token()
    return GaplessNode(nodelist)

gapless = register.tag(gapless)