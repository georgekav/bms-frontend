__author__ = 'Georgios Lilis'
import re

from django.utils.translation import ugettext_lazy as _
from django.forms import fields, ModelChoiceField
from django.db import models

UUID_RE = r'^([0-9A-Fa-f]{2}[:-])+([0-9A-Fa-f]{2})$'
uuid_re = re.compile(UUID_RE)

class UUIDFormField(fields.RegexField):
    default_error_messages = {
        'invalid': _(u'Enter a valid hex UUID(with \':\' or \'-\').'),
    }

    def __init__(self, *args, **kwargs):
        super(UUIDFormField, self).__init__(uuid_re, *args, **kwargs)

class UUIDField(models.Field):
    empty_strings_allowed = False
    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 25 #up to 12 bytes uuid
        super(UUIDField, self).__init__(*args, **kwargs)

    def get_internal_type(self):
        return "CharField"

    #Change before save
    def get_db_prep_value(self, value, *args, **kwargs): #
        if self.blank == self.null == self.unique == True and value == '':
            #Force a null value save to accomondate the uniqueness that empty string dont allow.
            value = None
        elif value is not None:
            #Replace the - with universal system compatible :
            value = value.replace('-',':')
        return value

    def formfield(self, **kwargs):
        defaults = {'form_class': UUIDFormField}
        defaults.update(kwargs)
        return super(UUIDField, self).formfield(**defaults)

class RoomChoiceField(ModelChoiceField):
    #Overides the displayed menu in dropdown menu
    def label_from_instance(self, obj):
        return "{0} in {1}".format(obj.name, obj.unit.name)