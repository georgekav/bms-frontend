from django.conf.urls import patterns, url, include
import views


urlpatterns = patterns('',

                       url(r'^index/', views.index, name='index'),
                       url(r'^$', views.index, name='index'),

                       url(r'^unit/(?P<unit_id>\d+)/$', views.detail_unit, name='detail_unit'),
                       url(r'^room/(?P<room_id>\d+)/$', views.detail_room, name='detail_room'),
                       url(r'^load/(?P<load_id>\d+)/$', views.detail_load, name='detail_load'),

                       #mobile urls
                       url(r'^index_m/', views.index, {'mobile_view' : True}, name='index_m'), #not very useful, since no room to select
                       url(r'^unit_m/(?P<unit_id>\d+)/$', views.detail_unit, {'mobile_view' : True}, name='detail_unit_m'),
                       url(r'^room_m/(?P<room_id>\d+)/$', views.detail_room, {'mobile_view' : True}, name='detail_room_m'),
                       url(r'^load_m/(?P<load_id>\d+)/$', views.detail_load, {'mobile_view' : True}, name='detail_load_m'),


)
