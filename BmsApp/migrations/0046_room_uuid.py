# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import BmsApp.modelFields


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0045_auto_20150323_1906'),
    ]

    operations = [
        migrations.AddField(
            model_name='room',
            name='uuid',
            field=BmsApp.modelFields.UUIDField(null=True, max_length=25, blank=True, help_text=b"The UUID in hex value with ':' or '-'", unique=True, verbose_name=b'Room UUID'),
            preserve_default=True,
        ),
    ]
