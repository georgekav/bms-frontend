# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import BmsApp.modelFields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('BmsApp', '0041_auto_20150218_1243'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProfileUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40, verbose_name=b'Name')),
                ('surname', models.CharField(max_length=40, verbose_name=b'Surname')),
                ('uuid', BmsApp.modelFields.UUIDField(max_length=25, verbose_name=b'User UUID')),
                ('int_user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
