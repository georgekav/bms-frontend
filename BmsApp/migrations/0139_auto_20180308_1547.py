# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0138_auto_20180109_1938'),
    ]

    operations = [
        migrations.CreateModel(
            name='ElectricalVehicle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=45)),
                ('comment', models.TextField(blank=True)),
                ('battery', models.ForeignKey(to='BmsApp.Storage')),
            ],
        ),
        migrations.CreateModel(
            name='SchedulingConstraints',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=45)),
                ('scheduling_constraints_min_start_mean', models.FloatField(default=61200, help_text=b'Enter the mean value of minimum starting time(in s)', verbose_name=b'Mean minimum starting time')),
                ('scheduling_constraints_min_start_std', models.FloatField(default=0, help_text=b'Enter the Standard Deviation of minimum starting time (in s)', verbose_name=b'Standard Deviation of the min. start.')),
                ('scheduling_constraints_max_stop_mean', models.FloatField(default=21600, help_text=b'Enter the mean value of maximum ending time (in s)', verbose_name=b'Mean minimum stopping time')),
                ('scheduling_constraints_max_stop_std', models.FloatField(default=0, help_text=b'Enter the Standard Deviation of maximum ending time (in s)', verbose_name=b'Standard Deviation of the max. start.')),
            ],
        ),
        migrations.AddField(
            model_name='electricalvehicle',
            name='schedule',
            field=models.ForeignKey(to='BmsApp.SchedulingConstraints'),
        ),
    ]
