# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0027_auto_20141030_2021'),
    ]

    operations = [
        migrations.AddField(
            model_name='middleware',
            name='name',
            field=models.CharField(default='a', unique=True, max_length=45),
            preserve_default=False,
        ),
    ]
