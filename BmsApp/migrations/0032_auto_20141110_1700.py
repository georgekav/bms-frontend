# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0031_auto_20141110_1558'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actuator',
            name='load',
            field=models.ForeignKey(blank=True, to='BmsApp.Load', null=True),
        ),
    ]
