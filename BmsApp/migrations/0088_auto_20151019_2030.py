# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0087_auto_20151014_1654'),
    ]

    operations = [
        migrations.AlterField(
            model_name='middleware',
            name='REP_port',
            field=models.DecimalField(decimal_places=0, max_digits=5, blank=True, help_text=b'Defaults: db_50100 plc_50200 zw_50300', null=True, verbose_name=b"Middleware's REP port"),
        ),
    ]
