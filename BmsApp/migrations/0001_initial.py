# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Actuator',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ref', models.IntegerField(verbose_name=b'BackendID')),
                ('name', models.CharField(max_length=45, blank=True)),
                ('comment', models.TextField(blank=True)),
                ('action', models.CharField(default=b'RELAY', max_length=10, choices=[(b'RELAY', b'On/Off'), (b'DIMMER', b'Dimmer'), (b'MOD', b'Switch mode')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AuxInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('auxinfo', models.CharField(default=None, help_text=b'Enter the info for user of the room', max_length=40, verbose_name=b'The auxiliary info')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Device',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=45)),
                ('virtual', models.BooleanField(default=False)),
                ('comment', models.TextField(blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Load',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=45)),
                ('user_Driven', models.BooleanField(default=True, help_text=b'False if the load is automated (e.g. server, automatic window)')),
                ('cardinality', models.IntegerField(default=1, help_text=b'If a load is constituted of several devices (e.g. ceilling lamp with 2 pears)')),
                ('comment', models.TextField(blank=True)),
                ('device', models.ForeignKey(to='BmsApp.Device')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='actuator',
            name='load',
            field=models.ForeignKey(to='BmsApp.Load'),
            preserve_default=True,
        ),
        migrations.CreateModel(
            name='Mode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=45)),
                ('power', models.FloatField(help_text=b'To anticipate power decrease if change or turnOff, without accessing measurements.', verbose_name=b'Nominal power')),
                ('powerTo', models.CommaSeparatedIntegerField(help_text=b'Capacity of heating, humidifying etc in order: AirTemp, Luminance, Humidity, PIR. Comma separated integers, efficiency in %', max_length=24)),
                ('afforded_actions', models.CharField(default=None, help_text=b'Technical constraints allow, or not, given actions - a computer should not be unplugged neither dimmed, a screen can be unplugged but not dimmed...', max_length=24, verbose_name=b'Afforded actions', choices=[(b'(RELAY)|(DIMMER)', b'(RELAY)|(DIMMER)'), (b'(MOD)', b'(MOD)'), (b'(RELAY)', b'(RELAY)'), (b'', b''), (b'(RELAY)|(DIMMER)|(MOD)', b'(RELAY)|(DIMMER)|(MOD)'), (b'(DIMMER)|(MOD)', b'(DIMMER)|(MOD)'), (b'(DIMMER)', b'(DIMMER)'), (b'(RELAY)|(MOD)', b'(RELAY)|(MOD)')])),
                ('device', models.ForeignKey(to='BmsApp.Device')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='mode',
            unique_together=set([(b'name', b'device')]),
        ),
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=45)),
                ('surface', models.FloatField(help_text=b'Floor surface, to compute consumption indicators', blank=True)),
                ('controlCoeff', models.CommaSeparatedIntegerField(help_text=b'Tendency to cool, dry etc in order: AirTemp, Luminance, Humidity, PIR. Comma separated integers, in % - for instance thermal coefficient in W/K', max_length=24, verbose_name=b'Loss coefficients', blank=True)),
                ('comment', models.TextField(blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='load',
            name='room',
            field=models.ForeignKey(to='BmsApp.Room'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='load',
            unique_together=set([(b'name', b'room')]),
        ),
        migrations.AddField(
            model_name='auxinfo',
            name='room',
            field=models.ForeignKey(to='BmsApp.Room'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='actuator',
            name='room',
            field=models.ForeignKey(to='BmsApp.Room'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='actuator',
            unique_together=set([(b'ref', b'action')]),
        ),
        migrations.CreateModel(
            name='Room_Type',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=45)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='room',
            name='type',
            field=models.ForeignKey(to='BmsApp.Room_Type'),
            preserve_default=True,
        ),
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fromTime', models.TimeField(help_text=b'Not required', null=True, verbose_name=b'From', blank=True)),
                ('toTime', models.TimeField(help_text=b'Not required', null=True, verbose_name=b'To', blank=True)),
                ('mon', models.BooleanField(default=True, verbose_name=b'Mondays')),
                ('tue', models.BooleanField(default=True, verbose_name=b'Tuesdays')),
                ('wed', models.BooleanField(default=True, verbose_name=b'Wednesdays')),
                ('thu', models.BooleanField(default=True, verbose_name=b'Thursdays')),
                ('fri', models.BooleanField(default=True, verbose_name=b'Fridays')),
                ('sat', models.BooleanField(default=False, verbose_name=b'Saturdays')),
                ('sun', models.BooleanField(default=False, verbose_name=b'Sundays')),
                ('fromDay', models.DateField(help_text=b'Not required', null=True, verbose_name=b'Start', blank=True)),
                ('toDay', models.DateField(help_text=b'Not required', null=True, verbose_name=b'End', blank=True)),
                ('allowAction', models.CharField(help_text=b'Action allowed during the specified timeslot', max_length=10, verbose_name=b'Allowed action', choices=[(b'RELAY', b'On/Off'), (b'DIMMER', b'Dimmer'), (b'MOD', b'Switch mode')])),
                ('conditional', models.BooleanField(default=True, help_text=b'True if user intervention allowed')),
                ('comment', models.TextField(blank=True)),
                ('load', models.ForeignKey(to='BmsApp.Load')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sensor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ref', models.IntegerField(verbose_name=b'BackendID')),
                ('name', models.CharField(max_length=45, blank=True)),
                ('comment', models.TextField(blank=True)),
                ('measure', models.CharField(max_length=10, choices=[(b'ROOM', ((b'TEM', b'AirTemp'), (b'LUM', b'Luminance'), (b'HUM', b'Humidity'), (b'PRE', b'PIR'))), (b'LOAD', ((b'P', b'Active Power'), (b'Q', b'Reactive Power'), (b'S', b'Apparent Power'), (b'PF', b'Power Factor')))])),
                ('load', models.ForeignKey(blank=True, to='BmsApp.Load', null=True)),
                ('room', models.ForeignKey(to='BmsApp.Room')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='sensor',
            unique_together=set([(b'ref', b'measure')]),
        ),
        migrations.CreateModel(
            name='Setting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fromTime', models.TimeField(help_text=b'Not required', null=True, verbose_name=b'From', blank=True)),
                ('toTime', models.TimeField(help_text=b'Not required', null=True, verbose_name=b'To', blank=True)),
                ('mon', models.BooleanField(default=True, verbose_name=b'Mondays')),
                ('tue', models.BooleanField(default=True, verbose_name=b'Tuesdays')),
                ('wed', models.BooleanField(default=True, verbose_name=b'Wednesdays')),
                ('thu', models.BooleanField(default=True, verbose_name=b'Thursdays')),
                ('fri', models.BooleanField(default=True, verbose_name=b'Fridays')),
                ('sat', models.BooleanField(default=False, verbose_name=b'Saturdays')),
                ('sun', models.BooleanField(default=False, verbose_name=b'Sundays')),
                ('fromDay', models.DateField(help_text=b'Not required', null=True, verbose_name=b'Start', blank=True)),
                ('toDay', models.DateField(help_text=b'Not required', null=True, verbose_name=b'End', blank=True)),
                ('setQuantity', models.CharField(help_text=b'Quantity that the control loop should keep constant', max_length=10, verbose_name=b'Set Quantity', choices=[(b'TEM', b'AirTemp'), (b'LUM', b'Luminance'), (b'HUM', b'Humidity'), (b'PRE', b'PIR')])),
                ('setValue', models.FloatField(help_text=b'Value that the control loop should keep', verbose_name=b'Set Value')),
                ('comment', models.TextField(blank=True)),
                ('room', models.ForeignKey(to='BmsApp.Room')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Type',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=45)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='device',
            name='type',
            field=models.ForeignKey(to='BmsApp.Type'),
            preserve_default=True,
        ),
        migrations.CreateModel(
            name='Unit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=45)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='room',
            name='unit',
            field=models.ForeignKey(to='BmsApp.Unit'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='room',
            unique_together=set([(b'name', b'unit')]),
        ),
    ]
