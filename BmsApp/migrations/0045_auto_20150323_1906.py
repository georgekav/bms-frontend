# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0044_auto_20150323_1904'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profileuser',
            name='int_user',
            field=models.OneToOneField(null=True, blank=True, to=settings.AUTH_USER_MODEL, verbose_name=b'Site Username'),
            preserve_default=True,
        ),
    ]
