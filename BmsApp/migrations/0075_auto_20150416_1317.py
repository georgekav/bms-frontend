# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0074_auto_20150415_2000'),
    ]

    operations = [
        migrations.AddField(
            model_name='zoneborder',
            name='window',
            field=models.ForeignKey(default=1, verbose_name=b'Window Attributes', to='BmsApp.WindowAttribute'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='zoneborder',
            name='window_surface',
            field=models.FloatField(default=5, help_text=b'The window Attributes'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='fromDay',
            field=models.DateField(default=django.utils.timezone.now, help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
        ),
        migrations.AlterField(
            model_name='roomsetting',
            name='fromDay',
            field=models.DateField(default=django.utils.timezone.now, help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
        ),
        migrations.AlterField(
            model_name='zoneborder',
            name='border_attr',
            field=models.ForeignKey(verbose_name=b'Border Attributes', to='BmsApp.ZoneBorderAttribute'),
        ),
    ]
