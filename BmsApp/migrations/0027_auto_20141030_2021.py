# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0026_auto_20141030_2009'),
    ]

    operations = [
        migrations.AddField(
            model_name='sensor',
            name='store_middleware',
            field=models.ForeignKey(related_name=b'sensor_store_middleware', default=0, to='BmsApp.Middleware'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='actuator',
            name='network_middleware',
            field=models.ForeignKey(related_name=b'actuator_network_middleware', to='BmsApp.Middleware'),
        ),
        migrations.AlterField(
            model_name='sensor',
            name='network_middleware',
            field=models.ForeignKey(related_name=b'sensor_network_middleware', to='BmsApp.Middleware'),
        ),
    ]
