# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0126_auto_20161004_1432'),
    ]

    operations = [
        migrations.AddField(
            model_name='room',
            name='volume',
            field=models.FloatField(help_text=b'Room volume', null=True, blank=True),
        ),
    ]
