# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0071_auto_20150415_1955'),
    ]

    operations = [
        migrations.AlterField(
            model_name='loadsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 15, 19, 56, 24, 418000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='roomsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 15, 19, 56, 24, 418000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborder',
            name='border_attr',
            field=models.ForeignKey(verbose_name=b'Border Attribute', to='BmsApp.ZoneBorderAttribute'),
            preserve_default=True,
        ),
    ]
