# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0033_auto_20141205_1847'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='load',
            name='room',
        ),
    ]
