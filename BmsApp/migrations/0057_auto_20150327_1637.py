# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0056_auto_20150326_1651'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='travel_method',
            field=models.CharField(default=b'walking', help_text=b'Prefered method of traveling towards unit', max_length=20, choices=[(b'driving', b'Driving'), (b'walking', b'Walking'), (b'bicycling', b'Bicycling'), (b'transit', b'Public Transportation')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 3, 27, 16, 37, 28, 965000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='roomsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 3, 27, 16, 37, 28, 965000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='current_location',
            field=models.ForeignKey(verbose_name=b'Current fine location', blank=True, to='BmsApp.Room', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='distance',
            field=models.FloatField(null=True, verbose_name=b'Corse distance from Unit', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='unit',
            field=models.ForeignKey(blank=True, to='BmsApp.Unit', help_text=b'The building interest to track the corse distance from', null=True, verbose_name=b'Unit reference'),
            preserve_default=True,
        ),
    ]
