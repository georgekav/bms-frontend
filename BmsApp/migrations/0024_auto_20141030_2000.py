# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0023_auto_20141030_1950'),
    ]

    operations = [
        migrations.RenameField(
            model_name='actuator',
            old_name='backend_type',
            new_name='middleware_type',
        ),
        migrations.RenameField(
            model_name='sensor',
            old_name='backend_type',
            new_name='middleware_type',
        ),
    ]
