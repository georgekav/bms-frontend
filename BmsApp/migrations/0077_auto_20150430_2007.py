# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0076_auto_20150429_1746'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userprofile',
            name='travel_method',
        ),
        migrations.AddField(
            model_name='userprofile',
            name='direction',
            field=models.CharField(default=b'Unknown', help_text=b'Direction of movement corresponding to corse location', max_length=20, verbose_name=b'Direction of movement'),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='duration',
            field=models.IntegerField(null=True, verbose_name=b'Time to travel the distance if direction is inward', blank=True),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='location_type',
            field=models.CharField(default=b'Unknown', max_length=45, verbose_name=b'Last corse location type'),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='distance',
            field=models.DecimalField(null=True, verbose_name=b'Corse distance from Unit (km)', max_digits=8, decimal_places=2, blank=True),
        ),
    ]
