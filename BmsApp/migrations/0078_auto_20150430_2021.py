# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0077_auto_20150430_2007'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='accuracy',
            field=models.DecimalField(decimal_places=2, max_digits=8, blank=True, help_text=b'The accuracy returned by localization services', null=True, verbose_name=b'Corse location accuracy'),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='distance',
            field=models.DecimalField(decimal_places=2, max_digits=8, blank=True, help_text=b'Distance from unit of reference in km', null=True, verbose_name=b'Corse distance'),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='duration',
            field=models.IntegerField(help_text=b'The distance to travel if direction is towards unit', null=True, verbose_name=b'Time to travel', blank=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='location_type',
            field=models.CharField(default=b'Unknown', help_text=b'The activity of the user at that location', max_length=45, verbose_name=b'Corse location type'),
        ),
    ]
