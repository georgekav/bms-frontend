# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0098_auto_20160121_1942'),
    ]

    operations = [
        migrations.AlterField(
            model_name='storage',
            name='currect_cycles',
            field=models.IntegerField(blank=True, help_text=b'The currectly recorded charge/discharge cycles', null=True, validators=[django.core.validators.MinValueValidator(0)]),
        ),
        migrations.AlterField(
            model_name='storage',
            name='state_of_charge',
            field=models.FloatField(blank=True, help_text=b'State of charge n% [0-100]', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)]),
        ),
    ]
