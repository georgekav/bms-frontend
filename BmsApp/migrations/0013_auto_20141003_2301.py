# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0012_auto_20141003_2251'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='actuator',
            name='backend',
        ),
        migrations.RemoveField(
            model_name='sensor',
            name='backend',
        ),
    ]
