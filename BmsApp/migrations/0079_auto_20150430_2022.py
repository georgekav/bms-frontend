# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0078_auto_20150430_2021'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='accuracy',
            field=models.DecimalField(decimal_places=2, max_digits=8, blank=True, help_text=b'The accuracy returned by localization services in meters', null=True, verbose_name=b'Corse location accuracy'),
        ),
    ]
