# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0051_auto_20150325_1528'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserPreferance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('load_setting', models.ForeignKey(blank=True, to='BmsApp.LoadSetting', help_text=b'Choose the desired load setting', null=True)),
                ('room_setting', models.ForeignKey(blank=True, to='BmsApp.RoomSetting', help_text=b'Choose the desired room setting', null=True)),
                ('user', models.ForeignKey(to='BmsApp.UserProfile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 3, 25, 15, 42, 48, 138000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='roomsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 3, 25, 15, 42, 48, 138000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
    ]
