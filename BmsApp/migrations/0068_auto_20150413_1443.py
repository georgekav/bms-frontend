# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0067_auto_20150413_1432'),
    ]

    operations = [
        migrations.AlterField(
            model_name='loadsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 13, 14, 43, 25, 501000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='roomsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 13, 14, 43, 25, 501000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='unitzone',
            name='room',
            field=models.OneToOneField(null=True, to='BmsApp.Room', blank=True, help_text=b"Optional 'real-life' room", verbose_name=b'Building Room'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='mat1',
            field=models.ForeignKey(related_name='+', verbose_name=b'Material', blank=True, to='BmsApp.MaterialAttribute', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='mat2',
            field=models.ForeignKey(related_name='+', verbose_name=b'Material', blank=True, to='BmsApp.MaterialAttribute', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='mat3',
            field=models.ForeignKey(related_name='+', verbose_name=b'Material', blank=True, to='BmsApp.MaterialAttribute', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='mat4',
            field=models.ForeignKey(related_name='+', verbose_name=b'Material', blank=True, to='BmsApp.MaterialAttribute', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='mat5',
            field=models.ForeignKey(related_name='+', verbose_name=b'Material', blank=True, to='BmsApp.MaterialAttribute', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='thickness1',
            field=models.FloatField(help_text=b'Thickness in m', null=True, verbose_name=b'Thickness', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='thickness2',
            field=models.FloatField(help_text=b'Thickness in m', null=True, verbose_name=b'Thickness', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='thickness3',
            field=models.FloatField(help_text=b'Thickness in m', null=True, verbose_name=b'Thickness', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='thickness4',
            field=models.FloatField(help_text=b'Thickness in m', null=True, verbose_name=b'Thickness', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='thickness5',
            field=models.FloatField(help_text=b'Thickness in m', null=True, verbose_name=b'Thickness', blank=True),
            preserve_default=True,
        ),
    ]
