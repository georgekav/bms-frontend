# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings
import BmsApp.modelFields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('BmsApp', '0050_auto_20150325_1514'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('surname', models.CharField(max_length=40, verbose_name=b'Surname')),
                ('name', models.CharField(max_length=40, verbose_name=b'Name')),
                ('uuid', BmsApp.modelFields.UUIDField(null=True, max_length=25, blank=True, help_text=b"The UUID in hex value with ':' or '-'", unique=True, verbose_name=b'User UUID')),
                ('current_location', models.ForeignKey(blank=True, to='BmsApp.Room', null=True)),
                ('int_user', models.OneToOneField(null=True, blank=True, to=settings.AUTH_USER_MODEL, verbose_name=b'Site Username')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='profileuser',
            name='current_location',
        ),
        migrations.RemoveField(
            model_name='profileuser',
            name='int_user',
        ),
        migrations.DeleteModel(
            name='ProfileUser',
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 3, 25, 15, 28, 5, 788000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='roomsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 3, 25, 15, 28, 5, 788000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
    ]
