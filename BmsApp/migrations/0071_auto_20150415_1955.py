# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0070_auto_20150415_1953'),
    ]

    operations = [
        migrations.AddField(
            model_name='zoneborder',
            name='l_zone',
            field=models.ForeignKey(related_name='zoneborder_left_set', default=1, verbose_name=b'Left adjacent unit zone', to='BmsApp.Zone'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='zoneborder',
            name='r_zone',
            field=models.ForeignKey(related_name='zoneborder_right_set', default=1, verbose_name=b'Right adjacent unit zone', to='BmsApp.Zone'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 15, 19, 55, 5, 328000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='roomsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 15, 19, 55, 5, 328000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
    ]
