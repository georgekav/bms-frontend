# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0043_auto_20150323_1824'),
    ]

    operations = [
        migrations.RenameField(
            model_name='load',
            old_name='rfid',
            new_name='uuid',
        ),
    ]
