# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0135_roomconstraints_ideal_quantity'),
    ]

    operations = [
        migrations.AddField(
            model_name='room',
            name='nature',
            field=models.CharField(default=b'STANDARD', max_length=45, choices=[(b'STANDARD', b'Human space'), (b'ENERGY', b'Energy storage'), (b'ENV', b'Environment')]),
        ),
        migrations.AlterField(
            model_name='roomconstraints',
            name='ideal_quantity',
            field=models.FloatField(default=None, null=True, verbose_name=b'Ideal constrained quantity', blank=True),
        ),
    ]
