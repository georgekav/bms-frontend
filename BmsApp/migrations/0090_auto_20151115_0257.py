# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0089_auto_20151113_1806'),
    ]

    operations = [
        migrations.AlterField(
            model_name='middleware',
            name='IP',
            field=models.GenericIPAddressField(help_text=b'Enter: 255.255.255.255 for auto resolving, 127.0.0.1 if the given service is running local to rt+django server', verbose_name=b'Middleware IP'),
        ),
    ]
