# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0130_auto_20171102_1433'),
    ]

    operations = [
        migrations.AddField(
            model_name='load',
            name='lp_activity_list',
            field=jsonfield.fields.JSONField(default=b'[{"t_start_stat_model_type": "NORM", "dur_stat_model_type": "NORM", "start_state": "state", "t_start_stat_model_param": [0, 0], "stop_state": "state", "dur_stat_model_param": [0, 0], "param": null, "proba": 1}]', help_text=b'Enter the load activities as a list', verbose_name=b'Activities list', blank=True),
        ),
        migrations.AddField(
            model_name='load',
            name='lp_activity_period',
            field=models.FloatField(default=0, help_text=b'The period of the list, in seconds', verbose_name=b'Activities period'),
        ),
        migrations.AddField(
            model_name='load',
            name='lp_modes',
            field=jsonfield.fields.JSONField(default=b'{"name": {"type_power_distribution": "NORM", "param_power_distribution": [0, 0], "min_p": 0, "max_p": 0}}', help_text=b'Enter the list of load modes', blank=True),
        ),
        migrations.AddField(
            model_name='load',
            name='lp_seq',
            field=jsonfield.fields.JSONField(default=b'{"state_name": {"sequence_transitions": {}, "sequence_list": [{"seq_dur_type": "NORM", "seq_mode": "name_mode", "seq_dur_param": [0, 0]}]}}', help_text=b'Enter the list of sequences per state', blank=True),
        ),
        migrations.AddField(
            model_name='load',
            name='lp_state_trans',
            field=jsonfield.fields.JSONField(default=b'{}', help_text=b'Enter the transition sequence between states', blank=True),
        ),
        migrations.AddField(
            model_name='load',
            name='lp_states_default',
            field=models.CharField(default=b'OFF', max_length=20),
        ),
        migrations.AddField(
            model_name='load',
            name='lp_states_list',
            field=jsonfield.fields.JSONField(default=b'{["OFF"]}', help_text=b'Enter the list of load states (on, off, standby, etc)', blank=True),
        ),
        migrations.AddField(
            model_name='load',
            name='scheduling_constraints_max_stop',
            field=models.FloatField(default=0, help_text=b'Enter the maximum ending time of the deferrable load', verbose_name=b'Maximum stopping time'),
        ),
        migrations.AddField(
            model_name='load',
            name='scheduling_constraints_min_start',
            field=models.FloatField(default=0, help_text=b'Enter the minimum starting time of the deferrable load', verbose_name=b'Minimum starting time'),
        ),
    ]
