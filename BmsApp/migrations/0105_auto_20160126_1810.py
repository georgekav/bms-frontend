# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0104_auto_20160122_1443'),
    ]

    operations = [
        migrations.CreateModel(
            name='RoomRemark',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('info', models.CharField(default=b' ', help_text=b'Enter the messages for users of the room', max_length=40, verbose_name=b'The room remarks')),
                ('room', models.ForeignKey(to='BmsApp.Room')),
            ],
            options={
                'verbose_name_plural': 'Room remarks',
            },
        ),
        migrations.RemoveField(
            model_name='roominfo',
            name='room',
        ),
        migrations.AddField(
            model_name='load',
            name='additional_parameters',
            field=models.TextField(help_text=b'Additional specification and operation parameters, in JSON format used by EMS and vMiddleware', blank=True),
        ),
        migrations.DeleteModel(
            name='RoomInfo',
        ),
    ]
