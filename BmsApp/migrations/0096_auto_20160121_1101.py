# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0095_remove_device_virtual'),
    ]

    operations = [
        migrations.CreateModel(
            name='Generator',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=45)),
                ('generator_type', models.CharField(help_text=b'On-Site electrical generator type', max_length=10, choices=[(b'Solar', b'Photovoltaic'), (b'Wind', b'Wind Turbine'), (b'Hydro', b'Micro-Hydro Turbine')])),
                ('nominal_power', models.FloatField(help_text=b'The installed capacity in W', verbose_name=b'Nominal generation power')),
                ('integrated_efficiency', models.FloatField(help_text=b'The efficiency of energy trasfer from generation directly to integrated storage', verbose_name=b'Integrated storage efficiency')),
                ('additional_parameters', models.TextField(help_text=b'Additional specification and operation parameters, in JSON format used by EMS and vMiddleware', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Storage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=45)),
                ('storage_type', models.CharField(help_text=b'On-Site storage type', max_length=10, choices=[(b'Mechanical', [(b'Flywheel', b'Flywheel'), (b'Air', b'Compressed Air Storage'), (b'Hydro', b'Pumped Hydro-Power')]), (b'Electrochemical', [(b'Secondary', b'Secondary Battery'), (b'Flow', b'Flow Battery')]), (b'Electrical', [(b'DLC', b'Double-layer Capacitor'), (b'SMES', b'Superconducting Magnetic Coil')]), (b'Chemical', [(b'Hydrogen', b'Fuel Cell')]), (b'Thermal', [(b'Thermal', b'Heat storage')])])),
                ('nominal_capacity', models.FloatField(help_text=b'The installed capacity in Wh', verbose_name=b'Nominal generation power')),
                ('average_efficiency', models.FloatField(help_text=b'Average efficiency n% [0-100]', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)])),
                ('life_cycles', models.IntegerField(help_text=b'The nominal charge/discharge cycles supported', validators=[django.core.validators.MinValueValidator(0)])),
                ('currect_cycles', models.IntegerField(help_text=b'The currectly recorded charge/discharge cycles', validators=[django.core.validators.MinValueValidator(0)])),
                ('self_dicharge', models.FloatField(help_text=b'The self discharge rate at %/month', verbose_name=b'Self discharge rate')),
                ('peak_charge', models.TextField(help_text=b'Peak power charge vs stage of charge (SOC), JSON array of two variables')),
                ('peak_discharge', models.TextField(help_text=b'Peak power discharge vs stage of charge (SOC), JSON array of two variables')),
                ('additional_parameters', models.TextField(help_text=b'Additional specification and operation parameters, in JSON format used by EMS and vMiddleware', blank=True)),
                ('location', models.ForeignKey(help_text=b'The building unit in which this storage is installed', to='BmsApp.Unit')),
            ],
        ),
        migrations.AlterField(
            model_name='mode',
            name='power',
            field=models.FloatField(help_text=b'To anticipate power decrease if change or turnOff, without accessing measurements', verbose_name=b'Nominal power'),
        ),
        migrations.AddField(
            model_name='generator',
            name='integrated_storage',
            field=models.ForeignKey(blank=True, to='BmsApp.Storage', help_text=b'Storage capacity associated/integrated with the generation'),
        ),
        migrations.AddField(
            model_name='generator',
            name='location',
            field=models.ForeignKey(help_text=b'The building unit in which this generator is installed', to='BmsApp.Unit'),
        ),
    ]
