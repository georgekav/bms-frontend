# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0002_auto_20140805_2119'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Type',
            new_name='DeviceType',
        ),
        migrations.RenameModel(
            old_name='Room_Type',
            new_name='RoomType',
        ),
    ]
