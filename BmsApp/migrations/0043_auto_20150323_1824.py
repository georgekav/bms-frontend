# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import BmsApp.modelFields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0042_profileuser'),
    ]

    operations = [
        migrations.AlterField(
            model_name='load',
            name='rfid',
            field=BmsApp.modelFields.UUIDField(null=True, max_length=25, blank=True, help_text=b"The UUID in hex value with ':' or '-'", unique=True, verbose_name=b'Load UUID'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profileuser',
            name='int_user',
            field=models.OneToOneField(verbose_name=b'Site Username', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profileuser',
            name='uuid',
            field=BmsApp.modelFields.UUIDField(null=True, max_length=25, blank=True, help_text=b"The UUID in hex value with ':' or '-'", unique=True, verbose_name=b'User UUID'),
            preserve_default=True,
        ),
    ]
