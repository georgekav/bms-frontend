# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0134_auto_20171122_1853'),
    ]

    operations = [
        migrations.AddField(
            model_name='roomconstraints',
            name='ideal_quantity',
            field=models.FloatField(null=True, verbose_name=b'Ideal constrained quantity', blank=True),
        ),
    ]
