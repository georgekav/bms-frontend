# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0099_auto_20160121_1944'),
    ]

    operations = [
        migrations.AlterField(
            model_name='storage',
            name='nominal_capacity',
            field=models.FloatField(help_text=b'The installed capacity in Wh', verbose_name=b'Nominal storage capacity'),
        ),
    ]
