# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0102_auto_20160121_1948'),
    ]

    operations = [
        migrations.AddField(
            model_name='generator',
            name='current_production',
            field=models.FloatField(help_text=b'The real-time generating power', null=True, verbose_name=b'Current generation power', blank=True),
        ),
    ]
