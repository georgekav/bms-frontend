# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0032_auto_20141110_1700'),
    ]

    operations = [
        migrations.AlterField(
            model_name='schedule',
            name='fri',
            field=models.BooleanField(default=True, verbose_name=b'FRI'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='schedule',
            name='mon',
            field=models.BooleanField(default=True, verbose_name=b'MON'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='schedule',
            name='sat',
            field=models.BooleanField(default=False, verbose_name=b'SAT'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='schedule',
            name='sun',
            field=models.BooleanField(default=False, verbose_name=b'SUN'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='schedule',
            name='thu',
            field=models.BooleanField(default=True, verbose_name=b'THU'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='schedule',
            name='tue',
            field=models.BooleanField(default=True, verbose_name=b'TUE'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='schedule',
            name='wed',
            field=models.BooleanField(default=True, verbose_name=b'WED'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='setting',
            name='fri',
            field=models.BooleanField(default=True, verbose_name=b'FRI'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='setting',
            name='mon',
            field=models.BooleanField(default=True, verbose_name=b'MON'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='setting',
            name='sat',
            field=models.BooleanField(default=False, verbose_name=b'SAT'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='setting',
            name='sun',
            field=models.BooleanField(default=False, verbose_name=b'SUN'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='setting',
            name='thu',
            field=models.BooleanField(default=True, verbose_name=b'THU'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='setting',
            name='tue',
            field=models.BooleanField(default=True, verbose_name=b'TUE'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='setting',
            name='wed',
            field=models.BooleanField(default=True, verbose_name=b'WED'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='load',
            unique_together=set([]),
        ),
    ]
