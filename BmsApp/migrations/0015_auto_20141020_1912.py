# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0014_auto_20141017_1535'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Backend',
            new_name='Entity',
        ),
        migrations.AlterField(
            model_name='sensor',
            name='measure',
            field=models.CharField(max_length=10, choices=[(b'ROOM', ((b'TEM', b'Air Temperature'), (b'HUM', b'Luminance'), (b'LUM', b'Humidity'), (b'PRE', b'PIR'))), (b'LOAD', ((b'P', b'Active Power'), (b'Q', b'Reactive Power'), (b'S', b'Apparent Power'), (b'PF', b'Power Factor')))]),
        ),
        migrations.AlterField(
            model_name='setting',
            name='set_quantity',
            field=models.CharField(help_text=b'Quantity that the control loop should keep constant', max_length=10, verbose_name=b'Set Quantity', choices=[(b'TEM', b'Air Temperature'), (b'HUM', b'Luminance'), (b'LUM', b'Humidity'), (b'PRE', b'PIR')]),
        ),
    ]
