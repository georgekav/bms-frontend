# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0139_auto_20180308_1547'),
    ]

    operations = [
        migrations.AddField(
            model_name='virtualentity',
            name='phev',
            field=models.OneToOneField(null=True, blank=True, to='BmsApp.ElectricalVehicle', help_text=b'The linked PHEV object, if any'),
        ),
    ]
