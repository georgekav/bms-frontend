# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0008_auto_20141003_1659'),
    ]

    operations = [
        migrations.RenameField(
            model_name='load',
            old_name='user_Driven',
            new_name='user_driven',
        ),
        migrations.RenameField(
            model_name='schedule',
            old_name='allowAction',
            new_name='allow_action',
        ),
        migrations.RenameField(
            model_name='setting',
            old_name='setQuantity',
            new_name='set_quantity',
        ),
        migrations.RenameField(
            model_name='setting',
            old_name='setValue',
            new_name='set_value',
        ),
    ]
