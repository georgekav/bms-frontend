# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0086_auto_20150914_1921'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='actuator',
            unique_together=set([('ref', 'action', 'network_middleware')]),
        ),
    ]
