# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0025_auto_20141030_2004'),
    ]

    operations = [
        migrations.AddField(
            model_name='actuator',
            name='network_middleware',
            field=models.ForeignKey(default=0, to='BmsApp.Middleware'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='sensor',
            name='network_middleware',
            field=models.ForeignKey(default=0, to='BmsApp.Middleware'),
            preserve_default=False,
        ),
    ]
