# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0105_auto_20160126_1810'),
    ]

    operations = [
        migrations.AddField(
            model_name='generator',
            name='comment',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='storage',
            name='comment',
            field=models.TextField(blank=True),
        ),
    ]
