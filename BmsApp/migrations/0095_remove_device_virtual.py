# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0094_auto_20151119_1555'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='device',
            name='virtual',
        ),
    ]
