# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0053_auto_20150325_1545'),
    ]

    operations = [
        migrations.AddField(
            model_name='unit',
            name='latitude',
            field=models.FloatField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='unit',
            name='longitude',
            field=models.FloatField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='distance',
            field=models.FloatField(null=True, verbose_name=b'Distance from Unit', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='unit',
            field=models.ForeignKey(blank=True, to='BmsApp.Unit', help_text=b'The unit to track the corse distance from', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='action_setting',
            field=models.IntegerField(help_text=b'0 is OFF | 100 is ON', verbose_name=b'Above action setting', validators=[django.core.validators.MaxValueValidator(100)]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 3, 26, 16, 43, 36, 263000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='toTime',
            field=models.TimeField(default=datetime.time(0, 0), help_text=b'Not required', null=True, verbose_name=b'To', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='roomsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 3, 26, 16, 43, 36, 263000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='roomsetting',
            name='toTime',
            field=models.TimeField(default=datetime.time(0, 0), help_text=b'Not required', null=True, verbose_name=b'To', blank=True),
            preserve_default=True,
        ),
    ]
