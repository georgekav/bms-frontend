# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0020_auto_20141021_1220'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='entity',
            name='RTServer_ip',
        ),
        migrations.RemoveField(
            model_name='entity',
            name='RTServer_port',
        ),
    ]
