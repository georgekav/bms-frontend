# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0028_middleware_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='middleware',
            name='PUB_port',
            field=models.DecimalField(null=True, verbose_name=b"Middleware's PUB port", max_digits=5, decimal_places=0, blank=True),
        ),
        migrations.AlterField(
            model_name='middleware',
            name='REP_port',
            field=models.DecimalField(null=True, verbose_name=b"Middleware's REP port", max_digits=5, decimal_places=0, blank=True),
        ),
    ]
