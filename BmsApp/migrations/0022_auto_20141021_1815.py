# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0021_auto_20141021_1554'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='entity',
            name='TCP_PUB_port',
        ),
        migrations.RemoveField(
            model_name='entity',
            name='TCP_ip',
        ),
        migrations.AlterField(
            model_name='entity',
            name='DB_ip',
            field=models.GenericIPAddressField(null=True, verbose_name=b'DB IP', blank=True),
        ),
    ]
