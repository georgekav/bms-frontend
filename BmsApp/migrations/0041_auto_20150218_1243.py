# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0040_auto_20150129_1537'),
    ]

    operations = [
        migrations.AlterField(
            model_name='middleware',
            name='IP',
            field=models.GenericIPAddressField(help_text=b'Enter 255.255.255.255 if you want auto resolving', verbose_name=b'Middleware IP'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='middleware',
            name='PUB_port',
            field=models.DecimalField(decimal_places=0, max_digits=5, blank=True, help_text=b'Defaults: plc_50250 zw_50350 tcp_51050', null=True, verbose_name=b"Middleware's PUB port"),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='middleware',
            name='REP_port',
            field=models.DecimalField(decimal_places=0, max_digits=5, blank=True, help_text=b'Defaults: db_5010 plc_50200 zw_50300', null=True, verbose_name=b"Middleware's REP port"),
            preserve_default=True,
        ),
    ]
