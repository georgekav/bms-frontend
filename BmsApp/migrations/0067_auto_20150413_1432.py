# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0066_auto_20150413_1431'),
    ]

    operations = [
        migrations.AlterField(
            model_name='loadsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 13, 14, 32, 40, 813000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='materialattribute',
            name='name',
            field=models.CharField(help_text=b'Enter the unique material name', unique=True, max_length=45, verbose_name=b'Name'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='parameter',
            name='name',
            field=models.CharField(default=None, help_text=b'Enter the unique parameter name', unique=True, max_length=20, verbose_name=b'Name'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='parameter',
            name='value',
            field=models.CharField(default=None, help_text=b'Enter the parameter value', max_length=15, verbose_name=b'Value'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='roomsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 13, 14, 32, 40, 813000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='name',
            field=models.CharField(max_length=45, verbose_name=b'Name'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='surname',
            field=models.CharField(max_length=45, verbose_name=b'Surname'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='windowattribute',
            name='name',
            field=models.CharField(help_text=b'Enter the unique window type name', unique=True, max_length=45, verbose_name=b'Name'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='name',
            field=models.CharField(help_text=b'Enter the unique border type name', unique=True, max_length=45, verbose_name=b'Name'),
            preserve_default=True,
        ),
    ]
