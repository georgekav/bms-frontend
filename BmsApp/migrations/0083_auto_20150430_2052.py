# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0082_auto_20150430_2049'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='time_of_location',
            field=models.DateTimeField(help_text=b'Location update event time', null=True, verbose_name=b'Location time', blank=True),
        ),
    ]
