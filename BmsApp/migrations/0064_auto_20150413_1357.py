# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0063_auto_20150413_1348'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='zoneborderattribute',
            options={'verbose_name': 'Zone border attributes', 'verbose_name_plural': 'Zone border attributes'},
        ),
        migrations.AddField(
            model_name='zoneborderattribute',
            name='name',
            field=models.CharField(default='test', help_text=b'Enter the unique border type name', unique=True, max_length=15, verbose_name=b'Name'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 13, 13, 57, 39, 590000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='roomsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 13, 13, 57, 39, 590000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='mat1',
            field=models.ForeignKey(related_name='+', verbose_name=b'Material Layer 1', blank=True, to='BmsApp.MaterialAttribute', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='mat2',
            field=models.ForeignKey(related_name='+', verbose_name=b'Material Layer 2', blank=True, to='BmsApp.MaterialAttribute', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='mat3',
            field=models.ForeignKey(related_name='+', verbose_name=b'Material Layer 3', blank=True, to='BmsApp.MaterialAttribute', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='mat4',
            field=models.ForeignKey(related_name='+', verbose_name=b'Material Layer 4', blank=True, to='BmsApp.MaterialAttribute', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='thickness1',
            field=models.FloatField(null=True, verbose_name=b'Thickness Layer 1', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='thickness2',
            field=models.FloatField(null=True, verbose_name=b'Thickness Layer 1', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='thickness3',
            field=models.FloatField(null=True, verbose_name=b'Thickness Layer 1', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='thickness4',
            field=models.FloatField(null=True, verbose_name=b'Thickness Layer 1', blank=True),
            preserve_default=True,
        ),
    ]
