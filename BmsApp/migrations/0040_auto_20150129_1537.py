# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0039_auto_20150113_1933'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mode',
            name='powerTo',
            field=models.CommaSeparatedIntegerField(help_text=b'Capacity of heating, humidifying etc in order: Temperature, Luminance, Humidity, Presence. Comma separated integers, efficiency in %', max_length=24),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='room',
            name='controlCoeff',
            field=models.CommaSeparatedIntegerField(help_text=b'Tendency to cool, dry etc in order: Temperature, Luminance, Humidity, Presence. Comma separated integers, in % - for instance thermal coefficient in W/K', max_length=24, verbose_name=b'Loss coefficients', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sensor',
            name='measure',
            field=models.CharField(max_length=10, choices=[(b'ROOM', ((b'TEM', b'Temperature'), (b'LUM', b'Luminance'), (b'HUM', b'Humidity'), (b'PRE', b'Presence'))), (b'LOAD', ((b'P', b'Active Power'), (b'Q', b'Reactive Power'), (b'S', b'Apparent Power'), (b'PF', b'Power Factor'))), (b'VOID', ((b'NULL', b'Null Sensor'),))]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='setting',
            name='set_quantity',
            field=models.CharField(help_text=b'Quantity that the control loop should keep constant', max_length=10, verbose_name=b'Set Quantity', choices=[(b'TEM', b'Temperature'), (b'LUM', b'Luminance'), (b'HUM', b'Humidity'), (b'PRE', b'Presence')]),
            preserve_default=True,
        ),
    ]
