# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0100_auto_20160121_1946'),
    ]

    operations = [
        migrations.AlterField(
            model_name='storage',
            name='self_dicharge',
            field=models.FloatField(help_text=b'The self discharge rate at %/month', verbose_name=b'Self discharge rate', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)]),
        ),
    ]
