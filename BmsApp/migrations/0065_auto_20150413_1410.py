# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0064_auto_20150413_1357'),
    ]

    operations = [
        migrations.AddField(
            model_name='zoneborderattribute',
            name='mat5',
            field=models.ForeignKey(related_name='+', verbose_name=b'Material Layer 5', blank=True, to='BmsApp.MaterialAttribute', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='zoneborderattribute',
            name='thickness5',
            field=models.FloatField(null=True, verbose_name=b'Thickness Layer 5', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 13, 14, 10, 31, 832000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='roomsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 13, 14, 10, 31, 832000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='thickness2',
            field=models.FloatField(null=True, verbose_name=b'Thickness Layer 2', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='thickness3',
            field=models.FloatField(null=True, verbose_name=b'Thickness Layer 3', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='thickness4',
            field=models.FloatField(null=True, verbose_name=b'Thickness Layer 4', blank=True),
            preserve_default=True,
        ),
    ]
