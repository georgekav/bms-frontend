# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0075_auto_20150416_1317'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sensor',
            name='measure',
            field=models.CharField(max_length=10, choices=[(b'ROOM', ((b'TEM', b'Temperature'), (b'LUM', b'Luminance'), (b'HUM', b'Humidity'), (b'PRE', b'Presence'))), (b'LOAD', ((b'P', b'Active Power'), (b'Q', b'Reactive Power'), (b'S', b'Apparent Power'), (b'PF', b'Power Factor'))), (b'AUX', ((b'BAT', b'Battery Level'), (b'NULL', b'Null Sensor')))]),
        ),
    ]
