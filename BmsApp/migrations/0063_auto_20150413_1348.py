# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0062_auto_20150413_1336'),
    ]

    operations = [
        migrations.DeleteModel(
            name='MaterialAttributeAdmin',
        ),
        migrations.DeleteModel(
            name='UnitZoneAdmin',
        ),
        migrations.DeleteModel(
            name='WindowAttributeAdmin',
        ),
        migrations.RemoveField(
            model_name='zoneborderadmin',
            name='r_zone',
        ),
        migrations.DeleteModel(
            name='ZoneBorderAdmin',
        ),
        migrations.DeleteModel(
            name='ZoneBorderAttributeAdmin',
        ),
        migrations.AddField(
            model_name='windowattribute',
            name='name',
            field=models.CharField(default='aa', help_text=b'Enter the unique window type name', unique=True, max_length=15, verbose_name=b'Name'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 13, 13, 47, 56, 197000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='roomsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 13, 13, 47, 56, 197000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='unitzone',
            name='volume',
            field=models.FloatField(help_text=b'Zone volume in m3'),
            preserve_default=True,
        ),
    ]
