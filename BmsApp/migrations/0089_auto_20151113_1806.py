# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0088_auto_20151019_2030'),
    ]

    operations = [
        migrations.AlterField(
            model_name='middleware',
            name='IP',
            field=models.GenericIPAddressField(help_text=b'Enter: 255.255.255.255 for auto resolving, 127.0.0.1 if the subscriber is only the realtimeserver', verbose_name=b'Middleware IP'),
        ),
    ]
