# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0003_auto_20140805_2156'),
    ]

    operations = [
        migrations.CreateModel(
            name='RoomInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('RoomInfo', models.CharField(default=None, help_text=b'Enter the info for user of the room', max_length=40, verbose_name=b'The auxiliary info')),
                ('room', models.ForeignKey(to='BmsApp.Room')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='auxinfo',
            name='room',
        ),
        migrations.DeleteModel(
            name='AuxInfo',
        ),
    ]
