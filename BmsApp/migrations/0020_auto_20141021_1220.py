# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0019_auto_20141021_1218'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entity',
            name='ZWAVE_PUB_port',
            field=models.DecimalField(null=True, verbose_name=b'ZWAVE PUB port', max_digits=5, decimal_places=0, blank=True),
        ),
        migrations.AlterField(
            model_name='entity',
            name='ZWAVE_REQ_port',
            field=models.DecimalField(null=True, verbose_name=b'ZWAVE REQ port', max_digits=5, decimal_places=0, blank=True),
        ),
        migrations.AlterField(
            model_name='entity',
            name='ZWAVE_ip',
            field=models.GenericIPAddressField(unique=True, null=True, verbose_name=b'ZWAVE IP', blank=True),
        ),
    ]
