# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0065_auto_20150413_1410'),
    ]

    operations = [
        migrations.AlterField(
            model_name='loadsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 13, 14, 31, 18, 576000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='materialattribute',
            name='name',
            field=models.CharField(help_text=b'Enter the unique material name', unique=True, max_length=30, verbose_name=b'Name'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='roomsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 13, 14, 31, 18, 576000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='windowattribute',
            name='name',
            field=models.CharField(help_text=b'Enter the unique window type name', unique=True, max_length=30, verbose_name=b'Name'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborderattribute',
            name='name',
            field=models.CharField(help_text=b'Enter the unique border type name', unique=True, max_length=30, verbose_name=b'Name'),
            preserve_default=True,
        ),
    ]
