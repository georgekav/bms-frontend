# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0016_auto_20141020_1931'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actuator',
            name='backend_type',
            field=models.CharField(max_length=15, choices=[(b'PLC', b'PLC Network'), (b'ZWAVE', b'ZWAVE Network'), (b'IP', b'IP Network')]),
        ),
        migrations.AlterField(
            model_name='sensor',
            name='backend_type',
            field=models.CharField(max_length=15, choices=[(b'PLC', b'PLC Network'), (b'ZWAVE', b'ZWAVE Network'), (b'IP', b'IP Network')]),
        ),
    ]
