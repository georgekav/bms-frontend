# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0101_auto_20160121_1946'),
    ]

    operations = [
        migrations.AlterField(
            model_name='generator',
            name='integrated_efficiency',
            field=models.FloatField(help_text=b'The efficiency of energy trasfer from generation directly to integrated storage in %', verbose_name=b'Integrated storage efficiency', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)]),
        ),
        migrations.AlterField(
            model_name='generator',
            name='integrated_storage',
            field=models.ForeignKey(blank=True, to='BmsApp.Storage', help_text=b'Storage capacity associated/integrated with the generation', null=True),
        ),
    ]
