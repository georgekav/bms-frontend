# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0132_auto_20171108_1700'),
    ]

    operations = [
        migrations.AlterField(
            model_name='load',
            name='lp_seq',
            field=jsonfield.fields.JSONField(default=b'{"state_name": {"sequence_transitions": {}, "sequence_list": [{"seq_dur_type": "NORM", "seq_mode": "seq_label", "seq_dur_param": [0, 0]}]}}', help_text=b'Enter the list of sequences per state', blank=True),
        ),
        migrations.AlterField(
            model_name='load',
            name='lp_states_list',
            field=jsonfield.fields.JSONField(default=b'["OFF"]', help_text=b'Enter the list of load states (on, off, standby, etc)', blank=True),
        ),
        migrations.AlterField(
            model_name='mode',
            name='afforded_actions',
            field=models.CharField(default=None, help_text=b'Technical constraints allow, or not, given actions - a computer should not be unplugged neither dimmed, a screen can be unplugged but not dimmed...', max_length=30, verbose_name=b'Afforded actions', choices=[(b'(RELAY)|(DIMMER)', b'(RELAY)|(DIMMER)'), (b'(MOD)', b'(MOD)'), (b'(RELAY)', b'(RELAY)'), (b'', b''), (b'(RELAY)|(DIMMER)|(MOD)', b'(RELAY)|(DIMMER)|(MOD)'), (b'(DIMMER)|(MOD)', b'(DIMMER)|(MOD)'), (b'(DIMMER)', b'(DIMMER)'), (b'(RELAY)|(MOD)', b'(RELAY)|(MOD)')]),
        ),
        migrations.AlterField(
            model_name='mode',
            name='powerTo',
            field=models.CommaSeparatedIntegerField(help_text=b'Capacity of heating, humidifying etc in order: Temperature, Luminance, Humidity, Presence, Irradiance. Comma separated integers, efficiency in %', max_length=30),
        ),
        migrations.AlterField(
            model_name='roomsetting',
            name='set_quantity',
            field=models.CharField(help_text=b'Quantity that the control loop should keep constant', max_length=10, verbose_name=b'Set Quantity', choices=[(b'TEM', b'Temperature'), (b'LUM', b'Luminance'), (b'HUM', b'Humidity'), (b'PRE', b'Presence'), (b'IRR', b'Irradiance')]),
        ),
        migrations.AlterField(
            model_name='sensor',
            name='measure',
            field=models.CharField(max_length=10, choices=[(b'ENVIRONMENT', ((b'TEM', b'Temperature'), (b'LUM', b'Luminance'), (b'HUM', b'Humidity'), (b'PRE', b'Presence'), (b'IRR', b'Irradiance'))), (b'ENERGY', ((b'P', b'Active Power'), (b'Q', b'Reactive Power'), (b'S', b'Apparent Power'), (b'PF', b'Power Factor'))), (b'AUX', ((b'BAT', b'Battery Level'), (b'NULL', b'Null Sensor')))]),
        ),
    ]
