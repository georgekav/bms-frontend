# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0084_zone_heatpower'),
    ]

    operations = [
        migrations.AlterField(
            model_name='middleware',
            name='IP',
            field=models.GenericIPAddressField(help_text=b'Enter 255.255.255.255 for auto resolving', verbose_name=b'Middleware IP'),
        ),
        migrations.AlterField(
            model_name='zone',
            name='heatpower',
            field=models.FloatField(help_text=b'The installed heat power in watt'),
        ),
    ]
