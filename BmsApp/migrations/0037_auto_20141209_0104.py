# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0036_auto_20141208_1625'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sensor',
            name='measure',
            field=models.CharField(max_length=10, choices=[(b'ROOM', ((b'TEM', b'Air Temperature'), (b'LUM', b'Luminance'), (b'HUM', b'Humidity'), (b'PRE', b'PIR'))), (b'LOAD', ((b'P', b'Active Power'), (b'Q', b'Reactive Power'), (b'S', b'Apparent Power'), (b'PF', b'Power Factor'))), (b'VOID', ((b'Null', b'Null Sensor'),))]),
            preserve_default=True,
        ),
    ]
