# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0018_auto_20141021_1217'),
    ]

    operations = [
        migrations.RenameField(
            model_name='entity',
            old_name='RT_Server_ip',
            new_name='RTServer_ip',
        ),
        migrations.RenameField(
            model_name='entity',
            old_name='RT_Server_port',
            new_name='RTServer_port',
        ),
    ]
