# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0034_remove_load_room'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConfigMsg',
            fields=[
                ('configid', models.IntegerField(help_text=b'The random configuration ID', serialize=False, verbose_name=b'Configuration ID', primary_key=True)),
                ('configstring', models.TextField(help_text=b'The configuration in YAML format', verbose_name=b'Configuration string')),
                ('configtype', models.CharField(max_length=3, choices=[(b'REQ', b'Request'), (b'REP', b'Reply')])),
            ],
            options={
                'verbose_name': 'Config message',
                'verbose_name_plural': 'Config messages',
            },
            bases=(models.Model,),
        ),
    ]
