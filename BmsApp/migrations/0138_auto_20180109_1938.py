# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0137_auto_20180109_1434'),
    ]

    operations = [
        migrations.AddField(
            model_name='virtualentity',
            name='internal_interest_nonblocking',
            field=models.ManyToManyField(related_name='_virtualentity_internal_interest_nonblocking_+', verbose_name=b'vEntities of interest - Non-blocking', to='BmsApp.VirtualEntity', blank=True),
        ),
        migrations.AlterField(
            model_name='virtualentity',
            name='internal_interest',
            field=models.ManyToManyField(related_name='_virtualentity_internal_interest_+', verbose_name=b'vEntities of interest - blocking', to='BmsApp.VirtualEntity', blank=True),
        ),
    ]
