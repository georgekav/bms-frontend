# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0017_auto_20141020_2004'),
    ]

    operations = [
        migrations.RenameField(
            model_name='entity',
            old_name='DB_IP',
            new_name='DB_ip',
        ),
        migrations.RenameField(
            model_name='entity',
            old_name='plc_PUB_port',
            new_name='PLC_PUB_port',
        ),
        migrations.RenameField(
            model_name='entity',
            old_name='plc_REQ_port',
            new_name='PLC_REQ_port',
        ),
        migrations.RenameField(
            model_name='entity',
            old_name='plc_IP',
            new_name='PLC_ip',
        ),
        migrations.RenameField(
            model_name='entity',
            old_name='RT_Server_IP',
            new_name='RT_Server_ip',
        ),
        migrations.RenameField(
            model_name='entity',
            old_name='TCP_IP',
            new_name='TCP_ip',
        ),
        migrations.RenameField(
            model_name='entity',
            old_name='zwave_PUB_port',
            new_name='ZWAVE_PUB_port',
        ),
        migrations.RenameField(
            model_name='entity',
            old_name='zwave_REQ_port',
            new_name='ZWAVE_REQ_port',
        ),
        migrations.RenameField(
            model_name='entity',
            old_name='zwave_IP',
            new_name='ZWAVE_ip',
        ),
    ]
