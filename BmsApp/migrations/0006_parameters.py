# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0005_auto_20140829_1607'),
    ]

    operations = [
        migrations.CreateModel(
            name='Parameters',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=None, help_text=b'Enter the unique parameter name', unique=True, max_length=10, verbose_name=b'Parameter name')),
                ('value', models.FloatField(default=None, help_text=b'Enter the parameter value', verbose_name=b'Parameter value')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
