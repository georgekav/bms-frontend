# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0140_virtualentity_phev'),
    ]

    operations = [
        migrations.AddField(
            model_name='electricalvehicle',
            name='allow_discharge',
            field=models.BooleanField(default=False, verbose_name=b'Allow Discharge'),
        ),
    ]
