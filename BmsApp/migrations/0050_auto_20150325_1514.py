# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0049_auto_20150325_1457'),
    ]

    operations = [
        migrations.CreateModel(
            name='RoomSetting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fromTime', models.TimeField(default=datetime.time(0, 0), help_text=b'Not required', null=True, verbose_name=b'From', blank=True)),
                ('toTime', models.TimeField(default=datetime.time(23, 59), help_text=b'Not required', null=True, verbose_name=b'To', blank=True)),
                ('mon', models.BooleanField(default=True, verbose_name=b'MON')),
                ('tue', models.BooleanField(default=True, verbose_name=b'TUE')),
                ('wed', models.BooleanField(default=True, verbose_name=b'WED')),
                ('thu', models.BooleanField(default=True, verbose_name=b'THU')),
                ('fri', models.BooleanField(default=True, verbose_name=b'FRI')),
                ('sat', models.BooleanField(default=True, verbose_name=b'SAT')),
                ('sun', models.BooleanField(default=True, verbose_name=b'SUN')),
                ('fromDay', models.DateField(default=datetime.datetime(2015, 3, 25, 15, 14, 52, 739000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True)),
                ('toDay', models.DateField(default=datetime.date(2050, 1, 1), help_text=b'Not required', null=True, verbose_name=b'End', blank=True)),
                ('set_quantity', models.CharField(help_text=b'Quantity that the control loop should keep constant', max_length=10, verbose_name=b'Set Quantity', choices=[(b'TEM', b'Temperature'), (b'LUM', b'Luminance'), (b'HUM', b'Humidity'), (b'PRE', b'Presence')])),
                ('set_value', models.FloatField(help_text=b'Value that the control loop should keep', verbose_name=b'Set Value')),
                ('comment', models.TextField(blank=True)),
                ('room', models.ForeignKey(to='BmsApp.Room')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='setting',
            name='room',
        ),
        migrations.DeleteModel(
            name='Setting',
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='action_setting',
            field=models.IntegerField(verbose_name=b'Above action setting, 0 is OFF | 100 is ON ', validators=[django.core.validators.MaxValueValidator(100)]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 3, 25, 15, 14, 52, 739000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
    ]
