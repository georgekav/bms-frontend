# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0048_auto_20150325_1428'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='loadsetting',
            name='allow_action',
        ),
        migrations.RemoveField(
            model_name='loadsetting',
            name='conditional',
        ),
        migrations.AddField(
            model_name='loadsetting',
            name='action_setting',
            field=models.IntegerField(default=None, verbose_name=b'Above action setting', validators=[django.core.validators.MaxValueValidator(100)]),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='loadsetting',
            name='load_action',
            field=models.CharField(default=None, max_length=10, verbose_name=b'Action type', choices=[(b'RELAY', b'On/Off'), (b'DIMMER', b'Dimmer'), (b'MOD', b'Switch mode')]),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 3, 25, 14, 56, 48, 764000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='fromTime',
            field=models.TimeField(default=datetime.time(0, 0), help_text=b'Not required', null=True, verbose_name=b'From', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='sat',
            field=models.BooleanField(default=True, verbose_name=b'SAT'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='sun',
            field=models.BooleanField(default=True, verbose_name=b'SUN'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='toDay',
            field=models.DateField(default=datetime.date(2050, 1, 1), help_text=b'Not required', null=True, verbose_name=b'End', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='toTime',
            field=models.TimeField(default=datetime.time(23, 59), help_text=b'Not required', null=True, verbose_name=b'To', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='setting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 3, 25, 14, 56, 48, 764000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='setting',
            name='fromTime',
            field=models.TimeField(default=datetime.time(0, 0), help_text=b'Not required', null=True, verbose_name=b'From', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='setting',
            name='sat',
            field=models.BooleanField(default=True, verbose_name=b'SAT'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='setting',
            name='sun',
            field=models.BooleanField(default=True, verbose_name=b'SUN'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='setting',
            name='toDay',
            field=models.DateField(default=datetime.date(2050, 1, 1), help_text=b'Not required', null=True, verbose_name=b'End', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='setting',
            name='toTime',
            field=models.TimeField(default=datetime.time(23, 59), help_text=b'Not required', null=True, verbose_name=b'To', blank=True),
            preserve_default=True,
        ),
    ]
