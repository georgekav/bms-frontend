# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0046_room_uuid'),
    ]

    operations = [
        migrations.AddField(
            model_name='profileuser',
            name='current_location',
            field=models.ForeignKey(blank=True, to='BmsApp.Room', null=True),
            preserve_default=True,
        ),
    ]
