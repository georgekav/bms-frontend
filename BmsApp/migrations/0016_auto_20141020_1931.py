# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0015_auto_20141020_1912'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='entity',
            options={'verbose_name_plural': 'Entities'},
        ),
        migrations.AlterModelOptions(
            name='roominfo',
            options={'verbose_name_plural': 'Room info'},
        ),
        migrations.AddField(
            model_name='actuator',
            name='backend_type',
            field=models.CharField(default=1, max_length=15, choices=[(b'PLC', b'PLC Network'), (b'ZWAVE', b'ZWAVE Network'), (b'TCP', b'Internet Network')]),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='sensor',
            name='backend_type',
            field=models.CharField(default=5, max_length=15, choices=[(b'PLC', b'PLC Network'), (b'ZWAVE', b'ZWAVE Network'), (b'TCP', b'Internet Network')]),
            preserve_default=False,
        ),
    ]
