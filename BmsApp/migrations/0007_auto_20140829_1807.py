# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0006_parameters'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Parameters',
            new_name='Parameter',
        ),
    ]
