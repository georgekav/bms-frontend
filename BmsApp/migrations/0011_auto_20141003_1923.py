# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0010_auto_20141003_1727'),
    ]

    operations = [
        migrations.CreateModel(
            name='Backend',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('plc_IP', models.GenericIPAddressField(unique=True)),
                ('plc_REQ_port', models.DecimalField(max_digits=5, decimal_places=0)),
                ('plc_PUB_port', models.DecimalField(max_digits=5, decimal_places=0)),
                ('zwave_IP', models.GenericIPAddressField(unique=True)),
                ('zwave_REQ_port', models.DecimalField(max_digits=5, decimal_places=0)),
                ('zwave_PUB_port', models.DecimalField(max_digits=5, decimal_places=0)),
                ('DB_IP', models.GenericIPAddressField()),
                ('DB_REQ_port', models.DecimalField(max_digits=5, decimal_places=0)),
                ('TCP_IP', models.GenericIPAddressField(unique=True)),
                ('TCP_PUB_port', models.DecimalField(max_digits=5, decimal_places=0)),
                ('RT_Server_IP', models.GenericIPAddressField()),
                ('RT_Server_port', models.DecimalField(max_digits=5, decimal_places=0)),
                ('unit', models.ForeignKey(to='BmsApp.Unit')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='actuator',
            name='backend',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='BmsApp.Backend', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sensor',
            name='backend',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='BmsApp.Backend', null=True),
            preserve_default=True,
        ),
    ]
