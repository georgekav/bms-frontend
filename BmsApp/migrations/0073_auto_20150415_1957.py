# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0072_auto_20150415_1956'),
    ]

    operations = [
        migrations.AlterField(
            model_name='loadsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 15, 19, 57, 21, 41000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='roomsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 15, 19, 57, 21, 41000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborder',
            name='l_zone',
            field=models.ForeignKey(related_name='zoneborder_left_set', verbose_name=b'Left adjacent zone', to='BmsApp.Zone'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborder',
            name='r_zone',
            field=models.ForeignKey(related_name='zoneborder_right_set', verbose_name=b'Right adjacent zone', to='BmsApp.Zone'),
            preserve_default=True,
        ),
    ]
