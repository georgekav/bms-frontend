# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0129_auto_20171102_1225'),
    ]

    operations = [
        migrations.AlterField(
            model_name='load',
            name='ems_type',
            field=models.CharField(default=b'USER-DRIVEN', max_length=10, choices=[(b'USER-DRIVEN', b'Non-controllable'), (b'DEF', b'Deferrable'), (b'THERM', b'Thermal load')]),
        ),
    ]
