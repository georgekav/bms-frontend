# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-17 13:52
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0109_auto_20160317_1430'),
    ]

    operations = [
        migrations.AlterField(
            model_name='generator',
            name='additional_parameters',
            field=jsonfield.fields.JSONField(blank=True, help_text=b'Additional specification and operation parameters, in JSON format used by mainly EMS'),
        ),
        migrations.AlterField(
            model_name='load',
            name='additional_parameters',
            field=jsonfield.fields.JSONField(blank=True, help_text=b'Additional specification and operation parameters, in JSON format used by mainly EMS'),
        ),
        migrations.AlterField(
            model_name='storage',
            name='additional_parameters',
            field=jsonfield.fields.JSONField(blank=True, help_text=b'Additional specification and operation parameters, in JSON format used by mainly EMS'),
        ),
        migrations.AlterField(
            model_name='virtualentity',
            name='generator',
            field=models.ForeignKey(blank=True, help_text=b'The linked Generator object, if any', null=True, on_delete=django.db.models.deletion.CASCADE, to='BmsApp.Generator'),
        ),
        migrations.AlterField(
            model_name='virtualentity',
            name='load',
            field=models.ForeignKey(blank=True, help_text=b'The linked Load object, if any', null=True, on_delete=django.db.models.deletion.CASCADE, to='BmsApp.Load'),
        ),
        migrations.AlterField(
            model_name='virtualentity',
            name='simulation_parameters',
            field=jsonfield.fields.JSONField(help_text=b'The simulation parameters of this vEntity, in JSON format used mainly by vMiddleware'),
        ),
        migrations.AlterField(
            model_name='virtualentity',
            name='storage',
            field=models.ForeignKey(blank=True, help_text=b'The linked Storage object, if any', null=True, on_delete=django.db.models.deletion.CASCADE, to='BmsApp.Storage'),
        ),
    ]
