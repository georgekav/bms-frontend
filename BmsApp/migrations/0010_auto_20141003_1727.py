# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0009_auto_20141003_1726'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actuator',
            name='ref',
            field=models.IntegerField(verbose_name=b'Backend ID'),
        ),
        migrations.AlterField(
            model_name='sensor',
            name='ref',
            field=models.IntegerField(verbose_name=b'Backend ID'),
        ),
    ]
