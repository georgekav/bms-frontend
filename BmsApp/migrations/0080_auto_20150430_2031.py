# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0079_auto_20150430_2022'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='duration',
            field=models.IntegerField(help_text=b'The distance to travel if direction is towards unit in min', null=True, verbose_name=b'Time to travel', blank=True),
        ),
    ]
