# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0127_room_volume'),
    ]

    operations = [
        migrations.CreateModel(
            name='RoomConstraints',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity_type', models.CharField(max_length=15, choices=[(b'TEM', b'Temperature'), (b'LUM', b'Luminance'), (b'HUM', b'Humidity')])),
                ('constraints_timeseries', jsonfield.fields.JSONField(default=b'{"comfort_minvector": [0, 0], "comfort_maxvector": [0, 0], "comfort_timevector": [0, 86400]}', help_text=b"Enter the pairs (t,v) such that the constraints takes the value 'v' from time 't'. Repeat the last pair to end the constraint")),
                ('weekdays', models.BooleanField(default=True, verbose_name=b'Week-days')),
                ('weekend', models.BooleanField(default=True, verbose_name=b'Week-ends')),
                ('comment', models.TextField(blank=True)),
                ('room', models.ForeignKey(to='BmsApp.Room')),
            ],
        ),
        migrations.CreateModel(
            name='RoomInterface',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'Interface', max_length=30)),
                ('rc_model', jsonfield.fields.JSONField(default=b'{"c": [], "r": [0]}', help_text=b'(n+1)RnC equivalent model')),
                ('comment', models.TextField(blank=True)),
                ('room_x', models.ForeignKey(related_name='+', to='BmsApp.Room')),
                ('room_y', models.ForeignKey(related_name='+', to='BmsApp.Room')),
            ],
        ),
        migrations.AddField(
            model_name='load',
            name='ems_type',
            field=models.CharField(default=b'U-D', max_length=10, choices=[(b'U-D', b'Non-controllable'), (b'D', b'Deferrable'), (b'Th', b'Thermal load')]),
        ),
        migrations.AlterField(
            model_name='virtualentity',
            name='simulation_parameters',
            field=jsonfield.fields.JSONField(default=b'{"ventity_class": "classname", "receive_timeout_ms": 1000, "quantity_change_threshold": {}}', help_text=b'The simulation parameters of this vEntity, in JSON format used by vMiddleware in the vEntities models'),
        ),
    ]
