# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0080_auto_20150430_2031'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='time_of_location',
            field=models.DateField(help_text=b'Location update time', null=True, verbose_name=b'Location time', blank=True),
        ),
    ]
