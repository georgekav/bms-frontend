# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0083_auto_20150430_2052'),
    ]

    operations = [
        migrations.AddField(
            model_name='zone',
            name='heatpower',
            field=models.FloatField(default=0, help_text=b'The installed heat capacity'),
            preserve_default=False,
        ),
    ]
