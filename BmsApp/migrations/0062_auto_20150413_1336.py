# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0061_auto_20150413_1331'),
    ]

    operations = [
        migrations.CreateModel(
            name='MaterialAttributeAdmin',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('p', models.FloatField(verbose_name=b'\xcf\x81')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UnitZone',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('volume', models.FloatField(help_text=b'Zone colume')),
                ('room', models.OneToOneField(null=True, blank=True, to='BmsApp.Room', verbose_name=b'Room it corresponds')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UnitZoneAdmin',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('volume', models.FloatField(help_text=b'Zone colume')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WindowAttributeAdmin',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('shgc', models.FloatField(verbose_name=b'shgc')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ZoneBorderAdmin',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('r_zone', models.ForeignKey(related_name='zoneborder_right_set', verbose_name=b'Right adjacent unit zone', to='BmsApp.UnitZone')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ZoneBorderAttributeAdmin',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('conductive_coef_B', models.FloatField(verbose_name=b'Conductive Border Coefficient B')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='unitzones',
            name='room',
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 13, 13, 36, 38, 902000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='roomsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 13, 13, 36, 38, 902000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborder',
            name='l_zone',
            field=models.ForeignKey(related_name='zoneborder_left_set', verbose_name=b'Left adjacent unit zone', to='BmsApp.UnitZone'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='zoneborder',
            name='r_zone',
            field=models.ForeignKey(related_name='zoneborder_right_set', verbose_name=b'Right adjacent unit zone', to='BmsApp.UnitZone'),
            preserve_default=True,
        ),
        migrations.DeleteModel(
            name='UnitZones',
        ),
    ]
