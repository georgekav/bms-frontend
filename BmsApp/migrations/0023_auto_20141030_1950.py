# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0022_auto_20141021_1815'),
    ]

    operations = [
        migrations.CreateModel(
            name='Middleware',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('IP', models.GenericIPAddressField(unique=True, verbose_name=b'Middleware IP')),
                ('REP_port', models.DecimalField(verbose_name=b"Middleware's REP port", max_digits=5, decimal_places=0)),
                ('PUB_port', models.DecimalField(verbose_name=b"Middleware's PUB port", max_digits=5, decimal_places=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='entity',
            name='unit',
        ),
        migrations.DeleteModel(
            name='Entity',
        ),
        migrations.AlterField(
            model_name='actuator',
            name='ref',
            field=models.IntegerField(verbose_name=b'Endpoint ID'),
        ),
        migrations.AlterField(
            model_name='sensor',
            name='measure',
            field=models.CharField(max_length=10, choices=[(b'ROOM', ((b'TEM', b'Air Temperature'), (b'LUM', b'Luminance'), (b'HUM', b'Humidity'), (b'PRE', b'PIR'))), (b'LOAD', ((b'P', b'Active Power'), (b'Q', b'Reactive Power'), (b'S', b'Apparent Power'), (b'PF', b'Power Factor')))]),
        ),
        migrations.AlterField(
            model_name='sensor',
            name='ref',
            field=models.IntegerField(verbose_name=b'Endpoint ID'),
        ),
        migrations.AlterField(
            model_name='setting',
            name='set_quantity',
            field=models.CharField(help_text=b'Quantity that the control loop should keep constant', max_length=10, verbose_name=b'Set Quantity', choices=[(b'TEM', b'Air Temperature'), (b'LUM', b'Luminance'), (b'HUM', b'Humidity'), (b'PRE', b'PIR')]),
        ),
    ]
