# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0011_auto_20141003_1923'),
    ]

    operations = [
        migrations.AlterField(
            model_name='backend',
            name='DB_IP',
            field=models.GenericIPAddressField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='backend',
            name='DB_REQ_port',
            field=models.DecimalField(null=True, max_digits=5, decimal_places=0, blank=True),
        ),
        migrations.AlterField(
            model_name='backend',
            name='RT_Server_IP',
            field=models.GenericIPAddressField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='backend',
            name='RT_Server_port',
            field=models.DecimalField(null=True, max_digits=5, decimal_places=0, blank=True),
        ),
        migrations.AlterField(
            model_name='backend',
            name='TCP_IP',
            field=models.GenericIPAddressField(unique=True, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='backend',
            name='TCP_PUB_port',
            field=models.DecimalField(null=True, max_digits=5, decimal_places=0, blank=True),
        ),
        migrations.AlterField(
            model_name='backend',
            name='plc_IP',
            field=models.GenericIPAddressField(unique=True, null=True, verbose_name=b'PLC IP', blank=True),
        ),
        migrations.AlterField(
            model_name='backend',
            name='plc_PUB_port',
            field=models.DecimalField(null=True, verbose_name=b'PLC PUB port', max_digits=5, decimal_places=0, blank=True),
        ),
        migrations.AlterField(
            model_name='backend',
            name='plc_REQ_port',
            field=models.DecimalField(null=True, verbose_name=b'PLC REQ port', max_digits=5, decimal_places=0, blank=True),
        ),
        migrations.AlterField(
            model_name='backend',
            name='zwave_IP',
            field=models.GenericIPAddressField(unique=True, null=True, verbose_name=b'ZWave IP', blank=True),
        ),
        migrations.AlterField(
            model_name='backend',
            name='zwave_PUB_port',
            field=models.DecimalField(null=True, verbose_name=b'ZWave PUB port', max_digits=5, decimal_places=0, blank=True),
        ),
        migrations.AlterField(
            model_name='backend',
            name='zwave_REQ_port',
            field=models.DecimalField(null=True, verbose_name=b'ZWave REQ port', max_digits=5, decimal_places=0, blank=True),
        ),
    ]
