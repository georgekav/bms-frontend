# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0096_auto_20160121_1101'),
    ]

    operations = [
        migrations.RenameField(
            model_name='storage',
            old_name='peak_charge',
            new_name='peak_power_charge',
        ),
        migrations.RenameField(
            model_name='storage',
            old_name='peak_discharge',
            new_name='peak_power_discharge',
        ),
        migrations.RemoveField(
            model_name='generator',
            name='generator_type',
        ),
        migrations.RemoveField(
            model_name='storage',
            name='storage_type',
        ),
        migrations.AddField(
            model_name='generator',
            name='type',
            field=models.CharField(default='', help_text=b'On-Site electrical generator type', max_length=10, verbose_name=b'Generator type', choices=[(b'Solar', b'Photovoltaic'), (b'Wind', b'Wind Turbine'), (b'Hydro', b'Micro-Hydro Turbine')]),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='storage',
            name='state_of_charge',
            field=models.FloatField(default=100, help_text=b'State of charge n% [0-100]', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)]),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='storage',
            name='type',
            field=models.CharField(default='', help_text=b'On-Site storage type', max_length=10, verbose_name=b'Storage type', choices=[(b'Mechanical', [(b'Flywheel', b'Flywheel'), (b'Air', b'Compressed Air Storage'), (b'Hydro', b'Pumped Hydro-Power')]), (b'Electrochemical', [(b'Secondary', b'Secondary Battery'), (b'Flow', b'Flow Battery')]), (b'Electrical', [(b'DLC', b'Double-layer Capacitor'), (b'SMES', b'Superconducting Magnetic Coil')]), (b'Chemical', [(b'Hydrogen', b'Fuel Cell')]), (b'Thermal', [(b'Thermal', b'Heat storage')])]),
            preserve_default=False,
        ),
    ]
