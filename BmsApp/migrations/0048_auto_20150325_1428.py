# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0047_profileuser_current_location'),
    ]

    operations = [
        migrations.CreateModel(
            name='LoadSetting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fromTime', models.TimeField(help_text=b'Not required', null=True, verbose_name=b'From', blank=True)),
                ('toTime', models.TimeField(help_text=b'Not required', null=True, verbose_name=b'To', blank=True)),
                ('mon', models.BooleanField(default=True, verbose_name=b'MON')),
                ('tue', models.BooleanField(default=True, verbose_name=b'TUE')),
                ('wed', models.BooleanField(default=True, verbose_name=b'WED')),
                ('thu', models.BooleanField(default=True, verbose_name=b'THU')),
                ('fri', models.BooleanField(default=True, verbose_name=b'FRI')),
                ('sat', models.BooleanField(default=False, verbose_name=b'SAT')),
                ('sun', models.BooleanField(default=False, verbose_name=b'SUN')),
                ('fromDay', models.DateField(help_text=b'Not required', null=True, verbose_name=b'Start', blank=True)),
                ('toDay', models.DateField(help_text=b'Not required', null=True, verbose_name=b'End', blank=True)),
                ('allow_action', models.CharField(help_text=b'Action allowed during the specified timeslot', max_length=10, verbose_name=b'Allowed action', choices=[(b'RELAY', b'On/Off'), (b'DIMMER', b'Dimmer'), (b'MOD', b'Switch mode')])),
                ('conditional', models.BooleanField(default=True, help_text=b'True if user intervention allowed')),
                ('comment', models.TextField(blank=True)),
                ('load', models.ForeignKey(to='BmsApp.Load')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='schedule',
            name='load',
        ),
        migrations.DeleteModel(
            name='Schedule',
        ),
    ]
