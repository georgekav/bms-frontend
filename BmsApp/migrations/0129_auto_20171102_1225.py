# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0128_auto_20171102_1033'),
    ]

    operations = [
        migrations.AddField(
            model_name='roominterface',
            name='interface_type',
            field=models.CharField(default=b'wall', max_length=30, choices=[(b'wall', b'Wall'), (b'window', b'Window'), (b'door', b'Door')]),
        ),
        migrations.AddField(
            model_name='roominterface',
            name='surface',
            field=models.FloatField(default=0, help_text=b'Interface surface'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='roominterface',
            name='thickness',
            field=models.FloatField(default=0, help_text=b'Interface thickness'),
            preserve_default=False,
        ),
    ]
