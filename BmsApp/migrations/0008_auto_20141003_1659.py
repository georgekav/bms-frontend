# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0007_auto_20140829_1807'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='roominfo',
            name='RoomInfo',
        ),
        migrations.AddField(
            model_name='roominfo',
            name='info',
            field=models.CharField(default=b' ', help_text=b'Enter the info for user of the room', max_length=40, verbose_name=b'The room info'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='parameter',
            name='name',
            field=models.CharField(default=None, help_text=b'Enter the unique parameter name', unique=True, max_length=10, verbose_name=b'Name'),
        ),
        migrations.AlterField(
            model_name='parameter',
            name='value',
            field=models.FloatField(default=None, help_text=b'Enter the parameter value', verbose_name=b'Value'),
        ),
    ]
