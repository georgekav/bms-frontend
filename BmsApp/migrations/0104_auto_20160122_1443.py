# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0103_generator_current_production'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='generator',
            name='integrated_storage',
        ),
        migrations.RemoveField(
            model_name='storage',
            name='self_dicharge',
        ),
        migrations.AddField(
            model_name='generator',
            name='integrated_storage_1',
            field=models.ForeignKey(related_name='+', blank=True, to='BmsApp.Storage', help_text=b'Storage capacity associated/integrated with the generation', null=True),
        ),
        migrations.AddField(
            model_name='generator',
            name='integrated_storage_2',
            field=models.ForeignKey(related_name='+', blank=True, to='BmsApp.Storage', help_text=b'Storage capacity associated/integrated with the generation', null=True),
        ),
        migrations.AddField(
            model_name='generator',
            name='integrated_storage_3',
            field=models.ForeignKey(related_name='+', blank=True, to='BmsApp.Storage', help_text=b'Storage capacity associated/integrated with the generation', null=True),
        ),
        migrations.AddField(
            model_name='storage',
            name='soc_efficiency',
            field=models.TextField(default='', help_text=b'Charge/Discharge efficiency as a function of stage of charge (SOC), JSON array of two variables', verbose_name=b'Efficiency vs stage of charge (SOC)'),
            preserve_default=False,
        ),
    ]
