# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0097_auto_20160121_1934'),
    ]

    operations = [
        migrations.AlterField(
            model_name='storage',
            name='currect_cycles',
            field=models.IntegerField(help_text=b'The currectly recorded charge/discharge cycles', blank=True, validators=[django.core.validators.MinValueValidator(0)]),
        ),
        migrations.AlterField(
            model_name='storage',
            name='life_cycles',
            field=models.IntegerField(help_text=b'The nominal ability of charge/discharge cycles', validators=[django.core.validators.MinValueValidator(0)]),
        ),
        migrations.AlterField(
            model_name='storage',
            name='state_of_charge',
            field=models.FloatField(help_text=b'State of charge n% [0-100]', blank=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)]),
        ),
    ]
