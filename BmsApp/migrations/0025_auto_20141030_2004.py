# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0024_auto_20141030_2000'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='actuator',
            name='middleware_type',
        ),
        migrations.RemoveField(
            model_name='sensor',
            name='middleware_type',
        ),
        migrations.AddField(
            model_name='middleware',
            name='middleware_type',
            field=models.CharField(default=5, max_length=15, choices=[(b'PLC', b'PLC Network'), (b'ZWAVE', b'ZWAVE Network'), (b'IP', b'IP Network'), (b'DB', b'Database')]),
            preserve_default=False,
        ),
    ]
