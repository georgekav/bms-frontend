# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0073_auto_20150415_1957'),
    ]

    operations = [
        migrations.AddField(
            model_name='zone',
            name='unit',
            field=models.ForeignKey(default=1, to='BmsApp.Unit', help_text=b'Required, the unit this zone belongs'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 15, 20, 0, 14, 341000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='roomsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 15, 20, 0, 14, 341000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
    ]
