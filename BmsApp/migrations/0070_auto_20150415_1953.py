# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0069_auto_20150415_1350'),
    ]

    operations = [
        migrations.CreateModel(
            name='Zone',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('volume', models.FloatField(help_text=b'Zone volume in m3')),
                ('room', models.OneToOneField(null=True, to='BmsApp.Room', blank=True, help_text=b"Optional 'real-life' room", verbose_name=b'Building Room')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='unitzone',
            name='room',
        ),
        migrations.AlterModelOptions(
            name='userpreferences',
            options={'verbose_name': 'User Preferences', 'verbose_name_plural': 'User Preferences'},
        ),
        migrations.RemoveField(
            model_name='zoneborder',
            name='l_zone',
        ),
        migrations.RemoveField(
            model_name='zoneborder',
            name='r_zone',
        ),
        migrations.DeleteModel(
            name='UnitZone',
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 15, 19, 53, 40, 10000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='roomsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 15, 19, 53, 40, 10000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
    ]
