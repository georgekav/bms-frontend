# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0060_auto_20150413_1237'),
    ]

    operations = [
        migrations.CreateModel(
            name='MaterialAttribute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Enter the unique material name', unique=True, max_length=15, verbose_name=b'Name')),
                ('cp', models.FloatField(verbose_name=b'Cp')),
                ('d', models.FloatField(verbose_name=b'D')),
                ('p', models.FloatField(verbose_name=b'\xcf\x81')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UnitZones',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('volume', models.FloatField(help_text=b'Zone colume')),
                ('room', models.OneToOneField(null=True, blank=True, to='BmsApp.Room', verbose_name=b'Room it corresponds')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WindowAttribute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uvalue', models.FloatField(verbose_name=b'U-value')),
                ('shgc', models.FloatField(verbose_name=b'shgc')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ZoneBorder',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('surface', models.FloatField(help_text=b'Border surface')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ZoneBorderAttribute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('thickness1', models.FloatField(verbose_name=b'Thickness Layer 1')),
                ('thickness2', models.FloatField(verbose_name=b'Thickness Layer 1')),
                ('thickness3', models.FloatField(verbose_name=b'Thickness Layer 1')),
                ('thickness4', models.FloatField(verbose_name=b'Thickness Layer 1')),
                ('conductive_coef_A', models.FloatField(verbose_name=b'Conductive Border Coefficient A')),
                ('conductive_coef_B', models.FloatField(verbose_name=b'Conductive Border Coefficient B')),
                ('mat1', models.ForeignKey(related_name='+', verbose_name=b'Material Layer 1', to='BmsApp.MaterialAttribute')),
                ('mat2', models.ForeignKey(related_name='+', verbose_name=b'Material Layer 2', to='BmsApp.MaterialAttribute')),
                ('mat3', models.ForeignKey(related_name='+', verbose_name=b'Material Layer 3', to='BmsApp.MaterialAttribute')),
                ('mat4', models.ForeignKey(related_name='+', verbose_name=b'Material Layer 4', to='BmsApp.MaterialAttribute')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='zoneborder',
            name='border_attr',
            field=models.ForeignKey(verbose_name=b'Border Type', to='BmsApp.ZoneBorderAttribute'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='zoneborder',
            name='l_zone',
            field=models.ForeignKey(related_name='zoneborder_left_set', verbose_name=b'Left adjacent unit zone', to='BmsApp.UnitZones'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='zoneborder',
            name='r_zone',
            field=models.ForeignKey(related_name='zoneborder_right_set', verbose_name=b'Right adjacent unit zone', to='BmsApp.UnitZones'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='loadsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 13, 13, 31, 8, 806000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='room',
            name='surface',
            field=models.FloatField(help_text=b'Floor surface', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='roomsetting',
            name='fromDay',
            field=models.DateField(default=datetime.datetime(2015, 4, 13, 13, 31, 8, 806000), help_text=b'Not required', null=True, verbose_name=b'Start', blank=True),
            preserve_default=True,
        ),
    ]
