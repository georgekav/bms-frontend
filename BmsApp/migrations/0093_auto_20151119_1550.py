# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import BmsApp.models


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0092_userprofile_profile_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='device',
            name='profile_image',
            field=models.ImageField(null=True, upload_to=BmsApp.models.get_device_pic_filename, blank=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='profile_image',
            field=models.ImageField(null=True, upload_to=BmsApp.models.get_profile_pic_filename, blank=True),
        ),
    ]
