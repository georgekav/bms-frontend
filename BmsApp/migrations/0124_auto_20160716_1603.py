# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0123_auto_20160715_2129'),
    ]

    operations = [
        migrations.AlterField(
            model_name='virtualentity',
            name='internal_interest',
            field=models.ManyToManyField(related_name='+', verbose_name=b'vEntities of interest', to='BmsApp.VirtualEntity', blank=True),
        ),
        migrations.AlterField(
            model_name='virtualentity',
            name='sensors_interest',
            field=models.ManyToManyField(related_name='+', verbose_name=b'Sensors of interest', to='BmsApp.Sensor', blank=True),
        ),
    ]
