# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0038_auto_20141216_1439'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actuator',
            name='network_middleware',
            field=models.ForeignKey(related_name='actuator_network_middleware_set', to='BmsApp.Middleware'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sensor',
            name='network_middleware',
            field=models.ForeignKey(related_name='sensor_network_middleware_set', to='BmsApp.Middleware'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sensor',
            name='store_middleware',
            field=models.ForeignKey(related_name='sensor_store_middleware_set', to='BmsApp.Middleware'),
            preserve_default=True,
        ),
    ]
