# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0136_auto_20180102_1611'),
    ]

    operations = [
        migrations.AlterField(
            model_name='room',
            name='nature',
            field=models.CharField(default=b'STANDARD', max_length=45, choices=[(b'STANDARD', b'Human space'), (b'ENERGY', b'Energy water storage'), (b'ENV', b'Environment')]),
        ),
    ]
