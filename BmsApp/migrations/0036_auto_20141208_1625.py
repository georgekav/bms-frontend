# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0035_configmsg'),
    ]

    operations = [
        migrations.AlterField(
            model_name='parameter',
            name='value',
            field=models.CharField(default=None, help_text=b'Enter the parameter value', max_length=10, verbose_name=b'Value'),
            preserve_default=True,
        ),
    ]
