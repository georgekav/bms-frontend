# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0093_auto_20151119_1550'),
    ]

    operations = [
        migrations.RenameField(
            model_name='device',
            old_name='profile_image',
            new_name='device_image',
        ),
    ]
