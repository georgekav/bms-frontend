# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0133_auto_20171117_1811'),
    ]

    operations = [
        migrations.AddField(
            model_name='storage',
            name='max_soc',
            field=models.FloatField(default=1, help_text=b'The maximum allowable SoC, between 0 and 1', verbose_name=b'Maximum SoC'),
        ),
        migrations.AddField(
            model_name='storage',
            name='min_soc',
            field=models.FloatField(default=0, help_text=b'The minimum allowable SoC, between 0 and 1', verbose_name=b'Minimum SoC'),
        ),
    ]
