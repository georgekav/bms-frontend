# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0085_auto_20150709_1735'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='sensor',
            unique_together=set([('ref', 'measure', 'network_middleware')]),
        ),
    ]
