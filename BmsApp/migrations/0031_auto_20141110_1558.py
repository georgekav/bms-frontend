# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0030_auto_20141031_1043'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='load',
            name='cardinality',
        ),
        migrations.AddField(
            model_name='load',
            name='rfid',
            field=models.IntegerField(help_text=b"The UID in HEX value without ':' , '-' , '0x'", null=True, verbose_name=b'UID', blank=True),
            preserve_default=True,
        ),
    ]
