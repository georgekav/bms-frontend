# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BmsApp', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='room',
            name='surface',
            field=models.FloatField(help_text=b'Floor surface, to compute consumption indicators', null=True, blank=True),
        ),
    ]
