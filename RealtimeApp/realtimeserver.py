import os
import sys

PROJECT_DIR = os.path.abspath(__file__ + "/../../../")
sys.path.append(os.path.join(PROJECT_DIR, 'config'))
from conf_general import PUB_MSG_TYPE, CURRENT_IP, LOGGER_LVL
from conf_rtserver import *
from conf_rfid import NO_TAG_STRING
from conf_virtualization import EVENT_DELAY_MSG_TYPE, COMMAND_DELAY_MSG_TYPE, ROUNDTRIP_DELAY_MSG_TYPE, \
    HEALTH_REPORT_MSG_TYPE

from sockjs.tornado import SockJSConnection, SockJSRouter
import zmq, json, logging, copy
from zmq.eventloop import ioloop as zmqioloop
from zmq.eventloop import zmqstream

# We install so that we dont have to carry to zmqioloop arround,
# the global ioloop is always called by torbado it self, so we include zmq in it.
zmqioloop.install()

from tornado.web import Application, RequestHandler, StaticFileHandler
from tornado.ioloop import IOLoop
from tornado.httpserver import HTTPServer
from tornado.options import define, options

# Prepare the ORM
import django
from django.core.exceptions import ValidationError
from django.core import serializers
from SmartBuilding.settings import MEDIA_URL

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "SmartBuilding.settings")
django.setup()
from BmsApp import models

import loggercolorizer
from bitstring import CreationError

define("config_file", type=str)
define("bind_address", type=str)
define("unix_socket", type=str)


class ZmqSockets(object):
    """The API to the ZMQ stacks, the argument in constructor will be the callback function in case there is a received message
    in a ZMQ socket. By giving the session broadcast as a callback, we can send to all clients a message that is not originated
    from websockets that essentialy the on_message of router would have been.
    """

    def __init__(self, callback):
        self.callback = callback

    def connect(self):
        self.context = zmq.Context()
        self.SUBsocket = self.context.socket(zmq.SUB)
        for mid in models.Middleware.objects.exclude(PUB_port=None):  # We take from all that they have a "PUB" port
            ip = mid.IP
            port = mid.PUB_port
            if ip == '127.0.0.1': ip = OPENBMS_IP  # 127.0.0.1 means that service is local to rtserver, aka the openBMS is the same IP with service (RT and openBMS by definition in same server)
            # The IPs of the middlewares are relative to the openbms server...
            if ip == '255.255.255.255': continue  # We did not resolve yet the correct IP, start the daemon
            self.SUBsocket.connect('tcp://{0}:{1}'.format(ip, port))
        self.SUBstream = zmqstream.ZMQStream(self.SUBsocket)
        self.SUBstream.on_recv(self.callback)

    def subscribe(self):
        self.SUBsocket.setsockopt(zmq.SUBSCRIBE, '')


class IndexHandler(RequestHandler):
    """Regular HTTP handler to serve the chatroom page"""

    def get(self):
        self.render('debug_page/debug.html')


class Websocket(SockJSConnection):
    """The connection that will handle all the subscribers. It will use the websockets protocol.
    If not availiable then it will try the rest:
    'xhr-streaming',
    'iframe-eventsource',
    'iframe-htmlfile',
    'xhr-polling',
    'iframe-xhr-polling',
    'jsonp-polling'

    !!!every connected client is an Websocket object. That is why you add and remove self from the clients.!!!

    pending_msgs_for_reply is a dictionary with keys the pub_id and values list of the messages pending for the specific publisher
    clients_per_pub_id keeps the record of the clients interested in the pub_id (which are the keys in the dict)
    """
    # This two are class variables, meaning they are attributes shared with all the class instances, aka self clients
    clients = set()
    pending_msgs_for_reply = dict()
    clients_per_pub_id = {}

    def on_open(self, info):
        """Called when a new client is connected in the router.
        clients and pending_msgs_for_reply are referenced by class and not instance"""
        Websocket.clients.add(self)

    def on_message(self, message):
        """Called when there is an incomming websocket message"""
        try:
            jsonmsg = json.loads(message)
            pub_id = jsonmsg[
                'PUB_ID']  # The reply as confirmation has the same pub_id, for the new load it keeps the pub_id of room
            if not jsonmsg['MSG_TYPE'] == WEBSOCKET_WEB_MSG_TYPE: raise TypeError
            msgclass = jsonmsg['PAYLOAD']['MSG_CLASS']
            if msgclass == 'REG_PUBLISHER':
                # ex {u'MSG_TYPE': u'WEB_MSG', u'PUB_ID': 1, u'PAYLOAD': {u'MSG_CLASS': u'REG_PUBLISHER', "REGISTER_ID":0}}
                reg_id = jsonmsg['PAYLOAD']['REGISTER_ID']
                res = self.register_publisher_interest(pub_id=reg_id)
                if res:
                    jsonmsg['PAYLOAD']['STATUS'] = True
                else:
                    jsonmsg['PAYLOAD']['STATUS'] = False
                self.broadcast([self], json.dumps(jsonmsg))
                self.retransmit_pending_msg(pub_id=reg_id)
            elif msgclass == 'REG_VMIDDLEWARE':
                reg_vmid_id = jsonmsg['PAYLOAD']['VMIDDLEWARE_ID']
                rooms = models.Room.objects.filter(sensor__virtualentity__virtual_middleware=reg_vmid_id).distinct()
                for r in rooms:
                    res = self.register_publisher_interest(pub_id=r.id)
                    jsonmsg['PUB_ID'] = r.id #overwrite the pub_id with the result
                    if res:
                        jsonmsg['PAYLOAD']['STATUS'] = True
                    else:
                        jsonmsg['PAYLOAD']['STATUS'] = False
                    self.broadcast([self], json.dumps(jsonmsg))
            elif msgclass == 'NEW_LD':
                # In case we have incoming new load information
                uid = jsonmsg['PAYLOAD']['LD_UID']
                loadname = jsonmsg['PAYLOAD']['LD_NAME']
                deviceid = jsonmsg['PAYLOAD']['DEV_ID']
                if not deviceid:
                    devicename = jsonmsg['PAYLOAD']['DEV_NAME']
                    typeid = jsonmsg['PAYLOAD']['TYPE_ID']
                    # Create the new device
                    newdev = models.Device(type_id=typeid, name=devicename)
                    newdev.full_clean()
                    newdev.save()
                    deviceid = newdev.id
                # Create the new load
                newload = models.Load(name=loadname, device_id=deviceid, uuid=uid)
                newload.full_clean()
                newload.save()

                publisherpendinglist = Websocket.pending_msgs_for_reply[pub_id]
                for pendingmsg in publisherpendinglist:
                    if pendingmsg['PAYLOAD']['LD_UID'] == uid:
                        currentactuator = models.Actuator.objects.get(id=pendingmsg['ID'],
                                                                      action__in=models.Actuator.esmartactions)
                        # We dont do any checks to remove it from other actuators or sensor, since supposed to be a new
                        currentactuator.load = newload
                        currentactuator.full_clean()
                        currentactuator.save()
                        # Manipulate the 'LOAD' of the pending packet(it was none), all the rest are the same
                        pendingmsg['PAYLOAD']['LOAD'] = dict(ID=newload.id, NAME=newload.name,
                                                             TYPE_ID=newload.device.type.id)
                        self.broadcast(self.clients, json.dumps(pendingmsg))

                        # Remove the pending, try to send the rest messages and return.
                        publisherpendinglist.remove(pendingmsg)
                        self.retransmit_pending_msg(pub_id)
                        return
            else:
                logger.info('Not supported {0} class {1}'.format(WEBSOCKET_WEB_MSG_TYPE, jsonmsg['MSG_CLASS']))

        except (ValueError, TypeError):
            if not message[:4] == 'ECHO':
                logger.info('Not supported websocks message {0}'.format(message))
                self.broadcast([self], 'Not supported websocks message {0}'.format(message))
            else:
                logger.info('ECHOED: {0}'.format(message[4:]))
                self.broadcast([self], message[5:])  # 5 because we dont want space
        except ValidationError as e:
            logger.error('Impossible to create a new model object, {0}'.format(repr(e)))
        except Exception as e:
            logger.exception('An unknown exception was raised ' + repr(e))

    def on_close(self):
        """Called when a client disconnects"""
        Websocket.clients.remove(self)
        for key in Websocket.clients_per_pub_id:
            Websocket.clients_per_pub_id[key].remove(self)

    def register_publisher_interest(self, pub_id):
        """Registers the pub_id that self client is interested, and as value it is the middlewares of incoming msg"""
        # First attempt to register the broadcast address
        try:
            if self not in Websocket.clients_per_pub_id[PUB_ID_BROADCAST]:
                Websocket.clients_per_pub_id[PUB_ID_BROADCAST].append(self)
        except KeyError:
            Websocket.clients_per_pub_id[PUB_ID_BROADCAST] = [self]
        if pub_id == PUB_ID_BROADCAST:  # Register to all publishers
            r_id_registered = ''
            for r in models.Room.objects.all():
                try:
                    if self not in Websocket.clients_per_pub_id[r.id]:
                        Websocket.clients_per_pub_id[r.id].append(self)
                    else:
                        logger.info('Already registered Publisher ID: {0}'.format(r.id))
                except KeyError:
                    Websocket.clients_per_pub_id[r.id] = [self]
                r_id_registered += ' {0}'.format(r.id)
            for pub_id in EXTRA_PUB_ID_LIST:
                try:
                    if self not in Websocket.clients_per_pub_id[pub_id]:
                        Websocket.clients_per_pub_id[pub_id].append(self)
                    else:
                        logger.info('Already registered Publisher ID: {0}'.format(pub_id))
                except KeyError:
                    Websocket.clients_per_pub_id[pub_id] = [self]
                r_id_registered += ' {0}'.format(pub_id)
            logger.info('Mass registered Publisher IDs: {0}'.format(r_id_registered))
            return True
        elif pub_id in EXTRA_PUB_ID_LIST or pub_id in models.Room.objects.all().values_list('id', flat=True):
            #Register extra_pub or room
            try:
                if self not in Websocket.clients_per_pub_id[pub_id]:
                    Websocket.clients_per_pub_id[pub_id].append(self)
                else:
                    logger.info('Already registered Publisher ID: {0}'.format(pub_id))
            except KeyError:
                Websocket.clients_per_pub_id[pub_id] = [self]
            logger.info('Registered Publisher ID: {0}'.format(pub_id))
            return True
        else:
            logger.info('Unknown Publisher ID: {0}'.format(pub_id))
            return False

    def retransmit_pending_msg(self, pub_id):
        """Checks if there are pending messages for this pub_id retransmits them, to the client that just declared itself.
        It sends only one message each time"""
        if Websocket.pending_msgs_for_reply.has_key(pub_id):
            if len(Websocket.pending_msgs_for_reply[pub_id]) != 0:
                jsonmsg = Websocket.pending_msgs_for_reply[pub_id][0]
                self.broadcast([self], json.dumps(jsonmsg))

    @staticmethod
    def midlist_of_room(room_id):
        """Returns the list of middleware id that have sensors in this this room_id"""
        room = models.Room.objects.get(id=room_id)
        actuators = room.actuator_set.all()
        sensors = room.sensor_set.all()
        mid_list = []
        for sensor in sensors:  # Room sensors only
            if sensor.network_middleware_id not in mid_list:
                mid_list.append(sensor.network_middleware_id)
        for actuator in actuators:
            if actuator.network_middleware_id not in mid_list:
                mid_list.append(actuator.network_middleware_id)
        return mid_list


class ZmqToWebsockets:
    """This is the class which keeps the active session of all the connected clients using the realtime protocols, aka websockets.
    It is initialized only once(since it is the session handler) and takes as an argument the router of the incomming clients
    """

    def __init__(self, websocket):
        self.websocket = websocket

    def broadcast(self, msg):
        """This function is activated when there is an incomming message in ZmqSockets, since the latter take this function
        as an argument. ZmqSockets thus, activates the broadcaster from sessions, which on its behalve will activate the broadcast
        in the websocket. msg is a list equivalent of what is returned by recv_multipart()
        WEBSOCKETS key convention: they should be in capitals, change from the ZMQ ones"""
        # msg is multipart so we take the first.
        jsonmsg = json.loads(msg[0])
        zmqmsgtype = jsonmsg['MSG_TYPE']
        jsonmsg['MSG_TYPE'] = WEBSOCKET_ZMQ_MSG_TYPE
        jsonmsg['PAYLOAD']['MSG_CLASS'] = zmqmsgtype
        try:
            if zmqmsgtype == PUB_MSG_TYPE[0]:  # STATUS update of actuator
                try:
                    act = models.Actuator.objects.get(ref=jsonmsg['REF'], network_middleware=jsonmsg['MID_ID'])
                    clients = self.getpublishersubscribers(act.room.id)
                    if len(clients) == 0:
                        return
                    jsonmsg['ID'] = act.id
                    jsonmsg['PUB_ID'] = act.room.id
                    jsonmsg['PAYLOAD']['SETTING'] = jsonmsg['PAYLOAD']['Setting']
                    jsonmsg['PAYLOAD']['TYPE'] = jsonmsg['PAYLOAD']['Type']
                    jsonmsg['PAYLOAD']['TIME'] = jsonmsg['PAYLOAD']['Time']
                    del jsonmsg['PAYLOAD']['Setting']
                    del jsonmsg['PAYLOAD']['Type']
                    del jsonmsg['PAYLOAD']['Time']
                    del jsonmsg['REF']
                    del jsonmsg['MID_ID']
                    self.websocket.broadcast(clients, json.dumps(jsonmsg))
                except models.Actuator.DoesNotExist:
                    logger.warn('Unable to resolve endpoint id: ' + str(jsonmsg['REF']))
                except models.Actuator.MultipleObjectsReturned:
                    logger.warn(
                        'Multiple actuators in the DB with the same endpoing ID {0} returned, consider revising the DB'.format(
                            jsonmsg['REF']))
            elif zmqmsgtype == PUB_MSG_TYPE[1]:  # MEASURE update
                # ideally they should not change based on backend plc/zwave etc....
                if jsonmsg['MID_ID'] in PLC_MID_ID:  # PLC
                    originalmsg = copy.deepcopy(jsonmsg)  # jsonmsg is the original and we dont change
                    sensorbyref = models.Sensor.objects.filter(ref=originalmsg['REF'],
                                                               network_middleware=originalmsg['MID_ID'])
                    del jsonmsg['REF']
                    del jsonmsg['MID_ID']
                    del jsonmsg['PAYLOAD']['Time']
                    for sen in sensorbyref:
                        if originalmsg['PAYLOAD'].has_key(sen.measure):
                            clients = self.getpublishersubscribers(sen.room.id)
                            if len(clients) == 0:
                                return
                            jsonmsg['PUB_ID'] = sen.room.id
                            jsonmsg['ID'] = sen.id
                            jsonmsg['PAYLOAD']['TIME'] = originalmsg['PAYLOAD']['Time']
                            jsonmsg['PAYLOAD']['QUANTITY'] = sen.measure
                            jsonmsg['PAYLOAD']['VALUE'] = originalmsg['PAYLOAD'][sen.measure]

                            self.websocket.broadcast(clients, json.dumps(jsonmsg))
                elif jsonmsg['MID_ID'] in ZWAVE_MID_ID:  # ZWave
                    sensorbyref = models.Sensor.objects.filter(ref=jsonmsg['REF'],
                                                               measure=jsonmsg['PAYLOAD']['Quantity'],
                                                               network_middleware=jsonmsg['MID_ID'])
                    del jsonmsg['REF']
                    del jsonmsg['MID_ID']
                    msgtime = jsonmsg['PAYLOAD']['Time']
                    msgquant = jsonmsg['PAYLOAD']['Quantity']
                    msgval = jsonmsg['PAYLOAD']['Value']
                    del jsonmsg['PAYLOAD']['Time']
                    del jsonmsg['PAYLOAD']['Quantity']
                    del jsonmsg['PAYLOAD']['Value']
                    for sen in sensorbyref:
                        clients = self.getpublishersubscribers(sen.room.id)
                        if len(clients) == 0:
                            return
                        jsonmsg['PUB_ID'] = sen.room.id
                        jsonmsg['ID'] = sen.id
                        jsonmsg['PAYLOAD']['TIME'] = msgtime
                        jsonmsg['PAYLOAD']['QUANTITY'] = msgquant
                        jsonmsg['PAYLOAD']['VALUE'] = msgval
                        self.websocket.broadcast(clients, json.dumps(jsonmsg))
            elif zmqmsgtype == PUB_MSG_TYPE[2]:  # LOAD_REC update, only in PLC type MID_ID
                try:
                    currentactuator = models.Actuator.objects.get(ref=jsonmsg['REF'],
                                                                  action__in=models.Actuator.esmartactions,
                                                                  network_middleware=jsonmsg[
                                                                      'MID_ID'])  # A single plug only exists, esmart convertion
                    clients = self.getpublishersubscribers(currentactuator.room.id)
                    jsonmsg['PUB_ID'] = currentactuator.room.id
                    jsonmsg['ID'] = currentactuator.id
                    del jsonmsg['REF']
                    del jsonmsg['MID_ID']
                except models.Actuator.DoesNotExist:
                    logger.warn('Unable to resolve endpoint id: ' + str(jsonmsg['REF']))
                    return
                except models.Actuator.MultipleObjectsReturned:
                    logger.error(
                        'Multiple actuators with the same REF returned. This should not have happened due to esmart'
                        'convention. Consider verifying the DB structure')
                    return
                # -------------------------A load was removed from the actuator--------------------
                # So we remove the potential old load and inform
                if not jsonmsg['PAYLOAD']['LD_UID']:  # Empty LD_ID means (now) removal
                    jsonmsg['PAYLOAD']['LOAD'] = None
                    jsonmsg['PAYLOAD']['LD_UID'] = None
                    currentactuator.load = None
                    currentactuator.full_clean()  # Here will auto remove the sensors as well
                    currentactuator.save()
                    self.unregister_waiting_reply(currentactuator.id)  # Remove the waiting of new load info
                    if len(clients) != 0:
                        self.websocket.broadcast(clients, json.dumps(jsonmsg))
                    return
                else:
                    # -------------------A load was added to the actuator------------------------------
                    if currentactuator.load:  # if the actuator has still a load(normally shouldnt since it is informed at removal)
                        uid_t = jsonmsg['PAYLOAD']['LD_UID']  # keep the uid temporary
                        # Inform all the clients that THIS actuator at jsonmsg['ID'], has 'changed' load(we issue a removal)
                        jsonmsg['PAYLOAD']['LOAD'] = None
                        jsonmsg['PAYLOAD']['LD_UID'] = None
                        self.unregister_waiting_reply(currentactuator.id)  # Remove the waiting of new load info
                        if len(clients) != 0:
                            self.websocket.broadcast(clients, json.dumps(jsonmsg))
                        # Restore the uid in the original message
                        jsonmsg['PAYLOAD']['LD_UID'] = uid_t
                    else:
                        pass  # jsonmsg['PAYLOAD']['LD_UID'] LD_UID are kept intact
                    try:
                        # -------------------A load was recognized in DB-------------------------------
                        raw_uuid = jsonmsg['PAYLOAD']['LD_UID']  # in compatible string format with : between the bytes
                        uuid = ':'.join(raw_uuid[i:i + 2] for i in range(0, len(raw_uuid), 2))
                        currentload = models.Load.objects.get(uuid=uuid)
                        for act in currentload.actuator_set.filter(action=currentactuator.action):
                            act.load = None
                            act.full_clean()
                            act.save()
                        currentactuator.load = currentload
                        currentactuator.full_clean()  # Sensor is also updated at this point
                        currentactuator.save()
                        # Give the new load info
                        jsonmsg['PAYLOAD']['LOAD'] = self.prepare_load_payload(currentload)
                    except models.Load.DoesNotExist:
                        # -------------------A load was NOT recognized in DB----------------------------
                        # We cannot proceed to use ORM, we request the user for load input and wait for it
                        jsonmsg['PAYLOAD']['LOAD'] = None
                        self.register_waiting_reply(pendingreply=jsonmsg)
                    # Know or not load ('LOAD' set/not set) we send the jsonmsg
                    if len(clients) != 0:
                        self.websocket.broadcast(clients, json.dumps(jsonmsg))
            elif zmqmsgtype == PUB_MSG_TYPE[3]:  # User fine LOCATION update
                # Just forward the message as it is formulated by the viewAPI, no change necessary, no DB hit
                # only follow the capital convention in websockets msg keys
                # PUB_ID is the room the first message is for the one leaving from, and the next message send by viewAPI
                # is the one of arrival
                del jsonmsg['MID_ID']
                room_object = models.Room.objects.get(pk=jsonmsg['PAYLOAD']['Room'])
                user_object = models.UserProfile.objects.get(pk=jsonmsg['PAYLOAD']['User'])
                room_info = self.prepare_room(room_object)
                user_info = self.prepare_userprofile(user_object)

                jsonmsg['PAYLOAD']['TIME'] = jsonmsg['PAYLOAD']['Time']
                jsonmsg['PAYLOAD']['ROOM'] = room_info
                jsonmsg['PAYLOAD']['USER'] = user_info
                jsonmsg['PAYLOAD']['DIRECTION'] = jsonmsg['PAYLOAD']['Direction']

                jsonmsg['PUB_ID'] = PUB_ID_BMS_USER_LOC

                del jsonmsg['PAYLOAD']['Time']
                del jsonmsg['PAYLOAD']['Room']
                del jsonmsg['PAYLOAD']['User']
                clients = self.getpublishersubscribers(jsonmsg['PUB_ID'])
                if len(clients) == 0:
                    return
                else:
                    self.websocket.broadcast(clients, json.dumps(jsonmsg))
            elif zmqmsgtype == PUB_MSG_TYPE[4]:  # USER_REC update
                del jsonmsg['MID_ID']
                jsonmsg['PUB_ID'] = PUB_ID_BMS_USER_REC
                raw_uuid = jsonmsg['PAYLOAD']['User_uuid']
                dyn_uuid = ':'.join(raw_uuid[i:i + 2] for i in range(0, len(raw_uuid), 2))
                jsonmsg['PAYLOAD']['TIME'] = jsonmsg['PAYLOAD']['Time']
                del jsonmsg['PAYLOAD']['User_uuid']
                del jsonmsg['PAYLOAD']['Time']
                if raw_uuid == NO_TAG_STRING:
                    jsonmsg['PAYLOAD']['USER'] = None
                else:
                    try:
                        user_prof = models.UserProfile.objects.get(uuid=dyn_uuid)
                        user_info = self.prepare_userprofile(user_prof)
                        jsonmsg['PAYLOAD']['USER'] = user_info
                    except models.UserProfile.DoesNotExist:
                        jsonmsg['PAYLOAD']['USER'] = {'name': 'Unknown', 'surname': 'Unknown',
                                                      'profile_image': MEDIA_URL + 'user_profile/user_NA.jpg'}
                clients = self.getpublishersubscribers(
                    jsonmsg['PUB_ID'])  # We send to the clients interested in the BMS data.
                if len(clients) == 0:
                    return
                else:
                    self.websocket.broadcast(clients, json.dumps(jsonmsg))
            elif zmqmsgtype == PUB_MSG_TYPE[5]:
                del jsonmsg['MID_ID']
                jsonmsg['PUB_ID'] = PUB_ID_VMID_PING
                jsonmsg['PAYLOAD']['COMMAND_DELAY'] = jsonmsg['PAYLOAD'][COMMAND_DELAY_MSG_TYPE]
                jsonmsg['PAYLOAD']['EVENT_DELAY'] = jsonmsg['PAYLOAD'][EVENT_DELAY_MSG_TYPE]
                jsonmsg['PAYLOAD']['ROUNDTRIP_DELAY'] = jsonmsg['PAYLOAD'][ROUNDTRIP_DELAY_MSG_TYPE]
                jsonmsg['PAYLOAD']['HEALTH_REPORT'] = jsonmsg['PAYLOAD'][HEALTH_REPORT_MSG_TYPE]

                del jsonmsg['PAYLOAD'][COMMAND_DELAY_MSG_TYPE]
                del jsonmsg['PAYLOAD'][EVENT_DELAY_MSG_TYPE]
                del jsonmsg['PAYLOAD'][ROUNDTRIP_DELAY_MSG_TYPE]
                del jsonmsg['PAYLOAD'][HEALTH_REPORT_MSG_TYPE]

                clients = self.getpublishersubscribers(jsonmsg['PUB_ID'])
                if len(clients) == 0:
                    return
                else:
                    self.websocket.broadcast(clients, json.dumps(jsonmsg))

        except (KeyError, IndexError) as e:
            logger.error('Possible corrupted zmq PUB packet: {0} triggered: {1}'.format(jsonmsg,repr(e)))
        except CreationError:
            logger.error('UID \'{0}\' is corrupted or wrong'.format(jsonmsg['PAYLOAD']['LD_UID']))
        except Exception as e:
            logger.exception('An unknown exception was raised ' + repr(e))

    @staticmethod
    def prepare_userprofile(userprofile_object):
        user_prof_dict = {'name': userprofile_object.name, 'surname': userprofile_object.surname,
                          'ref_unit_id': userprofile_object.unit.id, 'ref_unit_name': userprofile_object.unit.name,
                          'profile_image': userprofile_object.profile_image.url}
        if userprofile_object.current_location:
            user_prof_dict['cur_room_id'] = userprofile_object.current_location.id
            user_prof_dict['cur_room_name'] = userprofile_object.current_location.name
        return user_prof_dict

    @staticmethod
    def prepare_room(room_object):
        room_serialized = serializers.serialize('json', [room_object])
        room_info_dict = json.loads(room_serialized)[0]  # serializers give a list, we take the first
        pk = room_info_dict['pk']
        room_info_dict = room_info_dict['fields']
        room_info_dict['roomID'] = pk
        room_info_dict['type'] = models.RoomType.objects.get(id=room_info_dict['type']).name
        room_info_dict['unit'] = models.Unit.objects.get(id=room_info_dict['unit']).name
        del room_info_dict['comment']
        return room_info_dict

    @staticmethod
    def prepare_load_payload(load_object):
        load_dict = {'load_id': load_object.id, 'load_name': load_object.name,
                     'type_id': load_object.device.type.id, 'device_id': load_object.device.id,
                     'device_name': load_object.device.name, 'comment': load_object.comment,
                     'load_settings': serializers.serialize('json', load_object.loadsetting_set.all()),
                     'device_image': load_object.device.device_image.url}
        if load_object.room():
            load_dict['load_room_id'] = load_object.room().id
            load_dict['load_room_name'] = load_object.room().name
        return load_dict

    def getpublishersubscribers(self, pub_id):
        # Returns the clients of the publisher that are interested in the pub_id arrived by zmq
        if pub_id in self.websocket._connection.clients_per_pub_id:
            clients = self.websocket._connection.clients_per_pub_id[pub_id]
        else:
            clients = []
        return clients

    def getallclients(self):
        # Returns all the clients of the Websocket server
        return self.websocket._connection.clients

    def register_waiting_reply(self, pendingreply):
        # We dont modify the pendingmsg because it is send in the parent function.
        tempjson = copy.deepcopy(pendingreply)
        publisher = tempjson['PUB_ID']
        del tempjson['PUB_ID']
        logger.info('Registered reply pending in publisher {0} for {1}'.format(publisher, tempjson))
        if self.websocket._connection.pending_msgs_for_reply.has_key(publisher):
            self.websocket._connection.pending_msgs_for_reply[publisher].append(pendingreply)
        else:
            self.websocket._connection.pending_msgs_for_reply[publisher] = [pendingreply]

    def unregister_waiting_reply(self, actid):
        # We remove the pending message for the actuator. ex new load removed again
        for pub_id, pendingmsglist in self.websocket._connection.pending_msgs_for_reply.iteritems():
            for pendingmsg in pendingmsglist:
                if pendingmsg['ID'] == actid:
                    pendingmsglist.remove(pendingmsg)
                    logger.info('Unregistered reply pending in publisher {0} for {1}'.format(pub_id, pendingmsg))
                    return


if __name__ == "__main__":
    # Process the command line options
    options.parse_command_line()
    if not options.config_file and not options.bind_address and not options.unix_socket:
        logging.error("Please provide at least some options for tornado to bind to correct interface/socket/address")
        sys.exit()
    if options.config_file:
        options.parse_config_file(options.config_file)
    if options.bind_address and options.unix_socket:
        logging.error("Cannot define both bind_address and unix_socket arguments. Exiting")
        sys.exit()
    if options.unix_socket:  # Unix domain socket do not support nagle disabling because they do not use nagle anyway.
        disable_nagle_setting = False
    else:
        disable_nagle_setting = True

    tornado_settings = {
        'debug': False,
    }
    sockjsrouter_settings = {
        'disable_nagle': disable_nagle_setting
    }

    logger = loggercolorizer.getColorLogger(level=LOGGER_LVL, activate_email_logging=True,
                                            email_sender='RTserver@{0}'.format(CURRENT_IP))
    logging.getLogger('tornado.access').setLevel(level='ERROR')  # change tornado http request log lvl

    # important to know the message origin, we take it from the sql database and not define
    PLC_MID_ID = list(models.Middleware.objects.filter(middleware_type='PLC').values_list('id', flat=True))
    ZWAVE_MID_ID = list(models.Middleware.objects.filter(middleware_type='ZWAVE').values_list('id', flat=True))

    realtimerouter = SockJSRouter(connection=Websocket, prefix='', user_settings=sockjsrouter_settings)
    session = ZmqToWebsockets(websocket=realtimerouter)
    pubsub = ZmqSockets(callback=session.broadcast)

    pubsub.connect()
    pubsub.subscribe()

    static_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'debug_page')
    handlers = [
        (r"/debug", IndexHandler),
        (r"/static/(.*)", StaticFileHandler, {'path': static_dir})  # not needed when proxing with nginx
    ]

    tornado_app = Application(handlers + realtimerouter.urls, tornado_settings)
    server = HTTPServer(tornado_app)

    if options.unix_socket:
        # only for linux
        if sys.platform == "win32":
            logging.error("Cannot define unix_socket in windows. Exiting")
            sys.exit()
        from tornado.netutil import bind_unix_socket

        socket = bind_unix_socket(file=options.unix_socket, mode=0o777)
        server.add_socket(socket)
        logger.info("RTserver listening at {0}".format(options.unix_socket))
    else:
        server.listen(RT_SERVER_PORT, address=options.bind_address)
        logger.info("RTserver listening at {0}:{1}".format(options.bind_address, RT_SERVER_PORT))

        if options.bind_address != "127.0.0.1" and CURRENT_IP != RT_SERVER_IP:
            logging.warning(
                "Warning 'RT_SERVER_IP' defined in conf_general.py is not the same as the one this server is running")
            logging.warning("Current IP: {0}  VS  Defined: {1}".format(CURRENT_IP, RT_SERVER_IP))

    # Start the rtserver
    IOLoop.instance().start()
