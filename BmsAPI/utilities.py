__author__ = 'Georgios Lilis, Thomas Guibentif'


from math import sqrt
import logging
from datetime import datetime
from time import mktime
from scipy.interpolate import interp1d

logger = logging.getLogger(name='django.console')
# -----------------------------------------------------------------------------------------------------------------------
# Computation of some electrical quantities from the others
def PfromQ_pf(Q, PF):
    if PF < -1 or PF > 1:
        raise PhysicException("Power factor " + str(PF) + " out of bounds (should be in [-1,1])")
    if not PF:
        return 0
    else:
        return Q / sqrt(1. / (1 - PF * PF) - 1.)


def PfromS_Q(S, Q):
    if Q > S or S < 0 or Q < 0:
        if (Q - S) / S < 0.05:
            return 0
        else:
            raise PhysicException("Reactive Power Q = " + str(Q) + "larger than apparent S = " + str(S) + "?!")
    else:
        return sqrt(S * S - Q * Q)


def pfFromS_P(S, P):
    if P > S or S < 0 or P < 0:
        raise PhysicException("Active Power P = " + str(P) + "larger than apparent S = " + str(S) + "?!")
    if not S:
        return float("NaN")
    else:
        return P / S


def pfFromS_Q(S, Q):
    if Q > S or S < 0 or Q < 0:
        raise PhysicException("Reactive Power Q = " + str(Q) + " larger than apparent S = " + str(S) + "?!")
    if not S:
        logger.warning("PF is NaN: S = " + str(S))
        return float("NaN")
    else:
        return sqrt(1. - Q * Q / (S * S))


def pfFromP_Q(P, Q):
    if P < 0 or Q < 0:
        raise PhysicException("Active or Reactive Power negative: P=" + str(P) + " Q = " + str(Q))
    if not P:
        logger.warning("PF is NaN: P = " + str(P))
        return float("NaN")
    else:
        return P / sqrt(Q * Q + P * P)


def QfromP_pf(P, PF):
    if PF < -1 or PF > 1 or P < 0:
        raise PhysicException("Active power P=" + str(P) + " or power factor pf=" + str(PF) + "out of bounds")
    if not PF:
        logger.warning("Q is NaN: pf = " + str(PF))
        return float("NaN")
    else:
        return P * sqrt(1. / (PF * PF) - 1)


def QfromS_P(S, P):
    if S < P or S < 0 or P < 0:
        raise PhysicException("Active Power P = " + str(P) + "larger than apparent S = " + str(S) + "?!")
    else:
        return sqrt(S * S - P * P)


def QfromS_PF(S, PF):
    if PF < -1 or PF > 1:
        raise PhysicException("Power factor " + str(PF) + " out of bounds (should be in [-1,1])")
    else:
        return S * sqrt(1 - PF * PF)


def SfromP_pf(P, PF):
    if PF < -1 or PF > 1:
        raise PhysicException("Power factor " + str(PF) + " out of bounds (should be in [-1,1])")
    if not PF:
        logger.warning("S is NaN: pf = " + str(PF))
        return float("NaN")
    else:
        return P / PF


def SfromQ_pf(Q, PF):
    if PF < -1 or PF > 1:
        raise PhysicException("Power factor " + str(PF) + " out of bounds (should be in [-1,1])")
    if Q < 0:
        raise PhysicException("Negative Reactive power Q= " + str(Q))
    if PF > 1 - 1e-6 or PF < -1 + 1e-6:
        logger.warning("S is NaN: pf = " + str(PF))
        return float("NaN")
    else:
        return Q / sqrt(1. - PF * PF)


def SfromP_Q(P, Q):
    if Q < 0 or P < 0:
        raise PhysicException("Active or reactive power negative: P=" + str(P) + " Q=" + str(Q))
    return sqrt(P * P + Q * Q)


# -----------------------------------------------------------------------------------------------------------------------
def interpolateData(datas, timeVector, kind, fillvalue):
    """
    This will return the mean of the data between each two successive time points in timeVector
    It accepts as input multiple timeseries to interpolate
    """
    toret = []
    for data in datas:
        #We assume that timevector is SORTED
        #we extrapolate to the endtime of the timeVector taking into assumption that backend register only in change
        #therefore no change register, thus the value remained the same.
        firsttime = data[0][0]
        if firsttime > timeVector[0]:
            logger.warning('Impossible to interpolate correctly. The availiable data is AFTER the first requested value. '
                            'Setting the start time of data at the requested point at the value of {0}'.format(fillvalue))
            data.insert(0,(timeVector[0], fillvalue))
        lastvalue = data[-1][1]
        data.append((timeVector[-1], lastvalue))
        times, values = zip(*data)

        interpolfunc = interp1d(x=times, y=values, kind=kind)
        new_y = interpolfunc(timeVector).tolist()
        toret.append(zip(timeVector, new_y))
    return toret

def integrateData(datas, timeVector):
    """
    This will return the integral of the data between each two successive time points in timeVector (not the cumulative sum).
    At the timeVector points are the integration from the exact previous 'datas' time  to that time. For the last one
    which is sooner than the end of timevector we cannot integrate until the end.
    """
    toret = resampleData(datas, timeVector, True)
    return toret

def interpolateMeanData(datas, timeVector):
    """
    This will run the resampleData without interpollation. Thus it will take the mean value of the point and attempt to
    resample so that we have a point a precise interval unlike the backendapi
    """
    toret = resampleData(datas, timeVector, False)
    return toret

def resampleData(datas, timeVector, integrate):
    """Resample the data on points timeVector while doing a mean. Can resample several timeseries at once
    Input: raw data [[[<time>,<value>]...]...], points timeVector (unix time) where to get the data,
    integrate if we need integration between samples
    Output: resampled data in same standard format: [[[<time>,<value>]...]...]
    TimeVector's first element must be after first of data,
    last element before last of data (in time), timeVector and data must be sorted in chronological order
    The mean is done so that when integrating (sum(value*dt)), the result will be the same than integrating the raw data
    This function can avoid doing the mean, thus returning the integral of the data between timeVector[i] and timeVector[i+1].
    Note that in this case the last "datapoint" will be None since we have no upper bound.
    """
    retValFinal = []
    if not timeVector:
        raise ArgumentException("No time vector provided")
    if not datas:
        raise ArgumentException("No data provided")

    if not isinstance(datas[0], list):
        datas = [datas]
    for data in datas:
        if not data:
            logger.warning("No data provided")
            return None
        retVal = []
        dataLen = len(data)
        timeLen = len(timeVector)
        if timeLen > 1:  # Verify that the timeVector is long enough
            i = 0
            j = 0
            lowBound = timeVector[0]  # -(timeVector[1]-timeVector[0])/2.     #Start a bit before the first point required
            if data[0][0] > lowBound:
                logger.warning("Provided data starting from " + str(data[0][0]) + " for evaluation of time " + str(lowBound))
                while j < timeLen - 1 and lowBound < data[0][0]:
                    retVal.append([timeVector[j], float("NaN")])
                    j += 1
                    lowBound = timeVector[j]  # -(timeVector[j+1]-timeVector[j])/2.

            while i < dataLen - 1 and data[i][0] <= lowBound:
                i += 1

            upBound = lowBound  # This is only to avoid having a special treatment for j=0...
            for j in range(j, timeLen):
                lowBound = upBound  # ...When shifting the lowBound
                if j == timeLen - 1:
                    # Specific upper bound for the last time value
                    upBound = timeVector[timeLen - 1] + (timeVector[timeLen - 1] - timeVector[timeLen - 2])
                else:
                    upBound = timeVector[j + 1]  # timeVector[j]+(timeVector[j+1]-timeVector[j])/2.
                if i < dataLen:
                    if data[i][0] < upBound:
                        timeSample = (data[i][0] - lowBound)
                    else:
                        timeSample = (upBound - lowBound)
                    # Take into account the last point before lowBound to fill the gap before the first meaningful point after lowBound
                    mean = data[i - 1][1] * timeSample
                    while i < dataLen and data[i][0] < upBound:
                    # Sum all values stricly between the current time requested and the next one
                        if i == dataLen - 1 or data[i + 1][0] > upBound:
                            timeLapse = upBound - data[i][0]
                        else:
                            timeLapse = data[i + 1][0] - data[i][0]
                        # Sum the corresponding times: assumed to be the time after the data was taken and until the next point was measured
                        timeSample += timeLapse
                        mean += data[i][1] * timeLapse
                        i += 1
                    if not integrate:
                        if timeSample:
                            mean = mean / timeSample  # Divide to get mean over time, if we are not integrating
                        else:
                            raise DataException("resampleData got corrupted data")
                else:
                    mean = retVal[j - 1][1]  # If no points just take same value as last point
                    # We guarantee before that there is data for the first point, otherwise throws exception
                retVal.append([timeVector[j], mean])

                if i == dataLen - 1:
                    logger.debug("Data " + str(data[dataLen - 1]) + " may not be sufficient to compute requested times "+ str(timeVector[j]))
        else:
            i = 0
            lowBound = timeVector[0]  # Return last measured point before requested time
            while i < dataLen - 1 and data[i][0] < lowBound:
                i += 1
            retVal = [[timeVector[0], data[i][1]]]

        if integrate:
            # Integration must be between two bounds, so the last point has no meaning, we delete it
            retVal[-1][1] = float("NaN")

        retValFinal.append(retVal)

    return retValFinal


def cumSum(data):
    """Cumulative sum of the data values ([[<time>,<value>]...]), while setting the first element to None
    (cumulative until the next time point). The last value is the total.
    """
    if data:
        retVal = [data[0][0],data[0][1]]
        dataLen = len(data)
        for i in range(1, dataLen-1):
            retVal = [data[i][0], retVal[1] + data[i][1]]

    else:
        raise DataException("No data provided for sum")
    return retVal

# -----------------------------------------------------------------------------------------------------------------------
# Time conversion utility functions


def unixToString(time):
    return datetime.fromtimestamp(time).strftime('%Y-%m-%d %H:%M:%S')


def strToUnix(time):
    time_unix = mktime(datetime.strptime(time, "%Y%m%d%H%M%S").timetuple())
    return time_unix


def local_time():
    return mktime(datetime.now().timetuple())


# substract DST offset.

def generateTimeVector(initTime, endTime, timeRes):
    """Generates a time vector with unix times separated by timeRes seconds between initTime and endTime (in "YYYYMMDDHHMMSS" format)
    """
    initTime = strToUnix(initTime)
    endTime = strToUnix(endTime)
    timeRes = int(timeRes)

    if timeRes <= 0:
        raise Exception("Asked for non positive resolution")

    if initTime > endTime:
        raise Exception("Starting time " + str(initTime) + " is after end time " + str(endTime))

    if timeRes > endTime - initTime:
        raise Exception("Time resolution is greater that distance between initTime and endTime")
    t = initTime
    timeVector = []
    while t <= endTime:
        timeVector.append(t)
        t += timeRes

    return timeVector


# -----------------------------------------------------------------------------------------------------------------------
# Formating functions for standard formats

def returnFormat(data):
    """Refactors data of one timeseries from backend format:
    [[[<unixTime>,<value>]...]]
    to a standard format:
    [[<unixTime>,<value>]...]
    """
    toret = [[item[0], round(item[1], 3) if item[1] == item[1] else None] for item in data[0]]
    if len(toret) == 1:
        return toret[0]
    else:
        return toret

def unicodetodatetime(parameters):
    """takes the incomming json from POST or PUT methods in views and converts the unicode times in datetime.
    parameters passed by reference so no return"""
    if 'fromDay' in parameters:
        parameters['fromDay'] = datetime.strptime(parameters['fromDay'] , "%Y-%m-%d").date()
    if 'toDay' in parameters:
        parameters['toDay'] = datetime.strptime(parameters['toDay'] , "%Y-%m-%d").date()
    if 'fromTime' in parameters:
        parameters['fromTime'] = datetime.strptime(parameters['fromTime'] , "%H:%M:%S").time()
    if 'toTime' in parameters:
        parameters['toTime'] = datetime.strptime(parameters['toTime'] , "%H:%M:%S").time()

#-----------------------------------------------------------------------------------------------------------------------
#Exception types

class DataException(Exception):
    pass


class ArgumentException(Exception):
    pass


class ActionException(Exception):
    pass


class PhysicException(Exception):
    pass