__author__ = 'Georgios Lilis, Thomas Guibentif'

#Command Line Tools HTTP requests
from django.conf.urls import patterns, url
import viewsAPI
from BmsApp.models import Sensor

RoomChoices = ""
for item in Sensor.room_measures+Sensor.batterylvl_measure:
    RoomChoices += '(' + str(item) + ')|'

LoadChoices = ""
for item in Sensor.LOAD_CHOICES:
    LoadChoices += '(' + str(item[0]) + ')|'

timeInput = '/from(?P<initTime>\d{14})/to(?P<endTime>\d{14})(/res(?P<timeRes>\d+))?'

urlpatterns = patterns('',
                       #Only for partial reverse url
                       url(r'^$', viewsAPI.viewRootAPI, name='viewRootAPI'),
                       #Docs of API
                       url(r'^docs', viewsAPI.viewGetDocs, name='docsAPI'),

                       #Get the middleware details
                       url(r'^middleware/(?P<midID>\d+)$', viewsAPI.viewMiddleware),
                       #Get the all objects referencing this middleware, processing of objects to be done by recepient
                       url(r'^middleware_objects/(?P<midID>\d+)$', viewsAPI.viewMiddlewareObjects),
                       #Get all the refs connected on the particular store or network middleware
                       url(r'^middleware_refs/(?P<midID>\d+)$', viewsAPI.viewMiddlewareRefs),
                       #Get all the ids connected on the particular store or network middleware
                       url(r'^middleware_ids/(?P<midID>\d+)$', viewsAPI.viewMiddlewareIds),
                       #Get all the network middlewares that send to the given by store middleware
                       url(r'^middleware_storetonet/(?P<midID>\d+)$', viewsAPI.viewMiddlewareStoretoNet),
                       #Report the middleware IP back to openBMS
                       url(r'^middleware_reportip/(?P<midID>\d+)$', viewsAPI.viewMiddlewareReportIP),

                       #To view the units in the database
                       url(r'^units$', viewsAPI.viewUnits, name='unitsAPI'),

                       #------------------------------------------------------------------------------------------------------------------
                       #Urls to explore the database
                       url(r'^unit/(?P<unitID>\d+)$', viewsAPI.viewSingleUnit, name='singleUnitAPI'),
                       url(r'^room/(?P<roomID>\d+)$', viewsAPI.viewSingleRoom, name='singleRoomAPI'),
                       url(r'^sensor/(?P<sensID>\d+)$', viewsAPI.viewSingleSensor, name='singleSensorAPI'),
                       url(r'^actuator/(?P<actID>\d+)$', viewsAPI.viewSingleActuator, name='singleActuatorAPI'),
                       url(r'^load/(?P<loadID>\d+)$', viewsAPI.viewSingleLoad.as_view(), name='singleLoadAPI'),
                       url(r'^generator/(?P<genID>\d+)$', viewsAPI.viewSingleGenerator.as_view(), name='singleGeneratorAPI'),
                       url(r'^storage/(?P<storID>\d+)$', viewsAPI.viewSingleStorage.as_view(), name='singleStorageAPI'),
                       url(r'^phev/(?P<evID>\d+)$', viewsAPI.viewSingleEV.as_view(), name='singleEVAPI'),

                       url(r'^unit_middlewares/(?P<unitID>\d+)$', viewsAPI.viewUnit_middlewares, name='unitMidsAPI'),
                       url(r'^unit_rooms/(?P<unitID>\d+)$', viewsAPI.viewUnit_rooms, name='unitRoomsAPI'),
                       url(r'^unit_room_interfaces/(?P<unitID>\d+)$', viewsAPI.viewInterfaces, name='unitRoomInterfacesAPI'),
                       url(r'^unit_loads/(?P<unitID>\d+)$', viewsAPI.viewUnit_loads, name='unitLoadsAPI'),
                       url(r'^unit_load_types/(?P<unitID>\d+)$', viewsAPI.viewUnit_load_types, name='unitLoadTypesAPI'),
                       url(r'^unit_generators/(?P<unitID>\d+)$', viewsAPI.viewUnit_generators, name='unitGensAPI'),
                       url(r'^unit_storage/(?P<unitID>\d+)$', viewsAPI.viewUnit_storage, name='unitStorsAPI'),
                       url(r'^unit_phevs/(?P<unitID>\d+)$', viewsAPI.viewUnit_phevs, name='unitPHEVsAPI'),

                       url(r'^phev/(?P<evID>\d+)$', viewsAPI.viewSingleEV.as_view(), name='singleEVAPI'),
                       url(r'^room_loads/(?P<roomID>\d+)$', viewsAPI.viewRoom_loads, name='roomLoadsAPI'),
                       url(r'^room_sensors/(?P<roomID>\d+)$', viewsAPI.viewRoom_sensors, name='roomSensAPI'),
                       url(r'^room_actuators/(?P<roomID>\d+)$', viewsAPI.viewRoom_actuators, name='roomActAPI'),
                       url(r'^room_settings/(?P<roomID>\d+)(/(?P<setID>\d+))?$', viewsAPI.viewRoomSettings.as_view(), name='roomSetAPI'),
                       url(r'^room_remark/(?P<roomID>\d+)$', viewsAPI.viewRoomRemark.as_view(), name='roomRemAPI'),
                       url(r'^room_constraints/(?P<roomID>\d+)$', viewsAPI.viewRoomConstraints, name='roomConstraintsAPI'),
                       url(r'^room_interfaces/(?P<roomID>\d+)$', viewsAPI.viewRoomInterfaces, name='roomInterfacesAPI'),

                       url(r'^load_settings/(?P<loadID>\d+)(/(?P<schID>\d+))?$', viewsAPI.viewLoadSettings.as_view(), name='loadSetAPI'),
                       url(r'^load_sensors/(?P<loadID>\d+)$', viewsAPI.viewLoad_sensors, name='loadSensAPI'),
                       url(r'^load_actuators/(?P<loadID>\d+)$', viewsAPI.viewLoad_actuators, name='loadActAPI'),
                       url(r'^type_loads/(?P<typeID>\d+)$', viewsAPI.viewType_loads, name='loadTypeAPI'),
                       url(r'^schedule_constraints/(?P<scheduleID>\d+)$', viewsAPI.viewSingleSchedule_constraint, name='scheduleConstraintAPI'),

                       url(r'^generation_sensors/(?P<genID>\d+)$', viewsAPI.viewGeneration_sensors, name='genSensAPI'),
                       url(r'^storage_sensors/(?P<storID>\d+)$', viewsAPI.viewStorage_sensors, name='storSensAPI'),

                       url(r'^v_middleware/(?P<vMidID>\d+)$', viewsAPI.viewsinglevMiddleware.as_view(), name='singlevMiddlewareAPI'),
                       url(r'^v_middleware_entities/(?P<vMidID>\d+)$', viewsAPI.viewvMiddlewareEntities.as_view(), name='viewvMiddlewareEntitiesAPI'),
                       url(r'^v_entity/(?P<vEntID>\d+)$', viewsAPI.viewSinglevEntity.as_view(), name='singlevEntityAPI'),


                       #------------------------------------------------------------------------------------------------------------------
                       #In the following urls that retrieve sensor data:
                       #timeseries: an array of data for the requested times
                       #last: the latest value registered by the backends
                       #poll: force a sampling by the backend. Should generally be avoided.
                       #------------------------------------------------------------------------------------------------------------------

                       #Urls to get data directly from Sensor objects (aka: quantities)---------------------------------------------------
                       url(r'^timeseries/quantity/(?P<sensID>\d+)' + timeInput, viewsAPI.viewTimeseriesQuantity,name='timeseriesQuantity'),
                       url(r'^last/quantity/(?P<sensID>\d+)$', viewsAPI.viewLastQuantity, name='lastQuantity'),
                       url(r'^poll/quantity/(?P<sensID>\d+)$', viewsAPI.viewPollQuantity, name='pollQuantity'),
                       #Without time for the template use to create the fragment for make graph fetch url
                       url(r'^timeseries/quantity/(?P<sensID>\d+)/$', viewsAPI.viewTimeseriesQuantity, name='timeseriesQuantity'),


                       #Urls to get data directly from Sensor(load or room type) using the backend ref------------------------------------
                       url(r'^timeseries/sensor/(?P<sensREF>\d+)' + timeInput, viewsAPI.viewTimeseriesSensor,name='timeseriesSensor'),
                       url(r'^last/sensor/(?P<sensREF>\d+)$', viewsAPI.viewLastSensor, name='lastSensor'),


                       #Urls to get data using the primary keys of the db for the corresponding table-------------------------------------
                       url(r'^timeseries/load/(?P<loadID>\d+)' + timeInput, viewsAPI.viewTimeseriesLoad, name='timeseriesLoad'),
                       url(r'^last/load/(?P<loadID>\d+)$', viewsAPI.viewLastLoad, name='lastLoad'),
                       url(r'^last/generator/(?P<genID>\d+)$', viewsAPI.viewLastGenerator, name='lastGenerator'),
                       url(r'^last/storage/(?P<storID>\d+)$', viewsAPI.viewLastStorage, name='lastStorage'),

                       url(r'^last/generators/unit/(?P<unitID>\d+)$', viewsAPI.viewLastGeneratorsUnit,name='lastGeneratorsUnit'),
                       url(r'^last/storage/unit/(?P<unitID>\d+)$', viewsAPI.viewLastStorageUnit, name='lastStorageUnit'),

                       url(r'^timeseries/room/(?P<roomID>\d+)' + timeInput, viewsAPI.viewTimeseriesRoom, name='timeseriesRoom'),
                       url(r'^last/room/(?P<roomID>\d+)$', viewsAPI.viewLastRoom, name='lastRoom'),
                       url(r'^last/loads/room/(?P<roomID>\d+)$', viewsAPI.viewLastLoadsRoom, name='lastLoadsRoom'),

                       url(r'^poll/room/(?P<roomID>\d+)$', viewsAPI.viewPollRoom, name='pollRoom'),


                       #--------------------------------------------------------------------------------------------------------------------
                       #Urls to get a given quantity on a load or room. No need to know the pk of the sensor connected to the room/load
                       url(r'^timeseries/' + '(?P<quantity>' + LoadChoices + ')' + '/load/(?P<loadID>\d+)' + timeInput,viewsAPI.viewTimeseriesLoadQuantity, name='timeseriesLoadQuantity'),
                       url(r'^timeseries/' + '(?P<quantity>' + RoomChoices + ')' + '/room/(?P<roomID>\d+)' + timeInput,viewsAPI.viewTimeseriesRoomQuantity, name='timeseriesRoomQuantity'),

                       url(r'^last/' + '(?P<quantity>' + LoadChoices + ')' + '/load/(?P<loadID>\d+)$',viewsAPI.viewLastLoadQuantity, name='lastLoadQuantity'),
                       url(r'^last/' + '(?P<quantity>' + RoomChoices + ')' + '/room/(?P<roomID>\d+)$',viewsAPI.viewLastRoomQuantity, name='lastRoomQuantity'),

                       #--------------------------------------------------------------------------------------------------------------------
                       #Urls to get aggregated consumption data for room/unit
                       url(r'^timeseries/aggregated/room/(?P<roomID>\d+)' + timeInput, viewsAPI.viewTimeseriesRoomAggregated,name='timeseriesRoomAggregated'),
                       url(r'^timeseries/aggregated/unit/(?P<unitID>\d+)' + timeInput, viewsAPI.viewTimeseriesUnitAggregated,name='timeseriesUnitAggregated'),
                       url(r'^timeseries/' + '(?P<quantity>' + LoadChoices + ')' + '/aggregated/room/(?P<roomID>\d+)' + timeInput, viewsAPI.viewTimeseriesRoomAggregated,name='timeseriesRoomAggregatedQuantity'),
                       url(r'^timeseries/' + '(?P<quantity>' + LoadChoices + ')' + '/aggregated/unit/(?P<unitID>\d+)' + timeInput, viewsAPI.viewTimeseriesUnitAggregated,name='timeseriesUnitAggregatedQuantity'),

                       url(r'^last/aggregated/room/(?P<roomID>\d+)$', viewsAPI.viewLastRoomAggregated,name='lastRoomAggregated'),
                       url(r'^last/aggregated/unit/(?P<unitID>\d+)$', viewsAPI.viewLastUnitAggregated,name='lastUnitAggregated'),
                       url(r'^last/' + '(?P<quantity>' + LoadChoices + ')' + '/aggregated/room/(?P<roomID>\d+)$', viewsAPI.viewLastRoomAggregated,name='lastRoomAggregatedQuantity'),
                       url(r'^last/' + '(?P<quantity>' + LoadChoices + ')' + '/aggregated/unit/(?P<unitID>\d+)$', viewsAPI.viewLastUnitAggregated,name='lastUnitAggregatedQuantity'),

                       #By load type
                       url(r'^timeseries/aggregated/room/(?P<roomID>\d+)/load_type/(?P<typeID>\d+)' + timeInput, viewsAPI.viewTimeseriesRoomTypeAggregated, name='timeseriesRoomTypeAggregated'),
                       url(r'^timeseries/aggregated/unit/(?P<unitID>\d+)/load_type/(?P<typeID>\d+)' + timeInput, viewsAPI.viewTimeseriesUnitTypeAggregated, name='timeseriesUnitTypeAggregated'),
                       url(r'^timeseries/' + '(?P<quantity>' + LoadChoices + ')' + '/aggregated/room/(?P<roomID>\d+)/load_type/(?P<typeID>\d+)' + timeInput, viewsAPI.viewTimeseriesRoomTypeAggregated, name='timeseriesRoomTypeAggregatedQuantity'),
                       url(r'^timeseries/' + '(?P<quantity>' + LoadChoices + ')' + '/aggregated/unit/(?P<unitID>\d+)/load_type/(?P<typeID>\d+)' + timeInput, viewsAPI.viewTimeseriesUnitTypeAggregated, name='timeseriesUnitTypeAggregatedQuantity'),

                       url(r'^last/aggregated/unit/(?P<unitID>\d+)/load_type/(?P<typeID>\d+)$', viewsAPI.viewLastUnitTypeAggregated, name='lastUnitTypeAggregated'),
                       url(r'^last/aggregated/room/(?P<roomID>\d+)/load_type/(?P<typeID>\d+)$', viewsAPI.viewLastRoomTypeAggregated, name='lastRoomTypeAggregated'),
                       url(r'^last/' + '(?P<quantity>' + LoadChoices + ')' + '/aggregated/unit/(?P<unitID>\d+)/load_type/(?P<typeID>\d+)$', viewsAPI.viewLastUnitTypeAggregated, name='lastUnitTypeAggregatedQuantity'),
                       url(r'^last/' + '(?P<quantity>' + LoadChoices + ')' + '/aggregated/room/(?P<roomID>\d+)/load_type/(?P<typeID>\d+)$', viewsAPI.viewLastRoomTypeAggregated, name='lastRoomTypeAggregatedQuantity'),

                       #For the templates use only (without the time input)
                       url(r'^timeseries/' + '(?P<quantity>' + LoadChoices + ')' + '/aggregated/room/(?P<roomID>\d+)/$', viewsAPI.viewTimeseriesRoomAggregated,name='timeseriesRoomAggregatedQuantity'),


                       #--------------------------------------------------------------------------------------------------------------------
                       #Urls to get energy consumption data, that is: integration of the powers of one or more
                       #loads according to their room, unit, type.
                       #Note that the first value of the timeseries will always be null (None),
                       #since we have no information about consumption before that point
                       #(we keep the point so that it is easy to see where the integration began)
                       #Timeseries when we need the energy profile of the give time period in the given resolution.
                       url(r'^timeseries/energy/load/(?P<loadID>\d+)' + timeInput, viewsAPI.viewTimeseriesLoadEnergy,name='timeseriesLoadEnergy'),
                       url(r'^timeseries/energy/room/(?P<roomID>\d+)' + timeInput, viewsAPI.viewTimeseriesRoomEnergy,name='timeseriesRoomEnergy'),
                       url(r'^timeseries/energy/unit/(?P<unitID>\d+)' + timeInput, viewsAPI.viewTimeseriesUnitEnergy,name='timeseriesUnitEnergy'),
                       url(r'^timeseries/energy/unit/(?P<unitID>\d+)/load_type/(?P<typeID>\d+)' + timeInput,viewsAPI.viewTimeseriesUnitTypeEnergy, name='timeseriesUnitTypeEnergy'),
                       url(r'^timeseries/energy/room/(?P<roomID>\d+)/load_type/(?P<typeID>\d+)' + timeInput,viewsAPI.viewTimeseriesRoomTypeEnergy, name='timeseriesRoomTypeEnergy'),
                       #Datapoint(with time vector!) when we need the total energy use of the given time period. The resolution is the
                       #one the resampler will use. The higher the more accurate results in energy total.
                       url(r'^datapoint/energy/load/(?P<loadID>\d+)' + timeInput, viewsAPI.viewDatapointLoadEnergy,name='datapointLoadEnergy'),
                       url(r'^datapoint/energy/room/(?P<roomID>\d+)' + timeInput, viewsAPI.viewDatapointRoomEnergy,name='datapointRoomEnergy'),
                       url(r'^datapoint/energy/unit/(?P<unitID>\d+)' + timeInput, viewsAPI.viewDatapointUnitEnergy,name='datapointUnitEnergy'),
                       url(r'^datapoint/energy/unit/(?P<unitID>\d+)/load_type/(?P<typeID>\d+)' + timeInput,viewsAPI.viewDatapointUnitTypeEnergy, name='datapointUnitTypeEnergy'),
                       url(r'^datapoint/energy/room/(?P<roomID>\d+)/load_type/(?P<typeID>\d+)' + timeInput,viewsAPI.viewDatapointRoomTypeEnergy, name='datapointRoomTypeEnergy'),

                       #For template use only
                       url(r'^timeseries/energy/room/(?P<roomID>\d+)/$' , viewsAPI.viewTimeseriesRoomEnergy,name='timeseriesRoomEnergy'),
                       url(r'^datapoint/energy/room/(?P<roomID>\d+)/$', viewsAPI.viewDatapointRoomEnergy,name='datapointRoomEnergy'),

                       #--------------------------------------------------------------------------------------------------------------------
                       #Urls to trigger actions, either directly through actuators or on loads
                       #Convention for actions: 0:OFF, 100: ON, 111: TOGGLE, XX: DIM to XX%.
                       #X: switch to mode X (will not work by load url when load has also switch actuator)

                       url(r'^action/load/(?P<loadID>\d+)/(?P<instruction>\d+)$', viewsAPI.viewActOnLoad, name='actLoad'),
                       url(r'^action/actuator/(?P<actID>\d+)/(?P<instruction>\d+)$', viewsAPI.viewActOnActuator,name='actActuator'),

                       #--------------------------------------------------------------------------------------------------------------------
                       #Urls to get status of actuators. Not support for asking through load.
                       url(r'^status/actuator/(?P<actID>\d+)$', viewsAPI.viewStatusActuator, name='statusActuator'),
                       url(r'^status/room/(?P<roomID>\d+)$', viewsAPI.viewStatusRoom, name='statusRoom'),

                       ##############Up to here documented...######################
                       #--------------------------------------------------------------------------------------------------------------------
                       #Configuration message deposit/withdrawl urls, content can be any type of string, for compatibility reasons yaml was desided by the two parties
                       url(r'^configmsg/REQ(/(?P<confID>\d+))?$', viewsAPI.viewConfigMsgREQ.as_view(), name='configmsgin'),
                       url(r'^configmsg/REP(/(?P<confID>\d+))?$', viewsAPI.viewConfigMsgREP.as_view(), name='configmsgout'),

                       #--------------------------------------------------------------------------------------------------------------------
                       #Urls for getting zone info for LTspice simulator
                       url(r'^zones/unit/(?P<unitID>\d+)?$', viewsAPI.viewZonesUnit, name='zonesUnit'),


                       #-------------------------------------------------------------------------------------------------------------------
                       #The UUID-UUID association urls, in url the personal uuid since it is a resource
                       #and in POST parameters the dynamic one
                       url(r'^association/per_uuid/(?P<per_uuid>([0-9A-Fa-f]{2}[:-])+([0-9A-Fa-f]{2}))$', viewsAPI.viewAssociateUUID.as_view(),name='associationUUID'),
                       #Get the client location and calculate the distance
                       url('^location/per_uuid/(?P<per_uuid>([0-9A-Fa-f]{2}[:-])+([0-9A-Fa-f]{2}))$', viewsAPI.viewUserLocation.as_view(),name='locationUUID'),
                       )

#--------------------------------------------------------------------------------------------------------------------
#Secure Urls that pass the key through the url path
urlsecpatterns = [
    url(r'^association/per_uuid/(?P<per_uuid>([0-9A-Fa-f]{2}[:-])+([0-9A-Fa-f]{2})$)$', viewsAPI.viewAssociateUUID.as_view(), {'keyinpath' : True}, name='associationUUIDsecAPI'),
    #Get the client location and calculate the distance
    url('^location/per_uuid/(?P<per_uuid>([0-9A-Fa-f]{2}[:-])+([0-9A-Fa-f]{2})$)$', viewsAPI.viewUserLocation.as_view(), {'keyinpath' : True}, name='locationUUIDsecAPI'),
]
