__author__ = 'Georgios Lilis'
import socket
import struct
import re
import cPickle
import os
import sys
import time
from subprocess import Popen, PIPE
import zmq
from conf_general import PUB_MSG_TYPE
from conf_tcp import *
from middlewareutilities import get_middleware_details
from BmsAPI.utilities import logger

class SleepApi(object):
    def __init__(self):
        middleware = get_middleware_details(midid=TCP_MID_ID) #Execution will stop if it fails to find openBMS
        if middleware['middleware_type'] !='IP':
            logger.error('Could not find the TCP middleware in openBMS, verify the ID in conf_tcp.py')
            sys.exit()
        ip = middleware['IP']
        rep_port = middleware['REP_port'] #not used here
        pub_port = middleware['PUB_port']
        self.tcppubip = ip
        self.tcppubport = pub_port
        logger.info('Starting the middleware "{0}" @ "{1}:{2}/{3}"'.format(middleware['name'], ip, rep_port,pub_port))

        context = zmq.Context()
        self.tcpREQsocket = context.socket(zmq.REQ)
        self.tcpPUBsocket = context.socket(zmq.PUB)
        self.tcpPUBsocket.bind('tcp://' + CURRENT_IP + ':' + pub_port)
        self.poller = zmq.Poller()
        self.mainbuffer = None
        self.createorread_buffer()

    def write_buffer(self):
        bufferfile = open(BUFFERNAME, 'wb', buffering=0)
        pickler = cPickle.Pickler(bufferfile, cPickle.HIGHEST_PROTOCOL)
        pickler.dump(self.mainbuffer)
        bufferfile.close()

    def createorread_buffer(self):
        bufferfile = None
        try:
            bufferfile = open(BUFFERNAME, 'rb', buffering=0)
            unpickler = cPickle.Unpickler(bufferfile)
            self.mainbuffer = unpickler.load()
            bufferfile.close()
            assert isinstance(self.mainbuffer, dict)

        except (EOFError, AssertionError):
            logger.info('Buffer file corrupted, creating a new')  # The creation is done in save_buffer()
            bufferfile.close()
            os.remove(BUFFERNAME)
            self.mainbuffer = dict()

        except IOError:
            logger.info('Buffer file doesn\'t exist, creating a new')  # The creation is done in save_buffer()
            self.mainbuffer = dict()

    def connect_remote(self, ip):
        self.tcpREQsocket.connect("tcp://" + NETWORK_PREFIX + str(ip) + ":"+TCP_REMOTE_PORT)
        self.poller.register(self.tcpREQsocket, zmq.POLLIN)
        if not ip in self.mainbuffer:
            self.mainbuffer[ip] = dict()  # Init in case ip doesnt exist

    def bind_publisher(self):
        self.tcpPUBsocket.bind('tcp://' + self.tcppubip + ':' + str(self.tcppubport))

    def unbind_publisher(self):
        self.tcpPUBsocket.unbind('tcp://' + self.tcppubip + ':' + str(self.tcppubport))

    def get_status(self, ip):
        try:
            return self.mainbuffer[ip]['Status']
        except KeyError:
            self.mainbuffer[ip] = dict([('Status', DEFAULT_STAT)])  # Init in case ip doesnt exist
            self.write_buffer()
            return DEFAULT_STAT

    def tcp_sleep(self, ip):
        try:
            self.connect_remote(ip)
            logger.info("Sending sleep at ip" + NETWORK_PREFIX + str(ip))
            # get the mac for the wake...
            Popen(["ping", "-w 200 -n 1", NETWORK_PREFIX + str(ip)], stdout=PIPE)
            mac = self.getmac(NETWORK_PREFIX + str(ip))
            if mac:
                self.mainbuffer[ip]['MAC'] = mac
                self.write_buffer()
            self.tcpREQsocket.send_json('SLEEP')
            socks = dict(self.poller.poll(1000))
            if socks.get(self.tcpREQsocket) == zmq.POLLIN:
                res = self.tcpREQsocket.recv_json()
                self.tcpREQsocket.disconnect("tcp://" + NETWORK_PREFIX + str(ip) + ":"+TCP_REMOTE_PORT)
                if res:
                    reqtime = time.time()
                    submsg = {'MID_ID': TCP_MID_ID,
                      'REF': ip,
                      'MSG_TYPE': PUB_MSG_TYPE[0],
                      'PAYLOAD': {'Type': MODE_API_TYPE, 'Setting': STATUS_SLEEP, 'Time': reqtime}}
                    #self.bind_publisher()
                    self.tcpPUBsocket.send_json(submsg)
                    #self.unbind_publisher()
                    self.mainbuffer[ip]['Status'] = STATUS_SLEEP
                    self.write_buffer()
                return res
            else:
                logger.warning('Sleep did not succeed')
                self.tcpREQsocket.close(linger=0)
                return False
        except Exception:
            logger.warning('Sleep did not succeed')
            self.tcpREQsocket.close(linger=0)
            return False

    def tcp_wake(self, ip):
        if not ip in self.mainbuffer:
            self.mainbuffer[ip] = dict()  # Init in case ip doesnt exist
        logger.info("Sending wake at ip" + NETWORK_PREFIX + str(ip))
        mac = self.getmac(NETWORK_PREFIX + str(ip))
        if not mac:  # Then we take it from buffer
            if not 'MAC' in self.mainbuffer[ip]:
                logger.warning("Cannot retrieve the MAC address")
                return False  # no way to have mac. abort
            mac = self.mainbuffer[ip]['MAC']
            self.write_buffer()
        res = self.wake_on_lan(mac)
        if res:
            reqtime = time.time()
            submsg = {'MID_ID': TCP_MID_ID,
                      'REF': ip,
                      'MSG_TYPE': PUB_MSG_TYPE[0],
                      'PAYLOAD': {'Type': MODE_API_TYPE, 'Setting': STATUS_ON, 'Time': reqtime}}
            self.tcpPUBsocket.send_json(submsg)
            self.mainbuffer[ip]['Status'] = STATUS_ON
            self.write_buffer()
        return res

    @staticmethod
    def wake_on_lan(macaddress):
        """ Switches on remote computers using WOL. """

        # Check macaddress format and try to compensate.
        if macaddress:
            if len(macaddress) == 12:
                pass
            elif len(macaddress) == 12 + 5:
                sep = macaddress[2]
                macaddress = macaddress.replace(sep, '')
            else:
                raise ValueError('Incorrect MAC address format')
        else:
            return False

        # Pad the synchronization stream.
        data = ''.join(['FFFFFFFFFFFF', macaddress * 16])
        send_data = ''

        # Split up the hex values and pack.
        for i in range(0, len(data), 2):
            send_data = ''.join([send_data, struct.pack('B', int(data[i: i + 2], 16))])

        # Broadcast it to the LAN.
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        ret = sock.sendto(send_data, ('128.178.19.255', 9))  # addr and port
        return True

    @staticmethod
    def getmac(ip):
        Popen(["ping", "-w 200 -n 1", ip], stdout=PIPE)
        pid = Popen(["arp", "-a", ip], stdout=PIPE)
        s = pid.communicate()[0]
        try:
            ipmac = re.search(r"Type\r\n\s+(\d+\.\d+\.\d+\.\d+)\s+(\w+-\w+-\w+-\w+-\w+-\w+)", s).groups()
            logger.info("%s --> %s" % (ipmac[0], ipmac[1]))
            return ipmac[1]
        except AttributeError:
            pass


