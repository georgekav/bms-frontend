__author__ = 'Georgios Lilis'

from threading import Lock
import time

from conf_openbms import BACKENDAPI_BASE_TIMEOUT
from conf_general import SENSORS_API_TYPES, LOADS_API_TYPES
from conf_plc import RELAY_CMD, DIMMER_CMD, PLC_ACTUATOR_TYPES
from sleepapi import *
from BmsAPI.utilities import logger
from math import isnan

class BackendAPI(object):
    """An interface class for the backend modules of the system."""

    def __init__(self):
        self.statusresettimer = time.time()

        self._threadlockdb = Lock()
        self._threadlockplc = Lock()
        self._threadlockzwave = Lock()
        self._threadlocktcp = Lock()

        self._middlewarestatus = dict()
        self._dbsockets = dict()
        self._dbpoller = zmq.Poller()
        self._plcsockets = dict()
        self._plcpoller = zmq.Poller()
        self._zwavesockets = dict()
        self._zwavepoller = zmq.Poller()
        from threading import Timer
        Timer(2, self.start_sleepapi_mid, ()).start()

    def start_sleepapi_mid(self):
        self._sleepapi = SleepApi()

    def auto_daemon_reset(self):
        """Reset statuses in defined intervals"""
        if time.time() - self.statusresettimer > 60:  # every 1min
            for key in self._middlewarestatus.keys():
                self._middlewarestatus[key] = True
            self.statusresettimer = time.time()

    def get_sensor_data(self, sensors, initendtime, resolution, nobuffer=False):
        """
        sensors get the frontend sensors objects for which we want the sensor values.
        It will recreate the ref list and the measure type list to be asked to the middlewares.
        The tuple (inittime, endtime) is in unix time the from to time of sensor data. If data doesnt
        exist for all the range, only the part that exists would be returned. resolution is the requested resolution of the
        data. sensorid and measuretype can be lists for multiple sensors request.
        (inittime, endtime) and resolution are always single (not list) and in int.
        This means that each request can have only one type of from to,
        and resolution for all requests in the same function execution.
        The return is ALWAYS a list, with 1 or n lines. Each line is a vector(list) with the data.
        'data' is a tuple (Time,value).
        If endtime=-1 then gives all they exist from init to EOF. If init=-1 then returns from the SOF(start of file) to endtime.
        If both are -1 then get the last value
        If inittime==endtime then you get a datapoint with exactly the time requested
        Res is not always possible to be forced in exact time steps. This require a resampling.
        nobuffer option is when we want to avoid any buffer and ask the lower backends to sample right now. The rest of the
        parameters passed should correspond to datapoint type(ex init=end etc)
        Thread SAFE!!! the zmq sockets is not reinitialized in every thread, and daemons lock the threads"""
        self.auto_daemon_reset()

        (inittime, endtime) = initendtime
        if isinstance(inittime, str):
            inittime = float(inittime)
        if isinstance(endtime, str):
            endtime = float(endtime)
        if inittime != -1 and endtime != -1:
            # Otherwise we have a datapoint request and we dont care for resolution
            if resolution < 0:
                raise BackendError('Resolution < 0')
            elif resolution is None:
                resolution = 0
        data = []
        dbtimeout = BACKENDAPI_BASE_TIMEOUT * 10  # We give plenty of time to the DB daemon even for a last point
        if inittime == endtime and inittime == -1:
            # FAST request from buffer of plc and zwave since it is last point
            if nobuffer:  # differentiate the timeout accourding to buffer or not access
                timeout = BACKENDAPI_BASE_TIMEOUT * 5  #Takes time a active poll
            else:
                timeout = BACKENDAPI_BASE_TIMEOUT
            sensors=sensors.select_related('network_middleware', 'store_middleware') #DB booster, to avoid consequent queries
            for sensor in sensors:
                measure = sensor.measure
                ref = sensor.ref
                mid_id = sensor.network_middleware_id
                if measure in SENSORS_API_TYPES:
                    if not nobuffer or measure == 'PRE' or measure == 'BAT':  # We always fetch the presence and battery from buffer
                        tosendzwave = (ref, 'SENSOR', measure)
                    else:  # We want to do active polling
                        tosendzwave = (ref, 'SYNCZWAVE', measure)

                    if not self._middlewarestatus.has_key(mid_id) or self._middlewarestatus[mid_id]:
                        logger.debug('Sending request for last point to the ZWAVE daemon')
                        middlewareip = sensor.network_middleware.IP
                        middlewareport = sensor.network_middleware.REP_port
                        retzwave = self._pass_to_zwave_daemon(pyobject=tosendzwave, timeout=timeout,
                                                              midid=mid_id, zwaveip=middlewareip, zwaveport=middlewareport)
                    if not self._middlewarestatus[mid_id] or not retzwave or retzwave is None:
                        logger.info('ZWAVE daemon is down, forwarding request for last point sen: {0} mid: {1} and measure: {2} to the DB daemon'.format(ref, mid_id, measure))
                        middlewareip = sensor.store_middleware.IP
                        middlewareport = sensor.store_middleware.REP_port
                        middlewareid = sensor.network_middleware_id  #Careful network middleware!
                        dbrequest = ('READ', middlewareid, ref, measure, (inittime, endtime), resolution)
                        retzwave = self._pass_to_db_daemon(pyobject=dbrequest, timeout=dbtimeout,
                                                           dbip=middlewareip, dbport=middlewareport)
                        if not retzwave or retzwave is None or isnan(retzwave[0]) or isnan(retzwave[1]) :  # The backup read by db failed also
                            logger.warning('It was not possible to retrieve sen: {0} mid: {1} and measure: {2} from the DB daemon, setting it at 0'.format(ref, mid_id, measure))
                            retzwave = [time.time(), 0]
                    data.append([retzwave])

                elif measure in LOADS_API_TYPES:
                    if not nobuffer:  # We want to do active polling
                        tosendplc = (ref, 'POWER', measure)
                    else:
                        tosendplc = (ref, 'SYNCPLC', measure)
                    if not self._middlewarestatus.has_key(mid_id) or self._middlewarestatus[mid_id]:
                        logger.debug('Sending request for last point to the PLC daemon')
                        middlewareip = sensor.network_middleware.IP
                        middlewareport = sensor.network_middleware.REP_port
                        retplc = self._pass_to_plc_daemon(pyobject=tosendplc, timeout=timeout,
                                                          midid=mid_id, plcip=middlewareip, plcport=middlewareport)
                    if not self._middlewarestatus[mid_id] or not retplc or retplc is None:
                        logger.info('PLC daemon is down, forwarding request for last point of sen: {0} mid: {1} and measure: {2} to the DB daemon'.format(ref, mid_id, measure))
                        middlewareip = sensor.store_middleware.IP
                        middlewareport = sensor.store_middleware.REP_port
                        middlewareid = sensor.network_middleware_id  #Careful network middleware!
                        dbrequest = ('READ', middlewareid, ref, measure, (inittime, endtime), resolution)
                        retplc = self._pass_to_db_daemon(pyobject=dbrequest, timeout=dbtimeout,
                                                         dbip=middlewareip, dbport=middlewareport)
                        if not retplc or retplc is None or isnan(retplc[0]) or isnan(retplc[1]):  # The backup read by db failed also
                            logger.warning('It was not possible to retrieve sen: {0} mid: {1} and measure: {2} from the DB daemon'.format(ref, mid_id, measure))
                            retplc = [time.time(), 0]
                    data.append([retplc])

        else:  # timeseries dbrequest
            sensors=sensors.select_related('store_middleware')
            for sensor in sensors:
                measure = sensor.measure
                ref = sensor.ref
                middlewareip = sensor.store_middleware.IP
                middlewareport = sensor.store_middleware.REP_port
                middlewareid = sensor.network_middleware_id  #Careful network middleware!
                logger.debug('Sending request for timeseries to the DB daemon')
                dbrequest = ('READ', middlewareid, ref, measure, (inittime, endtime), resolution)
                rettimeseries = self._pass_to_db_daemon(pyobject=dbrequest, timeout=dbtimeout,
                                                        dbip=middlewareip, dbport=middlewareport)
                if not rettimeseries or rettimeseries is None:  # The backup read by db failed also
                    raise BackendError('Error: not possible to retrieve all timeseries DATA sen: {0} and measure: {1} using the DB daemon'.format(ref, measure))
                data.append(rettimeseries)

        if data:
            return data
        else:
            raise BackendError('Error: not possible to retrieve any data using the backendAPI')

    def set_actuator_state(self, actuator, action, auxiliary=None):
        """Receives a actuator object and the action as well as if needed an auxiliary value. The
        action must be one of define.PLC_REQ_PAYLOAD. Return boolean True if success or False otherwise
        Thread SAFE!!"""
        self.auto_daemon_reset()

        relaydimact = list(RELAY_CMD)
        relaydimact.extend([DIMMER_CMD])
        mid_id = actuator.network_middleware_id
        if not self._middlewarestatus.has_key(mid_id) or self._middlewarestatus[mid_id]:
            if action in relaydimact:
                middlewareip = actuator.network_middleware.IP
                middlewareport = actuator.network_middleware.REP_port
                # Big timeout due to dimmer that tryies 3 times
                res = self._pass_to_plc_daemon((actuator.ref, action, auxiliary), timeout=BACKENDAPI_BASE_TIMEOUT * 3,
                                               midid=mid_id, plcip=middlewareip, plcport=middlewareport)
            elif action == MODE_API_TYPE:
                res = self._pass_to_tcp_daemon((actuator.ref, action, auxiliary))
            else:
                res = None
        else:
            res = None
        return res

    def get_actuators_state(self, actuators):
        """Receives a actuator object to get its current state. Return int according to actuator type.
        0 for OFF, !=0 for ON and in between the dimmer setting.
        Thread SAFE!!"""
        self.auto_daemon_reset()
        actuators=actuators.select_related('network_middleware')
        retstatuses = []
        for actuator in actuators:
            mid_id = actuator.network_middleware_id
            if not self._middlewarestatus.has_key(mid_id) or self._middlewarestatus[mid_id]:
                if actuator.action == MODE_API_TYPE:  # we have tcp actuator
                    res = self._pass_to_tcp_daemon((actuator.ref, actuator.action))
                elif actuator.action in PLC_ACTUATOR_TYPES:
                    middlewareip = actuator.network_middleware.IP
                    middlewareport = actuator.network_middleware.REP_port
                    res = self._pass_to_plc_daemon(pyobject=(actuator.ref, actuator.action), timeout=BACKENDAPI_BASE_TIMEOUT,
                                                   midid=mid_id, plcip=middlewareip, plcport=middlewareport)
                else:
                    raise BackendError("Not supported actuator type {0}".format(actuator.action))
            else:
                res = None
            retstatuses.append(res)
        return retstatuses

    def _pass_to_db_daemon(self, pyobject, timeout, dbip, dbport):
        self._threadlockdb.acquire()
        try:
            if dbip =='255.255.255.255':
                logger.error('DB middleware did not report yet its IP. Skipping connection')
                raise ValueError
            if self._dbsockets.has_key(dbip):#A way to avoid constant reconnections to sockets
                dbsock = self._dbsockets[dbip]
            else:
                dbsock = zmq.Context().socket(zmq.DEALER)
                dbsock.connect("tcp://" + str(dbip) + ":" + str(dbport))
                dbsock.setsockopt(zmq.LINGER, 0)
                self._dbpoller.register(dbsock, zmq.POLLIN)
                self._dbsockets[dbip] = dbsock
            dbsock.send('', flags=zmq.SNDMORE)  # On the "other side" there is a REP, so we have to respect the envelope
            dbsock.send_json(pyobject)
            socks = dict(self._dbpoller.poll(timeout=timeout))
            if socks.get(dbsock) == zmq.POLLIN:
                dbsock.recv()  # The REP gives this envelope that we need to remove, it is an empty message
                ret = dbsock.recv_json()
                return ret
            else:
                logger.warning('Backend DB daemon at {0}:{1} is not responding'.format(dbip, dbport))
                self._dbpoller.unregister(self._dbsockets[dbip])
                del self._dbsockets[dbip]
                return None
        except ValueError:   # in case it is impossible to decode json or not resolved IP
            return None
        finally:
            self._threadlockdb.release()

    def _pass_to_plc_daemon(self, pyobject, timeout, midid, plcip, plcport):
        self._threadlockplc.acquire()
        try:
            if plcip =='255.255.255.255':
                logger.error('PLC middleware with ID {0} did not report yet its IP. Skipping connection'.format(midid))
                self._middlewarestatus[midid] = False
                raise ValueError
            if self._plcsockets.has_key(plcip):#A way to avoid constant reconnections to sockets
                plcsock = self._plcsockets[plcip]
            else:
                plcsock = zmq.Context().socket(zmq.DEALER)
                plcsock.connect("tcp://" + str(plcip) + ":" + str(plcport))
                plcsock.setsockopt(zmq.LINGER, 0)
                self._plcpoller.register(plcsock, zmq.POLLIN)
                self._plcsockets[plcip] = plcsock
            plcsock.send('', flags=zmq.SNDMORE)  # On the "other side" there is a REP, so we have to respect the envelope
            plcsock.send_json(pyobject)
            socks = dict(self._plcpoller.poll(timeout=timeout))
            if socks.get(plcsock) == zmq.POLLIN:
                plcsock.recv()  # The REP gives this envelope that we need to remove, it is an empty message
                ret = plcsock.recv_json()
                self._middlewarestatus[midid] = True
                return ret
            else:
                logger.warning('Backend PLC daemon at {0}:{1} is not responding'.format(plcip, plcport))
                self._middlewarestatus[midid] = False
                self._plcpoller.unregister(self._plcsockets[plcip])
                del self._plcsockets[plcip]
                return None
        except ValueError:   # in case it is impossible to decode json or not resolved IP
            return None
        finally:
            self._threadlockplc.release()

    def _pass_to_zwave_daemon(self, pyobject, timeout, midid, zwaveip, zwaveport):
        self._threadlockzwave.acquire()
        try:
            if zwaveip =='255.255.255.255':
                logger.error('ZWAVE middleware with ID {0} did not report yet its IP. Skipping connection'.format(midid))
                self._middlewarestatus[midid] = False
                raise ValueError
            if self._zwavesockets.has_key(zwaveip):#A way to avoid constant reconnections to sockets
                zwavesock = self._zwavesockets[zwaveip]
            else:
                zwavesock = zmq.Context().socket(zmq.DEALER)
                zwavesock.connect("tcp://" + str(zwaveip) + ":" + str(zwaveport))
                zwavesock.setsockopt(zmq.LINGER, 0)
                self._zwavepoller.register(zwavesock, zmq.POLLIN)
                self._zwavesockets[zwaveip] = zwavesock
            zwavesock.send('', flags=zmq.SNDMORE)  # On the "other side" there is a REP, so we have to respect the envelope
            zwavesock.send_json(pyobject)
            socks = dict(self._zwavepoller.poll(timeout=timeout))
            if socks.get(zwavesock) == zmq.POLLIN:
                zwavesock.recv()  # The REP gives this envelope that we need to remove, it is an empty message
                ret = zwavesock.recv_json()
                self._middlewarestatus[midid] = True
                return ret
            else:
                logger.warning('Backend ZWAVE daemon at {0}:{1} is not responding'.format(zwaveip, zwaveport))
                self._middlewarestatus[midid] = False
                self._zwavepoller.unregister(self._zwavesockets[zwaveip])
                del self._zwavesockets[zwaveip]
                return None
        except ValueError:   # in case it is impossible to decode json or not resolved IP
            return None
        finally:
            self._threadlockzwave.release()

    def _pass_to_tcp_daemon(self, action):
        self._threadlocktcp.acquire()
        try:
            ip = action[0]  # give the id to backend, the DB stores it like a 19XXX int...
            if len(action) == 2:
                res = self._sleepapi.get_status(ip)
                return res
            if action[2] == '0':
                res = self._sleepapi.tcp_wake(ip)
            elif action[2] == '1':
                res = self._sleepapi.tcp_sleep(ip)
            elif action[2] == '2':
                logger.warning('We dont support shutdown yet')
                res = False
            else:
                res = None
            return res
        finally:
            self._threadlocktcp.release()


class BackendError(Exception):
    pass