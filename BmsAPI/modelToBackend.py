__author__ = 'Georgios Lilis, Thomas Guibentif'

# This file is a library of functions linking the data in the model with the measurements, aggregating it whenever
# needed. It should be the only one to modify in case of modifications of the backend (implementation).
# Except for measured quantities and actions in the models

# Relies on backendapi.py

from time import time
from BmsAPI.utilities import *
from backendapi import BackendAPI
from BmsApp.models import Sensor
from conf_tcp import MODE_API_TYPE, MODE_CMD
from conf_plc import PLC_ACTUATOR_TYPES, RELAY_CMD, DIMMER_CMD
from conf_openbms import INTERPOLATION_TYPE_SWITCH_POINT


# ---------------------------------------------------------------------------------------
# Optimize the requests, the backend is thread safe
backendapi = BackendAPI()
def toBackend_getSensorData(sensors, initTime=None, endTime=None, timeRes=None, nobuffer=False):
    """Basic data retrieval function:
    Input: sensor queryset, init and final time in unix, desired min resolution
    With current backend (29.05.2014):
    If res is omitted (or =0) returns raw data (all points available);
    if res>0 returns data at points spaced of at least res;
    If endTime is omitted (or = initTime) takes the direct ancestor before initTime
    If initTime is omitted takes returns the last measured point
    Output: [[(<time>,<value>)...]...]: tuples with measure points
    """

    if not initTime:
        initTime = -1
    if not endTime:
        endTime = initTime
    if not timeRes:
        timeRes = 0

    initendtime = (initTime, endTime)
    retVal = backendapi.get_sensor_data(sensors, initendtime, timeRes, nobuffer=nobuffer)

    if endTime == -1:
        endTime = time()
    if initTime == -1:
        initTime = endTime

    return retVal


# ---------------------------------------------------------------------------------------

def toBackend_resampleDataOrPoint(sensors, timeVector=None, interpolation='step', fillvalue=0):
    """Advanced data retrieval function:
    Input: sensor queryset, list of unix times where we require points, interpolation method, may be:
    "mean" (default): the y(for timevector) of the the linear connection between two raw points
    "step": returns the direct ancestor of each point in timeVector, as the zero interpolation does.
    Output: as before, [[(<time>,<value>)...]...], but the time is the same as the points in timeVector.
    The linear interpolation is recommended for the room sensors since they describe a measure with inertia
    The step interpolation is better suited for the load sensors since they change consumption sharply
    fillvalue will complete the missing data from the begining of the data vectors
    resolutions is in most cases 0 since we want all the data in order to do the in-house interpollation-mean that the csv one
    """
    if not timeVector:
        timeVector = [-1]
    if len(timeVector) == 1:
        # initTime==endTime causes the backend to return only the last point before initTime.
        getlast = toBackend_getSensorData(sensors=sensors, initTime=timeVector[0], endTime=timeVector[0], timeRes=1)
        return getlast

    #if the interpolation is left at auto then the resampleDataOrPoint will choose the collect
    if interpolation == 'auto':
        if timeVector[1]-timeVector[0] > INTERPOLATION_TYPE_SWITCH_POINT:
            interpolation = 'mean'
        else:
            #CAUTION, we check just the first of the sensors, this will not work well if you mix them. It will use the
            #interpolation appropriate for the first one to all
            if sensors[0].measure in Sensor.room_measures:
                #Room sensors have inertia so we have a linear correlation
                interpolation='linear'
                fillvalue=None
            elif sensors[0].measure in Sensor.load_measures:
                #Load sensors are event based so we have a step correlation
                interpolation='step'
                fillvalue=0
    if interpolation == 'step':
        # smooth data will make the mean of the raw data returned by the backend between each successive pair of times in timeVector
        tostepinterpolate = toBackend_getSensorData(sensors=sensors, initTime=timeVector[0], endTime=timeVector[-1], timeRes=0)
        kind = 'zero'
        try:
            return interpolateData(tostepinterpolate, timeVector, kind, fillvalue)
        except ValueError:  # ValueError from failing to interpolate due to bounds
            logger.error('Interpolation of sensors {0} failed during {1} interpolation'.format(sensors,kind))
            return tostepinterpolate
    elif interpolation == 'linear':
        # smooth data will make the mean of the raw data returned by the backend between each successive pair of times in timeVector
        #danger if the resolution is too low then you have a line between points. It is better the 'mean'
        tolinearinterpolate = toBackend_getSensorData(sensors=sensors, initTime=timeVector[0], endTime=timeVector[-1], timeRes=0)
        kind = 'linear'
        try:
            return interpolateData(tolinearinterpolate, timeVector, kind, fillvalue)
        except ValueError:  # ValueError from failing to interpolate due to bounds
            logger.error('Interpolation of sensors {0} failed during {1} interpolation'.format(sensors, kind))
            return tolinearinterpolate
    elif interpolation == 'mean':
        #the real initial mean algorithm, let the backendapi do the tricks
        tomeaninterpolate = toBackend_getSensorData(sensors=sensors, initTime=timeVector[0], endTime=timeVector[-1], timeRes=0)
        return interpolateMeanData(datas=tomeaninterpolate,timeVector=timeVector)
    elif interpolation == 'raw':
        # The backend returns all the datapoints since res is 0, good for debugging
        getraw = toBackend_getSensorData(sensors=sensors, initTime=timeVector[0], endTime=timeVector[-1], timeRes=0)
        return getraw
    elif interpolation == 'integration':
        # integrateData returns data integrated between each two pair of points in timeVector (without doing the cumulative sum!)
        tointegrate = toBackend_getSensorData(sensors=sensors, initTime=timeVector[0], endTime=timeVector[-1], timeRes=0)
        return integrateData(tointegrate, timeVector)


def getPower(load, timeVector=None, quantities=('P', 'Q')):
    """Get powers of a load:
    Input: load object, list of unix time points for the data (if omitted returns last data), list of quantities to return (if omitted only P and Q are returned)
    Output: [<P>, <Q>, <S> ,<PF>, <estimated>]
    <P>, <Q>, <S>, <PF> are of the form: [(<time>,<value>)...] where <time> are the unix times provided in timeVector
    <estimated> is [<P:true/false>, <Q: true/false>, <S: true/false>, <PF: true/false>]
    If not enough sensors to estimate all quantities returns zeros for all non estimated quantities and the corresponding estimated is True.
    Only the quantities in the required list "quantities" are returned
    """

    if not isinstance(quantities, tuple):
        quantities = tuple(quantities)

    toReturn = {'P': ('P' in quantities), 'Q': ('Q' in quantities), 'S': ('S' in quantities), 'PF': ('PF' in quantities)}

    minTimestamp = local_time()
    if timeVector:
        timeLen = len(timeVector)
        P = [[x, 0] for x in timeVector]
        Q = [[x, 0] for x in timeVector]
        S = [[x, 0] for x in timeVector]
        PF = [[x, 0] for x in timeVector]
    else:
        timeLen = 1
        P = [[minTimestamp, 0]]
        Q = [[minTimestamp, 0]]
        S = [[minTimestamp, 0]]
        PF = [[minTimestamp, 0]]

    iterRange = range(0, timeLen)


    estimated = [True] * 4

    Festimated = [False] * 4

    sensors = load.sensor_set.exclude(measure='NULL')

    # retVal = []
    # retEstimated = []
    # for q in quantities:
    #     if q == 'P':
    #         retVal.append(P)
    #         retEstimated.append(estimated[0])
    #     elif q == 'Q':
    #         retVal.append(Q)
    #         retEstimated.append(estimated[1])
    #     elif q == 'S':
    #         retVal.append(S)
    #         retEstimated.append(estimated[2])
    #     elif q == 'PF':
    #         retVal.append(PF)
    #         retEstimated.append(estimated[3])
    #
    # retVal.append(retEstimated)
    # return retVal

    # Depending on what sensors are available compute the phasors
    if sensors:
        # First try to find all necessary sensors, it is only one object in Queryset(enforced by model validator)
        sP = sensors.filter(measure="P")
        sQ = sensors.filter(measure="Q")
        sS = sensors.filter(measure="S")
        sPF = sensors.filter(measure="PF")

        found = False
        proceed = False
        force_computation = False

        while not (found or proceed):
            # Do two loops, in case we can't find the right sensors try to deduce the values from other sensors
            # Try first S ,then PF, then the others
            # since with our backend the measurements are stored as apparent power&Power Factor
            if sS and (toReturn['S'] or force_computation):
                if sPF and (toReturn['PF'] or force_computation):
                    if (toReturn['S'] and toReturn['PF']) or force_computation:
                        found = True
                        data = toBackend_resampleDataOrPoint(sensors=sS | sPF, timeVector=timeVector, interpolation='auto', fillvalue=0)
                        S = data[0]
                        PF = data[1]
                        if toReturn['P']:
                            for i in iterRange:
                                P[i][1] = S[i][1] * PF[i][1]
                        if toReturn['Q']:
                            for i in iterRange:
                                Q[i][1] = QfromS_PF(S[i][1], PF[i][1])
                        estimated = Festimated
                elif sQ and (toReturn['Q'] or force_computation):
                    if (toReturn['S'] and toReturn['Q']) or force_computation:
                        found = True
                        data = toBackend_resampleDataOrPoint(sensors=sS | sQ, timeVector=timeVector, interpolation='auto', fillvalue=0)
                        S = data[0]
                        Q = data[1]
                        if toReturn['P']:
                            for i in iterRange:
                                P[i][1] = PfromS_Q(S[i][1], Q[i][1])
                        if toReturn['PF']:
                            for i in iterRange:
                                PF[i][1] = pfFromS_Q(S[i][1], Q[i][1])
                        estimated = Festimated
                elif sP and (toReturn['P'] or force_computation):
                    if (toReturn['S'] and toReturn['P']) or force_computation:
                        found = True
                        data = toBackend_resampleDataOrPoint(sensors=sP | sS, timeVector=timeVector, interpolation='auto', fillvalue=0)
                        P = data[0]
                        S = data[1]
                        if toReturn['Q']:
                            for i in iterRange:
                                Q[i][1] = QfromS_P(S[i][1], P[i][1])
                        if toReturn['PF']:
                            for i in iterRange:
                                PF[i][1] = pfFromS_P(S[i][1], P[i][1])
                        estimated = Festimated
            elif sPF and (toReturn['PF'] or force_computation):
                if sQ and (toReturn['Q'] or force_computation):
                    if (toReturn['PF'] and toReturn['Q']) or force_computation:
                        found = True
                        data = toBackend_resampleDataOrPoint(sensors=sPF | sQ, timeVector=timeVector, interpolation='auto', fillvalue=0)
                        PF = data[0]
                        Q = data[1]
                        if toReturn['P']:
                            for i in iterRange:
                                P[i][1] = PfromQ_pf(Q[i][1], PF[i][1])
                        if toReturn['S']:
                            for i in iterRange:
                                S[i][1] = SfromQ_pf(Q[i][1], PF[i][1])
                        estimated = Festimated
                elif sP and (toReturn['P'] or force_computation):
                    if (toReturn['PF'] and toReturn['P']) or force_computation:
                        found = True
                        data = toBackend_resampleDataOrPoint(sensors=sPF | sP, timeVector=timeVector, interpolation='auto', fillvalue=0)
                        PF = data[0]
                        P = data[1]
                        if toReturn['Q']:
                            for i in iterRange:
                                Q = [[P[i][0], QfromP_pf(P[i][1], PF[i][1])]]
                        if toReturn['S']:
                            for i in iterRange:
                                S[i][1] = SfromP_pf(P[i][1], PF[i][1])
                        estimated = Festimated
            elif sQ and (toReturn['Q'] or force_computation):
                if sP and (toReturn['P'] or force_computation):
                    if (toReturn['PF'] and toReturn['P']) or force_computation:
                        found = True
                        data = toBackend_resampleDataOrPoint(sensors=sP | sQ, timeVector=timeVector, interpolation='auto', fillvalue=0)
                        P = data[0]
                        Q = data[1]
                        if toReturn['S']:
                            for i in iterRange:
                                S[i][1] = SfromP_Q(P[i][1], Q[i][1])
                        if toReturn['PF']:
                            for i in iterRange:
                                PF[i][1] = pfFromS_P(S[i][1], P[i][1])
                        estimated = Festimated

            if force_computation:
                proceed = True
            force_computation = True

        if not found:
            if sS and toReturn['S']:
                S = toBackend_resampleDataOrPoint(sensors=sS, timeVector=timeVector, interpolation='auto', fillvalue=0)[0]
                estimated[2] = False
            elif sPF and toReturn['PF']:
                PF = toBackend_resampleDataOrPoint(sensors=sPF, timeVector=timeVector, interpolation='auto', fillvalue=0)[0]
                estimated[3] = False
            elif sQ and toReturn['Q']:
                Q = toBackend_resampleDataOrPoint(sensors=sQ, timeVector=timeVector, interpolation='auto', fillvalue=0)[0]
                estimated[1] = False
            elif sP and toReturn['P']:
                P = toBackend_resampleDataOrPoint(sensors=sP, timeVector=timeVector, interpolation='auto', fillvalue=0)[0]
                estimated[0] = False

    retVal = []
    retEstimated = []
    for q in quantities:
        if q == 'P':
            retVal.append(P)
            retEstimated.append(estimated[0])
        elif q == 'Q':
            retVal.append(Q)
            retEstimated.append(estimated[1])
        elif q == 'S':
            retVal.append(S)
            retEstimated.append(estimated[2])
        elif q == 'PF':
            retVal.append(PF)
            retEstimated.append(estimated[3])

    retVal.append(retEstimated)
    return retVal


def sumPower(loads, timeVector=None, quantities=('P', 'Q', 'S', 'PF')):
    """Computes the sum of power phasors
    Input: load queryset of loads to take into account and timeVector, a list of unix times (int) where to compute these phasors
    If timeVector is omitted returns sum of last measured phasors (asynchronous measurements), which may be non accurate
    Output: [<P>,<Q>,<S>,<PF>,<estimated>], the coordinates of the sum of phasors
    <...> is of the form [[time, value]...]
    <estimated> is [<P:true/false>, <Q: true/false>, <S: true/false>, <PF: true/false>], one of them being False if one quantity could not be computed for one load
    Only the quantities in the required list "quantities" are returned
    """
    if not isinstance(quantities, tuple):
        quantities = tuple(quantities)

    toReturn = {'P': ('P' in quantities), 'Q': ('Q' in quantities), 'S': ('S' in quantities), 'PF': ('PF' in quantities)}
    loadQuantities = []
    if toReturn['S'] or toReturn['PF']:
        loadQuantities = ['P', 'Q']
    else:
        if toReturn['P']:
            loadQuantities.append('P')
        elif toReturn['Q']:
            loadQuantities.append('Q')

    minTimestamp = local_time()

    if timeVector:
        timeLen = len(timeVector)
        P = [[x, 0] for x in timeVector]
        Q = [[x, 0] for x in timeVector]
        S = [[x, 0] for x in timeVector]
        PF = [[x, 0] for x in timeVector]
    else:
        timeLen = 1
        P = [[minTimestamp, 0]]
        Q = [[minTimestamp, 0]]
        S = [[minTimestamp, 0]]
        PF = [[minTimestamp, 0]]


    iterRange = range(0, timeLen)

    estimated = [False] * 4

    if loads:
        # For each load sum the active and reactive power (sum phasors in cartesian coordinates)
        for l in loads:
            powers = getPower(l, timeVector, loadQuantities)
            if toReturn['P'] or toReturn['S'] or toReturn['PF']:
                for i in iterRange:
                    P[i][1] += powers[0][i][1]
                if toReturn['Q'] or toReturn['S'] or toReturn['PF']:
                    for i in iterRange:
                        Q[i][1] += powers[1][i][1]
                    estimated[0] = (estimated[0] or powers[2][0])
                    estimated[1] = (estimated[1] or powers[2][1])
                else:
                    estimated[0] = estimated[0] or powers[1][0]
            elif toReturn['Q']:
                for i in iterRange:
                    Q[i][1] += powers[0][i][1]
                estimated[1] = estimated[1] or powers[1][0]
            #Commented because we store only the change in measures. So this will returned the timestep of the least changing load
            #Since it is more in the past. We trust the power daemon to give data that are updated and represent the embedded
            #network state.
            # if timeLen == 1:
            #     minTimestamp = min(minTimestamp, powers[0][0][0])

    else:
        estimated = [True] * 4

    # Then compute apparent power and power factor from the sum
    if toReturn['S']:
        for i in iterRange:
            S[i][1] = SfromP_Q(Q[i][1], P[i][1])
        if estimated[0] or estimated[1]:
            estimated[2] = True
    if toReturn['PF']:
        for i in iterRange:
            PF[i][1] = pfFromP_Q(P[i][1], Q[i][1])
        if estimated[0] or estimated[1]:
            estimated[3] = True

    if timeLen == 1:
        P[0][0] = minTimestamp
        Q[0][0] = minTimestamp
        S[0][0] = minTimestamp
        PF[0][0] = minTimestamp

    retVal = {}
    retEstimated = {}
    for q in quantities:
        if q == 'P':
            retVal[q] = [P]
            retEstimated[q] = estimated[0]
        elif q == 'Q':
            retVal[q] = [Q]
            retEstimated[q] = estimated[1]
        elif q == 'S':
            retVal[q] = [S]
            retEstimated[q] = estimated[2]
        elif q == 'PF':
            retVal[q] = [PF]
            retEstimated[q] = estimated[3]

    return retVal, retEstimated


def getEnergy(load, timeVector=None):
    if not timeVector:
        # By default integrate over the last week
        timeVector = [local_time() - 604800, local_time()]
    # First try to find necessary sensor, it is only one object in Queryset(enforced by model validator)
    sP = load.sensor_set.filter(measure='P')

    if sP:
        E = toBackend_resampleDataOrPoint(sensors=sP, timeVector=timeVector, interpolation="integration", fillvalue=0)[0]
        estimated = False
    else:
        # We miss the P so we skip this load(give it 0 contribution of energy)
        E = [[timeVector[i], 0] for i in range(0, len(timeVector))]
        estimated = True
    return [E, [estimated]]


def sumEnergy(loads, timeVector=None):
    """
    Adds the energies of each of the loads to one
    Output: [[[<time>,<value>],...],[[<estimated>]]]
    """

    if not timeVector:
        timeVector = [local_time() - 604800, local_time()]

    timeLen = len(timeVector)
    iterRange = range(0, timeLen)

    E = [[timeVector[i], 0] for i in iterRange]

    estimated = [False] * 1
    if loads:
        for l in loads:
            Enew = getEnergy(l, timeVector)
            for i in iterRange:
                E[i][1] += Enew[0][i][1]
            estimated[0] = (estimated[0] or Enew[1][0])

    return [E, estimated]


def toBackend_useActuator(actuator, instruction):
    """Direct interaction with actuators:
    Input: Actuator object, instruction, as int
    The instruction is to be adapted while the backend grows with new actuators
    100 is: turn ON, 0 is turn OFF, XX is DIM to XX%, 111 is TOGGLE
    X: switch to mode X etc...
    Returns True if the action was successful (at Backend level).
    Raises exception if the actuator has not the desired action.
    Does not catch backend exceptions
    """
    instruction = int(instruction)

    if actuator.action == PLC_ACTUATOR_TYPES[0]:
        if instruction == 0:
            return backendapi.set_actuator_state(actuator, RELAY_CMD[1])
        elif instruction == 100:
            return backendapi.set_actuator_state(actuator, RELAY_CMD[0])
        elif instruction == 111:
            return backendapi.set_actuator_state(actuator, RELAY_CMD[2])
        else:
            raise ActionException(str(instruction) + " is not a valid instruction for actuator " + str(actuator.id))
    elif actuator.action == PLC_ACTUATOR_TYPES[1]:
        if 0 <= instruction <= 100:
            return backendapi.set_actuator_state(actuator, DIMMER_CMD, instruction)
        else:
            raise ActionException(str(instruction) + " is not a valid instruction for actuator " + str(actuator))
    elif actuator.action == MODE_API_TYPE:
        return backendapi.set_actuator_state(actuator, MODE_CMD, str(instruction))
    else:
        raise ActionException(str(instruction) + " is not a valid instruction for actuator " + str(actuator.id))


def toBackend_actLoad(load, instruction):
    """Indirect action on loads. Acts on FIRST actuator ONLY
    Input: load object, desired instruction, as int:
    100 is: turn ON, 0 is turn OFF, XX is DIM to XX%, 111 is TOGGLE
    X: switch to mode X etc...
    Output: boolean, true if the action was successful at backend level
    Raises exception if the actuator has not the desired action.
    Does not catch backend exceptions
    """
    instruction = int(instruction)
    actuators = load.actuator_set

    if actuators:
        if instruction == 0 or instruction == 100 or instruction == 111:
            actuators = actuators.filter(action=PLC_ACTUATOR_TYPES[0]) | actuators.filter(action=PLC_ACTUATOR_TYPES[1])
            if actuators:
                for act in actuators:
                    if instruction == 0:
                        if act.action == PLC_ACTUATOR_TYPES[0]:
                            return backendapi.set_actuator_state(act, RELAY_CMD[1])
                        else:  # it is DIM
                            return backendapi.set_actuator_state(act, DIMMER_CMD, instruction)
                    elif instruction == 100:
                        if act.action == PLC_ACTUATOR_TYPES[0]:
                            return backendapi.set_actuator_state(act, RELAY_CMD[0])
                        else:  # it is DIM
                            return backendapi.set_actuator_state(act, DIMMER_CMD, instruction)
                    elif instruction == 111:
                        return backendapi.set_actuator_state(act, RELAY_CMD[2])
            else:
                raise ActionException("Toggle/dim unavailable for load" + load.name)
        elif instruction > 0 and instruction < 100:
            actuators = actuators.filter(action=PLC_ACTUATOR_TYPES[1])
            if actuators:
                for act in actuators:
                    return backendapi.set_actuator_state(act, DIMMER_CMD, instruction)
            else:
                raise ActionException("No actuator to dim load" + load.name)
        elif actuators.filter(action=MODE_API_TYPE): #check if there is a mode changing actuator
            actuators = actuators.filter(action=MODE_API_TYPE)
            if actuators:
                for act in actuators:
                    return backendapi.set_actuator_state(act, MODE_CMD, str(instruction))
            else:
                raise ActionException("No actuator to change mode of load" + load.name)
        else:
            raise ActionException(str(instruction) + " is not a valid instruction for load " + str(load.id))
    else:
        raise ActionException("No actuator on load " + load.name)


def toBackend_getActuatorStatus(actuators):
    """Gets the status of the actuator(s).
    Input: Actuator object
    Output: 0-100 depending on status
    """
    statuses = backendapi.get_actuators_state(actuators)
    return statuses


def act_possibleStatus(actuator):
    if actuator.action == PLC_ACTUATOR_TYPES[0]:
        possibleStatus = (0, 100)
    elif actuator.action == PLC_ACTUATOR_TYPES[1]:
        possibleStatus = [0, 100]
    elif actuator.action == MODE_API_TYPE:
        if actuator.load:
            possibleStatus = [str(modeName) for modeName in actuator.load.mode_set().values_list('name', flat=True)]
        else:
            possibleStatus = "UnknownLoadModes"
    else:
        possibleStatus = "UnknownActuatorType"
    return possibleStatus
