__author__ = 'Georgios Lilis, Thomas Guibentif'

import json
import traceback
import re
import requests
import sys
import cPickle


from django.http import HttpResponse, Http404
from django.shortcuts import get_object_or_404
from django.core import serializers
from django.contrib.auth import login
from django.views.generic import View
from django.views.decorators.http import require_GET, require_POST
from django.conf import settings as django_settings
from django.shortcuts import render

from BmsAPI.modelToBackend import *
from BmsApp.models import *
from BmsApp.modelForms import *
from utilities import unicodetodatetime, logger
from middlewareutilities import get_middleware_details
import zmq
from conf_general import PUB_MSG_TYPE
from conf_localizer import LOC_MID_ID, LOC_BUFFERNAME
from conf_openbms import GOOGLE_API_KEY
from threading import Lock

#------------------------
logger_user_act = logging.getLogger('django.useraction')
def save_loc_buffer(towrite):
    bufferfile = open(LOC_BUFFERNAME, 'wb', buffering=0)
    pickler = cPickle.Pickler(bufferfile, cPickle.HIGHEST_PROTOCOL)
    pickler.dump(towrite)
    bufferfile.close()

def read_loc_buffer():
    try:
        bufferfile = open(LOC_BUFFERNAME, 'rb', buffering=0)
        unpickler = cPickle.Unpickler(bufferfile)
        toread = unpickler.load()
        bufferfile.close()
        assert isinstance(toread, dict)
        return toread
    except (IOError, EOFError, AssertionError):
        logger.info('Location buffer file corrupted or missing, creating a new')  # The creation is done in save_loc_buffer()
        save_loc_buffer({})# Write an empty dictionary
        return {}

location_storage = read_loc_buffer()

#We keep them at global namespace instead of openning a socket at every request. This middleware is for handling location updates
_threadlock = Lock()

def start_location_mid():
    global locPUBsocket
    middleware = get_middleware_details(midid=LOC_MID_ID) #Execution will stop if it fails to find openBMS
    if middleware['middleware_type'] !='IP':
        logger.error('Could not find the location middleware in openBMS, verify the ID in conf_loc.py')
        sys.exit()
    ip = middleware['IP']
    rep_port = middleware['REP_port'] #not used here
    pub_port = middleware['PUB_port']
    logger.info('Starting the middleware "{0}" @ "{1}:{2}/{3}"'.format(middleware['name'], ip, rep_port,pub_port))

    context = zmq.Context()
    locPUBsocket = context.socket(zmq.PUB)
    locPUBsocket.bind('tcp://' + ip + ':' + pub_port)

from threading import Timer
Timer(2, start_location_mid, ()).start()
#------------------------

# ---------------------------------------------------------------------------------
#ErrorCodes of returned diction when status=false, for M2M communication. For human description the statuserror is availiable in returned dictionary
#1: The error is known and is due to malformed request parameters or missing some
#2: The requested resource either in uri or in parameters was not found
#3: There is Error due to permissions
#5: Validation Error during the DB writing, refer to the passed output from the framework
@require_GET
def viewRootAPI(request):
    #Only for reverse url
    return HttpResponse('Root API url, please specify the sub-function', status=200)

@require_GET
def viewGetDocs(request):
    #Provides the API documentation in Swagger UI format
    context = {"skip_main_js_files" : True, "view_title": "openBMS API docs "} #Necessary since swagger ui needs its own bootstrap and jquery version
    return render(request, 'docs.html', context)

# Basic views to explore the content of the database
@require_GET
def viewMiddleware(request, midID):
    """Return the middleware given by the midID"""
    #It is a single object, comes from .get() and converted to list for easy serialization
    m = [get_object_or_404(Middleware, pk=midID)]
    return HttpResponse(serializers.serialize('json', m), status=200, content_type='application/json')


@require_GET
def viewMiddlewareObjects(request, midID):
    """Return all objects of the middleware given by the midID"""
    #It is a single object, comes from .get() and converted to list for easy serialization
    m = get_object_or_404(Middleware, pk=midID)
    if not m.middleware_type=='DB':
        sensors = list(m.sensor_network_middleware_set.all())
        actuators = list(m.actuator_network_middleware_set.all())
        objects = sensors + actuators
    else:
        sensors = list(m.sensor_store_middleware_set.all())
        objects = sensors
    return HttpResponse(serializers.serialize('json', objects), status=200, content_type='application/json')


@require_GET
def viewMiddlewareRefs(request, midID):
    """Return the backend refs on the store_middleware or network_middleware of the midID.
    The type of returned refs depend of the type of the middleware requested.
    Returned ref are unique."""
    m = get_object_or_404(Middleware, pk=midID) #It is a single object, comes from .get()
    if not m.middleware_type=='DB':
        sensorsref = list(m.sensor_network_middleware_set.all().values_list('ref', flat=True).distinct())
        actuatorsref = list(m.actuator_network_middleware_set.all().values_list('ref', flat=True).distinct())
        refs = sorted(list(set(sensorsref+actuatorsref)))
        return HttpResponse(json.dumps(refs), status=200, content_type='application/json')
    else:
        return HttpResponse(json.dumps({'Error': 'Middleware with type \'DB\' and refs requested. '
                                                 'This will potentially cause overlapping to the refs'
                                                 ' of different type of actuator/sensor middlewares (ex ZWAVE-PLC'}),
                            status=400, content_type='application/json')


@require_GET
def viewMiddlewareIds(request, midID):
    """Return the frontend ids on the store_middleware or network_middleware of the midID.
    The type of returned ids depend of the type of the middleware requested.
    Returned ref are unique."""
    m = get_object_or_404(Middleware, pk=midID) #It is a single object, comes from .get()
    if not m.middleware_type=='DB':
        sensorsref = list(m.sensor_network_middleware_set.all().values_list('id', flat=True).distinct())
        actuatorsref = list(m.actuator_network_middleware_set.all().values_list('id', flat=True).distinct())
        ids = sorted(list(set(sensorsref+actuatorsref)))
    else:
        #since they are ids and unique no overlapping problem like viewMiddlewareRefs
        ids = sorted(list(m.sensor_store_middleware_set.all().values_list('id', flat=True).distinct()))
    return HttpResponse(json.dumps(ids), status=200, content_type='application/json')

@require_GET
def viewMiddlewareStoretoNet(request, midID):
    """Return the network middlewares that send to the given by store middleware with the midID. If one network middleware
    reports to multiple store middleware there would be error if you try to get all the sensor of the returned network middleware"""
    #It is a single object, comes from .get() and converted to list for easy serialization
    m = get_object_or_404(Middleware, pk=midID)
    if not m.middleware_type=='DB':
        return HttpResponse(json.dumps({'Error': 'Middleware with type \'DB\' is required for getting the network one'}),
                            status=400, content_type='application/json')
    else:
        netmid = list(m.sensor_store_middleware_set.all().values_list('network_middleware', flat=True).distinct())
    return HttpResponse(json.dumps(netmid), status=200, content_type='application/json')


@require_POST
def viewMiddlewareReportIP(request, midID):
    """Using this the middleware can report their IP. The openBMS can update its entries in IP"""
    m = get_object_or_404(Middleware, pk=midID)
    if request.GET.has_key('api_sec_key') and request.GET.get('api_sec_key')==getattr(django_settings, 'URL_SECRET_KEY', None):
        try:
            ipparameter = request.GET['IP']
            m.IP = ipparameter
            m.full_clean()
            m.save()
            return HttpResponse(json.dumps({'Status': True, 'StatusType': 'Updated'}), status=200, content_type='application/json')
        except Exception as e:
            traceback.print_exc()
            return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')
    else:
        return HttpResponse('Unauthorized', status=401)

@require_GET
def viewUnits(request):
    """Return the list of the Units in the system"""
    unitlist = []
    for u in Unit.objects.all():
        unitlist.append(viewUserLocation.prepare_unit(u))
    return HttpResponse(json.dumps({'Units': unitlist}),status=200, content_type='application/json')

@require_GET
def viewInterfaces(request, unitID):
    """Return the details about interfaces between all rooms in a unit"""
    i = RoomInterface.objects.filter(room_x__unit=unitID)  # Get all unit's interfaces
    return HttpResponse(serializers.serialize('json', i), status=200, content_type='application/json')

@require_GET
def viewRoomConstraints(request, roomID):
    """Return the room constraints details"""
    c = RoomConstraints.objects.filter(room=roomID)  # Get all room's constraints
    return HttpResponse(serializers.serialize('json', c), status=200, content_type='application/json')

@require_GET
def viewRoomInterfaces(request, roomID):
    """Return the room interfaces details"""
    set1 = RoomInterface.objects.filter(room_x=roomID)  # Get all room's constraints
    set2 = RoomInterface.objects.filter(room_y=roomID)  # Get all room's constraints

    interfaces_list = {}

    for i in (set1 | set2):
        n_id = i.room_x.pk
        if i.room_x.pk == int(roomID):
            n_id = i.room_y.pk

        interf_detail = {'interface_type': i.interface_type,
                         'rc_model': i.rc_model,
                         'surface': i.surface,
                         'thickness': i.thickness,
                         'interface_id': i.id}

        id_key = str(n_id)
        if id_key not in interfaces_list.keys():
            interfaces_list[id_key] = []
        interfaces_list[id_key].append(interf_detail)

    return HttpResponse(json.dumps({'Interfaces': interfaces_list}), status=200, content_type='application/json')

@require_GET
def viewSingleUnit(request, unitID):
    """Return the unit details"""
    u = get_object_or_404(Unit, pk=unitID)
    return HttpResponse(serializers.serialize('json', [u]),status=200, content_type='application/json')


@require_GET
def viewSingleSchedule_constraint(request, scheduleID):
    """Returns the json serialization of a list of the Loads of a given Type
    """
    s = get_object_or_404(SchedulingConstraints, pk=scheduleID)
    return HttpResponse(serializers.serialize('json', [s]), status=200, content_type='application/json')


@require_GET
def viewSingleRoom(request, roomID):
    """Return the room details"""
    r = get_object_or_404(Room, pk=roomID)
    return HttpResponse(serializers.serialize('json', [r]),status=200, content_type='application/json')


@require_GET
def viewSingleSensor(request, sensID):
    """Return the room details"""
    s = get_object_or_404(Sensor, pk=sensID)
    return HttpResponse(serializers.serialize('json', [s]),status=200, content_type='application/json')


@require_GET
def viewSingleActuator(request, actID):
    """Return the room details"""
    a = get_object_or_404(Actuator, pk=actID)
    return HttpResponse(serializers.serialize('json', [a]),status=200, content_type='application/json')


class viewSingleLoad(View):
    """View class for interacting with a single Load entity"""
    def get(self,request, loadID):
        """GET the Load details"""
        l = get_object_or_404(Load, pk=loadID)
        return HttpResponse(serializers.serialize('json', [l]),status=200, content_type='application/json')

    def put(self, request, loadID):
        """PUT new parameters for the Load"""
        l = get_object_or_404(Load, pk=loadID)
        if request.GET.has_key('api_sec_key') and request.GET.get('api_sec_key')==getattr(django_settings, 'URL_SECRET_KEY', None):
            try:
                #TODO BE DONE HERE
                #Take the object and manipulate it according to the PUT body parameters
                return HttpResponse(json.dumps({'Status': True, 'StatusType': 'THIS FEATURE IS NOT READY YET'}), status=200, content_type='application/json')
            except Exception as e:
                traceback.print_exc()
                return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')
        else:
            return HttpResponse('Unauthorized', status=401)


class viewSingleGenerator(View):
    """View class for interacting with a single Generator entity"""
    def get(self,request, genID):
        """GET the Generator details"""
        g = get_object_or_404(Generator, pk=genID)
        return HttpResponse(serializers.serialize('json', [g]),status=200, content_type='application/json')

    def put(self, request, genID):
        """PUT new parameters for the Generator"""
        g = get_object_or_404(Generator, pk=genID)
        if request.GET.has_key('api_sec_key') and request.GET.get('api_sec_key')==getattr(django_settings, 'URL_SECRET_KEY', None):
            try:
                #TODO BE DONE HERE
                #Take the object and manipulate it according to the PUT body parameters
                return HttpResponse(json.dumps({'Status': True, 'StatusType': 'THIS FEATURE IS NOT READY YET'}), status=200, content_type='application/json')
            except Exception as e:
                traceback.print_exc()
                return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')
        else:
            return HttpResponse('Unauthorized', status=401)


class viewSingleStorage(View):
    """View class for interacting with a single Storage entity"""
    def get(self,request, storID):
        """GET the Storage details"""
        s = get_object_or_404(Storage, pk=storID)
        return HttpResponse(serializers.serialize('json', [s]),status=200, content_type='application/json')

    def put(self, request, storID):
        """PUT new parameters for the Storage"""
        s = get_object_or_404(Storage, pk=storID)
        if request.GET.has_key('api_sec_key') and request.GET.get('api_sec_key')==getattr(django_settings, 'URL_SECRET_KEY', None):
            try:
                #TODO BE DONE HERE
                #Take the object and manipulate it according to the PUT body parameters
                return HttpResponse(json.dumps({'Status': True, 'StatusType': 'THIS FEATURE IS NOT READY YET'}), status=200, content_type='application/json')
            except Exception as e:
                traceback.print_exc()
                return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')
        else:
            return HttpResponse('Unauthorized', status=401)

class viewSingleEV(View):
    """View class for interacting with a single Storage entity"""
    def get(self, request, evID):
        """GET the Storage details"""
        ev = get_object_or_404(ElectricalVehicle, pk=evID)
        ev_det = get_phev_details(ev)
        return HttpResponse(json.dumps([ev_det]), status=200, content_type='application/json')

@require_GET
def viewUnit_middlewares(request, unitID):
    """Returns the json serialization of a list of the Middlewares presented in a Unit"""
    u = get_object_or_404(Unit, pk=unitID)
    sensors = Sensor.objects.filter(room__unit=unitID)  #Get all unit's sensors
    actuators = Actuator.objects.filter(room__unit=unitID)  #Get all unit's actuators
    middlewares = []
    for sensor in sensors:
        if sensor.network_middleware_id not in middlewares:
            middlewares.append(sensor.network_middleware_id)
        if sensor.store_middleware_id not in middlewares:
            middlewares.append(sensor.store_middleware_id)
    for actuator in actuators:
        if actuator.network_middleware_id not in middlewares:
            middlewares.append(actuator.network_middleware_id)

    unitmiddlewares = Middleware.objects.filter(id__in = middlewares)
    return HttpResponse(serializers.serialize('json', unitmiddlewares), status=200, content_type='application/json')


@require_GET
def viewUnit_rooms(request, unitID):
    """Returns the json serialization of a list of the Rooms in a Unit"""
    u = get_object_or_404(Unit, pk=unitID)
    return HttpResponse(serializers.serialize('json', u.room_set.all()), status=200, content_type='application/json')

@require_GET
def viewUnit_loads(request, unitID):
    """Returns the json serialization of a list of the Loads in a Unit"""
    u = get_object_or_404(Unit, pk=unitID)
    return HttpResponse(serializers.serialize('json', u.load_set().all()), status=200, content_type='application/json')

@require_GET
def viewUnit_load_types(request, unitID):
    """Returns the json serialization of a list of the Load Types in a Unit"""
    u = get_object_or_404(Unit, pk=unitID)
    return HttpResponse(serializers.serialize('json', u.deviceTypes_set().all()), status=200, content_type='application/json')

@require_GET
def viewUnit_generators(request, unitID):
    """Returns the json serialization of a list of the Generators in a Unit"""
    u = get_object_or_404(Unit, pk=unitID)
    g = Generator.objects.filter(sensor__room__unit_id=unitID) | Generator.objects.filter(actuator__room__unit_id=unitID)
    return HttpResponse(serializers.serialize('json', g), status=200, content_type='application/json')

@require_GET
def viewUnit_storage(request, unitID):
    """Returns the json serialization of a list of the Storage elements in a Unit"""
    u = get_object_or_404(Unit, pk=unitID)
    s = Storage.objects.filter(sensor__room__unit_id=unitID) | Storage.objects.filter(actuator__room__unit_id=unitID)
    return HttpResponse(serializers.serialize('json', s), status=200, content_type='application/json')

@require_GET
def viewUnit_phevs(request, unitID):
    """Returns the json serialization of a list of the Storage elements in a Unit"""
    evs = ElectricalVehicle.objects.filter(battery__sensor__room__unit_id=unitID) | ElectricalVehicle.objects.filter(battery__actuator__room__unit_id=unitID)
    evs_detailed = [get_phev_details(e) for e in evs]  # Explore the linked objects

    return HttpResponse(json.dumps(evs_detailed), status=200, content_type='application/json')


def get_phev_details(ev):
    ev_data = serializers.serialize('json', [ev])
    ret_item = json.loads(ev_data)
    ret_item = ret_item[0]

    # Copy the constraints details
    schedule_id = ev.schedule.pk  # get the ID of the schedule object
    schd_constr = SchedulingConstraints.objects.filter(id=schedule_id)  # From the ID, get the object
    schd_constr_json = json.loads(serializers.serialize('json', schd_constr))
    ret_item['fields']['schedule_constraints'] = schd_constr_json[0]['fields']

    # Copy the battery param
    batt_id = ev.battery.pk  # get the ID of the schedule object
    batt_obj = Storage.objects.filter(id=batt_id)  # From the ID, get the object
    batt_json = json.loads(serializers.serialize('json', batt_obj))
    ret_item['fields']['battery_details'] = batt_json[0]['fields']

    return ret_item

@require_GET
def viewRoom_loads(request, roomID):
    """Returns the json serialization of a list of the Loads in a Room"""
    r = get_object_or_404(Room, pk=roomID)
    return HttpResponse(serializers.serialize('json', r.load_set().all()), status=200, content_type='application/json')

class viewRoomSettings(View):
    """Returns the json serialization of a list of the settings in a Room
    or it accepts the json using post to update the DB
    """
    def get(self, request, roomID, setID):
        """GET the room settings
        """
        r = get_object_or_404(Room, pk=roomID)
        return HttpResponse(serializers.serialize('json', r.roomsetting_set.all()), status=200, content_type='application/json')

    def post(self, request, roomID, setID):
        """POST an new room setting, the setID is not needed
        """
        if not request.user.is_authenticated():
            return HttpResponse('Unauthorized', status=401)
        r = get_object_or_404(Room, pk=roomID)
        try:
            parameters = json.loads(request.body)
            if 'csrfmiddlewaretoken' in parameters:
                del parameters['csrfmiddlewaretoken']
            for key in parameters.keys():
                if not key in RoomSetting._meta.get_all_field_names():
                    return HttpResponse(json.dumps({'Status': False, 'StatusError': 'Unknown parameter passed with POST', 'ErrorCode': 1}), status=400, content_type='application/json')
            if not 'set_value' in parameters.keys() or not 'set_quantity' in parameters.keys():
                return HttpResponse(json.dumps({'Status': False, 'StatusError': 'set_value or set_quantity not passed with the POST', 'ErrorCode': 1}), status=400, content_type='application/json')
            if not parameters['set_quantity'] in dict(Sensor.ROOM_CHOICES).keys():
                return HttpResponse(json.dumps({'Status': False, 'StatusError': 'set_quantity must be one of' + str(Sensor.ROOM_CHOICES), 'ErrorCode': 1}), status=400, content_type='application/json')
            unicodetodatetime(parameters)
            sett = RoomSetting(room=r, **parameters)
            sett.full_clean()
            sett.save()  # It will save here since it passed the exception
            return HttpResponse(json.dumps({'Status': True, 'StatusType': 'Created'}), status=201, content_type='application/json')
        except ValidationError as e:
            return HttpResponse(json.dumps({'Status': False, 'StatusError': repr(e), 'ErrorCode': 5}), status=400, content_type='application/json')
        except Exception as e:
            traceback.print_exc()
            return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')

    def put(self, request, roomID, setID):
        """PUT is used for updating the room setting, (we know the resourse id as per RESTful convetion)
        """
        if not request.user.is_authenticated():
            return HttpResponse('Unauthorized', status=401)
        try:
            sett = RoomSetting.objects.get(id=setID)
            parameters = json.loads(request.body)
            if 'csrfmiddlewaretoken' in parameters:
                del parameters['csrfmiddlewaretoken']
            for key in parameters.keys():
                if not key in RoomSetting._meta.get_all_field_names():
                    return HttpResponse(json.dumps({'Status': False, 'StatusError': 'Unknown parameter passed with PUT', 'ErrorCode': 1}), status=400, content_type='application/json')
            unicodetodatetime(parameters)
            for key in parameters.keys():
                setattr(sett, key, parameters[key])
            sett.full_clean()  # It will raise exception if not valid creation
            sett.save()  # It will save here since it passed the exception
            return HttpResponse(json.dumps({'Status': True, 'StatusType': 'Updated'}), status=200, content_type='application/json')
        except RoomSetting.DoesNotExist:
            return HttpResponse(json.dumps({'Status': False, 'StatusError': 'Nothing changed, id not found', 'ErrorCode': 2}), status=404, content_type='application/json')
        except ValidationError as e:
            return HttpResponse(json.dumps({'Status': False, 'StatusError': repr(e), 'ErrorCode': 5}), status=400, content_type='application/json')
        except Exception as e:
            traceback.print_exc()
            return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')


class viewRoomRemark(View):
    """Returns the json serialization of a list of the RoomRemark for a Room
    or it accepts the json using post to update the DB
    """
    def get(self, request, roomID):
        """GET all room info
        """
        r = get_object_or_404(Room, pk=roomID)
        return HttpResponse(serializers.serialize('json', r.roomremark_set.all()), status=200, content_type='application/json')

    def post(self, request, roomID):
        """POST an new room info
        """
        if not request.user.is_authenticated():
            return HttpResponse('Unauthorized', status=401)
        r = get_object_or_404(Room, pk=roomID)
        try:
            parameters = json.loads(request.body)
            if 'csrfmiddlewaretoken' in parameters:
                del parameters['csrfmiddlewaretoken']
            for key in parameters.keys():
                if not key in RoomRemark._meta.get_all_field_names():
                    return HttpResponse(json.dumps({'Status': False, 'StatusError': 'Unknown parameter passed with POST', 'ErrorCode': 1}), status=400, content_type='application/json')
            RoomRemark.objects.create(room=r, info=parameters['info'])
            # Some validation checks because we create
            if not 'info' in parameters.keys():
                return HttpResponse(json.dumps({'Status': False, 'StatusError': 'info parameter not passed with the POST', 'ErrorCode': 1}), status=400, content_type='application/json')
            return HttpResponse(json.dumps({'Status': True, 'StatusType': 'Created'}), status=201, content_type='application/json')
        except ValidationError as e:
            return HttpResponse(json.dumps({'Status': False, 'StatusError': repr(e), 'ErrorCode': 5}), status=400, content_type='application/json')
        except Exception as e:
            traceback.print_exc()
            return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')

    def delete(self, request, roomID):
        """DELETE to delete all the room info
        """
        if not request.user.is_authenticated():
            return HttpResponse('Unauthorized', status=401)
        r = get_object_or_404(Room, pk=roomID)
        try:
            info = RoomRemark.objects.filter(room=r)
            info.delete()
            return HttpResponse(json.dumps({'Status': True, 'StatusType': 'Deleted'}), status=200, content_type='application/json')
        except Exception as e:
            traceback.print_exc()
            return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')


class viewLoadSettings(View):
    """Returns the json serialization of a list of the Scedules on a Load
    or it accepts the json using post to update the DB
    """
    def get(self, request, loadID, schID):
        """GET all the load settings
        """
        l = get_object_or_404(Load, pk=loadID)
        return HttpResponse(serializers.serialize('json', l.loadsetting_set.all()), status=200, content_type='application/json')

    def post(self, request, loadID, schID):
        """POST an new load setting, the schID is not needed
        """
        if not request.user.is_authenticated():
            return HttpResponse('Unauthorized', status=401)
        l = get_object_or_404(Load, pk=loadID)
        try:
            parameters = json.loads(request.body)
            if 'csrfmiddlewaretoken' in parameters:
                del parameters['csrfmiddlewaretoken']
            for key in parameters.keys():
                if not key in LoadSetting._meta.get_all_field_names():
                    return HttpResponse(json.dumps({'Status': False, 'StatusError': 'Unknown parameter passed with POST', 'ErrorCode': 1}), status=400, content_type='application/json')
            if not 'load_action' in parameters.keys():
                return HttpResponse(json.dumps({'Status': False, 'StatusError': 'load_action not passed with the POST', 'ErrorCode': 1}), status=400, content_type='application/json')
            if not parameters['load_action'] in dict(Actuator.ACTION_CHOICES).keys():
                return HttpResponse(
                    json.dumps({'Status': False, 'StatusError': 'set_quantity must be one of' + str(Actuator.ACTION_CHOICES), 'ErrorCode': 1}), status=400, content_type='application/json')
            unicodetodatetime(parameters)
            sch = LoadSetting(load=l, **parameters)
            sch.full_clean()  # It will raise exception if not valid creation
            sch.save()  # It will save here since it passed the exception
            return HttpResponse(json.dumps({'Status': True, 'StatusType': 'Created'}), status=201, content_type='application/json')
        except ValidationError as e:
            return HttpResponse(json.dumps({'Status': False, 'StatusError': repr(e), 'ErrorCode': 5}), status=400, content_type='application/json')
        except Exception as e:
            traceback.print_exc()
            return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')

    def put(self, request, loadID, schID):
        """PUT is used for updating the load setting, (we know the resourse id as per RESTful convetion)
        """
        if not request.user.is_authenticated():
            return HttpResponse('Unauthorized', status=401)
        try:
            sch = LoadSetting.objects.get(id=schID)
            parameters = json.loads(request.body)
            if 'csrfmiddlewaretoken' in parameters:
                del parameters['csrfmiddlewaretoken']
            for key in parameters.keys():
                if not key in LoadSetting._meta.get_all_field_names():
                    return HttpResponse(json.dumps({'Status': False, 'StatusError': 'Unknown parameter passed with POST', 'ErrorCode': 1}), status=400, content_type='application/json')
            unicodetodatetime(parameters)
            for key in parameters.keys():
                setattr(sch, key, parameters[key])
            sch.full_clean()  # It will raise exception if not valid creation
            sch.save()  # It will save here since it passed the exception
            return HttpResponse(json.dumps({'Status': True, 'StatusType': 'Updated'}), status=200, content_type='application/json')
        except LoadSetting.DoesNotExist:
            return HttpResponse(json.dumps({'Status': False, 'StatusError': 'Nothing changed, id not found', 'ErrorCode': 2}), status=400, content_type='application/json')
        except ValidationError as e:
            return HttpResponse(json.dumps({'Status': False, 'StatusError': repr(e), 'ErrorCode': 5}), status=400, content_type='application/json')
        except Exception as e:
            traceback.print_exc()
            return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')


@require_GET
def viewType_loads(request, typeID):
    """Returns the json serialization of a list of the Loads of a given Type
    """
    t = get_object_or_404(DeviceType, pk=typeID)
    return HttpResponse(serializers.serialize('json', t.load_set()), status=200, content_type='application/json')

@require_GET
def viewRoom_sensors(request, roomID):
    """Returns the json serialization of a list of the Sensors in the room
    """
    r = get_object_or_404(Room, pk=roomID)
    return HttpResponse(serializers.serialize('json', r.sensor_set.all()), status=200, content_type='application/json')


@require_GET
def viewRoom_actuators(request, roomID):
    """Returns the json serialization of a list of the Actuators in the room
    """
    r = get_object_or_404(Room, pk=roomID)
    return HttpResponse(serializers.serialize('json', r.actuator_set.all()), status=200, content_type='application/json')


@require_GET
def viewLoad_sensors(request, loadID):
    """Returns the json serialization of a list of the Sensors on a Load
    """
    l = get_object_or_404(Load, pk=loadID)
    return HttpResponse(serializers.serialize('json', l.sensor_set.all()), status=200, content_type='application/json')

@require_GET
def viewGeneration_sensors(request, genID):
    """Returns the json serialization of a list of the Sensors on a Load
    """
    g = get_object_or_404(Generator, pk=genID)
    return HttpResponse(serializers.serialize('json', g.sensor_set.all()), status=200, content_type='application/json')

@require_GET
def viewStorage_sensors(request, storID):
    """Returns the json serialization of a list of the Sensors on a Load
    """
    s = get_object_or_404(Storage, pk=storID)
    return HttpResponse(serializers.serialize('json', s.sensor_set.all()), status=200, content_type='application/json')

@require_GET
def viewLoad_actuators(request, loadID):
    """Returns the json serialization of a list of the Actuators on a Load
    """
    l = get_object_or_404(Load, pk=loadID)
    return HttpResponse(serializers.serialize('json', l.actuator_set.all()), status=200, content_type='application/json')


# --------------------------------------------------------------------------------------
# The main function to retrieve the sensor data from the backend. It is capable of handling both
# requests for datapoint(last or not) or timeseries
@require_GET
def viewTimeseriesOrDatapoint(request, sensors, initTime=None, endTime=None, timeRes=None, includeSenRef=True, includeSenID=False, nobuffer=False):
    """It is for sensorIDs.
    If res =  0 then doesnt do any computations or resampling (ask for raw) . They are pure data.
    if res <> 0 then it interpolates step or linear based on the type of sensor (load or room) as explained in
    toBackend_resampleDataOrPoint
    initTime = None and endTime=None will yield the last point request by toBackend_resampleDataOrPoint
    Returns are {[<sensoref>if includeSenRef:{"<Quantity>":[[<unixTime>, <value>]...]...}...}.
    Nobuffer is for the poll url."""
    if not sensors:
        return HttpResponse(json.dumps({'Error': 'No sensor found'}), status=404, content_type='application/json')
    try:
        if timeRes is not None and int(timeRes) != 0:
            timeRes = int(timeRes)
            try:
                timeVector = generateTimeVector(initTime, endTime, timeRes)
            except Exception as e:
                return HttpResponse(json.dumps({'Bad Request': repr(e)}), status=400, content_type='application/json')
            datas = toBackend_resampleDataOrPoint(sensors=sensors, timeVector=timeVector, interpolation='auto')
        else:
            #Last point request if inittime and endtime is not given
            #or full raw if it is given since timeres = 0
            #in case of last point request, timeVector will have them both None and will force the toBackend_getSensorData
            #to get last point
            if initTime:
                initTime = strToUnix(initTime)
            if endTime:
                endTime = strToUnix(endTime)
            timeVector = [initTime, endTime]
            if not nobuffer:
                datas = toBackend_resampleDataOrPoint(sensors=sensors, timeVector=timeVector, interpolation='raw', fillvalue=None)
            else:  # we need poll, directly asking the toBackend_getSensorData
                datas = toBackend_getSensorData(sensors, initTime, endTime, timeRes, nobuffer=nobuffer)

        if not len(sensors) == len(datas):
            raise Exception('Number of data series provided not equal to the number of labels')


        toreturn = dict()
        if includeSenID:
            if not includeSenRef ^ includeSenID:
                raise Exception('Cannot be both the inclusion of ref and id of a sensor true')
            for sensor in sensors:
                toreturn[sensor.id] = dict()
            for index, sensor in enumerate(sensors):
                toreturn[sensor.id]['Measure'] = sensor.measure
                toreturn[sensor.id]['Time'] = returnFormat([datas[index]])[0]
                toreturn[sensor.id]['Value'] = returnFormat([datas[index]])[1]
        elif includeSenRef :
            for sensor in sensors:
                toreturn[sensor.ref] = dict()
            for index, sensor in enumerate(sensors):
                toreturn[sensor.ref][sensor.measure] = returnFormat([datas[index]])
        else:
            for index, sensor in enumerate(sensors):
                toreturn[sensor.measure] = returnFormat([datas[index]])

        return HttpResponse(json.dumps(toreturn), status=200, content_type='application/json')
    except Exception as e:
        traceback.print_exc()
        return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')


# ------------------------------------------------------------------------------------------------------------
# Basic views to retrieve data from the backend using model objects ("Sensors") directly
# Note that a Sensor object, that we also call a quantity (corresponding to one line in the Sensors table) is not
# a real sensor but one of the physical quantities that that sensor can measure
@require_GET
def viewTimeseriesQuantity(request, sensID, initTime, endTime, timeRes):
    """Returns the timeseries for one particular measured quantity between given times
    Input: DB id of a Sensor object, init and end Times and resolution (in seconds)
    if timeRes=0 returns raw data
    if initTime=endTime returns one time point, the direct ancestor of initTime
    Output: {<quantity_Name>: [[<unixTime>,<value>]...]}
    Note that a Sensor object, that we also call a quantity (corresponding to one line in the Sensors table) is not
    a real sensor but one of the physical quantities that that sensor can measure
    """
    s = Sensor.objects.filter(id=sensID)
    return viewTimeseriesOrDatapoint(request, s, initTime, endTime, timeRes, includeSenRef=False)


@require_GET
def viewLastQuantity(request, sensID):
    """Returns the last point for a given measured quantity (at present time)
    Input: Sensor object DB id
    Output: last datapoint: {"<quantity>":[<Time>,<Value>]}
    Note that a Sensor object, that we also call a quantity (corresponding to one line in the Sensors table) is not
    a real sensor but one of the physical quantities that that sensor can measure
    """
    s = Sensor.objects.filter(id=sensID)
    return viewTimeseriesOrDatapoint(request, s, includeSenRef=False)


@require_GET
def viewPollQuantity(request, sensID):
    """Forces a polling to backend for realtime sensor value.
    CAUTION! It creates a high overhead to backend and use must at all cases be avoided.
    """
    s = Sensor.objects.filter(id=sensID)
    return viewTimeseriesOrDatapoint(request, s, includeSenRef=False, nobuffer=True)


@require_GET
def viewTimeseriesSensor(request, sensREF, initTime, endTime, timeRes):
    """Returns the timeseries for all quantities measured by a sensor at REF
    Input: sensor reference (backendID), starting and final time, time resolution
    if timeRes=0 returns raw data
    if initTime=endTime returns one time point, the direct ancestor of initTime
    Output: dict of the quantities measurable by that sensor: {<sensorid>:{"<Quantity>":[[<unixTime>,<value>]...]...}
    """
    s = Sensor.objects.filter(ref=sensREF)
    return viewTimeseriesOrDatapoint(request, s, initTime, endTime, timeRes)


@require_GET
def viewLastSensor(request, sensREF):
    """Returns all the last points for a given sensor (at present time, empty if the sensor is unavailable)
    Input: sensor reference (backendID)
    Output: last datapoints: {<sensorid>:{"<quantity>":[<Time>,<Value>]...}...}
    Note that a Sensor object, that we also call a quantity (corresponding to one line in the Sensors table) is not
    a real sensor but one of the physical quantities that that sensor can measure
    """
    s = Sensor.objects.filter(ref=sensREF)
    return viewTimeseriesOrDatapoint(request, s)

@require_GET
def viewLastGenerator(request, genID):
    """Returns all last data available for one Generator
    Input: Generator DB id
    Output: last datapoints:{"<quantity>":[<Time>,Value]}
    """
    g = get_object_or_404(Generator, id=genID) #Unused...
    #Fake response, take one pc powers as generation...ID=[11,10,7]
    #g.current_production
    pc_ld_ids = [11,10,7]
    import random
    l = get_object_or_404(Load, id=random.choice(pc_ld_ids))
    sensors = l.sensor_set.filter(measure='P')
    if not sensors:
        return HttpResponse(json.dumps({'Warning': 'That Generator has no sensors connected'}), status=501, content_type='application/json')
    else:
        return viewTimeseriesOrDatapoint(request, sensors, includeSenRef=False)

@require_GET
def viewLastGeneratorsUnit(request, unitID):
    """Returns all last data available for the Unit's Generators
    Input: Unit DB id
    Output: last datapoints:{"<quantity>":[<Time>,Value]}
    """
    u = get_object_or_404(Unit, id=unitID)
    #Fake response, take one pc powers as generation...ID=[11,10,7]
    pc_ld_ids = [11,10,7]
    sensors = Sensor.objects.filter(room__unit=u).filter(measure='P').filter(load__in=pc_ld_ids)
    return viewTimeseriesOrDatapoint(request, sensors, includeSenID=True, includeSenRef=False)

@require_GET
def viewLastStorage(request, storID):
    """Returns all last data available for one Storage
    Input: Storage DB id
    Output: last datapoints:{"<quantity>":[<Time>,Value]}
    """
    s = get_object_or_404(Storage, id=storID) #Unused...
    #Fake response, take one pc screen powers as storage...ID=[9,6,2]
    pcscreen_ld_ids = [9,6,2]
    import random
    l = get_object_or_404(Load, id=random.choice(pcscreen_ld_ids))
    sensors = l.sensor_set.filter(measure='P')
    #Check to make negative...
    if not sensors:
        return HttpResponse(json.dumps({'Warning': 'That Storage has no sensors connected'}), status=501, content_type='application/json')
    else:
        return viewTimeseriesOrDatapoint(request, sensors, includeSenRef=False)

@require_GET
def viewLastStorageUnit(request, unitID):
    """Returns all last data available for the Unit's Storage elements
    Input: Unit DB id
    Output: last datapoints:{"<quantity>":[<Time>,Value]}
    """
    u = get_object_or_404(Unit, id=unitID)
    #Fake response, take one pc screen powers as storage...ID=[9,6,2]
    pcscreen_ld_ids = [9,6,2]
    sensors = Sensor.objects.filter(measure='P').filter(load__in=pcscreen_ld_ids).filter(room__unit=u.pk)
    #Check to make negative...
    return viewTimeseriesOrDatapoint(request, sensors, includeSenID=True, includeSenRef=False)

@require_GET
def viewLastLoad(request, loadID):
    """Returns all last data available for one Load
    Input: load DB id
    Output: last datapoints:{"<quantity>":[<Time>,Value]}
    """
    l = get_object_or_404(Load, id=loadID)
    sensors = l.sensor_set.exclude(measure__in=Sensor.aux_measures)
    if not sensors:
        return HttpResponse(json.dumps({'Warning': 'That Load has no sensors connected'}), status=501, content_type='application/json')
    else:
        return viewTimeseriesOrDatapoint(request, sensors, includeSenRef=False)

@require_GET
def viewLastLoadQuantity(request, loadID, quantity):
    """Returns last data for quantity available for one Load
    Input: load DB id, quantity
    Output: last datapoints:{"<quantity>":[<Time>,Value]}
    """
    l = get_object_or_404(Load, id=loadID)
    sensors = l.sensor_set.filter(measure=quantity)
    if not sensors:
        return HttpResponse(json.dumps({'Warning': 'That Load has no sensors of that type connected'}), status=501, content_type='application/json')
    else:
        return viewTimeseriesOrDatapoint(request, sensors, includeSenRef=False)


@require_GET
def viewTimeseriesLoad(request, loadID, initTime, endTime, timeRes):
    """Returns the timeseries for ALL Sensors connected to a Load
    Input: load DB id, starting and final time, time resolution
    if timeRes=0 returns raw data
    if initTime=endTime returns one time point, the direct ancestor of initTime
    Output: dict of the quantities measurable on that load: {<sensorid>:{"<Quantity>":[[<unixTime>,<value>]...]...}...}
    """

    l = get_object_or_404(Load, id=loadID)
    sensors = l.sensor_set.exclude(measure__in=Sensor.aux_measures)
    if not sensors:
        return HttpResponse(json.dumps({'Warning': 'That Load has no sensors connected'}), status=501, content_type='application/json')
    else:
        return viewTimeseriesOrDatapoint(request, sensors, initTime, endTime, timeRes)


@require_GET
def viewTimeseriesLoadQuantity(request, loadID, quantity, initTime, endTime,timeRes):
    """Returns the timeseries for ONE Sensor connected to a load with a given quantity
    Input: load DB id, quantity to measure "P", "Q", "S", "PF" starting and final time, time resolution
    if timeRes=0 returns raw data
    if initTime=endTime returns one time point, the direct ancestor of initTime
    Output: dict of the quantities measurable on that load: {<sensorid>:{"<Quantity>":[[<unixTime>,<value>]...]...}...}
    """

    l = get_object_or_404(Load, id=loadID)
    sensors = l.sensor_set.filter(measure=quantity)
    if not sensors:
        return HttpResponse(json.dumps({'Warning': 'That Load has no sensors of that type connected'}), status=501, content_type='application/json')
    else:
        return viewTimeseriesOrDatapoint(request, sensors, initTime, endTime, timeRes)


@require_GET
def viewLastLoadsRoom(request, roomID):
    """Returns all last data available for a room excluding room measures for all P Q S PF
    Input: Room DB id
    Output: last datapoints: {<sensorref>:{"<quantity>":[<Time>,Value]}...}
    """
    #there is a bug here when sensor will be attached to Generation and Storage.
    r = get_object_or_404(Room, id=roomID)
    sensors = r.sensor_set.filter(measure__in=Sensor.load_measures).exclude(load=None)
    return viewTimeseriesOrDatapoint(request, sensors, includeSenID=True, includeSenRef=False)


@require_GET
def viewLastRoom(request, roomID):
    """Returns all last data available for a room excluding load measures
    Input: room DB id
    Output: last datapoints: {<sensorref>:{"<quantity>":[<Time>,Value]}...}
    """
    r = get_object_or_404(Room, id=roomID)
    sensors = r.sensor_set.filter(measure__in=Sensor.room_measures+Sensor.batterylvl_measure)
    return viewTimeseriesOrDatapoint(request, sensors)


@require_GET
def viewTimeseriesRoom(request, roomID, initTime, endTime, timeRes):
    """Returns the timeseries for all Sensors in a room  excluding load measures
    Input: room DB id, starting and final time, time resolution
    if timeRes=0 returns raw data
    if initTime=endTime returns one time point, the direct ancestor of initTime
    Output: dict of the quantities measurable in that room: {<sensoref>:{"<Quantity>":[[<unixTime>, <value>]...]...}...}
    """
    r = get_object_or_404(Room, id=roomID)
    sensors = r.sensor_set.filter(measure__in=Sensor.room_measures+Sensor.batterylvl_measure)
    return viewTimeseriesOrDatapoint(request, sensors, initTime, endTime, timeRes)


@require_GET
def viewPollRoom(request, roomID):
    """Returns all last data available for a room excluding load measures
    Input: room DB id
    Output: last datapoints: {<sensorref>:{"<quantity>":[<Time>,Value]}...}
    """
    r = get_object_or_404(Room, id=roomID)
    sensors = r.sensor_set.filter(measure__in=Sensor.room_measures+Sensor.batterylvl_measure)
    return viewTimeseriesOrDatapoint(request, sensors, includeSenRef=False, nobuffer=True)


@require_GET
def viewTimeseriesRoomQuantity(request, roomID, quantity, initTime, endTime, timeRes):
    """Returns the timeseries for one Sensor in a room with a given quantity
    Input: room DB id, quantity to measure "TEM", "HUM", "LUM", "PIR", "BAT" starting and final time, time resolution
    if timeRes=0 returns raw data
    if initTime=endTime returns one time point, the direct ancestor of initTime
    Can also return aggregated data for 'P', 'Q', 'S' or 'pf' but not urlAPI leads here (moved to other views)
    Output: dict of the quantities measurable on that room: {<sensoref>:{"<Quantity>":[[<unixTime>,<value>]...]...}...}
    """

    r = get_object_or_404(Room, id=roomID)

    if quantity in Sensor.room_measures + Sensor.batterylvl_measure:
        sensor = r.sensor_set.filter(measure=quantity)
        return viewTimeseriesOrDatapoint(request, sensor, initTime, endTime, timeRes, False)
    elif quantity in Sensor.load_measures:
        return viewTimeseriesRoomAggregated(request, roomID, initTime, endTime, timeRes, quantity)


@require_GET
def viewLastRoomQuantity(request, roomID, quantity):
    """Returns the last for a Sensor in a room with a given quantity
    Input: room DB id, quantity to measure "TEM", "HUM", "LUM", "PIR", "BAT"
    Can also return aggregated data for 'P', 'Q', 'S' or 'pf' but not urlAPI leads here (moved to other views)
    Output: dict of the quantities measurable on that room: {<sensoref>:{"<Quantity>":[[<unixTime>,<value>]...]...}...}
    """
    r = get_object_or_404(Room, id=roomID)
    if quantity in Sensor.room_measures+Sensor.batterylvl_measure:
        sensor = r.sensor_set.filter(measure=quantity)
        return viewTimeseriesOrDatapoint(request, sensor, includeSenRef=False)
    elif quantity in Sensor.load_measures:
        return viewLastRoomAggregated(request, roomID, quantity)

# -----------------------------------------------------------------------------------------------------------------------
#Advanced views returning aggregated data (sum of phasors)
@require_GET
def viewTimeseriesOrDatapointAggregated(request, loads, name, timeVector=None, quantities=('P', 'Q', 'S', 'PF')):
    """Like viewTimeseriesorDatapoint but with computational and resamplign abilities since the focus is on aggregated
    powers. It accepts loads not sensors, and does the best to find the powers. It flag as estimated when
    not 100% possible to extract powers from given sensors or generally a load misses power measure all together."""
    try:
        summation, estimated = sumPower(loads, timeVector, quantities)
        retVal = {'Name': name, 'estimated': {}}
        for q in quantities:
            retVal[q] = returnFormat(summation[q])
            retVal['estimated'][q] = estimated[q]

        return HttpResponse(json.dumps(retVal), status=200, content_type='application/json')
    except Exception as e:
        traceback.print_exc()
        return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')


@require_GET
def viewLastRoomAggregated(request, roomID, quantity=('P', 'Q', 'S', 'PF')):
    """Computation of the aggregated consumption of one room at present time
    Note that we take simply the last value measured, may not be updated...
    Input: room DB id
    Output: aggregated consumption: {room_name:<roomName>,"P":<P>,"Q":<Q>,"S":<S>,"pf":<PF>,estimated:<estimated>}
    <X> are in the standard form [<unixTime>,<value>]
    <estimated> is {"X":<true/false>...}
    is true if there are not enough sensors to monitor all the loads in the room.
    """
    r = get_object_or_404(Room, pk=roomID)
    return viewTimeseriesOrDatapointAggregated(request, r.load_set(), r.name, quantities=quantity)


@require_GET
def viewLastRoomTypeAggregated(request, roomID, typeID, quantity=('P', 'Q', 'S', 'PF')):
    """Computation of the aggregated consumption of one room at present time, taking only load of a given type
    Note that we take simply the last value measured, may not be updated...
    Input: room DB id
    Output: aggregated consumption: {room_name:<roomName>,"P":<P>,"Q":<Q>,"S":<S>,"pf":<PF>,estimated:<estimated>}
    <X> are in the standard form [<unixTime>,<value>]
    <estimated> is {"X":<true/false>...}
    is true if there are not enough sensors to monitor all the loads in the room.
    """
    r = get_object_or_404(Room, pk=roomID)
    t = get_object_or_404(DeviceType, pk=typeID)
    return viewTimeseriesOrDatapointAggregated(request, r.load_set().filter(device__type=t), r.name, quantities=quantity)


@require_GET
def viewTimeseriesRoomAggregated(request, roomID, initTime, endTime, timeRes, quantity=('P', 'Q', 'S', 'PF')):
    """Computation of the aggregated consumption of one room between initTime and endTime
    Input: room DB id
    Output: aggregated consumption: {room_name:<roomName>,"P":<P>,"Q":<Q>,"S":<S>,"pf":<PF>,estimated:<estimated>}
    <X> are in the standard form [[<unixTime>,<value>]...]
    <estimated> is {"X":<true/false>...}
    is true if there are not enough sensors to monitor all the loads in the room.
    """
    r = get_object_or_404(Room, pk=roomID)
    if timeRes:
        timeRes = int(timeRes)
        try:
            timeVector = generateTimeVector(initTime, endTime, timeRes)
        except Exception as e:
            return HttpResponse(json.dumps({'Bad Request': repr(e)}), status=400, content_type='application/json')
    else:
        timeVector = [strToUnix(initTime), strToUnix(endTime)]
    return viewTimeseriesOrDatapointAggregated(request, r.load_set(), r.name, timeVector, quantity)


@require_GET
def viewTimeseriesUnitAggregated(request, unitID, initTime, endTime, timeRes, quantity=('P', 'Q', 'S', 'PF')):
    """Computation of the aggregated consumption of one room between initTime and endTime
    Input: room DB id
    Output: aggregated consumption: {room_name:<roomName>,"P":<P>,"Q":<Q>,"S":<S>,"pf":<PF>,estimated:<estimated>}
    <X> are in the standard form [[<unixTime>,<value>]...]
    <estimated> is {"X":<true/false>...}
    is true if there are not enough sensors to monitor all the loads in the room.
    """
    u = get_object_or_404(Unit, pk=unitID)
    if timeRes:
        timeRes = int(timeRes)
        timeVector = generateTimeVector(initTime, endTime, timeRes)
    else:
        timeVector = [strToUnix(initTime), strToUnix(endTime)]
    return viewTimeseriesOrDatapointAggregated(request, u.load_set(), u.name, timeVector, quantity)


@require_GET
def viewTimeseriesUnitTypeAggregated(request, unitID, typeID, initTime, endTime, timeRes, quantity=('P', 'Q', 'S', 'PF')):
    """Computation of the aggregated consumption all loads of a given type in a unit, between initTime and endTime
    Input: room DB id
    Output: aggregated consumption: {room_name:<roomName>,"P":<P>,"Q":<Q>,"S":<S>,"pf":<PF>,estimated:<estimated>}
    <X> are in the standard form [[<unixTime>,<value>]...]
    <estimated> is {"X":<true/false>...}
    is true if there are not enough sensors to monitor all the loads in the room.
    """
    u = get_object_or_404(Unit, pk=unitID)
    t = get_object_or_404(DeviceType, pk=typeID)
    if timeRes:
        timeRes = int(timeRes)
        try:
            timeVector = generateTimeVector(initTime, endTime, timeRes)
        except Exception as e:
            return HttpResponse(json.dumps({'Bad Request': repr(e)}), status=400, content_type='application/json')
    else:
        timeVector = [strToUnix(initTime), strToUnix(endTime)]
    return viewTimeseriesOrDatapointAggregated(request, u.load_set__type(t), u.name + ", " + t.name, timeVector, quantity)


@require_GET
def viewTimeseriesRoomTypeAggregated(request, roomID, typeID, initTime, endTime, timeRes, quantity=('P', 'Q', 'S', 'PF')):
    """Computation of the aggregated consumption of all loads of a given type in a room, between initTime and endTime
    Input: room DB id
    Output: aggregated consumption: {room_name:<roomName>,"P":<P>,"Q":<Q>,"S":<S>,"pf":<PF>,estimated:<estimated>}
    <X> are in the standard form [[<unixTime>,<value>]...]
    <estimated> is {"X":<true/false>...}
    is true if there are not enough sensors to monitor all the loads in the room.
    """

    r = get_object_or_404(Room, pk=roomID)
    t = get_object_or_404(DeviceType, pk=typeID)
    if timeRes:
        timeRes = int(timeRes)
        try:
            timeVector = generateTimeVector(initTime, endTime, timeRes)
        except Exception as e:
            return HttpResponse(json.dumps({'Bad Request': repr(e)}), status=400, content_type='application/json')
    else:
        timeVector = [strToUnix(initTime), strToUnix(endTime)]
    return viewTimeseriesOrDatapointAggregated(request, r.load_set().filter(device__type=t), r.name + ', ' + t.name, timeVector, quantity)


@require_GET
def viewLastUnitAggregated(request, unitID, quantity=('P', 'Q', 'S', 'PF')):
    """Computation of the aggregated consumption of one unit at present time
    Note that we take simply the last value measured, may be unactualised...
    Input: room DB id
    Output: aggregated consumption: {unit_name:<unitName>,"P":<P>,"Q":<Q>,"S":<S>,"pf":<PF>,estimated:<estimated>}
    <X> are in the standard form [<unixTime>,<value>]
    <estimated> is {"X":<true/false>...}
    is true if there are not enough sensors to monitor all the loads in the room.
    """
    u = get_object_or_404(Unit, pk=unitID)
    return viewTimeseriesOrDatapointAggregated(request, u.load_set(), u.name, quantities=quantity)


@require_GET
def viewLastUnitTypeAggregated(request, unitID, typeID, quantity=('P', 'Q', 'S', 'PF')):
    """Computation of the aggregated consumption of one unit at present time
    Note that we take simply the last value measured, may not be updated...
    Input: room DB id
    Output: aggregated consumption: {unit_name:<unitName>,"P":<P>,"Q":<Q>,"S":<S>,"pf":<PF>,estimated:<estimated>}
    <X> are in the standard form [<unixTime>,<value>]
    <estimated> is {"X":<true/false>...}
    is true if there are not enough sensors to monitor all the loads in the room.
    """
    u = get_object_or_404(Unit, pk=unitID)
    t = get_object_or_404(DeviceType, pk=typeID)
    return viewTimeseriesOrDatapointAggregated(request, u.load_set().filter(device__type=t), u.name + ", " + t.name, quantities=quantity)


#-----------------------------------------------------------------------------------------------------------------------
#Advanced views returning energy data (integration of powers)
@require_GET
def viewTimeseriesOrDatapointEnergy(request, loads, name, initTime, endTime, timeRes, energyprofile=True):
    """Like the rest of the viewTimeseriesorDatapoint but this one integrates & resamples at the same time. First
    it resamples to the give time period and resolution and then integrates for each Dt(so we need two datapoints)
    The datapoint part is when we need the total energy per given period and not just energy profile of the period for each
    of the integration Dt areas. This resamples all the data and then integrate the whole P(t). (in fact what it does,
    under the hood is to sum the previous (timeseries based) areas.
    """
    try:
        try:
            if timeRes:
                timeRes = int(timeRes)
                timeVector = generateTimeVector(initTime, endTime, timeRes)
            else:
                timeVector = [strToUnix(initTime), strToUnix(endTime)]
        except Exception as e:
            return HttpResponse(json.dumps({'Bad Request': repr(e)}), status=400, content_type='application/json')
        if energyprofile:
            retData = sumEnergy(loads, timeVector)
            retVal = {
                'E': returnFormat([retData[0]]),# we send only the data and not the estimated flag(at 1)
            }
        else:  # There is request for datapoint so we call the cumSum to add all the points
            retData = sumEnergy(loads, timeVector)
            totalE = cumSum(retData[0]) #In the last point is the total energy, we ignore the first NaN
            retVal = {
                'Etotal': returnFormat([[totalE]])# we send only the data and not the estimated flag(at 1)
            }

        retVal = dict(retVal.items() + {
            'Name': name,
            'estimated':  retData[1][0]
        }.items())

        return HttpResponse(json.dumps(retVal), status=200, content_type='application/json')
    except Exception as e:
        traceback.print_exc()
        return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')


@require_GET
def viewTimeseriesLoadEnergy(request, loadID, initTime, endTime, timeRes):
    l = get_object_or_404(Load, pk=loadID)
    return viewTimeseriesOrDatapointEnergy(request, [l], l.name, initTime, endTime, timeRes)


@require_GET
def viewTimeseriesRoomEnergy(request, roomID, initTime, endTime, timeRes):
    r = get_object_or_404(Room, pk=roomID)
    return viewTimeseriesOrDatapointEnergy(request, r.load_set(), r.name, initTime, endTime, timeRes)


@require_GET
def viewTimeseriesUnitEnergy(request, unitID, initTime, endTime, timeRes):
    u = get_object_or_404(Unit, pk=unitID)
    return viewTimeseriesOrDatapointEnergy(request, u.load_set(), u.name, initTime, endTime, timeRes)


@require_GET
def viewTimeseriesRoomTypeEnergy(request, roomID, typeID, initTime, endTime,timeRes):
    r = get_object_or_404(Room, pk=roomID)
    t = get_object_or_404(DeviceType, pk=typeID)
    return viewTimeseriesOrDatapointEnergy(request, r.load_set().filter(device__type=t), r.name + ', ' + t.name, initTime, endTime, timeRes)


@require_GET
def viewTimeseriesUnitTypeEnergy(request, unitID, typeID, initTime, endTime, timeRes):
    u = get_object_or_404(Unit, pk=unitID)
    t = get_object_or_404(DeviceType, pk=typeID)
    return viewTimeseriesOrDatapointEnergy(request, u.load_set__type(t), u.name + ', ' + t.name, initTime, endTime, timeRes)


#For total Energy in the period(datapoint)
@require_GET
def viewDatapointLoadEnergy(request, loadID, initTime, endTime, timeRes):
    l = get_object_or_404(Load, pk=loadID)
    return viewTimeseriesOrDatapointEnergy(request, [l], l.name, initTime, endTime, timeRes, energyprofile=False)


@require_GET
def viewDatapointRoomEnergy(request, roomID, initTime, endTime, timeRes):
    r = get_object_or_404(Room, pk=roomID)
    return viewTimeseriesOrDatapointEnergy(request, r.load_set(), r.name, initTime, endTime, timeRes, energyprofile=False)


@require_GET
def viewDatapointUnitEnergy(request, unitID, initTime, endTime, timeRes):
    u = get_object_or_404(Unit, pk=unitID)
    return viewTimeseriesOrDatapointEnergy(request, u.load_set(), u.name, initTime, endTime, timeRes, energyprofile=False)


@require_GET
def viewDatapointRoomTypeEnergy(request, roomID, typeID, initTime, endTime,timeRes):
    r = get_object_or_404(Room, pk=roomID)
    t = get_object_or_404(DeviceType, pk=typeID)
    return viewTimeseriesOrDatapointEnergy(request, r.load_set().filter(device__type=t), r.name + ', ' + t.name, initTime, endTime, timeRes, energyprofile=False)


@require_GET
def viewDatapointUnitTypeEnergy(request, unitID, typeID, initTime, endTime, timeRes):
    u = get_object_or_404(Unit, pk=unitID)
    t = get_object_or_404(DeviceType, pk=typeID)
    return viewTimeseriesOrDatapointEnergy(request, u.load_set__type(t), u.name + ', ' + t.name, initTime, endTime, timeRes, energyprofile=False)



#-----------------------------------------------------------------------------------------------------------------------
#Actuator views
@require_GET
def viewActOnActuator(request, actID, instruction, requirelogin=True):
    """The active acting on actuator needs authorization"""
    if request.GET.has_key('api_sec_key') and request.GET.get('api_sec_key')==getattr(django_settings, 'URL_SECRET_KEY', None):
        requirelogin=False
    try:
        if request.user.is_authenticated() or not requirelogin:
            a = get_object_or_404(Actuator, pk=actID)
            m = get_object_or_404(Middleware, pk=a.network_middleware.id)
            if request.user.is_authenticated():
                logger_user_act.info('User:{0} || Middleware: {3}, ActuatorID:{1} >> Action:{2}'.format(request.user.username, actID, instruction, m.name))
            else:
                logger_user_act.info('User:{0} || Middleware: {3}, ActuatorID:{1} >> Action:{2}'.format('APIsecureKey', actID, instruction, m.name))
            return HttpResponse(json.dumps({'#Name': a.load.name, 'Status': toBackend_useActuator(a, instruction)}), status=200, content_type='application/json')
        else:
            return HttpResponse('Unauthorized', status=401)
    except Http404 as e: #Forward the Http404 from get_object_or_404
        raise e
    except ActionException as e:
        return HttpResponse(json.dumps({'Status': False, 'StatusError': repr(e), 'ErrorCode': 1}), status=400, content_type='application/json')
    except Exception as e:
        traceback.print_exc()
        return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')


@require_GET
def viewActOnLoad(request, loadID, instruction, requirelogin=True):
    """The active acting on actuator needs authorization"""
    if request.GET.has_key('api_sec_key') and request.GET.get('api_sec_key')==getattr(django_settings, 'URL_SECRET_KEY', None):
        requirelogin=False
    try:
        if request.user.is_authenticated() or not requirelogin:
            if request.user.is_authenticated():
                logger_user_act.info('User:{0} LoadID:{1} Action:{2}'.format(request.user.username, loadID, instruction))
            else:
                logger_user_act.info('User:{0} LoadID:{1} Action:{2}'.format('APIsecureKey', loadID, instruction))
            l = get_object_or_404(Load, pk=loadID)
            return HttpResponse(json.dumps({'#Name': l.name, 'Status': toBackend_actLoad(l, instruction)}), status=200, content_type='application/json')
        else:
            return HttpResponse('Unauthorized', status=401)
    except Http404 as e: #Forward the Http404 from get_object_or_404
        raise e
    except ActionException as e:
        return HttpResponse(json.dumps({'Status': False, 'StatusError': repr(e), 'ErrorCode': 1}), status=400, content_type='application/json')
    except Exception as e:
        traceback.print_exc()
        return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')


@require_GET
def viewStatusActuator(request, actID):
    a = get_object_or_404(Actuator, pk=actID)
    actuator = Actuator.objects.filter(id=a.id).select_related('load')  #load is used in check of possibleStatus
    possibleStatus = act_possibleStatus(a)
    try:
        status = toBackend_getActuatorStatus(actuator)[0] #0 because we ask for one, and we MUST provide a queryset
        if status and a.action == MODE_API_TYPE and (status >= len(possibleStatus) or status < 0):
            raise Exception('Status returned', status,
                            'does not correspond to the number of modes, please verify backend and model')
        return HttpResponse(json.dumps({'#Name': a.name, '#Action': a.action, 'Status': status, 'possibleStatus': possibleStatus}), status=200, content_type='application/json')
    except Exception as e:
        traceback.print_exc()
        return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')


@require_GET
def viewStatusRoom(request, roomID):
    r = get_object_or_404(Room, pk=roomID)
    actuator_set = r.actuator_set.all()
    toret = dict()
    try:
        statuses = toBackend_getActuatorStatus(actuator_set)
        actuator_set = actuator_set.select_related('load') #load is used in check of possibleStatus
        for i, actuator in enumerate(actuator_set):
            possibleStatus = act_possibleStatus(actuator)
            if statuses[i] and actuator.action == MODE_API_TYPE and (statuses[i] >= len(possibleStatus) or statuses[i] < 0):
                raise Exception('Status returned', statuses[i],
                                'does not correspond to the number of modes, please verify backend and model')
            actRet = {'#Name': actuator.name, '#Action': actuator.action, 'Status': statuses[i], 'possibleStatus': possibleStatus}
            toret[actuator.id] = actRet
    except Exception as e:
        traceback.print_exc()
        return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')

    return HttpResponse(json.dumps(toret), status=200, content_type='application/json')


class viewConfigMsgREQ(View):
    """Used by the initial sender of the data. It POST the yaml using the msg id selected by it in random. A recepient starts a GET
    on an empty id to get the list of pending messages. And then does in sequence a GET to a specific id to retrieve the full message"""
    def get(self, request, confID):
        if confID is None:
            #It requests all pending messages
            return HttpResponse(serializers.serialize('json', ConfigMsg.objects.filter(configtype='REQ')), status=200, content_type='application/json')
        else:
            conf = get_object_or_404(ConfigMsg, configtype='REQ', configid=confID)
            try:
                conf.delete()
                return HttpResponse(conf.configstring, status=200, content_type='application/json')
            except Exception as e:
                traceback.print_exc()
                return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')

    def post(self, request, confID):
        try:
            if confID is None: raise ValidationError('No configuration ID provided')
            conf = ConfigMsg(configid=confID, configtype='REQ', configstring=request.body)
            conf.full_clean()
            conf.save()
            return HttpResponse(json.dumps({'Status': True, 'StatusType': 'Created'}), status=201, content_type='application/json')
        except ValidationError as e:
            return HttpResponse(json.dumps({'Status': False, 'StatusError': repr(e), 'ErrorCode': 5}), status=400, content_type='application/json')
        except Exception as e:
            traceback.print_exc()
            return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')

    def delete(self, request, confID):
        conf = get_object_or_404(ConfigMsg, configtype='REQ', configid=confID)
        try:
            conf.delete()
            return HttpResponse(json.dumps({'Status': True, 'StatusType': 'Deleted'}), status=200, content_type='application/json')
        except Exception as e:
            traceback.print_exc()
            return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')


class viewConfigMsgREP(View):
    """The recipient POST here with the predefined id the reply. Since it is the same id the initial sender could retrieve this and
    only this message from the server by long polling GET on the REP view using his id"""
    def get(self, request, confID):
        if confID is None:
            #It requests all pending messages
            return HttpResponse(serializers.serialize('json', ConfigMsg.objects.filter(configtype='REP')), status=200, content_type='application/json')
        else:
            conf = get_object_or_404(ConfigMsg, configtype='REP', configid=confID)
            try:
                conf.delete()
                return HttpResponse(conf.configstring, status=200, content_type='application/json')
            except Exception as e:
                traceback.print_exc()
                return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')

    def post(self, request, confID):
        try:
            if confID is None: raise ValidationError('No configuration ID provided')
            conf = ConfigMsg(configid=confID, configtype='REP', configstring=request.body)
            conf.full_clean()
            conf.save()
            return HttpResponse(json.dumps({'Status': True, 'StatusType': 'Created'}), status=201, content_type='application/json')
        except ValidationError as e:
            return HttpResponse(json.dumps({'Status': False, 'StatusError': repr(e), 'ErrorCode': 5}), status=400, content_type='application/json')
        except Exception as e:
            traceback.print_exc()
            return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')


class viewAssociateUUID(View):
    """It gets from the URI the static uuid which is the personal badge and from the POST parameters get the dynamic one
    The static is in URI since it modelizes a resource according to the RESTful principles.
    It is the responsibility of this view to find the correct models, the dynamic uuid corresponds to. (it can do routing based
    on the hex of the UUID instead of multiple join and queries to sql tables.
    POST method to report the personal UUID and get user info
    PUT method to report user dynamic UUID which is the association of the two UUIDs"""

    def put(self, request, per_uuid, keyinpath=False): #For reporting the location
        if keyinpath or (request.GET.has_key('api_sec_key') and request.GET.get('api_sec_key')==getattr(django_settings, 'URL_SECRET_KEY', None)):
            #Key accepted
            per_uuid = per_uuid.replace('-', ':')
            user_prof = get_object_or_404(UserProfile, uuid=per_uuid)
            try:
                if not request.GET.has_key('dyn_uuid'):
                    return HttpResponse(json.dumps({'Status': False,
                                                    'StatusError': 'Missing Dynamic UUID parameter', 'ErrorCode': 1}),
                                        status=400, content_type='application/json')
                else:
                    re_group = re.search(r'^([0-9A-Fa-f]{2}[:-])+([0-9A-Fa-f]{2})$', request.GET['dyn_uuid'])
                    if not re_group:
                        return HttpResponse(json.dumps({'Status': False,
                                                        'StatusError': 'Wrong Dynamic UUID format', 'ErrorCode': 1}),
                                            status=400, content_type='application/json')
                    else:
                        dyn_uuid = re_group.group()
                        dyn_uuid = dyn_uuid.replace('-', ':')

                if request.GET.has_key('timestamp'):
                    timestamp = request.GET['timestamp']
                    # import datetime
                    # print(
                    #     datetime.datetime.fromtimestamp(
                    #         int(timestamp)
                    #     ).strftime('%Y-%m-%d %H:%M:%S')
                    # )
                else:
                    timestamp = time()

                #Test only, timeseries model is needed, pu timestamp and r will be used to register.
                try:
                    r = Room.objects.get(uuid=dyn_uuid)
                    old_location = user_prof.current_location
                    user_prof.current_location = r
                    user_prof.save()
                    room_info = self.prepare_room(r) #with pk

                    _threadlock.acquire() #better be safe than sorry
                    try:

                        if old_location != r: #if it is the same room we dont repeat
                            if old_location: #None if it is a fresh user
                                #Notofy that user left the room
                                submsg = {
                                    'MID_ID': LOC_MID_ID,
                                    'MSG_TYPE': PUB_MSG_TYPE[3],
                                    'PAYLOAD': {'Room':old_location.id, 'Direction': 'Out', 'User': user_prof.id, 'Time': timestamp}}
                                locPUBsocket.send_json(submsg)

                            submsg = {
                                'MID_ID': LOC_MID_ID,
                                'MSG_TYPE': PUB_MSG_TYPE[3],
                                'PAYLOAD': {'Room':r.id, 'Direction': 'In', 'User': user_prof.id,'Time': timestamp}}
                            locPUBsocket.send_json(submsg)
                    finally:
                        _threadlock.release()
                    return HttpResponse(json.dumps({'Status': True, 'Type':'Room', 'Room':room_info}), status=200, content_type='application/json')
                except Room.DoesNotExist:
                    return HttpResponse(json.dumps({'Status': False, 'ErrorCode': 2, 'StatusError': 'No matching Dynamic UUID found'}), status=400, content_type='application/json')
                except ValidationError as e:
                    return HttpResponse(json.dumps({'Status': False, 'StatusError': repr(e), 'ErrorCode': 5}), status=400, content_type='application/json')
            except Exception as e:
                traceback.print_exc()
                return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')
        else:
            return HttpResponse('Unauthorized', status=401)



    def post(self, request, per_uuid, keyinpath=False): #For reporting the personal UUID of user. (GET is better method...)
        if keyinpath or (request.GET.has_key('api_sec_key') and request.GET.get('api_sec_key')==getattr(django_settings, 'URL_SECRET_KEY', None)):
            #Key accepted
            per_uuid = per_uuid.replace('-', ':')
            allow_auto_register = True #Change accordingly...
            try:
                if not allow_auto_register: #Try to find or return 404
                    user_prof = get_object_or_404(UserProfile, uuid=per_uuid)
                    return HttpResponse(json.dumps({'Status': True, 'User': self.prepare_userprofile(user_prof)}), status=200, content_type='application/json')

                else: #the registration is open (allow_auto_register) and temporary user is constracted to keed the uuid. Log in with admin to change the details
                    try:
                        user_prof = UserProfile.objects.get(uuid=per_uuid)
                        internal_user = user_prof.int_user
                        #perform manual log in for the open client session
                        if internal_user:
                            internal_user.backend='django.contrib.auth.backends.ModelBackend'
                            login(request, internal_user)
                        return HttpResponse(json.dumps({'Status': True, 'User': self.prepare_userprofile(user_prof)}), status=200, content_type='application/json')
                    except UserProfile.DoesNotExist:
                        user_prof = UserProfile(name='tempuser', surname='tempuser', int_user=None, uuid=per_uuid)
                        user_prof.full_clean()
                        user_prof.save()
                        return HttpResponse(json.dumps({'Status': True, 'Type': 'NewUser', 'User': self.prepare_userprofile(user_prof)}), status=200, content_type='application/json')

            except ValidationError as e:
                return HttpResponse(json.dumps({'Status': False, 'StatusError': repr(e), 'ErrorCode': 5}), status=400, content_type='application/json')
            except Http404 as e: #Forward the Http404 from get_object_or_404
                raise e
            except Exception as e:
                traceback.print_exc()
                return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')
        else:
            return HttpResponse('Unauthorized', status=401)

    @staticmethod
    def prepare_userprofile(userprofile_object):
        user_prof_serialized = serializers.serialize('json', [userprofile_object])
        user_prof_dict = json.loads(user_prof_serialized)[0] #serializers give a list, we take the first
        user_prof_dict = user_prof_dict['fields']
        user_prof_dict['profile_image'] = getattr(django_settings, 'MEDIA_URL', None)+user_prof_dict['profile_image']
        del user_prof_dict['int_user']
        del user_prof_dict['uuid']  #for privacy reasons
        return user_prof_dict

    @staticmethod
    def prepare_room(room_object):
        room_serialized = serializers.serialize('json', [room_object])
        room_info_dict = json.loads(room_serialized)[0] #serializers give a list, we take the first
        pk = room_info_dict['pk']
        room_info_dict = room_info_dict['fields']
        room_info_dict['roomID'] = pk
        room_info_dict['type'] = RoomType.objects.get(id=room_info_dict['type']).name
        room_info_dict['unit'] = Unit.objects.get(id=room_info_dict['unit']).name
        del room_info_dict['comment']
        return room_info_dict


class viewUserLocation(View):
    """For reporting the location of the client in coordinates. This function also calculates the corse distance from the Unit
    if needed and defined in UserProfile."""
    def get(self, request, per_uuid, keyinpath=False):
        if keyinpath or (request.GET.has_key('api_sec_key') and request.GET.get('api_sec_key')==getattr(django_settings, 'URL_SECRET_KEY', None)):
            #Key accepted
            if not location_storage.has_key(per_uuid):
                return HttpResponse(json.dumps({'Error': 'No log for this user'}), status=501, content_type='application/json')
            return HttpResponse(json.dumps(location_storage[per_uuid]), status=200, content_type='application/json')
        else:
            return HttpResponse('Unauthorized', status=401)

    def delete(self, request, per_uuid, keyinpath=False):
        if keyinpath or (request.GET.has_key('api_sec_key') and request.GET.get('api_sec_key')==getattr(django_settings, 'URL_SECRET_KEY', None)):
            #Key accepted
            if not location_storage.has_key(per_uuid):
                return HttpResponse(json.dumps({'Error': 'No log for this user'}), status=501, content_type='application/json')
            del location_storage[per_uuid]
            return HttpResponse({'Status': True, 'StatusType': 'Deleted'}, status=200, content_type='application/json')
        else:
            return HttpResponse('Unauthorized', status=401)

    def put(self, request, per_uuid, keyinpath=False):
        if keyinpath or (request.GET.has_key('api_sec_key') and request.GET.get('api_sec_key')==getattr(django_settings, 'URL_SECRET_KEY', None)):
            #Key accepted
            per_uuid = per_uuid.replace('-', ':')
            user_prof = get_object_or_404(UserProfile, uuid=per_uuid)
            try:
                if not request.GET.has_key('latitude'):
                    return HttpResponse(json.dumps({'Status': False,
                                                    'StatusError': 'Missing latitude parameter', 'ErrorCode': 1}),
                                        status=400, content_type='application/json')
                elif not request.GET.has_key('longitude'):
                    return HttpResponse(json.dumps({'Status': False,
                                                    'StatusError': 'Missing longitude parameter', 'ErrorCode': 1}),
                                        status=400, content_type='application/json')
                elif not request.GET.has_key('accuracy'):
                    return HttpResponse(json.dumps({'Status': False,
                                                    'StatusError': 'Missing accuracy parameter', 'ErrorCode': 1}),
                                        status=400, content_type='application/json')
                #Validation of request passed
                if request.GET.has_key('timestamp'):
                    timestamp = request.GET['timestamp']
                else:
                    timestamp = time()

                if request.GET.has_key('location_type'):
                    location_type = request.GET['location_type']
                else:
                    location_type = 'Unknown'

                if request.GET.has_key('direction'):
                    direction = request.GET['direction']
                else:
                    direction = 'Unknown'

                if request.GET.has_key('travel_method'):
                    travel_method = request.GET['travel_method']
                else:
                    travel_method = 'walking'

                latitude = request.GET['latitude']
                longitude = request.GET['longitude']
                accuracy = request.GET['accuracy']

                try:
                    ref_unit = Unit.objects.get(id=user_prof.unit.id)
                except Unit.DoesNotExist:
                    return HttpResponse(json.dumps({'Status': False,
                                                    'StatusError': 'This user did not register a reference unit', 'ErrorCode': 2}),
                                        status=400, content_type='application/json')
                unit_latitude = ref_unit.latitude
                unit_longitude = ref_unit.longitude
                google_distance_api_url = 'https://maps.googleapis.com/maps/api/distancematrix/json'
                params = '?origins={0},{1}&destinations={2},{3}&mode={4}&key={5}'.format(latitude, longitude, unit_latitude, unit_longitude, travel_method, GOOGLE_API_KEY)
                req_url = google_distance_api_url + params
                r = requests.get(req_url)
                if r.status_code == 200:
                    elem = r.json()['rows'][0]['elements'][0]
                    if elem['status'] == 'OK':
                        distance = elem['distance']['value'] #in meters
                        duration = elem['duration']['value'] #in seconds
                        user_prof.distance = distance/1000.0
                        user_prof.duration = duration/60.0
                        user_prof.direction = direction
                        user_prof.location_type = location_type
                        user_prof.accuracy = accuracy
                        user_prof.time_of_location = datetime.datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')
                        user_prof.save()
                        if not location_storage.has_key(per_uuid):
                            location_storage[per_uuid] = []
                        fence_id = request.GET['fence_id']
                        location_storage[per_uuid].append({'Direction':user_prof.direction, 'Longitude': longitude, 'Latitude': latitude,
                                                   'Timestamp': timestamp, 'Accuracy': accuracy, 'Location_Type': location_type, 'Duration': duration,
                                                   'Distance': distance, 'FenceID': fence_id})
                        save_loc_buffer(location_storage)
                        return HttpResponse(json.dumps({'Status': True, 'Distance': distance/1000.0,
                                                        'Duration': duration/60.0, 'Method': travel_method,
                                                        'Type':'Unit', 'Unit': self.prepare_unit(ref_unit)}),status=200, content_type='application/json')
                    else:
                        return HttpResponse(json.dumps({'Status': False,
                                                        'StatusError': 'Cannot calculate the distance using the user preferred method of {0}'.format(travel_method), 'ErrorCode': 2}),
                                            status=400, content_type='application/json')
                else:
                    return HttpResponse(json.dumps({'Status': False,
                                                    'StatusError': 'Cannot calculate the distance, google API not availiable at the moment', 'ErrorCode': 2}),
                                        status=400, content_type='application/json')

            except ValidationError as e:
                return HttpResponse(json.dumps({'Status': False, 'StatusError': repr(e), 'ErrorCode': 5}), status=400, content_type='application/json')
            except Exception as e:
                traceback.print_exc()
                return HttpResponse(json.dumps({'Error': 'Exception raised: ' + repr(e)}), status=500, content_type='application/json')

        else:
            return HttpResponse('Unauthorized', status=401)

    @staticmethod
    def prepare_unit(unit_object):
        user_prof_serialized = serializers.serialize('json', [unit_object])
        user_prof_dict = json.loads(user_prof_serialized)[0] #serializers give a list, we take the first
        pk = user_prof_dict['pk']
        user_prof_dict = user_prof_dict['fields']
        user_prof_dict['unitID'] = pk
        del user_prof_dict['comment']
        return user_prof_dict

@require_GET
def viewZonesUnit(request, unitID):
    zones = Zone.objects.filter(unit=unitID)
    zoneborders = ZoneBorder.objects.filter(l_zone__in = zones) | ZoneBorder.objects.filter(r_zone__in = zones)
    zoneborders = zoneborders.distinct().select_related() #follow all relations
    zoneborderlist = []
    for zb in zoneborders:
        matlist = []
        zb_attr_obj = zb.border_attr
        if zb_attr_obj.mat1:
            mat1_dict = {'cp':zb_attr_obj.mat1.cp,'D':zb_attr_obj.mat1.d,'p':zb_attr_obj.mat1.p}
            matlist.append({'matterial':mat1_dict, 'thickness': zb_attr_obj.thickness1})
        if zb_attr_obj.mat2:
            mat2_dict = {'cp':zb_attr_obj.mat2.cp,'D':zb_attr_obj.mat2.d,'p':zb_attr_obj.mat2.p}
            matlist.append({'matterial':mat2_dict, 'thickness': zb_attr_obj.thickness2})
        if zb_attr_obj.mat3:
            mat3_dict = {'cp':zb_attr_obj.mat3.cp,'D':zb_attr_obj.mat3.d,'p':zb_attr_obj.mat3.p}
            matlist.append({'matterial':mat3_dict, 'thickness': zb_attr_obj.thickness3})
        if zb_attr_obj.mat4:
            mat4_dict = {'cp':zb_attr_obj.mat4.cp,'D':zb_attr_obj.mat4.d,'p':zb_attr_obj.mat4.p}
            matlist.append({'matterial':mat4_dict, 'thickness': zb_attr_obj.thickness4})
        if zb_attr_obj.mat5:
            mat5_dict = {'cp':zb_attr_obj.mat5.cp,'D':zb_attr_obj.mat5.d,'p':zb_attr_obj.mat5.p}
            matlist.append({'matterial':mat5_dict, 'thickness': zb_attr_obj.thickness5})

        zb_attr_dict = {'conjA':zb_attr_obj.conductive_coef_A,'conjB':zb_attr_obj.conductive_coef_B,'matlist':matlist}
        window_dict = {'uvalue':zb.window.uvalue,'shgc':zb.window.shgc}

        zb_dict = {'surface':zb.surface, 'zb_attr':zb_attr_dict, 'l_zoneID': zb.l_zone.id, 'r_zoneID': zb.r_zone.id, 'window_attr': window_dict, 'window_surface':zb.window_surface}
        zoneborderlist.append(zb_dict)

    zonelist = []
    zoneheaterlist = []
    for zone in zones:
        #The zones
        zone_serialized = serializers.serialize('json', [zone])
        zone_dict = json.loads(zone_serialized)[0]
        pk = zone_dict['pk']
        zone_dict = zone_dict['fields']
        zone_dict['roomID'] = zone_dict['room'] #change the key for better readability
        zone_dict['zoneID'] = pk
        del zone_dict['unit']
        del zone_dict['room']
        zonelist.append(zone_dict)

        #The heaters of rooms
        if zone_dict['heatpower']:
            zoneheater_dict={"zoneID": zone_dict['zoneID'], "HeatValue": zone_dict['heatpower']}
            zoneheaterlist.append(zoneheater_dict)

    #Encapsulate all of them in one dict
    root_dict = {'Zonelist' : zonelist, 'ZoneBorderlist': zoneborderlist, 'ZoneHeaterlist':zoneheaterlist }
    return HttpResponse(json.dumps(root_dict), status=200, content_type='application/json')


####-------------vWorld-------------####

class viewsinglevMiddleware(View):
    """View class for interacting with a single virtual Middleware"""
    def get(self,request, vMidID):
        """GET the vEntity details"""
        vMid = get_object_or_404(Middleware, pk=vMidID, middleware_type='vMID')
        return HttpResponse(serializers.serialize('json', [vMid]),status=200, content_type='application/json')

class viewvMiddlewareEntities(View):
    """View class for getting all the virtual entities with the given vMidID"""
    def get(self,request, vMidID):
        """GET the vEntity details"""
        vMid = get_object_or_404(Middleware, pk=vMidID, middleware_type='vMID')
        vEs = VirtualEntity.objects.select_related().filter(virtual_middleware=vMid)
        return HttpResponse(serializers.serialize('json', vEs, indent=2, use_natural_foreign_keys=True),status=200, content_type='application/json')

class viewSinglevEntity(View):
    """View class for interacting with a single virtual entity"""
    def get(self,request, vEntID):
        """GET the vEntity details"""
        vEnt = get_object_or_404(VirtualEntity, pk=vEntID)
        return HttpResponse(serializers.serialize('json', [vEnt]),status=200, content_type='application/json')
