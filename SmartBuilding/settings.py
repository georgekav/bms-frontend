# Django settings for SmartBuilding project.
# Build paths inside the project like this: os.path.join(FRONTEND_DIR, ...)
import os, sys

from conf_openbms import APIsecKey, OPENBMS_DEBUG, OPENBMS_IP
#MAILGUN_ACCESS_KEY, MAILGUN_SERVER is nesessary for the django_mailgun that fetches them from the setting
from conf_general import CURRENT_IP, MAILGUN_ACCESS_KEY, MAILGUN_SERVER_NAME, EMAIL_LOGGING_RECIPIENT

FRONTEND_DIR = os.path.abspath(__file__ + "/../../")
print("Initializing settings.py - if you see this more than once use 'runserver --noreload'")


DEBUG = OPENBMS_DEBUG

ADMINS = (
    ('George Lilis', EMAIL_LOGGING_RECIPIENT),
)

MANAGERS = ADMINS
BANISH_ENABLED = True
BANISH_ABUSE_THRESHOLD = 100
BANISH_ABUSE_TIME_THRESHOLD = 10  #in seconds
PRELOAD_BANS = False
BAN_RESET_PERIOD = 60


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',  # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': FRONTEND_DIR + '/SmartBuilding.sqlite',  # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': '',
        'PASSWORD': '',
        'HOST': '',  # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',  # Set to empty string for default.
    }
}



# Set the TEST_RUNNER for django 1.7 compatibility
TEST_RUNNER = 'django.test.runner.DiscoverRunner'

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = [OPENBMS_IP]
#Take from nginx has as forwarded host or own IP/hostname is it recognizes the correct IP/hostname
USE_X_FORWARDED_HOST = True

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Paris'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = os.path.join(FRONTEND_DIR, "SmartBuilding/media/")

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = ''

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Login redirect page
LOGIN_REDIRECT_URL = '/BuildingWebApp/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(FRONTEND_DIR, "SmartBuilding/static/"),
    os.path.join(FRONTEND_DIR, "BmsAPI/static/"),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '0-$gi^c0##1gkvl=7c-d9w&csm6blr!yv&cdj423o65ctjwoj#'

#This is used by the url dispacher to bypass the login
URL_SECRET_KEY = APIsecKey

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(FRONTEND_DIR, 'SmartBuilding/templates'),
            os.path.join(FRONTEND_DIR, 'BmsApp/templates'),
            os.path.join(FRONTEND_DIR, 'BmsAPI/templates')
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.core.context_processors.request',
                'SmartBuilding.mycontext.version',
                # list if you haven't customized them:
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

MIDDLEWARE_CLASSES = (
	'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'SmartBuilding.middleware.DisableCSRF',
    'SmartBuilding.djangoprofiler.ProfileMiddleware',
    'banish.middleware.BanishMiddleware'
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'SmartBuilding.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'SmartBuilding.wsgi.application'

INSTALLED_APPS = (

    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'suit', #must be before admin
    'django.contrib.admin',
	'corsheaders',
    'password_reset',
    'banish',
    'BmsApp',
    'BmsAPI',
)
CORS_ORIGIN_ALLOW_ALL = True

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

EMAIL_BACKEND = 'mailgun.django_mailgun.MailgunBackend'
DEFAULT_FROM_EMAIL = 'webmaster@openBMS' #used mainly by password reset module and send_mail function
SERVER_EMAIL = 'openBMS@{0}'.format(CURRENT_IP)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOG_FILE = 'logfile.log'
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': "%(levelname)-8s %(asctime)s   %(message)s",
            'datefmt': "%Y-%m-%d %H:%M:%S",
        },
        'color': {
            '()': 'colorlog.ColoredFormatter',
            'format': "%(log_color)s%(levelname)-8s %(asctime)s   %(message)s",
            'datefmt': "%Y-%m-%d %H:%M:%S",
            'log_colors': {
                'DEBUG':    'bold_black',
                'INFO':     'white',
                'WARNING':  'yellow',
                'ERROR':    'red',
                'CRITICAL': 'bold_red',
            },
        }
    },
    'handlers': {
        'console_color': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'color'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'color'
        },
        'console_simple': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
        'logfile': {
            'level':'DEBUG',
            'class':'logging.FileHandler',
            'filename': FRONTEND_DIR+"/"+LOG_FILE,
            'formatter': 'standard',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': False,
            'formatter': 'standard'
        },
        'mail_admins_access': {
            'level': 'WARNING',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': False,
            'formatter': 'standard'
        },
    },
    'loggers': {
        'django': {  # This is the default, handling all the internal django log
            'handlers': ['console_color', 'mail_admins'],
            'level': 'INFO',
            'propagate': False,
        },
        'django.security': {
            'handlers': ['console_color', 'mail_admins'],
            'level': 'DEBUG',
            'propagate': False,
        },

        'django.useraction': {
            'handlers': ['logfile', 'mail_admins'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'django.console': {  # This is the personal console
            'handlers': ['console_color', 'mail_admins'],
            'level': 'INFO',
            'propagate': False,
        },

        #For gunnicorn console
        'gunicorn.error': {
            'level': 'INFO',
            'handlers': ['console_color', 'mail_admins'],
            'propagate': False,
        },
        'gunicorn.access': {
            'level': 'INFO',
            'handlers': ['console_color', 'mail_admins_access'],
            'propagate': False,
        }
    }
}

SUIT_CONFIG = {
    'ADMIN_NAME': 'openBMS Admin',
    'HEADER_DATE_FORMAT': 'l, jS F Y',
    'HEADER_TIME_FORMAT': 'H:i',

    'SHOW_REQUIRED_ASTERISK': True,
    'CONFIRM_UNSAVED_CHANGES': True,

    'MENU_OPEN_FIRST_CHILD': True,
    'MENU_ICONS': {
        'auth': 'icon-lock',
    },
    'LIST_PER_PAGE': 100,

    'MENU' : (
        # Rename app and set icon
        {'app': 'auth', 'label': 'Authentication', 'models': ('auth.user', 'auth.group', {'app':'banish', 'label': 'Banish'})},
        {'app': 'bmsapp', 'label': 'openBMS', 'icon':'icon-leaf'},
    )
}
