from django.shortcuts import render
from django.contrib.auth.views import login
from django.views.decorators.http import require_http_methods, require_GET
from django.http import HttpResponse
from SmartBuilding import settings
from BmsApp.models import *
from mycontext import version_str
from conf_rtserver import *
from conf_general import PUB_MSG_TYPE

mobile_context = {"skip_navbar" : True, "general_mobile_view": True}
basic_context = {"static_serve_all_js_css": True}
central_loc = {"center_latitude" : 46.519468, "center_longitude" : 6.565315}

@require_GET
def home(request, mobile_view = False):
    context = {"centering_zoom" : mobile_view, "view_title":"openBMS "+version_str}
    render_context = {}
    render_context.update(basic_context)
    render_context.update(context)
    if mobile_view:
        render_context.update(mobile_context)
    return render(request, 'home.html', render_context)


@require_GET
def contact(request, mobile_view = False):
    context = {"view_title": "openBMS Contact"}
    render_context = {}
    render_context.update(basic_context)
    render_context.update(context)
    if mobile_view:
        render_context.update(mobile_context)
    return render(request, 'contact.html', render_context)


@require_http_methods(["GET","POST"])
def authview(request, mobile_view = False):
    context = {"view_title": "openBMS Account"}
    render_context = {}
    render_context.update(basic_context)
    render_context.update(context)
    if mobile_view:
        render_context.update(mobile_context)
    return login(request, 'accounts.html', extra_context=render_context)


@require_GET
def location(request, mobile_view = False):
    if request.user.is_authenticated(): #try to get the location of interest of the user
        try:
            user_prof = UserProfile.objects.get(int_user=request.user.pk)
            unit_interest = user_prof.unit
            context = {"center_latitude" : unit_interest.latitude, "center_longitude" : unit_interest.longitude}
        except UserProfile.DoesNotExist:
            context = central_loc
    else:
        context = central_loc
    context["view_title"] = 'openBMS Location'
    render_context = {}
    render_context.update(basic_context)
    render_context.update(context)
    if mobile_view:
        render_context.update(mobile_context)
    return render(request, 'location.html', render_context)


@require_GET
def logs(request):
    if not request.user.is_staff:
        return HttpResponse('Unauthorized', status=401)
    logtext = []
    for line in open(settings.LOG_FILE).readlines()[:-500:-1]:
        logtext.append(line)
    return HttpResponse(logtext, status=200, content_type="text/plain")


@require_GET
def android(request, mobile_view = False):
    context = {"view_title": "openBMS Android"}
    render_context = {}
    render_context.update(basic_context)
    render_context.update(context)
    if mobile_view:
        render_context.update(mobile_context)
    return render(request, 'android.html', render_context)

@require_GET
def joulesense(request, mobile_view = False):
    context = {"view_title": "openBMS JouleSence"}
    render_context = {}
    render_context.update(basic_context)
    render_context.update(context)
    if mobile_view:
        render_context.update(mobile_context)
    return render(request, 'joulesense.html', render_context)



@require_GET
def vmiddleware(request, mobile_view = False):
    context = {"view_title": "openBMS vMiddleware"}
    websocket_context = {'rt_server_ip': RT_SERVER_IP, 'rt_server_port': RT_SERVER_PORT, 'rt_room_list': [],
                         'frontend_websock_id': PUB_ID_FRONTEND, 'extra_pub_id_list' : [PUB_ID_VMID_PING],
                         'websocket_msg_class': PUB_MSG_TYPE}
    render_context = {}
    render_context.update(basic_context)
    render_context.update(websocket_context)
    render_context.update(context)
    if mobile_view:
        render_context.update(mobile_context)
    return render(request, 'vmiddleware.html', render_context)

@require_GET
def easydashboard(request, mobile_view = False):
    context = {"view_title": "openBMS Easy Dashboard"}
    render_context = {}
    render_context.update(basic_context)
    render_context.update(context)
    if mobile_view:
        render_context.update(mobile_context)
    return render(request, 'easydashboard.html', render_context)
