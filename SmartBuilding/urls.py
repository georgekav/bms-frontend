from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from BmsAPI.urlsAPI import urlsecpatterns
from SmartBuilding import settings
from SmartBuilding.settings import URL_SECRET_KEY
import password_reset.urls


admin.autodiscover()


urlpatterns = patterns('',
                       # Examples:
                       url(r'^$', 'SmartBuilding.views.home', name='home'),
                       url(r'^home/', 'SmartBuilding.views.home', name='home'),
                       url(r'^BuildingWebApp/', include('BmsApp.urls', namespace="BmsApp")),
                       url(r'^contact/', 'SmartBuilding.views.contact', name='contact'),
                       url('', include('django.contrib.auth.urls')),
                       url(r'^password_reset/', include('password_reset.urls')),
                       url(r'^account/', 'SmartBuilding.views.authview', name='auth_account'),
                       url(r'^logout/', 'django.contrib.auth.views.logout', name="auth_logout"),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^logs/', 'SmartBuilding.views.logs' , name="django_log"),
                       url(r'^geolocation/', 'SmartBuilding.views.location' , name="current_location"),
                       url(r'^android/', 'SmartBuilding.views.android' , name="android"),
                       url(r'^joulesense/', 'SmartBuilding.views.joulesense' , name="joulesense"),
                       url(r'^vmiddleware/', 'SmartBuilding.views.vmiddleware' , name="vmiddleware"),
                       url(r'^easydashboard/', 'SmartBuilding.views.easydashboard' , name="easydashboard"),

                       #mobile urls
                       url(r'^home_m/', 'SmartBuilding.views.home', {'mobile_view' : True}, name='home_m'),
                       url(r'^contact_m/', 'SmartBuilding.views.contact', {'mobile_view' : True}, name='contact_m'),
                       url(r'^account_m/', 'SmartBuilding.views.authview', {'mobile_view' : True}, name='auth_account_m'),
                       url(r'^geolocation_m/', 'SmartBuilding.views.location', {'mobile_view' : True} , name="current_location_m"),
                       url(r'^android_m/', 'SmartBuilding.views.android', {'mobile_view' : True} , name="android_m"),
                       url(r'^joulesense_m/', 'SmartBuilding.views.joulesense', {'mobile_view' : True} , name="joulesense_m"),
                       url(r'^API/', include('BmsAPI.urlsAPI', namespace='BmsAPI')),
                       url(r'^APIsec/'+URL_SECRET_KEY+'/', include(urlsecpatterns, namespace='APIsec')),
)

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += patterns('',
        url(r'^media\/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes': False}),
)

