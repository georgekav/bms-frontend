__author__ = 'Georgios Lilis'
"""Global context to provided to all views using TEMPLATE_CONTEXT_PROCESSORS in setting.py"""
from conf_rtserver import RT_SERVER_IP, RT_SERVER_PORT

version_str = 'v3.2.0'
def version(request):
    return {'VERSION_APP': version_str, 'REALTIMESERVER' : '{0}:{1}'.format(RT_SERVER_IP, RT_SERVER_PORT) }
