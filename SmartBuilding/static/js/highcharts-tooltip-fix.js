/**
 * Created by lilis on 29.01.2015.
 * My custom fix for the scrolling on touch devices
 */
Highcharts.Chart.prototype.callbacks.push(function(chart) {
    var hasTouch = document.documentElement.ontouchstart !== undefined,
        mouseTracker = chart.pointer,
        container = chart.container,
        mouseMove,
        click;
    var lastY;
    mouseMove = function (e) {//What will happen with a mouse/touch movement
        if (hasTouch) {
            if (e && e.touches && e.touches.length > 1) {
            //If it is a touch device and it is a touch with multipoint (2 finger etc) do what the highcharts suggests
            //We add the mousemove for the graph to redraw correctly
                mouseTracker.onContainerTouchMove(e);
                mouseTracker.onContainerMouseMove(e);
            }
            else{ //Single touch
                var currentY = e.touches[0].pageY;
                if (Math.abs(currentY - lastY) < 10) { //Moving sideway. Panning
                    mouseTracker.onContainerTouchMove(e);
                }
                //Otherwise do nothing and pass control to browser for single touch move
                lastY = currentY;
            }
        } else { //If it is a move move, just do the default for mouse.
            mouseTracker.onContainerMouseMove(e);
        }
    };
    click = function (e) { //What will happen with a click/touch
        if (hasTouch) {
            mouseTracker.onContainerMouseMove(e); //If it is a touch device, emulate a mouse move
        }
        mouseTracker.onContainerClick(e); //Otherwise default to click
    };
    container.onmousemove = container.ontouchstart = container.ontouchmove = mouseMove; //Set all the event to the previous defined mouseMove
    container.onclick = click; //Override click event as well
});