# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('banish', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='whitelist',
            options={'verbose_name': 'Rule', 'verbose_name_plural': 'While list'},
        ),
        migrations.AlterModelTable(
            name='banishment',
            table=None,
        ),
    ]
