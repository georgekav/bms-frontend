# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Banishment',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('ban_reason', models.CharField(help_text=b'Reason for the ban?', max_length=255)),
                ('type', models.CharField(default=0, help_text=b'Type of ban to store', max_length=20, choices=[(b'ip-address', b'IP Address'), (b'user-agent', b'User Agent')])),
                ('content', models.CharField(help_text=b'The rule subject, based on the ban type goes here', max_length=255)),
            ],
            options={
                'db_table': 'banishments',
                'verbose_name': 'Banishments',
                'verbose_name_plural': 'Banishments',
                'permissions': (('can_ban_user', 'Can Ban User'),),
            },
        ),
        migrations.CreateModel(
            name='WhiteList',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('rule_name', models.CharField(help_text=b'Name of rule', max_length=255)),
                ('type', models.CharField(default=0, help_text=b'Type of rule', max_length=20, choices=[(b'ip-address', b'IP Address'), (b'user-agent', b'User Agent')])),
                ('content', models.CharField(help_text=b'The rule subject, based on the rule type goes here', max_length=255)),
            ],
        ),
    ]
