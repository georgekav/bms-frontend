# Copyright 2008-2010 Yousef Ourabi

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from django.db import models
from django.db.models.signals import pre_save, pre_delete
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist

BANISH_PREFIX = 'DJANGO_BANISH:'
PROTECTION_PREFIX = 'DJANGO_WHITELIST'

class Banishment(models.Model):
    id = models.AutoField(primary_key=True)



    BAN_TYPES = (
        ('ip-address', 'IP Address'),
        ('user-agent', 'User Agent'),
    )
    ban_reason = models.CharField(max_length=255, help_text="Reason for the ban?")
    type = models.CharField(max_length=20, choices=BAN_TYPES, default=0, help_text="Type of ban to store")
    content = models.CharField(max_length=255, help_text='The rule subject, based on the ban type goes here')

    def __unicode__(self):
        return "Banished, %s: %s " % (self.type, self.content)

    def __str__(self):
        return self.__unicode__()


    class Meta:
        permissions = (("can_ban_user", "Can Ban User"),)
        verbose_name = "Banishments"
        verbose_name_plural = "Banishments"

class WhiteList(models.Model):
    id = models.AutoField(primary_key=True)

    PROTECTION_TYPES = (
        ('ip-address', 'IP Address'),
        ('user-agent', 'User Agent'),
    )
    rule_name = models.CharField(max_length=255, help_text="Name of rule")
    type = models.CharField(max_length=20, choices=PROTECTION_TYPES, default=0, help_text="Type of rule")
    content = models.CharField(max_length=255, help_text='The rule subject, based on the rule type goes here')

    def __unicode__(self):
        return "Whitelisted, %s: %s " % (self.type, self.content)

    def __str__(self):
        return self.__unicode__()

    class Meta:
        verbose_name = "Rule"
        verbose_name_plural = "While list"



def _update_cache_banish(sender, instance, **kwargs):
    try: #if therewas before an entry remove it from cache
        cache_key_old = BANISH_PREFIX + Banishment.objects.get(pk=instance.pk).content
        cache.delete(cache_key_old)
    except ObjectDoesNotExist:
        pass
    cache_key_new = BANISH_PREFIX + instance.content
    cache.set(cache_key_new, "1")

def _delete_cache_banish(sender, instance, **kwargs):
    cache_key_old = BANISH_PREFIX + Banishment.objects.get(pk=instance.pk).content
    cache.delete(cache_key_old)


def _update_cache_whitelist(sender, instance, **kwargs):
    try: #if therewas before an entry remove it from cache so that we update the key
        cache_key_old = PROTECTION_PREFIX + WhiteList.objects.get(pk=instance.pk).content
        cache.delete(cache_key_old)
    except ObjectDoesNotExist:
        pass
    cache_key_new = PROTECTION_PREFIX + instance.content
    cache.set(cache_key_new, "1")

def _delete_cache_whitelist(sender, instance, **kwargs):
    cache_key_old = PROTECTION_PREFIX + WhiteList.objects.get(pk=instance.pk).content
    cache.delete(cache_key_old)


#For live updates of cache without server restart
#pre_save to retrieve old entry to delete it
pre_save.connect(_update_cache_banish, sender=Banishment)
pre_delete.connect(_delete_cache_banish, sender=Banishment)

pre_save.connect(_update_cache_whitelist, sender=WhiteList)
pre_delete.connect(_delete_cache_whitelist, sender=WhiteList)