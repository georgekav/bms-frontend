# Copyright 2008-2013 Yousef Ourabi

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import django
from django.conf import settings
from django.http import HttpResponseForbidden
from django.core.exceptions import MiddlewareNotUsed
from django.core.cache import cache

from banish.models import Banishment, WhiteList, BANISH_PREFIX, PROTECTION_PREFIX
from BmsAPI.utilities import logger
from time import time

class BanishMiddleware(object):
    def __init__(self):
        """
        Middleware init is called once per server on startup - do the heavy
        lifting here.
        """

        # If disabled or not enabled raise MiddleWareNotUsed so django
        # processes next middleware.
        self.ENABLED = getattr(settings, 'BANISH_ENABLED', False)
        self.ABUSE_THRESHOLD = getattr(settings, 'BANISH_ABUSE_THRESHOLD', 1000) #In settings.py is the conf value
        self.ABUSE_TIME_THRESHOLD = getattr(settings, 'BANISH_ABUSE_TIME_THRESHOLD', 30) #In settings.py is the conf value
        self.PRELOAD_BANS = getattr(settings, 'PRELOAD_BANS', False)
        self.BAN_RESET_PERIOD = getattr(settings, 'BAN_RESET_PERIOD', 10)
        self.BANISH_MESSAGE = getattr(settings, 'BANISH_MESSAGE', 'You are banned. Reduce the number '
                                                                  'of API commands bellow {0} per {1} seconds. Ban resets every {2} seconds'
                                                                .format(self.ABUSE_THRESHOLD, self.ABUSE_TIME_THRESHOLD, self.BAN_RESET_PERIOD))

        if not self.ENABLED:
            raise MiddlewareNotUsed("django-banish is not enabled via settings.py")

        logger.debug("[django-banish] status = enabled")

        # Prefix All keys in cache to avoid key collisions
        self.BANISH_PREFIX = BANISH_PREFIX
        self.ABUSE_PREFIX = 'DJANGO_BANISH_ABUSE:'
        self.BANISH_TIME_PREFIX = 'DJANGO_BANISH_TIME:'
        self.PROTECTION_PREFIX = PROTECTION_PREFIX


        if self.PRELOAD_BANS: #If false dont fetch the DB at the restart for old bans
            # Populate various 'banish' buckets
            for ban in Banishment.objects.all():
                logger.warning("BANISHMENT: {0}".format(ban.content))
                cache_key = self.BANISH_PREFIX + ban.content
                cache.set(cache_key, "1", self.BAN_RESET_PERIOD)

        #this will avoid being banish no matter what
        for rule in WhiteList.objects.all():
            logger.warning("WHITELIST: {0}".format(rule.content))
            cache_key = self.PROTECTION_PREFIX + rule.content
            cache.set(cache_key, "1", None) #do not expire the whitelist


    # To Handle X_FORWARDED_FOR, use SetRemoteAddrFromForwardedFor MiddleWare
    def process_request(self, request):
        ip = request.META['REMOTE_ADDR']
        if (not ip or ip == '127.0.0.1') and 'HTTP_X_FORWARDED_FOR' in request.META:
            ip = request.META['HTTP_X_FORWARDED_FOR']

        user_agent = request.META.get('HTTP_USER_AGENT', 'API_AGENT')
        logger.debug("Request from IP: {0}".format(ip))

        # Check ban conditions
        #whitelist has priority over bans
        if not cache.get(self.PROTECTION_PREFIX + user_agent) and not cache.get(self.PROTECTION_PREFIX + ip):
            #It is a controlled user_agent and IP here
            if self.is_banned(ip) or self.monitor_abuse(ip) or cache.get(self.BANISH_PREFIX + user_agent):
                return self.http_response_forbidden(self.BANISH_MESSAGE, content_type="text/html")

    def http_response_forbidden(self, message, content_type):
        if django.VERSION[:2] > (1,3):
            kwargs = {'content_type': content_type}
        else:
            kwargs = {'mimetype': content_type}
        return HttpResponseForbidden(message, **kwargs)

    def is_banned(self, ip):
        # If a key BANISH MC key exists we know the user is banned.
        is_banned = cache.get(self.BANISH_PREFIX + ip)
        timeofban = cache.get(self.BANISH_TIME_PREFIX+ip)
        if is_banned and timeofban and timeofban + self.BAN_RESET_PERIOD < time(): #Auto reset the bans and remove from DB
            logger.info("Removing Ban from IP: {0}".format(ip))
            cache.delete(self.BANISH_PREFIX + ip)
            cache.delete(self.ABUSE_PREFIX + ip)
            Banishment.objects.filter(content=ip).delete()
            is_banned = None
        return is_banned

    def monitor_abuse(self, ip):
        """
        Track the number of hits per second for a given IP.
        If the count is over ABUSE_THRESHOLD banish user
        """
        cache_key = self.ABUSE_PREFIX + ip
        abuse_count = cache.get(cache_key)
        logger.debug("ABUSE COUNT: {0}".format(abuse_count))

        over_abuse_limit = False

        if not abuse_count:
            cache.set(cache_key, 1, self.ABUSE_TIME_THRESHOLD)
        else:
            if abuse_count >= self.ABUSE_THRESHOLD:
                over_abuse_limit = True
                # Store IP Abuse in memcache and database
                ban = Banishment(
                    ban_reason="IP Abuse limit exceeded",
                    type="ip-address",
                    content=ip,
                )
                ban.save()
                cache.set(self.BANISH_PREFIX + ip, "1", self.BAN_RESET_PERIOD) #automatic expires
                cache.set(self.BANISH_TIME_PREFIX + ip, time())
                logger.warning("Banned the IP: {0}".format(ip))

            cache.incr(cache_key)
        return over_abuse_limit
