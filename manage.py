#!/usr/bin/env python
import os
import sys

PROJECT_DIR = os.path.abspath(__file__ + "/../../")
sys.path.append(os.path.join(PROJECT_DIR, 'config'))
sys.path.append(os.path.join(PROJECT_DIR, 'frontend/SmartBuilding')) #Necessary for the settings
from conf_openbms import DJANGO_DEPENDENCIES

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "SmartBuilding.settings")
    #Check if we have the dependancies
    import imp
    try:
        for dep in DJANGO_DEPENDENCIES:
            imp.find_module(dep)

    except ImportError as e:
        exit(e)

    from django.core.management import execute_from_command_line
    execute_from_command_line(sys.argv)
